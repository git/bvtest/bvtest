#!/bin/bash
#
# lrun.sh
# Bltsville sanity tests: Linux main script
#
# Joins all the other support scripts together, to run the Linux sanity tests.
#

clean=0
# Create this on the /sdcard partition, as presumably filling that up won't
# crash the device.
targetdir=/sdcard/TI
lib=
get_lib=0
spreadsheet_output=0
sanity_flags=""
##############################################
# Parse the command line
for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    lib=$arg
    get_lib=0
  fi

  if [ "$arg" = "-clean" ]
  then
    clean=1
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-csv" ]
  then
    spreadsheet_output=1
  fi
done

# Clean up old tests?
if [ "$clean" = "1" ]
then
    ./asetup.sh -clean
    exit
fi

if [ "$lib" = "" ]
then
  echo Please Enter the target library to be tested - cpu/proxy/hw2d etc
  read OP
  lib=$OP
  if [ "$lib" = "" ]
  then
  echo Please specify target library to be tested.
      exit
  fi
fi

if [ "$spreadsheet_output" = "1" ]
then
  sanity_flags="-csv"
fi

# create the test scripts
echo
echo
echo === Preparing tests =======================================================
./asetup.sh $lib $sanity_flags

# execute the sanity test scripts
echo
echo
echo === Executing tests =======================================================
adb shell "cd ${targetdir};/system/bin/sanity.sh"

# run the performance tests
echo
echo
echo === Measuring Performance =================================================
adb shell "cd ${targetdir};/system/bin/perf.sh"
adb pull ${targetdir}/perfresults.csv .

# verify the results
echo
echo
echo === Retrieving  Results======================================================
adb pull ${targetdir}
echo
echo === Verifying Results======================================================
./verify-sanity.sh

echo Performance results are in 'perfresults.csv'.
rm *.raw
rm *.jpg

# If there are any reference files left, then there was a failure
ls *ref.tga > /dev/null 2> /dev/null
if [ "$?" -eq 0 ]
then
    #There was a failure
    exit 1
fi
exit 0
