#!/bin/bash

# Reference file and dimensions
src1file=./Fruit-2048x1536.jpg
w=2048
h=1536

# Intermediate file containing unit test calls
simfile=sim.sh

# Command line settings
inp=0
sim=0
bmp=1
libflagarg=

###

dim=${w}x${h}
rc=0,0-${dim}
rdim=${h}x${w}
rrc=0,0-${rdim}

#
# Parse command line arguments
#
# makeinputs - Make the raw input files used for the unit tests
# simulate - Create the intermediate script, but don't run it
# nobmp - Do not generate output bitmaps
# ti - Test only the TI version of the library
# bx - Test only the drawElements (Blitrix) version of the library
#

for arg in "$@"
do
  if [ "$arg" = "makeinputs" ]
  then
    inp=1
  fi
  if [ "$arg" = "simulate" ]
  then
    sim=1
  fi
  if [ "$arg" = "nobmp" ]
  then
    bmp=0
  fi
  if [ "$arg" = "ti" ]
  then
    libflagarg=80000000
  fi
  if [ $"arg" = "bx" ]
  then
    libflagarg=40000000
  fi
done

# Clear temporary/simulation file
echo -n >$simfile ''

if [ "$inp" = "1" ]
then

##############################################
# Build the raw input files from the src1file
#

# Source and destination color formats being created (followed by pixel size info)
  srcfmts=(  LUT8 RGB16 BGR24 BGR132 BGRA32 nBGRA32 UYVY IYUV )
  srcfmtsize=( 1    2     3     4      4      4      2    1   )
  fauxfmts=( LUT8 RGB16 BGR24 BGR132 BGR132 BGR132  UYVY IYUV )

  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcsize=${srcfmtsize[srcidx]}
    srcstride=$(( $w * $srcsize ))
    fauxfmt=${fauxfmts[srcidx]}
    
    echo >>$simfile "echo Creating ${srcfmt} input file."

    if [ "$srcfmt" != "$fauxfmt" ]
    then
      echo >>$simfile "cp input-${fauxfmt}-${dim}\(${srcstride}\).raw input-${srcfmt}-${dim}\(${srcstride}\).raw"
    else
      echo >>$simfile "./bvtest -src1file ${src1file} -src1 RGB24 ${dim} -src1rect ${rc} -dst ${srcfmt} ${dim} -dstrect ${rc} -dstfile input.raw -rop CCCC"
      echo >>$simfile 'if [ "$?" -ne "0" ]'
      echo >>$simfile "then"
        echo >>$simfile "echo Cannot create ${srcfmt} input file.  Please create manually \(may be included with distribution\)."
      echo >>$simfile "fi"
     fi
    srcidx=$(( $srcidx + 1 ))
  done

#
#
#############################################

else

#############################################
# Run the unit tests
#

resfile=unittest_results.csv

# Universal unit test argments
args="-preseed -pretouch 4096 -profile -profout $resfile"

#----------------------------------------------------------------------
# Fill
#
  dstfmts=( RGB16 BGR24 BGR132 BGRA32 nBGRA32 )
  dstfmtsize=( 2     3     4      4      4    )

  echo >>$simfile "echo >$resfile ",Fill""

  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

    echo >>$simfile "echo -n >>$resfile $dstfmt,"
    echo >>$simfile "echo -n "Fill ${dstfmt}: ""
    echo -n >>$simfile "./bvtest -src2rect 0,0-1x1 -dst ${dstfmt} ${dim} -dstrect ${rc} -rop F0F0 $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile " -src2file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Fill.tga"
    else
      echo >>$simfile " -brush ${dstfmt} 1x1"
    fi
    echo >>$simfile "echo >>$resfile ''"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#-----------------------------------------------------------------------

echo >>$simfile "echo >>$resfile ''"

#-----------------------------------------------------------------------
# Rotation
#

  dstfmts=( LUT8 RGB16 UYVY IYUV )
  dstfmtsize=( 1   2    2    1  )

# CSV column headings
  echo >>$simfile "echo >>$resfile ,90,180,270"

  rop=CCCC
  
  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

    echo >>$simfile "echo -n >>$resfile $dstfmt,"
    echo >>$simfile "echo -n "${dstfmt} 90: ""
    echo -n >>$simfile "./bvtest -src1rect ${rc} -src1angle 0 -dst ${dstfmt} ${rdim} -dstrect ${rrc} -dstangle 90 -rop CCCC $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile " -src1file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Rot90.tga"
    else
      echo >>$simfile " -src1 ${dstfmt} ${dim}"
    fi
    echo >>$simfile "echo -n >>$resfile ,"

    echo >>$simfile "echo -n "${dstfmt} 180: ""
    echo -n >>$simfile "./bvtest -src1rect ${rc} -src1angle 0 -dst ${dstfmt} ${dim} -dstrect ${rc} -dstangle 180 -rop ${rop} $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile  " -src1file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Rot180.tga"
    else
      echo >>$simfile " -src1 ${dstfmt} ${dim}"
    fi
    echo >>$simfile "echo -n >>$resfile ,"

    echo >>$simfile "echo -n "${dstfmt} 270: ""
    echo -n >>$simfile "./bvtest -src1rect ${rc} -src1angle 0 -dst ${dstfmt} ${rdim} -dstrect ${rrc} -dstangle 270 -rop ${rop} $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile " -src1file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Rot270.tga"
    else
      echo >>$simfile " -src1 ${dstfmt} ${dim}"
    fi
    echo >>$simfile "echo >>$resfile ''"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#------------------------------------------------------------------------

echo >>$simfile "echo >>$resfile ''"

#------------------------------------------------------------------------
# Copy and color space conversion
#

  srcfmts=( LUT8 RGB16 BGR24 BGR132 BGRA32 nBGRA32 UYVY IYUV )
  srcfmtsize=( 1    2     3     4      4      4      2    1  )
  dstfmts=( LUT8 RGB16 BGR24 BGR132 BGRA32 nBGRA32 UYVY IYUV )
  dstfmtsize=( 1    2     3     4      4      4      2    1  )

  rop=CCCC

# CSV column headings
  echo >>$simfile "echo -n >>$resfile SRC\\DST,"
  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    echo >>$simfile "echo -n >>$resfile ${dstfmt},"
    dstidx=$(( $dstidx + 1 ))
  done
  echo >>$simfile "echo >>$resfile " ""

  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcsize=${srcfmtsize[srcidx]}
    srcstride=$(( $w * $srcsize ))

    echo >>$simfile "echo -n >>$resfile $srcfmt,"
    dstidx=0
    while [ "x${dstfmts[dstidx]}" != "x" ]
    do
      dstfmt=${dstfmts[dstidx]}
      dstsize=${dstfmtsize[dstidx]}
      dststride=$(( $w * $dstsize ))

      echo >>$simfile "echo -n "${srcfmt} to ${dstfmt}: ""
      echo -n >>$simfile "./bvtest -src1rect ${rc} -dst ${dstfmt} ${dim} -dstrect ${rc} -rop ${rop} $args"
      if [ "$bmp" = "1" ]
      then
        echo >>$simfile " -src1file input-${srcfmt}-${dim}\(${srcstride}\).raw -dstfile ${srcfmt}to${dstfmt}.tga"
      else
        echo >>$simfile " -src1 ${srcfmt} ${dim}"
      fi
      echo >>$simfile "echo -n >>$resfile ,"
      dstidx=$(( $dstidx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"
    srcidx=$(( $srcidx + 1 ))
  done

#
#
#-----------------------------------------------------------------------

echo >>$simfile "echo >>$resfile ''"

#-----------------------------------------------------------------------
# Global alpha blend source and source 2 into destination
#

  hexalfa=40
  eqn=srcover

  dstfmts=( RGB16 BGR132 )
  dstfmtsize=( 2     4   )
  srcfmts=( RGB16 BGR24 BGR132  UYVY )
  srcfmtsize=( 2     3     4      2  )
  src2fmts=( RGB16 BGR24 BGR132 UYVY )
  src2fmtsize=( 2     3     4     2  )

  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

# CSV column headings
    echo >>$simfile "echo -n >>$resfile Global to $dstfmt,"
    src2idx=0
    while [ "x${src2fmts[src2idx]}" != "x" ]
    do
      src2fmt=${src2fmts[src2idx]}
      echo >>$simfile "echo -n >>$resfile ${src2fmt},"
      src2idx=$(( $src2idx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"

    srcidx=0
    while [ "x${srcfmts[srcidx]}" != "x" ]
    do
      srcfmt=${srcfmts[srcidx]}
      srcsize=${srcfmtsize[srcidx]}
      srcstride=$(( $w * $srcsize ))
      
      echo >>$simfile "echo -n >>$resfile $srcfmt,"
      src2idx=0
      while [ "x${src2fmts[src2idx]}" != "x" ]
      do
        src2fmt=${src2fmts[src2idx]}
        src2size=${src2fmtsize[src2idx]}
        src2stride=$(( $w * $src2size ))

        echo >>$simfile "echo -n "${srcfmt} ${eqn} ${src2fmt} to ${dstfmt} global 25%: ""
        echo -n >>$simfile "./bvtest -dst ${dstfmt} "
        if [ "$src2fmt" != "$dstfmt" ]
        then
          echo -n >>$simfile "${dim}"
        else
          echo -n >>$simfile "src2"
        fi
          echo -n >>$simfile " -src1rect ${rc} -src2rect ${rc} -dstrect ${rc} -blend ${eqn} -alpha ${hexalfa} $args"
        if [ "$1" != "nobmp" ]
        then
          echo >>$simfile " -src1file input-${srcfmt}-${dim}\(${srcstride}\).raw -src2file input-${src2fmt}-${dim}\(${src2stride}\).raw -dstfile blend-${srcfmt}_${eqn}_${src2fmt}_to_${dstfmt}_0x${hexalfa}.tga"
        else
          echo >>$simfile " -src1 ${srcfmt} ${dim} -src2 ${src2fmt} ${dim}"
        fi
        echo >>$simfile "echo -n >>$resfile ,"
        src2idx=$(( $src2idx + 1 ))
      done
      echo >>$simfile "echo >>$resfile ' '"
      srcidx=$(( $srcidx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#-------------------------------------------------------------------------

# echo >>$simfile "echo >>$resfile ''"

#-----------------------------------------------------------------------
# Local alpha blend source and source 2 into destination
#

  eqn=srcover

  dstfmts=( RGB16 BGR132 BGRA32 )
  dstfmtsize=( 2     4      4   )

  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

    if [ "$dstfmt" != "BGRA32" ]
    then
      srcfmts=( nBGRA32 BGRA32 )
      srcfmtsize=( 4      4  )
      src2fmts=( RGB16 BGR24 BGR132 UYVY )
      src2fmtsize=( 2     3     4     2  )
    else
      srcfmts=( RGB16 BGR24 BGR132 nBGRA32 BGRA32 UYVY )
      srcfmtsize=( 2     3     4      4       4     2  )
      src2fmts=( nBGRA32 BGRA32 )
      src2fmtsize=( 4      4  )
    fi

# CSV column headings
    echo >>$simfile "echo -n >>$resfile Local to $dstfmt,"
    src2idx=0
    while [ "x${src2fmts[src2idx]}" != "x" ]
    do
      src2fmt=${src2fmts[src2idx]}
      echo >>$simfile "echo -n >>$resfile ${src2fmt},"
      src2idx=$(( $src2idx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"

    srcidx=0
    while [ "x${srcfmts[srcidx]}" != "x" ]
    do
      srcfmt=${srcfmts[srcidx]}
      srcsize=${srcfmtsize[srcidx]}
      srcstride=$(( $w * $srcsize ))

      echo >>$simfile "echo -n >>$resfile $srcfmt,"
      src2idx=0
      while [ "x${src2fmts[src2idx]}" != "x" ]
      do
        src2fmt=${src2fmts[src2idx]}
        src2size=${src2fmtsize[src2idx]}
        src2stride=$(( $w * $src2size ))

        echo >>$simfile "echo -n "${srcfmt} ${eqn} ${src2fmt} to ${dstfmt} local only: ""
        echo -n >>$simfile "./bvtest -dst ${dstfmt} "
        if [ "$src2fmt" != "$dstfmt" ]
        then
          echo -n >>$simfile "${dim}"
        else
          echo -n >>$simfile "src2"
        fi
          echo -n >>$simfile " -src1rect ${rc} -src2rect ${rc} -dstrect ${rc} -blend ${eqn} $args"
        if [ "$1" != "nobmp" ]
        then
          echo >>$simfile " -src1file input-${srcfmt}-${dim}\(${srcstride}\).raw -src2file input-${src2fmt}-${dim}\(${src2stride}\).raw -dstfile blend-${srcfmt}_${eqn}_${src2fmt}_to_${dstfmt}.tga"
        else
          echo >>$simfile " -src1 ${srcfmt} ${dim} -src2 ${src2fmt} ${dim}"
        fi
        echo >>$simfile "echo -n >>$resfile ,"
        src2idx=$(( $src2idx + 1 ))
      done
      echo >>$simfile "echo >>$resfile ' '"
      srcidx=$(( $srcidx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#-------------------------------------------------------------------------

fi

chmod 777 $simfile

if [ "$sim" = "0" ]
then
  ./$simfile
  rm $simfile
fi

