vb='-verbose'
lf='-libflag 80000000'
rc='-src1rect 0,0-96x84 -src2rect 0,0-96x84 -dstrect 0,0-96x84 -maskrect 0,0-96x84'
sz='96x84\(384\)'
s1f='-src1file src-trans'
s2f='-src2file src2-trans'
df='-dstfile blend'
dstfmt='-dst BGRA24 96x84'

# Test CLEAR
bd='clear'
bt='-blendtype clear'
./bvtest ${vb} ${dstfmt} ${rc} ${bt} -dstfile blend-${bd}.raw ${lf}

# Test SRC
bd='src'
bt='-blendtype src'
./bvtest ${vb} ${dstfmt} ${s1f}-RGB16-${sz}.raw ${rc} ${bt} ${df}-${bd}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-BGR24-${sz}.raw ${rc} ${bt} ${df}-${bd}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-BGR024-${sz}.raw ${rc} ${bt} ${df}-${bd}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-nBGRA24-${sz}.raw ${rc} ${bt} ${df}-${bd}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-BGRA24-${sz}.raw ${rc} ${bt} ${df}-${bd}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-UYVY-${sz}.raw ${rc} ${bt} ${df}-${bd}-UYVY.raw ${lf}

# Test SRC2
bd='src2'
bt='-blendtype src2'
./bvtest ${vb} ${dstfmt} ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${df}-${bd}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${df}-${bd}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${df}-${bd}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${df}-${bd}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${df}-${bd}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${df}-${bd}-UYVY.raw ${lf}

# Test SRCOVER
bd='local-srcover'
bt='-blendtype srcover'
## local only
ad=''
srcfmt='RGB16'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR024'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='nBGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='UYVY'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
## global (+ local)
bd='global-srcover'
ad='-alpha 80'
srcfmt='RGB16'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR024'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='nBGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='UYVY'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
## remote (+ local)
ad=''
maskfmt='ALPHA8'
bd='remote-ALPHA8-srcover'
mf='-maskfile mask-ALPHA8-96x84\(96\).raw'
srcfmt='RGB16'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR024'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='nBGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='UYVY'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
## global + remote (+ local)
ad='-alpha 80'
maskfmt='ALPHA8'
bd='global-remote-ALPHA8-srcover'
mf='-maskfile mask-ALPHA8-96x84\(96\).raw'
srcfmt='RGB16'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGR024'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='nBGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='BGRA24'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}
srcfmt='UYVY'
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-RGB16-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-RGB16.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGR024-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGR024.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-nBGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-nBGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-BGRA24-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-BGRA24.raw ${lf}
./bvtest ${vb} ${dstfmt} ${s1f}-${srcfmt}-${sz}.raw ${s2f}-UYVY-${sz}.raw ${mf} ${rc} ${bt} ${ad} ${df}-${bd}-${srcfmt}-UYVY.raw ${lf}

