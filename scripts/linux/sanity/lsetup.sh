#!/bin/bash
#
# lsetup.sh
# Bltsville sanity tests: Linux setup
#
# Prepares the current directory to run the sanity tests
#   Copy in bvtest binary
#   copy in source images.
#   Copy in reference images
# Also, clean these up if requested
#

# Paths to things we need
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
bvt=../../../src/bvtest/bvtest

vb=0
verbose=
clean=0
lib=$1

##############################################
# Parse the command line
for arg in "$@"
do
  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi
  if [ "$arg" = "-clean" ]
  then
    clean=1
  fi

done


##############################################
# Clean up old tests?
if [ "$clean" = "1" ]
then
    rm *.raw
    rm *.tga
    rm *.jpg
    rm *.png
    rm *.csv
    rm sanity.sh
    rm verify-sanity.sh
    rm perf.sh
    rm bvtest
    rm junit.xml
    exit
fi


# Copy bvtest executable and source image.
echo Locating source images.
cp ${bvt} .
cp ../../../images/Fruit-2048x1536.jpg .
cp ../../../images/kings_1_bg_051802.jpg .

# copy reference images
echo Locating reference images.
cp ../../common/sanity/golden-${lib}/*.tga .
echo Locating global-alpha reference images.
cp ../../common/sanity/golden-${lib}/global-alpha/*.tga .

echo Generating Sanity Tests
# generate the sanity test script
../../common/sanity/gen-test-script.sh $lib sanity.sh ./

echo
echo Generating Performance Tests
# generate the performance test script
../../common/sanity/gen-perf-script.sh $lib perf.sh ./

