#!/bin/bash
#
# lrun.sh
# Bltsville sanity tests: Linux main script
#
# Joins all the other support scripts together, to run the Linux sanity tests.
#

clean=0

lib=
get_lib=0
##############################################
# Parse the command line
for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    lib=$arg
    get_lib=0
  fi

  if [ "$arg" = "-clean" ]
  then
    clean=1
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi
done

# Clean up old tests?
if [ "$clean" = "1" ]
then
    ./lsetup.sh -clean
    exit
fi

if [ "$lib" = "" ]
then
echo Please Enter the target library to be tested - cpu/proxy/hw2d etc
read OP
lib=$OP
if [ "$lib" = "" ]
then
echo Please specify target library to be tested.
    exit
fi
fi

# create the test scripts
echo
echo
echo === Preparing tests =======================================================
./lsetup.sh $lib

# execute the sanity test scripts
echo
echo
echo === Executing tests =======================================================
./sanity.sh

# run the performance tests
echo
echo
echo === Measuring Performance =================================================
./perf.sh

# verify the results
echo
echo
echo === Verifying Results======================================================
./verify-sanity.sh
echo Performance results are in 'perfresults.csv'.
rm *.raw
rm *.jpg

# If there are any reference files left, then there was a failure
ls *ref.tga > /dev/null 2> /dev/null
if [ "$?" -eq 0 ]
then
    # There was a failure
    exit 1
fi
exit 0




