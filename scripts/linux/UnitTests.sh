#!/bin/bash
#
# To run this script, you will need to make sure that bltsville library is
# in your libary search path
# You will also need to created a file link from ./bvtest to your bvtest
# executable.
#
# For benchmarking, use the nobmp option.  Loading and saving image files adds
# considerbly to the time needed to run. While this time isn't counted for
# benchmarking purposes, generating the images increases the run time by over
# an hour, and requires several gigabytes of disk space.
# run "UnitTests.sh nobmp"
# The benchmark results will appear in "unittest_results.csv"
#
# If you are testing functionality, you will need the source images.
# This script uses images from the current directory, so you need to copy this
# script, and the files from the images directory to a working directory.
#
# Next run "UnitTests.sh makeinputs" to create the  input images.
# Then run "UnitTests.sh" without parameters to run the full set of tests
# This will take over an hour to run and generate several gigabytes of
# results images.  There is no way to automatically determine the correctness
# of the results, you need to look at the images yourself.
#
# Other useful parameters:
#    2d  = use the 2D library for drawing
#    cpu  = use the cpu library for drawing
#    simulate = Create a "sim.sh" file filled with all the commands needed
#               to execute the tests, but don't execute them.
#

# Reference file and dimensions
src1file=./Fruit-2048x1536.jpg
w=2048
h=1536

# Intermediate file containing unit test calls
simfile=sim.sh

# Command line settings
inp=0
sim=0
bmp=1
libflagarg=

###

dim=${w}x${h}
rc=0,0-${dim}
rdim=${w}x${h}
rrc=0,0-${rdim}

#
# Parse command line arguments
#
# makeinputs - Make the raw input files used for the unit tests
# simulate - Create the intermediate script, but don't run it
# nobmp - Do not generate output bitmaps
# cpu - Choose cpu version of bltsville
# 2d - Choose 2d version of bltsville

for arg in "$@"
do
  if [ "$arg" = "makeinputs" ]
  then
    inp=1
  fi
  if [ "$arg" = "simulate" ]
  then
    sim=1
  fi
  if [ "$arg" = "nobmp" ]
  then
    bmp=0
  fi
  if [ "$arg" = "2d" ]
  then
    libflagarg=-2d
  fi
  if [ "$arg" = "cpu" ]
  then
    libflagarg=-cpu
  fi
done

bvt="./bvtest ${libflagarg}"

# Clear temporary/simulation file
echo -n >$simfile ''

if [ "$inp" = "1" ]
then

##############################################
# Build the raw input files from the src1file
#

# Source and destination color formats being created (followed by pixel size info)
  srcfmts=(  LUT8 RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24    0RGB24 0BGR24 xRGB24 xBGR24 ARGB24 ABGR24 nARGB24 nABGR24       UYVY IYUV NV12)
  srcfmtsize=( 1    2   3     3         4      4      4      4      4       4     4       4          4      4      4      4      4       4     4       4             2    1    1   )
  fauxfmts=( LUT8 RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBx24 BGRx24 RGBx24  BGRx24     0RGB24 0BGR24 xRGB24 xBGR24 xRGB24 xBGR24 xRGB24  xBGR24        UYVY IYUV NV12)

  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcsize=${srcfmtsize[srcidx]}
    srcstride=$(( $w * $srcsize ))
    fauxfmt=${fauxfmts[srcidx]}
    
    echo >>$simfile "echo Creating ${srcfmt} input file."

    if [ "$srcfmt" != "$fauxfmt" ]
    then
      echo >>$simfile "cp input-${fauxfmt}-${dim}\(${srcstride}\).raw input-${srcfmt}-${dim}\(${srcstride}\).raw"
    else
      echo >>$simfile "${bvt} -src1file ${src1file} -src1 RGB24 ${dim} -src1rect ${rc} -dst ${srcfmt} ${dim} -dstrect ${rc} -dstfile input.raw -rop CCCC"
      echo >>$simfile 'if [ "$?" -ne "0" ]'
      echo >>$simfile "then"
        echo >>$simfile "echo Cannot create ${srcfmt} input file.  Please create manually \(may be included with distribution\)."
      echo >>$simfile "fi"
     fi
    srcidx=$(( $srcidx + 1 ))
  done

#
#
#############################################

else

#############################################
# Run the unit tests
#

resfile=unittest_results.csv

# Universal unit test argments
args="-preseed -pretouch 4096 -profile -profout $resfile"

#----------------------------------------------------------------------
# Fill
#
  dstfmts=(  LUT8 RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24    0RGB24 0BGR24 xRGB24 xBGR24 ARGB24 ABGR24 nARGB24 nABGR24 )
  dstfmtsize=( 1    2   3     3         4      4      4      4      4       4     4       4          4      4      4      4      4       4     4       4       )

  echo >>$simfile "echo >$resfile ",Fill""

  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

    echo >>$simfile "echo -n >>$resfile $dstfmt,"
    echo >>$simfile "echo -n "Fill ${dstfmt}: ""
    echo -n >>$simfile "${bvt} -src2rect 0,0-1x1 -dst ${dstfmt} ${dim} -dstrect ${rc} -rop F0F0 $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile " -src2file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Fill.tga"
    else
      echo >>$simfile " -src2 ${dstfmt} 1x1"
    fi
    echo >>$simfile "echo >>$resfile ''"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#-----------------------------------------------------------------------

echo >>$simfile "echo >>$resfile ''"

#-----------------------------------------------------------------------
# Rotation
#

  dstfmts=(  LUT8 RGB16  UYVY IYUV NV12)
  dstfmtsize=( 1    2    2    1    1   )

# CSV column headings
  echo >>$simfile "echo >>$resfile ,90,180,270"

  rop=CCCC
  
  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

    echo >>$simfile "echo -n >>$resfile $dstfmt,"
    echo >>$simfile "echo -n "${dstfmt} 90: ""
    echo -n >>$simfile "${bvt} -src1rect ${rc} -src1angle 0 -dst ${dstfmt} ${rdim} -dstrect ${rrc} -dstangle 90 -rop CCCC $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile " -src1file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Rot90.tga"
    else
      echo >>$simfile " -src1 ${dstfmt} ${dim}"
    fi
    echo >>$simfile "echo -n >>$resfile ,"

    echo >>$simfile "echo -n "${dstfmt} 180: ""
    echo -n >>$simfile "${bvt} -src1rect ${rc} -src1angle 0 -dst ${dstfmt} ${dim} -dstrect ${rc} -dstangle 180 -rop ${rop} $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile  " -src1file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Rot180.tga"
    else
      echo >>$simfile " -src1 ${dstfmt} ${dim}"
    fi
    echo >>$simfile "echo -n >>$resfile ,"

    echo >>$simfile "echo -n "${dstfmt} 270: ""
    echo -n >>$simfile "${bvt} -src1rect ${rc} -src1angle 0 -dst ${dstfmt} ${rdim} -dstrect ${rrc} -dstangle 270 -rop ${rop} $args"
    if [ "$bmp" = "1" ]
    then
      echo >>$simfile " -src1file input-${dstfmt}-${dim}\(${dststride}\).raw -dstfile ${dstfmt}-Rot270.tga"
    else
      echo >>$simfile " -src1 ${dstfmt} ${dim}"
    fi
    echo >>$simfile "echo >>$resfile ''"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#------------------------------------------------------------------------

echo >>$simfile "echo >>$resfile ''"

#------------------------------------------------------------------------
# Copy and color space conversion
#

  srcfmts=(  LUT8 RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24    0RGB24 0BGR24 xRGB24 xBGR24 ARGB24 ABGR24 nARGB24 nABGR24       UYVY IYUV NV12)
  srcfmtsize=( 1    2   3     3         4      4      4      4      4       4     4       4          4      4      4      4      4       4     4       4             2    1    1   )
  dstfmts=(  LUT8 RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24    0RGB24 0BGR24 xRGB24 xBGR24 ARGB24 ABGR24 nARGB24 nABGR24       UYVY IYUV NV12)
  dstfmtsize=( 1    2   3     3         4      4      4      4      4       4     4       4          4      4      4      4      4       4     4       4             2    1    1   )

  rop=CCCC

# CSV column headings
  echo >>$simfile "echo -n >>$resfile SRC\\DST,"
  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    echo >>$simfile "echo -n >>$resfile ${dstfmt},"
    dstidx=$(( $dstidx + 1 ))
  done
  echo >>$simfile "echo >>$resfile " ""

  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcsize=${srcfmtsize[srcidx]}
    srcstride=$(( $w * $srcsize ))

    echo >>$simfile "echo -n >>$resfile $srcfmt,"
    dstidx=0
    while [ "x${dstfmts[dstidx]}" != "x" ]
    do
      dstfmt=${dstfmts[dstidx]}
      dstsize=${dstfmtsize[dstidx]}
      dststride=$(( $w * $dstsize ))

      echo >>$simfile "echo -n "${srcfmt} to ${dstfmt}: ""
      echo -n >>$simfile "${bvt} -src1rect ${rc} -dst ${dstfmt} ${dim} -dstrect ${rc} -rop ${rop} $args"
      if [ "$bmp" = "1" ]
      then
        echo >>$simfile " -src1file input-${srcfmt}-${dim}\(${srcstride}\).raw -dstfile ${srcfmt}to${dstfmt}.tga"
      else
        echo >>$simfile " -src1 ${srcfmt} ${dim}"
      fi
      echo >>$simfile "echo -n >>$resfile ,"
      dstidx=$(( $dstidx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"
    srcidx=$(( $srcidx + 1 ))
  done

#
#
#-----------------------------------------------------------------------

echo >>$simfile "echo >>$resfile ''"

#-----------------------------------------------------------------------
# Global alpha blend source and source 2 into destination
#

  hexalfa=40
  eqn=src1over

  dstfmts=(    RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24   )
  dstfmtsize=( 2     3     3         4      4      4      4      4       4       )
  srcfmts=(    RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24     UYVY )
  srcfmtsize=( 2   3     3           4      4      4      4      4       4     4       4           2    )
  src2fmts=(    RGB16 RGB24 BGR24    RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24     UYVY )
  src2fmtsize=( 2   3     3          4      4      4      4      4       4     4       4           2    )

  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

# CSV column headings
    echo >>$simfile "echo -n >>$resfile Global to $dstfmt,"
    src2idx=0
    while [ "x${src2fmts[src2idx]}" != "x" ]
    do
      src2fmt=${src2fmts[src2idx]}
      echo >>$simfile "echo -n >>$resfile ${src2fmt},"
      src2idx=$(( $src2idx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"

    srcidx=0
    while [ "x${srcfmts[srcidx]}" != "x" ]
    do
      srcfmt=${srcfmts[srcidx]}
      srcsize=${srcfmtsize[srcidx]}
      srcstride=$(( $w * $srcsize ))
      
      echo >>$simfile "echo -n >>$resfile $srcfmt,"
      src2idx=0
      while [ "x${src2fmts[src2idx]}" != "x" ]
      do
        src2fmt=${src2fmts[src2idx]}
        src2size=${src2fmtsize[src2idx]}
        src2stride=$(( $w * $src2size ))

        echo >>$simfile "echo -n "${srcfmt} ${eqn} ${src2fmt} to ${dstfmt} global 25%: ""
        echo -n >>$simfile "${bvt} -dst ${dstfmt} "
        echo -n >>$simfile "${dim}"
        echo -n >>$simfile " -src1rect ${rc} -src2rect ${rc} -dstrect ${rc} -blend ${eqn} -alpha ${hexalfa} $args"
        if [ "$bmp" = "1" ]
        then
          echo >>$simfile " -src1file input-${srcfmt}-${dim}\(${srcstride}\).raw -src2file input-${src2fmt}-${dim}\(${src2stride}\).raw -dstfile blend-${srcfmt}_${eqn}_${src2fmt}_to_${dstfmt}_0x${hexalfa}.tga"
        else
          echo >>$simfile " -src1 ${srcfmt} ${dim} -src2 ${src2fmt} ${dim}"
        fi
        echo >>$simfile "echo -n >>$resfile ,"
        src2idx=$(( $src2idx + 1 ))
      done
      echo >>$simfile "echo >>$resfile ' '"
      srcidx=$(( $srcidx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#-------------------------------------------------------------------------

# echo >>$simfile "echo >>$resfile ''"

#-----------------------------------------------------------------------
# Local alpha blend source and source 2 into destination
#

  eqn=src1over

  dstfmts=(    RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24   )
  dstfmtsize=( 2     3     3         4      4      4      4      4       4       )
  srcfmts=(    RGB16 RGB24 BGR24     RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24     UYVY )
  srcfmtsize=( 2   3     3           4      4      4      4      4       4     4       4           2    )
  src2fmts=(    RGB16 RGB24 BGR24    RGB024 BGR024 RGBx24 BGRx24 RGBA24 BGRA24 nRGBA24 nBGRA24     UYVY )
  src2fmtsize=( 2   3     3          4      4      4      4      4       4     4       4           2    )

  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    dstfmt=${dstfmts[dstidx]}
    dstsize=${dstfmtsize[dstidx]}
    dststride=$(( $w * $dstsize ))

# CSV column headings
    echo >>$simfile "echo -n >>$resfile Local to $dstfmt,"
    src2idx=0
    while [ "x${src2fmts[src2idx]}" != "x" ]
    do
      src2fmt=${src2fmts[src2idx]}
      echo >>$simfile "echo -n >>$resfile ${src2fmt},"
      src2idx=$(( $src2idx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"

    srcidx=0
    while [ "x${srcfmts[srcidx]}" != "x" ]
    do
      srcfmt=${srcfmts[srcidx]}
      srcsize=${srcfmtsize[srcidx]}
      srcstride=$(( $w * $srcsize ))

      echo >>$simfile "echo -n >>$resfile $srcfmt,"
      src2idx=0
      while [ "x${src2fmts[src2idx]}" != "x" ]
      do
        src2fmt=${src2fmts[src2idx]}
        src2size=${src2fmtsize[src2idx]}
        src2stride=$(( $w * $src2size ))

        echo >>$simfile "echo -n "${srcfmt} ${eqn} ${src2fmt} to ${dstfmt} local only: ""
        echo -n >>$simfile "${bvt} -dst ${dstfmt} "
        echo -n >>$simfile "${dim}"
        echo -n >>$simfile " -src1rect ${rc} -src2rect ${rc} -dstrect ${rc} -blend ${eqn} $args"
        if [ "$bmp" = "1" ]
        then
          echo >>$simfile " -src1file input-${srcfmt}-${dim}\(${srcstride}\).raw -src2file input-${src2fmt}-${dim}\(${src2stride}\).raw -dstfile blend-${srcfmt}_${eqn}_${src2fmt}_to_${dstfmt}.tga"
        else
          echo >>$simfile " -src1 ${srcfmt} ${dim} -src2 ${src2fmt} ${dim}"
        fi
        echo >>$simfile "echo -n >>$resfile ,"
        src2idx=$(( $src2idx + 1 ))
      done
      echo >>$simfile "echo >>$resfile ' '"
      srcidx=$(( $srcidx + 1 ))
    done
    echo >>$simfile "echo >>$resfile ' '"
    dstidx=$(( $dstidx + 1 ))
  done

#
#
#-------------------------------------------------------------------------

fi

chmod 777 $simfile

if [ "$sim" = "0" ]
then
  echo Executing simfile...
  ./$simfile
  rm $simfile
fi

