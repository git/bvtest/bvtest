#!/bin/bash

pushd ../../common/conform
./conform.sh -linux $1 $2 $3 $4 $5 $6 $7 $8 $9
popd

if [ -f "../../common/conform/junit.xml" ]
then
    cp ../../common/conform/junit.xml .
else
    rm junit.xml
fi


