Bltsville conformance tests.

These tests are designed to verify proper operation of your
Bltisville implementation.  

The full suite of tests can be run automatically by executing "conform.sh".

Suites can be run individually as well, however you will need to run the
"Source Image Creation" suite first to generate the needed source images for
the rest of the tests.

The conformance tests are very comprehensive, so unless your implementation
of Bltsville supports all possible operations on all possible pixel formats,
some errors for unsupported pixel formats are to be expected.

At this time, results images for the tests are not automatically analyzed
for correctness, so the test results images must be viewed manually to
determine whether the test passed or failed.


Test Descriptions
------------------------------------------------------------------------------
------------------------------------------------------------------------------

Source image creation tests
---------------------------
(Note: if you are running with the libbltsville_cpu library, this step can
be skipped.  All the input images for pixel formats supported by
libbltsville_cpu are pre-generated. You only need this step if your
implementation supports pixel formats other than the ones included with
these tests.)

This test starts with a single 24bpp JPG image, and uses Bltsville to convert
that image to every possible format Bltsville supports.  Assuming this
conversion is successful, these images are then used as input images for the
remaining tests (below).   If you need input images for the below tests, and
your Bltsville library can not generate them automatically, you can also create
the input tests manually using your favorite editor or image conversion tools.
TI may also supply some input images with these conformance tests

The results images from this test all have the name format of:
    input-<FMT>-<width>x<height>(stride).raw




Pixel Format conversion tests.
------------------------------
For each pixel format, try to convert an image from that pixel format to every
other pixel fomat.

As with the Source Image Creation tests, an error can be expected for every
format that your Bltsville library doesn't support.  The test can be easily
edited to remove combinations known not to work.

The results images from this test all have the the name format of:
from-<FMT>-to-<FMT>.bmp, and should all look identical.

If there is a failure, a zero-length file named
from-<FMT>-to-<FMT>-unsupported.bmp will be created.  This allows the tester
to get a quick list of all failed conversions for examination.

To run this test:
1) review the list of "from" and "to" formats in the test script itself. Edit
this list if need be to match the abilities of your bltsville library.
2) first make sure the input files have been generated for your "from" formats,
and the input files are in the "input-images" directory
3) run ./conform-color-xlate.sh
4) in the xlate-results directory, list all the "*-unsupported.bmp" files.
Review this list to make sure they really are all unsupported.
5) view the remaining output files manually with an image viewer.  They should
all look the same.


Rotate-To tests
---------------------------
Not implimented yet


Rotate-From tests
---------------------------
Not implimented yet

Scaling tests
---------------------------
This test attempts to do scaling of unrotated images.  For certian image format
types, some format conversion is possible with scaling.
The tests do scaling up and down, of various scaling factors, and also some
special case scaling (horizontal only, vertical only).  Clipped scaling is
also tested.

Prior to running, review the lists of formats, and types of scaling,  used
in the test.  Add or remove formats and scale types as needed. The current
formats being tested are those supported by libbltsville_cpu.

To run, execute "./conform-scale.sh"
Output files have the format "scale-<FMT>-to-<FMT>-<dim>-<type>-<tag>.bmp
where:
  dim is the destination dimensions.
  type is the scaling algorithm used (best, point, fastest, etc)
  tag is a comment about the scale operation, such as "scale-up" or "scale-up-clipped"

After running, the results files will be in the "scale-results" directory.
Look for any files ending in "-unsupported" to find operations which Bltsville
could not execute.  Examine the remaining files visually for correctness.



Fill tests
---------------------------
Not implimented yet


Blend tests
---------------------------
Not implimented yet


Clip tests
---------------------------
Not implimented yet

Src2 ops
---------------
Not implimented yet

Boundary tests
--------------
The goal of these tests is to fail gracefully.  Blts are fed into bltsville that
violate pixel boundary requirements (such as splitting macropixels in YUV
surfaces).  For each test, Bltsville should exit with an error message, and
no output file should be generated.
