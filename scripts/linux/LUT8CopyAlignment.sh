#!/bin/bash

offs=32
w=800
bw=$(( ${w} - ${offs} ))
h=600

srcfmt=LUT8
srcstride=800
dstfmt=LUT8

rop=CCCC

simfile=sim.sh

echo >$simfile -n ""

  srcoff=0
  while [ "$srcoff" -lt "$offs" ]
  do
    dstoff=0
    while [ "$dstoff" -lt "$offs" ]
    do
      echo >>$simfile "echo Doing ${srcoff} to ${dstoff}"
      echo >>$simfile "./bvtest -src1file input-${srcfmt}-${w}x${h}\(${srcstride}\).raw -src1rect ${srcoff},0-${bw}x${h} -dst ${dstfmt} ${w}x${h} -dstrect ${dstoff},0-${bw}x${h} -rop ${rop} -dstfile test${srcoff}-${dstoff}.tga -libflag 80000000"
      dstoff=$(( $dstoff + 1 ))
    done
    srcoff=$(( $srcoff + 1 ))
  done

chmod 777 $simfile

./$simfile
rm $simfile


# ./bvtest -dst LUT8 800x600 -src1file input-LUT8-800x600\(800\).raw -dstrect 0,0-800x600 -src1rect 0,0-800x600 -rop CCCC -dstfile test00-00.tga
