#!/bin/bash

# Paths to things we need
and_bvt=./bin/bvtest

# Create this on the /sdcard partition, as presumably filling that up won't
# crash the device.
targetdir=/sdcard/TI

clean=0
csv_output=""
lib=$1

##############################################
# Parse the command line
for arg in "$@"
do
  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi
  if [ "$arg" = "-clean" ]
  then
    clean=1
  fi

  if [ "$arg" = "-adb" ]
  then
    useadb=1
  fi

  if [ "$arg" = "-csv" ]
  then
    csv_output="-csv"
  fi

done

##############################################
# Clean up old tests?
if [ "$clean" = "1" ]
then
    adb shell rm ${targetdir}/*
    adb shell rm /system/bin/bvtest
    adb shell rm /system/bin/perf.sh
    adb shell rm /system/bin/sanity.sh
    adb shell rmdir ${targetdir}
    rm *.raw
    rm *.tga
    rm *.jpg
    rm *.png
    rm *.csv
    rm sanity.sh
    rm verify-sanity.sh
    rm perf.sh
    rm junit.xml
    exit
fi

adb root && sleep 3 && adb wait-for-device && adb remount && adb wait-for-device

echo Locating source and reference images
# create the temp directory
adb shell mkdir -p    ${targetdir}

# Copy bvtest executable and source image
adb push ${and_bvt} /system/bin
adb push ../../images/Fruit-2048x1536.jpg ${targetdir}
adb push ../../images/kings_1_bg_051802.jpg ${targetdir}

# copy reference images
cp ./golden-${lib}/*.tga .

echo
echo Generating Sanity Tests
# generate the sanity test script
./agentest.sh $lib sanity.sh /system/bin/ ${csv_output}
adb shell "cat /dev/null > /system/bin/sanity.sh"
adb push sanity.sh  /system/bin
