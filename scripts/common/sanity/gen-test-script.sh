#!/bin/bash
#
# gen-test-script.sh
# Bltsville sanity tests: test script generator
#
# This script creates the actual sanity test scripts.
# Due to the limited number of shell command available on Android,
# we don't execute the tests directly.  Instead we generate the test
# commands here, and pipe them out to another sript file which only uses
# very basic shell commands.  This generated test script can then be
# executed on either Linux or Android to run the tests.
#
# Usage:
# gen-test-script.sh <libname> <scriptname> <path to bvtest>
#
# We assume that everything we need to run the script (input
# images, and reference images) are all located in this directory.
#
#
# Tests to run:
# -a. RGBA24 to RGBA24
# -b. RGBx24 to RGB16
# -c. RGBx24 to RGB16 with dithering
# -d. RGBA24 to RGBA24 with non-interpolated scaling
# -e. RGBA24 src1over RGBx24 to RGBx24 (src2 == dest)
# -f. RGBA24 src1over RGBA24 to RGBA24 (src2 == dest)
# -g. RGBA24 src1over RGBA24 to RGBA24 with non-interpolated scaling (src2 == dest)
# -h. RGBA24 src1over RGB16 to RGB16 (src2 == dest)
# -i. RGBA24 src1over RGB16 to RGB16 with dither (src2 == dest)
# -j. RGBA24 src1over RGB16 to RGB16 with non-interpolated scaling (src2 == dest)
# -k. RGBA24 src1over RGB16 to RGB16 with non-interpolated scaling and dither (src2 == dest)
# -l. RGB16 to RGB124
# -m. Each of the above with clipping, including negative values
#
# We also create a second script which verifies each file against its
# reference image, and generates results messages which are both printed
# on stdout and saved in a junit XML file for parsing by Jenkins
#
version=2.0.05
lib=$1
scriptname=$2
bvtpath=$3
verify_scriptname=verify-${scriptname}
junit_file=junit.xml
csv_file=comparetest.csv

src_w=320
src_h=256
stride32=1280
stride24=960
stride16=640

large_w=480
large_h=384
half_w=160
half_h=128
third_w=116
third_h=84
qtr_w=80
qtr_h=64

# Size of source image
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# Useful destination rectangles
dim_no_scale=${src_dim}
rc_no_scale=0,0-${dim_no_scale}
dim_scale_up=${large_w}x${large_h}
rc_scale_up=0,0-${dim_scale_up}
dim_scale_half=${half_w}x${half_h}
rc_scale_half=0,0-${dim_scale_half}
rc_scale_half_center=${qtr_w},${qtr_h}-${dim_scale_half}
dim_scale_third=${third_w}x${third_h}
rc_scale_third=0,0-${dim_scale_third}
dim_scale_qtr=${qtr_w}x${qtr_h}
rc_scale_qtr=0,0-${dim_scale_qtr}
rc_scale_qtr_center=${third_w},${third_h}-${dim_scale_qtr}


#Useful clip rects
clip_ul=0,0-128x96
clip_lr=${half_w},${half_h}-${src_w}x${src_h}
clip_center=${qtr_w},${qtr_h}-${half_w}x${half_h}

sc=0

for arg in "$@"
do
  if [ "$arg" = "-csv" ]
  then
    sc=1
echo "BN: making CSV!!!!"
  fi
done

##############################################################################
# Clear all the params out for a new operation.
# After calling this, just set what you need and then call Blit()
#
function Clear-Parms() {
  blt_tag=
  src1_cmd=
  src2_cmd=
  mask_cmd=
  rot_cmd=
  dither_cmd=
  scale_cmd=
  clip_cmd=
  clip_tag=
  rop_cmd=
  blend_cmd=
  dst_cmd=
  dst_file_name=
  dst_file_cmd=
}


##############################################################################
# Function to simplify the rather lengthy process of diffing the
# output file against the reference file.
#
function Verify-XML-File() {

  echo "let tested++" >> $verify_scriptname
  echo "diff ${dst_file_name}.tga ${dst_file_name}-ref.tga" >> $verify_scriptname
  echo "  if [ "\$\?" -ne "0" ]"                            >> $verify_scriptname
  echo "  then"                                             >> $verify_scriptname
  echo "      echo FAILED ${dst_file_name}.tga"             >> $verify_scriptname
  echo "      echo \ \ \ \ \ \<testcase classname=\\\"bltsville-$lib-sanity\\\" name=\\\"${dst_file_name}\\\" \>           >> ${junit_file}" >> $verify_scriptname
  echo "      echo \ \ \ \ \ \ \ \ \ \ \<failure\> TEST FAILED \<\/failure\>   >> ${junit_file}" >> $verify_scriptname
  echo "      echo \ \ \ \ \ \<\/testcase\>                                    >> ${junit_file}" >> $verify_scriptname
  echo "  else"                                             >> $verify_scriptname
  echo "      echo PASSED ${dst_file_name}.tga"             >> $verify_scriptname
  echo "      rm ${dst_file_name}.tga"                      >> $verify_scriptname
  echo "      rm ${dst_file_name}-ref.tga"                  >> $verify_scriptname
  echo "      let passed++" >> $verify_scriptname
  echo "      echo \ \ \ \ \ \<testcase  classname=\\\"bltsville-$lib-sanity\\\" name=\\\"${dst_file_name}\\\"\/\> >> ${junit_file}"  >> $verify_scriptname
  echo "  fi"                                               >> $verify_scriptname

}

function Verify-CSV-File() {


  echo "let tested++" >> $verify_scriptname
  echo "diff ${dst_file_name}.tga ${dst_file_name}-ref.tga" >> $verify_scriptname
  echo "  if [ "\$\?" -ne "0" ]"                            >> $verify_scriptname
  echo "  then"                                             >> $verify_scriptname
  echo "      echo FAILED ${dst_file_name}.tga"             >> $verify_scriptname
  echo "      echo \\\"${dst_file_name}\\\",FAIL		    >> ${csv_file}" >> $verify_scriptname
  echo "  else"                                             >> $verify_scriptname
  echo "      echo PASSED ${dst_file_name}.tga"             >> $verify_scriptname
  echo "      rm ${dst_file_name}.tga"                      >> $verify_scriptname
  echo "      rm ${dst_file_name}-ref.tga"                  >> $verify_scriptname
  echo "      let passed++" >> $verify_scriptname
  echo "      echo \\\"${dst_file_name}\\\",PASS		    >> ${csv_file}" >> $verify_scriptname
  echo "  fi"                                               >> $verify_scriptname
}


##############################################################################
# Generate a blit command from all the parameters.
#
function Blit() {

  dst_file_name="${blt_tag}-${clip_tag}"
  dst_file_cmd="-dstfile ${dst_file_name}.tga"
  echo "rm  ${dst_file_name}.tga 2> /dev/null" >> $scriptname
  echo "${bvtpath}bvtest ${src1_cmd} ${src2_cmd} ${mask_cmd} ${blend_cmd} ${clip_cmd} ${rot_cmd} ${dst_cmd} ${dst_file_cmd} ${dither_cmd} ${scale_cmd} ${rop_cmd} -imp $lib" >> $scriptname
  if [ "$sc" = "0" ]
  then
    Verify-XML-File
  else
    Verify-CSV-File
  fi
}


##############################################################################
# Function to execute the blits with a number of different clip rectangles.
function Clipped-Blits() {

  # No clip
  clip_cmd=
  clip_tag=clip-none
  Blit

  #clip to upper left
  clip_cmd="-cliprect ${clip_ul}"
  clip_tag=clip-ul
  Blit

  #clip to lower right
  clip_cmd="-cliprect ${clip_lr}"
  clip_tag=clip-lr
  Blit

  #clip to center
  clip_cmd="-cliprect ${clip_center}"
  clip_tag=clip-center
  Blit

}





##############################################################################
#
# Start with an empty script
echo "# This script was automatically generated by 'gen-test-script.sh'" > $scriptname
echo "# This script was automatically generated by 'gen-test-script.sh'" > $verify_scriptname
echo "tested=0" >> $verify_scriptname
echo "passed=0" >> $verify_scriptname
echo "echo Bltsville QuickTest $version" >> $scriptname
echo "${bvtpath}bvtest -version" >> $scriptname


##############################################################################
#
# write the junit results file header
echo "echo  > $junit_file"  >> $verify_scriptname
echo "echo \<testsuite name=\\\"bltsville-$lib-sanity\\\"\> >> ${junit_file}"  >> $verify_scriptname


echo Creating input images
##############################################################################
#
# Generate RGBA24 source image
#
echo "echo Generating RGBA24 source image." >> $scriptname
echo "${bvtpath}bvtest -src1file Fruit-2048x1536.jpg -src1 RGB124 2048x1536 -src1rect 0,0-2048x1536 -dst RGB124 ${src_dim} -dstrect ${src_rc} -dstfile test-pattern.raw -rop CCCC"  >> $scriptname
RGBA24_file_name="test-pattern-RGBA24-${src_dim}\(${stride32}\).raw"
echo "mv test-pattern-RGBx24-${src_dim}\(${stride32}\).raw  ${RGBA24_file_name}"  >> $scriptname

##############################################################################
#
# Generate RGBx24 source image
#
echo "echo Generating RGBx24 source image." >> $scriptname
echo "${bvtpath}bvtest -src1file Fruit-2048x1536.jpg -src1 RGB124 2048x1536 -src1rect 0,0-2048x1536 -dst RGB124 ${src_dim} -dstrect ${src_rc} -dstfile test-pattern.raw -rop CCCC"  >> $scriptname
RGBx24_file_name="test-pattern-RGBx24-${src_dim}\(${stride32}\).raw"

##############################################################################
#
# Generate RGB24 source image
#
echo "echo Generating RGB24 source image." >> $scriptname
echo "${bvtpath}bvtest -src1file ${RGBx24_file_name} -src1rect ${src_rc} -dst RGB24 ${src_dim} -dstrect ${src_rc} -dstfile test-pattern.raw -rop CCCC"  >> $scriptname
RGB24_file_name="test-pattern-RGB24-${src_dim}\(${stride24}\).raw"

##############################################################################
#
# Generate RGB16 source image
#
echo "echo Generating RGB16 source image." >> $scriptname
echo "${bvtpath}bvtest -src1file ${RGB24_file_name} -src1rect ${src_rc} -dst RGB16 ${src_dim} -dstrect ${src_rc} -dstfile test-pattern.raw -rop CCCC"  >> $scriptname
RGB16_file_name="test-pattern-RGB16-${src_dim}\(${stride16}\).raw"


##############################################################################
#
# Generate the RGBA24 overlay image
#
echo "echo Generating RGBA24 overlay image." >> $scriptname
echo "${bvtpath}bvtest -src1file kings_1_bg_051802.jpg -src1 RGB124 800x600 -src1rect 0,0-800x600 -dst RGB124 ${src_dim} -dstrect ${src_rc} -dstfile overlay-pattern.raw -rop CCCC"  >> $scriptname
RGBA24_overlay_file_name="overlay-pattern-RGBA24-${src_dim}\(${stride32}\).raw"
echo "mv overlay-pattern-RGBx24-${src_dim}\(${stride32}\).raw  ${RGBA24_overlay_file_name}"  >> $scriptname

##############################################################################
#
# Generate the RGBx24 overlay image
#
echo "echo Generating RGBX24 overlay image." >> $scriptname
echo "${bvtpath}bvtest -src1file kings_1_bg_051802.jpg -src1 RGB124 800x600 -src1rect 0,0-800x600 -dst RGB124 ${src_dim} -dstrect ${src_rc} -dstfile overlay-pattern.raw -rop CCCC"  >> $scriptname
RGBx24_overlay_file_name="overlay-pattern-RGBx24-${src_dim}\(${stride32}\).raw"


##############################################################################
# -a. RGBA24 to RGBA24
#
echo Creating test 'RGBA24 to RGBA24'
echo "echo Executing test 'RGBA24 to RGBA24'." >> $scriptname
Clear-Parms
blt_tag=copy-RGBA24-to-RGBA24
rop_cmd="-rop CCCC"
src1_cmd="-src1file ${RGBA24_file_name} -src1rect ${src_rc}"
dst_cmd="-dst RGBA24 ${dim_no_scale} -dstrect ${src_rc}"
Clipped-Blits


##############################################################################
# -b. RGBx24 to RGB16
#
echo Creating test 'RGBx24 to RGB16'
echo "echo Executing test 'RGBx24 to RGB16'." >> $scriptname
Clear-Parms
blt_tag=copy-RGBx24-to-RGB16
rop_cmd="-rop CCCC"
src1_cmd="-src1file ${RGBx24_file_name} -src1rect ${src_rc}"
dst_cmd="-dst RGB16 ${dim_no_scale} -dstrect ${src_rc}"
Clipped-Blits


##############################################################################
# -c. RGBx24 to RGB16 with dithering
#
echo Creating test 'RGBx24 to RGB16 with dither'
echo "echo Executing test 'RGBx24 to RGB16 with dither'." >> $scriptname
Clear-Parms
blt_tag=dither-RGBx24-to-RGB16
rop_cmd="-rop CCCC"
src1_cmd="-src1file ${RGBx24_file_name} -src1rect ${src_rc}"
dst_cmd="-dst RGB16 ${dim_no_scale} -dstrect ${src_rc}"
dither_cmd="-dither best_on"
Clipped-Blits


##############################################################################
# -d. RGBA24 to RGBA24 with non-interpolated scaling
#
echo Creating test 'RGBA24 to RGBA24 with scale'
echo "echo Executing test 'RGBA24 to RGBA24 with scale'." >> $scriptname
Clear-Parms
rop_cmd="-rop CCCC"
scale_cmd="-scale fastest"
src1_cmd="-src1file ${RGBA24_file_name} -src1rect ${src_rc}"

# scale up
blt_tag=scale-up-RGBA24-to-RGBA24
dst_cmd="-dst RGBA24 ${dim_scale_up} -dstrect ${rc_scale_up}"
Clipped-Blits

#scale down half
blt_tag=scale-down-half-RGBA24-to-RGBA24
dst_cmd="-dst RGBA24 ${dim_no_scale} -dstrect ${rc_scale_half_center}"
Clipped-Blits

#scale down quarter
blt_tag=scale-down-qtr-RGBA24-to-RGBA24
dst_cmd="-dst RGBA24 ${dim_no_scale} -dstrect ${rc_scale_qtr_center}"
Clipped-Blits


##############################################################################
# -e. RGBA24 src1over RGBx24 to RGBx24 (src2 == dest)
#
echo Creating test 'RGBA24 src1over RGBx24 to RGBx24 with global alpha'
echo "echo Executing test 'RGBA24 src1over RGBx24 to RGBx24 with global alpha'." >> $scriptname
Clear-Parms
blt_tag=blend-RGBA24-src1over+global-RGBx24
blend_cmd="-blend src1over+global -alpha 80"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGBx24_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGBx24 src2 -dstrect ${src_rc}"
Clipped-Blits

echo Creating test 'RGBA24 src1over RGBx24 to RGBx24 with local alpha'
echo "echo Executing test 'RGBA24 src1over RGBx24 to RGBx24 with local alpha'." >> $scriptname
Clear-Parms
blt_tag=blend-RGBA24-src1over-RGBx24
blend_cmd="-blend src1over"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGBx24_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGBx24 src2 -dstrect ${src_rc}"
Clipped-Blits


##############################################################################
# -f. RGBA24 src1over RGBA24 to RGBA24 (src2 == dest)
#
echo Creating test 'RGBA24 src1over RGBA24 to RGBA24 with global alpha'
echo "echo Executing test 'RGBA24 src1over RGBA24 to RGBA24 with global alpha'." >> $scriptname
Clear-Parms
blt_tag=blend-RGBA24-src1over+global-RGBA24
blend_cmd="-blend src1over+global -alpha 80"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGBA24_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGBA24 src2 -dstrect ${src_rc}"
Clipped-Blits

echo Creating test 'RGBA24 src1over RGBA24 to RGBA24 with local alpha'
echo "echo Executing test 'RGBA24 src1over RGBA24 to RGBA24 with local alpha'." >> $scriptname
Clear-Parms
blt_tag=blend-RGBA24-src1over-RGBA24
blend_cmd="-blend src1over"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGBA24_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGBA24 src2 -dstrect ${src_rc}"
Clipped-Blits

##############################################################################
# -g. RGBA24 src1over RGBA24 to RGBA24 with non-interpolated scaling (src2 == dest)
#
echo Creating test 'RGBA24 src1over RGBA24 to RGBA24 with scale'
echo "echo Executing test 'RGBA24 src1over RGBA24 to RGBA24 with scale'." >> $scriptname
Clear-Parms
blt_tag=blend-scale-RGBA24-src1over-RGBA24
blend_cmd="-blend src1over"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${rc_scale_qtr}"
src2_cmd="-src2file ${RGBA24_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGBA24 src2 -dstrect ${src_rc}"
scale_cmd="-scale fastest"
Clipped-Blits


##############################################################################
# -h. RGBA24 src1over RGB16 to RGB16 (src2 == dest)
#
echo Creating test 'RGBA24 src1over RGB16 to RGB16 with global alpha'
echo "echo Executing test 'RGBA24 src1over RGB16 to RGB16 with global alpha'." >> $scriptname
Clear-Parms
blt_tag=blend-RGBA24-src1over+global-RGB16
blend_cmd="-blend src1over+global -alpha 80"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGB16 src2 -dstrect ${src_rc}"
Clipped-Blits

echo Creating test 'RGBA24 src1over RGB16 to RGB16 with local alpha'
echo "echo Executing test 'RGBA24 src1over RGB16 to RGB16 with local alpha'." >> $scriptname
Clear-Parms
blt_tag=blend-RGBA24-src1over-RGB16
blend_cmd="-blend src1over"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGB16 src2 -dstrect ${src_rc}"
Clipped-Blits

##############################################################################
# -i. RGBA24 src1over RGB16 to RGB16 with dither (src2 == dest)
#
echo Creating test 'RGBA24 src1over RGB16 to RGB16 with global alpha'
echo "echo Executing test 'RGBA24 src1over RGB16 to RGB16 with global alpha'." >> $scriptname
Clear-Parms
blt_tag=dither-blend-RGBA24-src1over+global-RGB16
blend_cmd="-blend src1over+global -alpha 80"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGB16 src2 -dstrect ${src_rc}"
dither_cmd="-dither best_on"
Clipped-Blits

echo Creating test 'RGBA24 src1over RGB16 to RGB16 with local alpha'
echo "echo Executing test 'RGBA24 src1over RGB16 to RGB16 with local alpha'." >> $scriptname
Clear-Parms
blt_tag=dither-blend-RGBA24-src1over-RGB16
blend_cmd="-blend src1over"
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGB16 src2 -dstrect ${src_rc}"
dither_cmd="-dither best_on"
Clipped-Blits


##############################################################################
# -j. RGBA24 src1over RGB16 to RGB16 with non-interpolated scaling (src2 == dest)
#
echo Creating test 'RGBA24 src1over RGB16 to RGB16 with scale'
echo "echo Executing test 'RGBA24 src1over RGB16 to RGB16 with scale'." >> $scriptname
Clear-Parms
blend_cmd="-blend src1over"

# scale up
blt_tag=scale-up-blend-RGBA24-src1over-RGB16
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${rc_scale_half}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGB16 src2 -dstrect ${src_rc}"
Clipped-Blits

#scale down half
blt_tag=scale-down-half-blend-RGBA24-src1over-RGB16
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${rc_scale_half}"
dst_cmd="-dst RGB16 src2 -dstrect ${qtr_w},${qtr_h}-${dim_scale_half}"
Clipped-Blits

#scale down third
blt_tag=scale-down-third-blend-RGBA24-src1over-RGB16
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${rc_scale_third}"
dst_cmd="-dst RGB16 src2 -dstrect ${qtr_w},${qtr_h}-${dim_scale_third}"
Clipped-Blits

##############################################################################
# -k. RGBA24 src1over RGB16 to RGB16 with non-interpolated scaling and dither (src2 == dest)
#
echo Creating test 'RGBA24 src1over RGB16 to RGB16 with scale and dither'
echo "echo Executing test 'RGBA24 src1over RGB16 to RGB16 with scale and dither'." >> $scriptname
Clear-Parms
blend_cmd="-blend src1over"
dither_cmd="-dither best_on"

# scale up
blt_tag=dither-scale-up-blend-RGBA24-src1over-RGB16
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${rc_scale_half}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${src_rc}"
dst_cmd="-dst RGB16 src2 -dstrect ${src_rc}"
Clipped-Blits

# scale down half
blt_tag=dither-scale-down-half-blend-RGBA24-src1over-RGB16
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${rc_scale_half}"
dst_cmd="-dst RGB16 src2 -dstrect ${qtr_w},${qtr_h}-${dim_scale_half}"
Clipped-Blits

# scale down third
blt_tag=dither-scale-down-third-blend-RGBA24-src1over-RGB16
src1_cmd="-src1file ${RGBA24_overlay_file_name} -src1rect ${src_rc}"
src2_cmd="-src2file ${RGB16_file_name} -src2rect ${rc_scale_third}"
dst_cmd="-dst RGB16 src2 -dstrect ${qtr_w},${qtr_h}-${dim_scale_third}"
Clipped-Blits

##############################################################################
# -l. RGB16 to RGB124
#
echo Creating test 'RGB16 to RGB124'
echo "echo Executing test 'RGB16 to RGB124'." >> $scriptname
Clear-Parms
blt_tag=copy-RGB16-to-RGB124
rop_cmd="-rop CCCC"
src1_cmd="-src1file ${RGB16_file_name} -src1rect ${src_rc}"
dst_cmd="-dst RGB124 ${dim_no_scale} -dstrect ${src_rc}"
Clipped-Blits


##############################################################################
# -m. NV12 to RGBx24
#
echo Creating test 'NV12 to RGBx24'
echo "echo Executing test 'NV12 to RGBx24'." >> $scriptname
cp ../../../images/test-pattern-NV12-1920x1080\(1920\).raw .
YUV_file_name="test-pattern-NV12-1920x1080\(1920\).raw"
Clear-Parms
blt_tag=copy-NV12-to-RGBx24
rop_cmd="-rop CCCC"
src1_cmd="-src1file ${YUV_file_name} -src1rect 0,0-1920x1080"
dst_cmd="-dst RGBx24 1920x1080 -dstrect 0,0-1920x1080"
Clipped-Blits

##############################################################################
# -n. YUYV to RGBx24
#
echo Creating test 'YUYV to RGBx24'
echo "echo Executing test 'YUYV to RGBx24'." >> $scriptname
cp ../../../images/test-pattern-YUYV-720x240\(1440\).raw .
YUV_file_name="test-pattern-YUYV-720x240\(1440\).raw"
Clear-Parms
blt_tag=copy-YUYV-to-RGBx24
rop_cmd="-rop CCCC"
src1_cmd="-src1file ${YUV_file_name} -src1rect 0,0-720x240"
dst_cmd="-dst RGBx24 720x240 -dstrect 0,0-720x240"
Clipped-Blits



##############################################################################
#
# write the junit results file footer
echo "echo \<\/testsuite\> >> ${junit_file}"  >> $verify_scriptname


##############################################################################
# All done
echo "echo Passed \$passed of \$tested tests." >> $verify_scriptname
echo "echo Passing results have been deleted. Any remaining TGA images represent failures." >> $verify_scriptname

chmod 777 $scriptname
chmod 777 $verify_scriptname
