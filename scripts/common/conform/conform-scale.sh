#!/bin/bash
#
# conform-scale.sh
# Bltsville conformance tests: Scaling
#
# Tests executed:
#    Scale with color covert
#    Scale same format
#    Scale up
#    Scale down
#    Scale horizontal only
#    Scale vertical only
#    Scale partial image
#    Scale with clipping
#    Scale to same surface
#
#
# This test uses two separate helper functions to test scaleing.
# The first does format conversion while scaling. It takes two arrays of pixel
# format names: the "from" array and the # "to" array.
#
# However, most scaling functions only support source and destination formats
# that are the same, so trying to scale everything in the source list to
# everything in the destination list will result in lots of unsupported
# operations.  To reduce the number of errors due to unsupported operations
# we have a second helper function which only takes a single array of source
# formats, and scaled them to the same destination format.
#
# Additionally, there is an array of scaling types to try for each copy.
#
# Source and destination rectangles are varied in size to create scaled-up and
# scaled-down images.
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#
#



###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done



###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
targetdir=.
outputdir=scale-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
here=.
touch="touch "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect='\>'
  push="adb push "
  pull="adb pull "
  here=
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin

fi

${make_dir} ${targetdir}  
${make_dir} ${targetdir}/this_run  
${make_dir} ${outputpath}  

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null



###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
plus1_width=321
plus1_height=257
double_width=640
double_height=512
threeqtr_width=240
threeqtr_height=192
half_width=160
half_height=128
third_width=96
third_height=80
qtr_width=80
qtr_height=64

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}

##############################################
# Function to scale from source to destination pixel formats
# srcfmt is the array of source formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
# dstfmt is the array of destination formats to create.
#
function Scale-And-Convert() {
  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcstride=${srcstrides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}

    dstidx=0
    while [ "x${dstfmts[dstidx]}" != "x" ]
    do
      dstfmt=${dstfmts[dstidx]}

      scaleidx=0
      while [ "x${scaletypes[scaleidx]}" != "x" ]
      do
        scaletype=${scaletypes[scaleidx]}

        dstfile=${outputpath}/scale-${tag}-${srcfmt}-to-${dstfmt}-${dst_dim}-${scaletype}

        # remove previous test results if they exist
        if [ -f "${dstfile}.tga" ]
        then
           ${remove} ${dstfile}.tga
        fi
        if [ -f "${dstfile}-unsupported.tga" ]
        then
          ${remove} ${dstfile}-unsupported.tga
        fi
        if [ -f "${dstfile}-segfault.tga" ]
        then
           ${remove} ${dstfile}-segfault.tga
        fi
        if [ -f "${dstfile}.crc" ]
        then
           ${remove} ${dstfile}.crc
        fi

        echo Scaling from ${srcfmt} to ${dstfmt}-${dst_dim} using ${scaletype}
        if [ "$vb" = "1" ]
        then
          echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dest_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -scale ${scaletype} -rop CCCC ${bvt_ret}
        fi
        ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dest_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -scale ${scaletype} -rop CCCC ${bvt_ret}

        # If scale failed, print a warning. Also create a dummy file, so as
        # to immediately call attention to the error, to whatever person (or
        # script) is examining the output.
        retcode=$?
        if [ "$android" = "1" ]
        then
          retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
        fi
        # Check for android system error
        if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
        fi

        if [ "$retcode" = "139" ]
        then
          echo ERROR: Cannot scale from ${srcfmt} to ${dstfmt} using ${scaletype}.
          ${touch} ${dstfile}-segfault.tga
        else
          if [ "$retcode" -ne "0" ]
          then
            echo ERROR: Cannot scale from ${srcfmt} to ${dstfmt} using ${scaletype}.
            ${touch} ${dstfile}-unsupported.tga
          fi
        fi

        # Also create a CRC for the file we just wrote.
        if [ "$android" = "1" ]
        then
            ${crc} ${dstfile}.tga \>  ${dstfile}.crc
        else
            ${crc} ${dstfile}.tga > ${dstfile}.crc
        fi

        if [ "$no_tga" = "1" ]
        then
          ${remove} ${dstfile}*.tga
        fi

        scaleidx=$(( $scaleidx + 1 ))
      done

      dstidx=$(( $dstidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done
}


##############################################
# Function to scale from a source format to the same destination pixel formats
# srcfmt is the array of formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
#
function Scale-Image() {
  srcidx=0
  while [ "x${fmts[srcidx]}" != "x" ]
  do
    srcfmt=${fmts[srcidx]}
    srcstride=${strides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}
    dstfmt=${srcfmt}

    scaleidx=0
    while [ "x${scaletypes[scaleidx]}" != "x" ]
    do
      scaletype=${scaletypes[scaleidx]}

      dstfile=${outputpath}/scale-${tag}-${srcfmt}-to-${dstfmt}-${dst_dim}-${scaletype}

      # remove previous test results if they exist
      if [ -f "${dstfile}.tga" ]
      then
         ${remove} ${dstfile}.tga
      fi
      if [ -f "${dstfile}-unsupported.tga" ]
      then
        ${remove} ${dstfile}-unsupported.tga
      fi
      if [ -f "${dstfile}-segfault.tga" ]
      then
         ${remove} ${dstfile}-segfault.tga
      fi
      if [ -f "${dstfile}.crc" ]
      then
         ${remove} ${dstfile}.crc
      fi

      echo Scaling from ${srcfmt} to ${dstfmt}-${dst_dim} using ${scaletype}
      if [ "$vb" = "1" ]
      then
          echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dest_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -scale ${scaletype} -rop CCCC ${bvt_ret}
      fi
      ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dest_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga  -scale ${scaletype} -rop CCCC ${bvt_ret}

      # If scale failed, print a warning. Also create a dummy file, so as
      # to immediately call attention to the error, to whatever person (or
      # script) is examining the output.
      retcode=$?
      if [ "$android" = "1" ]
      then
        retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
      fi
      # Check for android system error
      if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
      fi

      if [ "$retcode" = "139" ]
      then
        echo ERROR: Segfault scaling from ${srcfmt} to ${dstfmt} using ${scaletype}.
        ${touch} ${dstfile}-segfault.tga
      else
        if [ "$retcode" -ne "0" ]
        then
          echo ERROR: Cannot scale from ${srcfmt} to ${dstfmt} using ${scaletype}.
          ${touch} ${dstfile}-unsupported.tga
        fi
      fi

      # Also create a CRC for the file we just wrote.
      if [ "$android" = "1" ]
      then
          ${crc} ${dstfile}.tga \>  ${dstfile}.crc
      else
          ${crc} ${dstfile}.tga > ${dstfile}.crc
      fi

      if [ "$no_tga" = "1" ]
      then
        ${remove} ${dstfile}*.tga
      fi

      scaleidx=$(( $scaleidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done

  Scale-And-Convert
}


##############################################
# here are the types of scaling to try for each copy.
# bltsville doesn't support bilinear or modbiliner
scaletypes=( best fastest point  )

##############################################
# Test same-format scaling.  Each of these formats will be scaled to an image
# of the same format, but a different size.
fmts=(    BGR024 BGRx24 RGB024 RGBx24 0BGR24 xBGR24 0RGB24 xRGB24 RGBA24 BGRA24 ARGB24 ABGR24 nRGBA24 nBGRA24 nARGB24 nABGR24)
strides=( 1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280    1280    1280    1280   )

##############################################
# Format conversion scaling
# Convert from the source format to the destination format while scaling
srcfmts=(    RGB16 )
srcstrides=( 640   )
dstfmts=(   RGB124 )

# Scale up 1
tag=up1
dest_w=${src_w}
dest_h=${src_h}
dest_h=${plus1_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image

# Scale up ( 1.0x-)
tag=up
dest_w=${double_width}
dest_h=${double_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image


# Scale down (0.5x-1.0x)
tag=down-three-quarter
dest_w=${threeqtr_width}
dest_h=${threeqtr_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image

# Scale down (0.33x-0.5x)
tag=down-half
dest_w=${half_width}
dest_h=${half_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image

# Scale down (0.25x-0.33x)
tag=down-third
dest_w=${third_width}
dest_h=${third_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image

# Scale down under (-0.25x)
tag=down-quarter
dest_w=${qtr_width}
dest_h=${qtr_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image

# Scale entire src to center of destination.
tag=to-center
dest_w=${src_w}
dest_h=${src_h}
dest_dim=${dest_w}x${dest_h}
dest_rc=${middle_rc}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image

# Scale center portion of src to entire destination
tag=from-center
dest_w=${src_w}
dest_h=${src_h}
dest_dim=${dest_w}x${dest_h}
dest_rc=${dst_rc}
clip_rc=${dest_rc}
src1_rc=${middle_rc}
Scale-Image

# Scale Vertically only
tag=vert
dest_w=${src_w}
dest_h=${double_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image

# Scale Horizontally only
tag=horz
dest_w=${double_width}
dest_h=${src_h}
dest_dim=${dest_w}x${dest_h}
dest_rc=0,0-${dest_dim}
clip_rc=${dest_rc}
src1_rc=${src_rc}
Scale-Image


#################################################################
# clipped scaling
#################################################################

# Right now they are all the same, so only test one method.
scaletypes=( best )

# create a src rect that is the full source surface.
src1_rc=${src_rc}

# Create a destination surface the same size as the source.
dest_dim=${src_dim}

# We will scale up 4x.  Create a dst rect that is twice as high, twice as wide.
# make the origin be negative in both x and y
dest_rc=${oversize_rc}

# The above scale will exceed the destination surface on all edges.
# Now run the scale but clip it in various ways to make it a legal blt


# Clip to entire destination
tag=clip-to-dest
clip_rc=${dst_rc}
Scale-Image


# Clip to a center rectangle
tag=clip-to-center
clip_rc=${middle_rc}
Scale-Image

# Clip to a upper left
tag=clip-to-ul
clip_rc=${ul_rc}
Scale-Image

# Clip to a upper right
tag=clip-to-ur
clip_rc=${ur_rc}
Scale-Image

# Clip to a lower left
tag=clip-to-ll
clip_rc=${ll_rc}
Scale-Image

# Clip to a lower right
tag=clip-to-lr
clip_rc=${lr_rc}
Scale-Image

# Clip to NULL.
tag=clip-outside-dest
clip_rc=-${half_width},-${half_height}-${half_width}x${half_height}
Scale-Image

# Clip away entirely
tag=clip-all
dest_rc=${ul_rc}
clip_rc=${lr_rc}
Scale-Image


# ====================================================
# Scale to same surf
# ====================================================

# Disable convert-while-scale.  Result is undefined if source and
# dest are the same.
srcfmts=(     )
srcstrides=(    )
dstfmts=(    )

# Scale upper left quadrant into lower right quadrant
# Reduce each dimension by half.
tag=Same-Surf
dest_w=${qtr_width}
dest_h=${qtr_height}
dest_dim=${dest_w}x${dest_h}
dest_rc=${half_width},${half_height}-${dest_dim}
clip_rc=${dest_rc}
src1_rc=0,0-${half_width}x${half_height}
dst_dim=src1
dest_dim=src1
Scale-Image





##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi
