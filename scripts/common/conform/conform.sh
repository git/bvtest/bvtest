#!/bin/bash
#
# conform.sh
# Bltsville conformance tests: Main script
#
# This test ties together and automates all the other conformance tests
#
#
# The -verbose command line argument will cause the tests to print the bvtest
# command line used to create each image.
#
# The -clean command line argument will remove all the results files from
# previous tests
#

version=2.0.0.7
# Defaults for command line parameters
vb=0
verbose=
clean=0
no_tga=
platform=
preferredlib=
libname=
get_lib=0


echo Bltsville Conformance Tests version ${version}

##############################################
# Parse the command line
for arg in "$@"
do

  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    libname=$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-clean" ]
  then
    clean=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=-no_tga
  fi

  if [ "$arg" = "-linux" ]
  then
    platform=-linux
  fi

  if [ "$arg" = "-android" ]
  then
    platform=-android
  fi

done


##############################################
# Setup for Linux
#
targetdir=.
remove="rm "
remove_dir="rmdir "


##############################################
# Setup for Android
#
if [ "$platform" = "-android" ]
then
  targetdir=/sdcard/TI-conform
  remove="adb shell cd ${targetdir};rm "
  remove_dir="adb shell cd ${targetdir};rmdir "

fi

##############################################
# Clean up old tests?
if [ "$clean" = "1" ]
then

    ${remove} this_run/input-images/*
    ${remove_dir} this_run/input-images

    ${remove} this_run/dither-results/*
    ${remove_dir} this_run/dither-results

    ${remove} this_run/scale-results/*
    ${remove_dir} this_run/scale-results

    ${remove} this_run/xlate-results/*
    ${remove_dir} this_run/xlate-results

    ${remove} this_run/callback-results/*
    ${remove_dir} this_run/callback-results

    ${remove} this_run/rotate-results/*
    ${remove_dir} this_run/rotate-results

    ${remove} this_run/fill-results/*
    ${remove_dir} this_run/fill-results

    ${remove} this_run/blend-results/*
    ${remove_dir} this_run/blend-results

    ${remove} this_run/jpg-convert/*
    ${remove_dir} this_run/jpg-convert

    ${remove} this_run/undefined-blend-results/*
    ${remove_dir} this_run/undefined-blend-results

    ${remove} this_run/undefined-formats-results/*
    ${remove_dir} this_run/undefined-formats-results

    ${remove} this_run/same-surf-results/*
    ${remove_dir} this_run/same-surf-results

    ${remove} this_run/dither-pattern-results/*
    ${remove_dir} this_run/dither-pattern-results

    ${remove} this_run/boundary-results/*
    ${remove_dir} this_run/boundary-results

    ${remove} this_run/rot-scale-results/*
    ${remove_dir} this_run/rot-scale-results

    ${remove} this_run/*
    ${remove_dir} this_run

    ${remove} "input-images/*"
    ${remove_dir} input-images

    ${remove} *.tga
    ${remove} *.jpg
    ${remove} *.png
    ${remove} retval

    rm junit.xml
    rm -rf this_run
    rm -rf golden-cpu
    rm -rf golden-hw2d
    rm -rf golden-gc2d
    rm -rf golden-proxy

    exit
fi

if [ "$libname" = "" ]
then
  echo
  echo "USAGE: ./conform.sh -imp {cpu|hw2d|proxy} [-clean] [-no_tga] [-verbose]"
  echo
  exit
fi


##############################################
# Unpack the golden images
tar -xf golden-${libname}.tar

##############################################
# Run the conformance tests.

./conform-make-inputs.sh        ${verbose} ${preferredlib} ${platform}
./conform-make-inputs.sh        ${verbose} -imp cpu ${platform}

./conform-fill.sh               ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-scale.sh              ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-dither.sh             ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-rotate.sh             ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-blend.sh              ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-same-surf.sh          ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-dither-pattern.sh     ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-boundaries.sh         ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-undefined-formats.sh  ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-undefined-blend.sh    ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-callback.sh           ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-color-xlate.sh        ${verbose} ${preferredlib} ${platform} ${no_tga}
./conform-rot-scale.sh          ${verbose} ${preferredlib} ${platform} ${no_tga}


#############################################################################
# Evaluate results. Generate Jenkins-compatible output file.
#
echo \<testsuite name=\"bltsville-${libname}-conformance\"\> > junit.xml

function Parse-Results()
{
  local testname=$1
  local testdesc=$2

  local passed=`diff -w -r -q -s golden-${libname}/${testname}-results this_run/${testname}-results | grep identical | wc -l`
  local attempted=`ls this_run/${testname}-results/*.crc | wc -l`
  local expected=`ls golden-${libname}/${testname}-results/*.crc | wc -l`
  local failed=$(($expected-$passed))
  local not_supported=$(($attempted-$expected))
  echo "${testdesc}" passed ${passed} of ${expected} tests.
  if [ "$passed" = "$expected" ]
  then
    echo "    "\<testcase classname=\"bltsville-${libname}-conformance\" name=\"${testdesc}\"/\> >> junit.xml
  else
    echo "    "\<testcase classname=\"bltsville-${libname}-conformance\" name=\"${testdesc}\"\> >> junit.xml
    echo "        "\<failure\> TEST FAILED ${failed} of ${expected} tests\<\/failure\>   >> junit.xml
    echo "    "\<\/testcase\>  >> junit.xml
  fi

}

Parse-Results fill              "Fill              "
Parse-Results scale             "Scale             "
Parse-Results dither            "Dither            "
Parse-Results rotate            "Rotate            "
Parse-Results xlate             "Color Translate   "
Parse-Results blend             "Blend             "
Parse-Results same-surf         "Same Surface      "
Parse-Results dither-pattern    "Dither Pattern    "
Parse-Results boundary          "Boundary          "
Parse-Results undefined-formats "Undefined Formats "
Parse-Results undefined-blend   "Undefined Blend   "
Parse-Results callback          "Callback          "
Parse-Results rot-scale         "Rotate And Scale  "

echo \</testsuite\> >> junit.xml

echo Bltsville Conformance Tests version ${version}

