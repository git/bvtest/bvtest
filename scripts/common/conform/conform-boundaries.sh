#!/bin/bash
#
# conform-boundaries.sh
# Bltsville conformance tests: Boundary conditions
#
# Tests executed:
#   Src rectangle out of bounds of source surface
#   Dst rectangle out of bounds of dest surface
#
# Tests blts that are supposed to fail.  All blits should gracefully fail with
# an error message and no image generated.  If the test generates an image, or
# causes a crash, the test fails.
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#
#

###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done

###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
outputdir=boundary-results
targetdir=.
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
touch="touch "
echo="echo "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  quote=\"
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect=\>
  push="adb push "
  pull="adb pull "
  touch="adb shell touch "
  echo="adb shell \" echo "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin
fi

# Create the directory to store the results in
${make_dir} ${targetdir} 
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null

###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
half_width=160
half_height=128
qtr_width=80
qtr_height=64

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}


##############################################
# Function to blit from a source format to the same destination pixel formats
# srcfmt is the array of formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
#
function Test-Blt() {
  srcidx=0
  while [ "x${fmts[srcidx]}" != "x" ]
  do
    srcfmt=${fmts[srcidx]}
    srcstride=${strides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}
    dstfmt=${srcfmt}

    dstfile=${outputpath}/boundary-${srcfmt}-to-${dstfmt}-${dst_dim}-${tag}

    echo Blitting from ${srcfmt} to ${dstfmt}-${dst_dim}
    if [ "$vb" = "1" ]
    then
        echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -dstfile ${dstfile}.tga  -rop CCCC ${bvt_ret}
    fi
    ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -dstfile ${dstfile}.tga  -rop CCCC  ${bvt_ret}

    # If blit failed, print a warning. Also create a dummy file, so as
    # to immediately call attention to the error, to whatever person (or
    # script) is examining the output.
    # Also create a CRC for the file we just wrote.
    # Since this test doesn't make images, there is nothing to CRC.
    # All we have is a "pass" file, "fail" file, or a "segfault" file.
    # So create a fake CRC of 0 for pass and 0xFFFFFFFF for fail.
      retcode=$?
    if [ "$android" = "1" ]
    then
      retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
    fi
    # Check for android system error
    if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
    fi

    if [ "$retcode" = "139" ]
    then
        echo ERROR: Segfault during blit from ${srcfmt} to ${dstfmt} using ${scaletype}.
        ${touch} ${dstfile}-segfault.tga
        if [ "$android" = "1" ]
        then
          adb shell "echo 0xFFFFFFFF > ${dstfile}.crc"
        else
          echo 0xFFFFFFFF > ${dstfile}.crc
        fi
    else
        # Recall for these tests, we don't expect an output file, we just expect bvtest to not crash.
        if [ "$retcode" = "1" ]
        then
          ${touch} ${dstfile}-pass.tga
          if [ "$android" = "1" ]
          then
            adb shell "echo 0 > ${dstfile}.crc"
          else
            echo 0 > ${dstfile}.crc
          fi
        else
          echo "Test failed. Creating fail file and CRC"
          ${touch} ${dstfile}-fail.tga
          if [ "$android" = "1" ]
          then
            adb shell "echo 0 > ${dstfile}.crc"
          else
            echo 0 > ${dstfile}.crc
          fi
        fi
    fi

    if [ "$no_tga" = "1" ]
    then
      ${remove} ${dstfile}*.tga
    fi

    srcidx=$(( $srcidx + 1 ))
  done
}

##############################################
# Test same-format copy.  
fmts=(    0BGR24 0RGB24 1BGR24 1RGB24 ABGR24 ARGB24 BGR024 BGR124 BGR24 BGRA24 BGRx24 IYUV LUT8 MONO8 NV12 RGB024 RGB12 RGB124 RGB15 RGB16 RGB24 RGBA24 RGBx24 UYVY YUYV YV12 nBGRA24 nRGBA24 xBGR24 xRGB24 )
strides=( 1280   1280   1280   1280   1280   1280   1280   1280   960   1280   1280   320  320  320   320  1280   640   1280   640   640   960   1280   1280   640  640  320  1280    1280    1280   1280   )

##############################################
tag=src-off-top
src1_rc=0,-100-${src_dim}
dest_rc=0,0-${dst_dim}
Test-Blt

tag=src-off-left
src1_rc=-100,0-${src_dim}
dest_rc=0,0-${dst_dim}
Test-Blt

tag=src-off-bottom
src1_rc=0,100-${src_dim}
dest_rc=0,0-${dst_dim}
Test-Blt

tag=src-off-right
src1_rc=100,0-${src_dim}
dest_rc=0,0-${dst_dim}
Test-Blt

##############################################
tag=dst-off-top
src1_rc=0,0-${src_dim}
dest_rc=0,-100-${dst_dim}
Test-Blt

tag=dst-off-left
src1_rc=0,0-${src_dim}
dest_rc=-100,0-${dst_dim}
Test-Blt

tag=dst-off-bottom
src1_rc=0,0-${src_dim}
dest_rc=0,100-${dst_dim}
Test-Blt

tag=dst-off-right
src1_rc=0,0-${src_dim}
dest_rc=100,0-${dst_dim}
Test-Blt



##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi




