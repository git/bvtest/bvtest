#!/bin/bash
#
# conform-undefined-formats.sh
# Bltsville conformance tests: undefined formats
#
# This test calls blitsville using surface formats which have not been
# defined yet.  The formats look like normal defined formats, except
# the vendor information is different.
#
# Output images are named "undef-<FMT>-to-<FMT>.tga" and should all fail gracefully.
# The test should not produce any actual output images, nor should it segfault.
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#




###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done

###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
targetdir=.
outputdir=undefined-formats-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
touch="touch "
echo="echo "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  quote=\"
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect=\>
  push="adb push "
  pull="adb pull "
  touch="adb shell touch "
  echo="adb shell \" echo "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin
fi

# Create the directory to store the results in
${make_dir} ${targetdir}
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null


# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
threeqtr_width=240
threeqtr_height=192
half_width=160
half_height=128
third_width=96
third_height=80
qtr_width=80
qtr_height=64

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}

##############################################
# Function to convert from source to destination pixel formats
# srcfmt is the array of source formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
# dstfmt is the array of destination formats to create.
#
function Xlate-Files() {
  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcstride=${srcstrides[srcidx]}
    src1file="${srcfmt} ${src_dim}"

    dstidx=0
    while [ "x${dstfmts[dstidx]}" != "x" ]
    do

      dstfmt=${dstfmts[dstidx]}
      dstfile=${outputpath}/undef-${srcfmt}-to-${dstfmt}-${tag}

      echo Converting from ${srcfmt} to ${dstfmt}.
      if [ "$vb" = "1" ]
      then
          echo ${bvt} -src1 ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -rop CCCC ${bvt_ret}
      fi
               ${bvt} -src1 ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -rop CCCC ${bvt_ret}

      # If conversion failed, print a warning. Also create a dummy file, so as
      # to immediately call attention to the error, to whatever person (or
      # script) is examining the output.
    # Also create a CRC for the file we just wrote.
    # Since this test doesn't make images, there is nothing to CRC.
    # All we have is a "pass" file, "fail" file, or a "segfault" file.
    # So create a fake CRC of 0 for pass and 0xFFFFFFFF for fail.
    retcode=$?
    if [ "$android" = "1" ]
    then
      retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
    fi
    # Check for android system error
    if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
    fi

    if [ "$retcode" = "139" ]
    then
        echo ERROR: Segfault during blit from ${srcfmt} to ${dstfmt} using ${scaletype}.
        ${touch} ${dstfile}-segfault.tga
        if [ "$android" = "1" ]
        then
          adb shell "echo 0xFFFFFFFF > ${dstfile}.crc"
        else
          echo 0xFFFFFFFF > ${dstfile}.crc
        fi
    else
        # Recall for these tests, we don't expect an output file, we just expect bvtest to not crash.
        if [ "$retcode" = "1" ]
        then
          ${touch} ${dstfile}-pass.tga
          if [ "$android" = "1" ]
          then
            adb shell "echo 0 > ${dstfile}.crc"
          else
            echo 0 > ${dstfile}.crc
          fi
        else
          echo "Test failed. Creating fail file and CRC"
          ${touch} ${dstfile}-fail.tga
          if [ "$android" = "1" ]
          then
            adb shell "echo 0 > ${dstfile}.crc"
          else
            echo 0 > ${dstfile}.crc
          fi
        fi
    fi

      if [ "$no_tga" = "1" ]
      then
        rm ${dstfile}*.tga
      fi

      dstidx=$(( $dstidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done
}


##############################################
# Now go through all the formats we support and do the translation
# These formats were hand picked based on what the bltsville software-engine
# can do.  Add or remove formats as required for your implementation.
#
# Source and destination color formats being tested (followed by stride info)

srcfmts=(    f_MONO8 f_LUT8  f_RGB16 f_RGB24 f_BGR24 f_xRGB24 f_0RGB24 f_xBGR24 f_0BGR24 f_RGBx24 f_RGB024 f_BGRx24 f_BGR024 f_ARGB24 f_ABGR24 f_RGBA24 f_BGRA24 f_nRGBA24 f_nBGRA24 f_nARGB24 f_nABGR24 f_YUYV f_UYVY f_YV12 f_IYUV f_NV12 )
srcstrides=( 320     320     640     960     960     1280     1280     1280     1280     1280     1280     1280     1280     1280     1280     1280     1280     1280      1280      1280      1280      640    640    320    320    320  )
dstfmts=(    f_MONO8 f_LUT8  f_RGB16 f_RGB24 f_BGR24 f_xRGB24 f_0RGB24 f_xBGR24 f_0BGR24 f_RGBx24 f_RGB024 f_BGRx24 f_BGR024 f_ARGB24 f_ABGR24 f_RGBA24 f_BGRA24 f_nRGBA24 f_nBGRA24 f_nARGB24 f_nABGR24 f_YUYV f_UYVY f_YV12 f_IYUV f_NV12 )


# do the translation
tag=full
dest_rc=${dst_rc}
src1_rc=${src_rc}
clip_rc=${dst_rc}
Xlate-Files

dstfmts=(    MONO8 LUT8  RGB16 RGB24 BGR24 xRGB24 0RGB24 xBGR24 0BGR24 RGBx24 RGB024 BGRx24 BGR024 ARGB24 ABGR24 RGBA24 BGRA24 nRGBA24 nBGRA24 nARGB24 nABGR24 YUYV UYVY YV12 IYUV NV12 )
Xlate-Files

##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi


