#!/bin/bash
#
# conform-blend.sh
# Bltsville conformance tests: blend with invalid format
#
# Tests executed:
#     Clear
#     Src1
#     Src2
#     Src1over
#     global alpha
#     remote alpha
#
# Currently this test uses the same format for src1, src2, and dst
#
# This test provides an invalid surface format for blend.  Any test using
# an invalid format should fail gracefully.  No tests should segfault.
#
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#
#




###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done

###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
targetdir=.
outputdir=undefined-blend-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
touch="touch "
echo="echo "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  quote=\"
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret="; echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect=\>
  push="adb push "
  pull="adb pull "
  touch="adb shell touch "
  echo="adb shell \" echo "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin
fi

# Create the directory to store the results in
${make_dir} ${targetdir}  
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath}  

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null


# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# mask file dimensions
mask_w=800
mask_h=600
mask_dim=${mask_w}x${mask_h}
maskpath=../../../images/
maskformat=ALPHA8
#maskpath=${inputpath}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
half_width=160
half_height=128
qtr_width=80
qtr_height=64

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}



#########################################################################
#
function Blend-Images() {
  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    # There is always a destination
    dstfmt=${dstfmts[dstidx]}

    # Do we need a src1?
    # If so, use same format at dest.
    src1file=
    if [ "${src1filecmd}" == "-src1" ]
    then
      src1fmt=${src1fmts[dstidx]}
      src1dim=${src_dim}
      src1stride=${dststrides[dstidx]}
      src1file="${src1fmt} ${src1dim} -src1rect ${src1_rc}"
    fi

    # Do we need a src2?
    # If so, use same format at dest.
    src2file=
    if [ "${src2filecmd}" == "-src2" ]
    then
      src2fmt=${src2fmts[dstidx]}
      src2dim=${src_dim}
      src2stride=${dststrides[dstidx]}
      src2file="${src2fmt} ${src2dim} -src2rect ${src2_rc}"
    fi

    # Do we need a mask?
    # If so, use ALPHA8.
    maskfile=
    if [ "${maskfilecmd}" == "-mask" ]
    then
      maskfmt=${maskformat}
      maskdim=${mask_dim}
      maskstride=${mask_w}
      maskfile="${maskfmt} ${maskdim} -maskrect ${mask_rc}"
    fi

    dstfile=${outputpath}/undefined-blend-${blendop}-${tag}-${dstfmt}-${src1fmt}-${src2fmt}-${maskfmt}-${dst_dim}


    echo Blending to ${dstfmt} using ${blendop}
    if [ "$vb" = "1" ]
    then
      echo ${bvt} ${blendcmd} ${blendop} ${alpha} ${src1filecmd} ${src1file}  ${src2filecmd} ${src2file} ${maskfilecmd} ${maskfile} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} ${clipcmd} ${clip_rc} -dstfile ${dstfile}.tga ${bvt_ret}
    fi
    ${bvt} ${blendcmd} ${blendop} ${alpha} ${src1filecmd} ${src1file}  ${src2filecmd} ${src2file} ${maskfilecmd} ${maskfile} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} ${clipcmd} ${clip_rc} -dstfile ${dstfile}.tga ${bvt_ret}

    # If blend failed, print a warning. Also create a dummy file, so as
    # to immediately call attention to the error, to whatever person (or
    # script) is examining the output.
    # Also create a CRC for the file we just wrote.
    # Since this test doesn't make images, there is nothing to CRC.
    # All we have is a "pass" file, "fail" file, or a "segfault" file.
    # So create a fake CRC of 0 for pass and 0xFFFFFFFF for fail.
    retcode=$?
    if [ "$android" = "1" ]
    then
      retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
    fi
    # Check for android system error
    if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
    fi

    if [ "$retcode" = "139" ]
    then
        echo ERROR: Segfault during blend from ${srcfmt} to ${dstfmt} using ${scaletype}.
        ${touch} ${dstfile}-segfault.tga
        if [ "$android" = "1" ]
        then
          adb shell "echo 0xFFFFFFFF > ${dstfile}.crc"
        else
          echo 0xFFFFFFFF > ${dstfile}.crc
        fi
    else
        echo test returned $retcode
        # Recall for these tests, we don't expect an output file, we just expect bvtest to not crash.
        if [ "$retcode" = "1" ]
        then
          ${touch} ${dstfile}-pass.tga
          if [ "$android" = "1" ]
          then
            adb shell "echo 0 > ${dstfile}.crc"
          else
            echo 0 > ${dstfile}.crc
          fi
        else
          if [ "$android" = "1" ]
          then
            ${crc} ${dstfile}.tga \>  ${dstfile}.crc
          else
            ${crc} ${dstfile}.tga > ${dstfile}.crc
          fi
        fi
    fi

    if [ "$no_tga" = "1" ]
    then
      rm ${dstfile}*.tga
    fi

    dstidx=$(( $dstidx + 1 ))
  done
}


###########################################################################
#
function Clip-Blend() {

    # No clipping.
    dest_rc=${dst_rc}
    src1_rc=${src_rc}
    src2_rc=${src_rc}
    mask_rc=0,210-${src_dim}
    clipcmd=
    clip_rc=
    Blend-Images
}


function Exec-Tests(){
  ############################################
  # Start with a simple "clear" test
  #
  blendcmd=-blendtype
  blendop=clear
  src1filecmd=
  src2filecmd=
  maskfilecmd=
  alpha=
  Clip-Blend

  ############################################
  # Src 1 test
  #
  blendcmd=-blendtype
  blendop=src1
  src1filecmd=-src1
  src2filecmd=
  maskfilecmd=
  alpha=
  Clip-Blend


  ############################################
  # Src 2 test
  #
  blendcmd=-blendtype
  blendop=src2
  src1filecmd=
  src2filecmd=-src2
  maskfilecmd=
  alpha=
  Clip-Blend


  ############################################
  # Src1Over with global alpha
  #
  blendcmd=-blendtype
  blendop=src1over+global
  src1filecmd=-src1
  src2filecmd=-src2
  maskfilecmd=
  alpha='-alpha 80'
  Clip-Blend

  ############################################
  # Src1Over with mask
  #
  blendcmd=-blendtype
  blendop=src1over+remote
  src1filecmd=-src1
  src2filecmd=-src2
  maskfilecmd=-mask
  alpha=
  Clip-Blend
}




# Try an invalid destination format
dstfmts=(     f_RGB16 f_RGB24 f_BGR24 f_RGBx24 f_RGB024 f_BGRx24 f_BGR024 f_RGBA24 f_BGRA24 )
src1fmts=(    RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
src2fmts=(    RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
dststrides=( 640   960   960   1280   1280   1280   1280   1280   1280   )
tag=invalid_dest
Exec-Tests

# Try an invalid src1 format
dstfmts=(     RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
src1fmts=(    f_RGB16 f_RGB24 f_BGR24 f_RGBx24 f_RGB024 f_BGRx24 f_BGR024 f_RGBA24 f_BGRA24 )
src2fmts=(    RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
dststrides=( 640   960   960   1280   1280   1280   1280   1280   1280   )
tag=invalid_src1
Exec-Tests

# Try an invalid src2 format
dstfmts=(     RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
src1fmts=(    RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
src2fmts=(    f_RGB16 f_RGB24 f_BGR24 f_RGBx24 f_RGB024 f_BGRx24 f_BGR024 f_RGBA24 f_BGRA24 )
dststrides=( 640   960   960   1280   1280   1280   1280   1280   1280   )
tag=invalid_src2
Exec-Tests

# Try an invalid mask format
maskformat=f_ALPHA8
dstfmts=(     RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
src1fmts=(    RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
src2fmts=(    RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
dststrides=( 640   960   960   1280   1280   1280   1280   1280   1280   )
tag=invalid_mask
Exec-Tests

##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi

