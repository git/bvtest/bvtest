#!/bin/bash
#
# conform-rot-scale.sh
# Bltsville conformance tests: Scaling + rotation
#
# Tests executed:
#    Same-format rot+scale
#    rot+scale with format convert.
# Additionally, there is an array of scaling types to try for each copy.
#
# Source and destination rectangles are varied in size to create scaled-up and
# scaled-down images.
#
# The number of potential combinations is huge, so many cases are commented
# out, to keep the total number of tests reasonable.  Feel change this to suit
# your needs.
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#
#



###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi


done

###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
targetdir=.
outputdir=rot-scale-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
quote=
push="cp "
make_dir="mkdir -p    "
pull="cp "
touch="touch "

###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  quote=\"
  push="adb push "
  pull="adb pull "
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin 2> /dev/null
  adb push ${and_crc} /system/bin 2> /dev/null

fi

${make_dir} ${targetdir}  
${make_dir} ${targetdir}/this_run  
${make_dir} ${outputpath}  

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null

###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256

scale_factor_index=0
scale_type_index=0

###############################################################################
# Retrieve the results
#
function Fetch-Results()
{
  if [ "$android" = "1" ]; then
    # Create a local directory to store the results
    mkdir -p    this_run
    mkdir -p    this_run/${outputdir}

    # Remove results from previous tests
    rm this_run/${outputdir}/*

    # Pull the results from the device to here.
    pushd this_run/${outputdir}
    ${pull} ${outputpath}
    popd

    # Delete the CRC files on the device since we don't need them anymore.
    if [ "$no_tga" = "1" ]; then
        adb shell rm ${outputpath}/*.crc
    fi
  fi
}

###############################################################################
# 'retcode' has already been set to $?
# However, if we are pushing commands to Android, this will be the return value
# of the "adb" command, not the command we executed.
#
function Check-Return()
{
    if [ "$android" = "1" ]; then
      retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
    fi

    # Check for android system error
    if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
    fi
}


###############################################################################
# Check if two rectangles intersect.
# Set retcode to 1 if they do, 0 if it they don't
# This is used to see if the clip rect and destination rect will overlap
# If they don't, there is no point in running the test.
#
function Rect-Intersect()
{
  local srcleft=$1
  local srctop=$2
  local srcwidth=$3
  local srcheight=$4
  local dstleft=$5
  local dsttop=$6
  local dstwidth=$7
  local dstheight=$8

  local srcright=$((${srcleft}+${srcwidth}))
  local dstright=$((${dstleft}+${dstwidth}))
  local srcbottom=$((${srctop}+${srcheight}))
  local dstbottom=$((${dsttop}+${dstheight}))

  retcode=1

  if [ ${srcleft} -ge ${dstright} ]; then
    retcode=0
    return 0
  fi

  if [ $srcright -le $dstleft ]; then
    retcode=0
    return 0
  fi

  if [ $srctop -ge $dstbottom ]; then
    retcode=0
    return 0
  fi

  if [ $srcbottom -le $dsttop ]; then
    retcode=0
    return 0
  fi

}

###############################################################################
# Check if a file exists.
# Set retcode to 1 if it does, 0 if it doesn't
#
function Test-For-File()
{
  local file=$1

  if [ "$android" = "1" ]; then
    adb shell "ls ${file}" | grep "No such" > /dev/null
    retcode=$?
  else
    if [ -e ${file} ]; then
      retcode=1
    else
      retcode=0
    fi
  fi

}



###############################################################################
# Create rotated source helper function
# Assumes there is already a file available with the requested format and stride
# Uses that file to create a rotated copy with the requested rotation.
#
function Create-Rotated-File() {
  local fmt=$1
  local stride=$2
  local dstrot=$3
  local command=
  local src1file=
  local dstfile=

  src1file=${quote}${inputpath}/input-${fmt}-${src_w}x${src_h}\(${stride}\).raw${quote}
  dstfile=${inputpath}/input-rot${dstrot}

  if [ "$vb" = "1" ]; then
     echo
  fi

  # If there is no source file in this format, then this driver doesn't
  # support this format.  Save time by bailing out now rather than trying
  # to run a test we know will fail.
  Test-For-File ${src1file}
  if [ "$retcode" = "0" ]; then
    if [ "$vb" = "1" ]; then
      echo "ERROR: Can not create rotated version of ${src1file} because it does not exist."
    fi
    return 0
  fi

  echo Creating rotated source file with format=${fmt} and rotation=${dstrot}

  command="${bvt} -src1file ${src1file} -src1rect 0,0-${src_w}x${src_h} -dst ${fmt} ${src_w}x${src_h} -dstangle ${dstrot} -dstrect 0,0-${src_w}x${src_h} -dstfile ${dstfile}.raw  -rop CCCC ${bvt_ret}"
  if [ "$vb" = "1" ]; then
    echo ${command}
  fi

  ${command}
  retcode=$? ; Check-Return

  if [ "$retcode" = "139" ]; then
    # segfault
    echo ERROR: Segfault while creating rotated version of ${src1file}
  elif [ "$retcode" = "255" ]; then
    # Device error
    echo ERROR: Device error while creating rotated version of ${src1file}
  elif [ "$retcode" -ne "0" ]; then
    # Driver returned an error
    echo ERROR: Failed to creating rotated version of ${src1file}
  fi

}


###############################################################################
# Function to create new rotated sources from each source image
#   fmts is the array of formats to use.
#   strides is a corrosponding array of the stride for each format
# This function assumes that for each requested format, there is already an
# unrotated input file.  We will use that to create rotated input files.
#
function Create-Rotated-Sources() {
  local fmts=(${!1})
  local strides=(${!2})
  local idx=0
  local dstrot=0
  local fmt=
  local stride=

  #echo First three formats are ${fmts[0]} ${fmts[1]} ${fmts[2]}
  #echo First three strides are ${strides[0]} ${strides[1]} ${strides[2]}
  if [ "$vb" = "1" ]; then
    echo "Creating rotated version of the input files."
  fi

  while [ "x${fmts[idx]}" != "x" ]
  do
    fmt=${fmts[idx]}
    stride=$((${src_w} * ${strides[idx]}))

    # We could use a loop for rotation values, too. But there are only 4 of them
    dstrot=0
    Create-Rotated-File ${fmt} ${stride} ${dstrot}
    dstrot=90
    Create-Rotated-File ${fmt} ${stride} ${dstrot}
    dstrot=180
    Create-Rotated-File ${fmt} ${stride} ${dstrot}
    dstrot=270
    Create-Rotated-File ${fmt} ${stride} ${dstrot}

    idx=$(( $idx + 1 ))
  done
}

###############################################################################
# Calculate the CRC for the file we just made.
#
function Calc-CRC()
{
  local dstfile=$1

  # If there was a seg fault, set the crc to 0xFFFFFFFF
  Test-For-File ${dstfile}-segfault.tga
  if [ "$retcode" = "0" ]; then
    if [ "$android" = "1" ]; then
      adb shell "echo 0xFFFFFFFF > ${dstfile}.crc"
    else
      echo 0xFFFFFFFF > ${dstfile}.crc
    fi
    return 0
  fi

  # Otherwise, calculate the CRC of the file.
  if [ "$android" = "1" ]; then
      ${crc} ${dstfile}.tga \>  ${dstfile}.crc
  else
      ${crc} ${dstfile}.tga > ${dstfile}.crc
  fi

}


##############################################
# Execute the blit
#
function Blit() {
  local src1filecmd=$1
  local dstfilecmd=$2
  local rectangles=$3
  local scalecmd=$4
  local dstfilename=$5

  echo ${dstfilename}.tga
  command="${bvt} ${src1filecmd} ${dstfilecmd} ${rectangles} ${scalecmd} -dstfile  ${outputpath}/${dstfilename}.tga -rop CCCC ${bvt_ret}"
  if [ "$vb" = "1" ]; then
     echo ${command}
  fi

  ${command}
  retcode=$? ; Check-Return

  if [ "$retcode" = "139" ]; then
    echo ERROR: Segmentation fault blitting to output file ${dstfilename}.tga
    ${touch} ${outputpath}/${dstfilename}-segfault.tga
  elif [ "$retcode" = "255" ]; then
    # Device error
    echo ERROR: Device error blitting to output file ${dstfilename}.tga
    ${touch} ${outputpath}/${dstfilename}-failed.tga
  elif [ "$retcode" -ne "0" ]; then
    # Driver returned an error
    echo ERROR: while blitting to output file ${dstfilename}.tga
    ${touch} ${outputpath}/${dstfilename}-failed.tga
  fi

  # Also create a CRC for the file we just wrote.
  Calc-CRC ${outputpath}/${dstfilename}

  if [ "$no_tga" = "1" ]; then
    ${remove} ${outputpath}/${dstfilename}*.tga
  fi

}


##############################################
# Set the type of scaling.
#
function SetScaleAndBlit() {
  local src1filecmd=$1
  local dstfilecmd=$2
  local rectangles=$3
  local dstfilename=$4

  # here are the types of scaling to try for each copy.
  local scaletypes=( nearest_neighbor 3tap 5tap 7tap 9tap  )
  local newdstfilename=

  scale_type_index=$(( $scale_type_index + 1 ))

  if [ $scale_type_index -eq 1 ]; then
    scaletype=nearest_neighbor
    scalecmd="-scale ${scaletype}"
    newdstfilename=${dstfilename}-${scaletype}
    Blit "${src1filecmd}" "${dstfilecmd}" "${rectangles}" "${scalecmd}" "${newdstfilename}"

  elif [ $scale_type_index -eq 2 ]; then
    scaletype=3tap
    scalecmd="-scale ${scaletype}"
    newdstfilename=${dstfilename}-${scaletype}
    Blit "${src1filecmd}" "${dstfilecmd}" "${rectangles}" "${scalecmd}" "${newdstfilename}"

  elif [ $scale_type_index -eq 3 ]; then
    scaletype=5tap
    scalecmd="-scale ${scaletype}"
    newdstfilename=${dstfilename}-${scaletype}
    Blit "${src1filecmd}" "${dstfilecmd}" "${rectangles}" "${scalecmd}" "${newdstfilename}"

  elif [ $scale_type_index -eq 4 ]; then
    scaletype=7tap
    scalecmd="-scale ${scaletype}"
    newdstfilename=${dstfilename}-${scaletype}
    Blit "${src1filecmd}" "${dstfilecmd}" "${rectangles}" "${scalecmd}" "${newdstfilename}"

  elif [ $scale_type_index -eq 5 ]; then
    scaletype=9tap
    scalecmd="-scale ${scaletype}"
    newdstfilename=${dstfilename}-${scaletype}
    Blit "${src1filecmd}" "${dstfilecmd}" "${rectangles}" "${scalecmd}" "${newdstfilename}"

  else
    scale_type_index=0
  fi



}



###############################################################################
# Set the clipping
#
function ClipAndBlit() {
  local src1filecmd=$1
  local dstfilecmd=$2
  local src1rect=$3
  local dstX=$4
  local dstY=$5
  local dstW=$6
  local dstH=$7
  local dstfilename=$8

  local rectangles="${src1rect} -dstrect ${dstX},${dstY}-${dstW}x${dstH}"
  local clipleft=0
  local cliptop=0
  local clipwidth=0
  local clipheight=0
  local tag=
  local cliprc=

  tag=clip-to-dest
  clipleft=0
  cliptop=0
  clipwidth=${src_w}
  clipheight=${src_h}
  Rect-Intersect $clipleft $cliptop $clipwidth $clipheight $dstX $dstY $dstW $dstH
  if [ $retcode -eq 1 ]; then
    cliprc="-cliprect ${clipleft},${cliptop}-${clipwidth}x${clipheight}"
    SetScaleAndBlit "${src1filecmd}" "${dstfilecmd}" "${rectangles} ${cliprc}" "${dstfilename}-${tag}"
  else
    if [ "$vb" = "1" ]; then
       echo Discarding because clip ${clipleft},${cliptop}-${clipwidth}x${clipheight} and dest ${dstX},${dstY}-${dstW}x${dstH} do not intersect intersect.
    fi
  fi

  tag=clip-to-center
  clipleft=$(($src_w/4))
  cliptop=$(($src_h/4))
  clipwidth=$(($src_w/2))
  clipheight=$(($src_h/2))
  Rect-Intersect $clipleft $cliptop $clipwidth $clipheight $dstX $dstY $dstW $dstH
  if [ $retcode -eq 1 ]; then
    cliprc="-cliprect ${clipleft},${cliptop}-${clipwidth}x${clipheight}"
    SetScaleAndBlit "${src1filecmd}" "${dstfilecmd}" "${rectangles} ${cliprc}" "${dstfilename}-${tag}"
  else
    if [ "$vb" = "1" ]; then
       echo Discarding because clip ${clipleft},${cliptop}-${clipwidth}x${clipheight} and dest ${dstX},${dstY}-${dstW}x${dstH} do not intersect intersect.
    fi
  fi

# Skip this for now
return 0

  tag=clip-to-lr
  clipleft=$((${src_w}/2))
  cliptop=$((${src_h}/2))
  clipwidth=$((${src_w}-${clipleft}))
  clipheight=$((${src_h}-${clipheight}))
  Rect-Intersect $clipleft $cliptop $clipwidth $clipheight $dstX $dstY $dstW $dstH
  if [ $retcode -eq 1 ]; then
    cliprc="-cliprect ${clipleft},${cliptop}-${clipwidth}x${clipheight}"
    SetScaleAndBlit "${src1filecmd}" "${dstfilecmd}" "${rectangles} ${cliprc}" "${dstfilename}-${tag}"
  else
    if [ "$vb" = "1" ]; then
       echo Discarding because clip ${clipleft},${cliptop}-${clipwidth}x${clipheight} and dest ${dstX},${dstY}-${dstW}x${dstH} do not intersect intersect.
    fi
  fi

}


###############################################################################
# Set destination width and height. This sets the stretch factor
#
function SetDstHWandBlit() {
  local src1filecmd=$1
  local dstfilecmd=$2
  local srcX=$3
  local srcY=$4
  local dstX=$5
  local dstY=$6
  local srcW=$7
  local srcH=$8
  local dstfilename=$9

  local tag=
  local dstW=
  local dstH=
  local temp=
  local src1rect="-src1rect ${srcX},${srcY}-${srcW}x${srcH}"

  scale_factor_index=$(($scale_factor_index + 1))

  if [ $scale_factor_index -eq 1 ]; then
    tag=down-4x
    dstW=$(($srcW/4))
    dstH=$(($srcH/4))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 2 ]; then
    tag=down-half
    dstW=$(($srcW/2))
    dstH=$(($srcH/2))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 3 ]; then
    tag=down-10
    dstW=$(($srcW-10))
    dstH=$(($srcH-10))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 4 ]; then
    tag=up-10
    dstW=$(($srcW+10))
    dstH=$(($srcH+10))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 5 ]; then
    tag=up-150
    temp=$(($srcW*3))
    dstW=$(($temp/2))
    temp=$(($srcH*3))
    dstH=$(($temp/2))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 6 ]; then
    tag=up-double
    dstW=$(($srcW*2))
    dstH=$(($srcH*2))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 7 ]; then
    tag=vert-up-2x
    dstW=$srcW
    dstH=$(($srcH*2))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 8 ]; then
    tag=vert-shrink-half
    dstW=$srcW
    dstH=$(($srcH/2))
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 9 ]; then
    tag=horz-up-2x
    dstW=$(($srcW*2))
    dstH=$srcH
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  elif [ $scale_factor_index -eq 10 ]; then
    tag=horz-shrink-half
    dstW=$(($srcW/2))
    dstH=$srcH
    ClipAndBlit "${src1filecmd}" "${dstfilecmd}" "${src1rect}" ${dstX} ${dstY}  ${dstW} ${dstH} "${dstfilename}-${tag}"

  else
    scale_factor_index=0
  fi

}

###############################################################################
# Set dst X and Y 
#
function SetDstXYandBlit() {
  local src1filecmd=$1
  local dstfilecmd=$2
  local srcX=$3
  local srcY=$4
  local srcW=$5
  local srcH=$6
  local dstfilename=$7

  local dstleft=0
  local dsttop=0
  local tag=A
  local dstxy="${dstleft},${dsttop}"
  SetDstHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcX} ${srcY} ${dstleft} ${dsttop} ${srcW} ${srcH} "${dstfilename}${tag}"

# Skip this for now
return 0

  dstleft=$(($src_w/2))
  dsttop=0
  tag=B
  dstxy="${dstleft},${dsttop}"
  SetDstHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcX} ${srcY} ${dstleft} ${dsttop} ${srcW} ${srcH} "${dstfilename}${tag}"

  dstleft=0
  dsttop=$(($src_h/2))
  tag=C
  dstxy="${dstleft},${dsttop}"
  SetDstHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcX} ${srcY} ${dstleft} ${dsttop} ${srcW} ${srcH} "${dstfilename}${tag}"

  dstleft=$(($src_w/2))
  dsttop=$(($src_h/2))
  tag=D
  dstxy="${dstleft},${dsttop}"
  SetDstHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcX} ${srcY} ${dstleft} ${dsttop} ${srcW} ${srcH} "${dstfilename}${tag}"

}

###############################################################################
# Set src width and height
#
function SetSrcHWandBlit() {
  local src1filecmd=$1
  local dstfilecmd=$2
  local srcX=$3
  local srcY=$4
  local dstfilename=$5

  local srcW=${src_w}
  local srcH=${src_h}
  local tag=A
  SetDstXYandBlit "${src1filecmd}" "${dstfilecmd}" ${srcX} ${srcY} ${srcW} ${srcH} "${dstfilename}${tag}"

  local srcW=$((${src_w}/2))
  local srcH=$((${src_h}/2))
  local tag=B
  SetDstXYandBlit "${src1filecmd}" "${dstfilecmd}" ${srcX} ${srcY} ${srcW} ${srcH} "${dstfilename}${tag}"

  local srcW=$((${src_w}/3))
  local srcH=$((${src_h}/3))
  local tag=C
#  SetDstXYandBlit "${src1filecmd}" "${dstfilecmd}" ${srcX} ${srcY} ${srcW} ${srcH} "${dstfilename}${tag}"

}


###############################################################################
# Set the source XY
#
function SetSrcXYandBlit() {
  local src1filecmd=$1
  local dstfilecmd=$2
  local dstfilename=$3

  local srctop=
  local srcleft=
  local tag=

  srcleft=0
  srctop=0
  tag=A
  SetSrcHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcleft} ${srctop} "${dstfilename}-${tag}"

# Skip these for now
return 0

  srcleft=$(($src_w/2))
  srctop=0
  tag=B
  SetSrcHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcleft} ${srctop} "${dstfilename}-${tag}"

  srcleft=0
  srctop=$(($src_h/2))
  tag=C
  SetSrcHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcleft} ${srctop} "${dstfilename}-${tag}"

  srcleft=$(($src_w/2))
  srctop=$(($src_h/2))
  tag=C
  SetSrcHWandBlit "${src1filecmd}" "${dstfilecmd}" ${srcleft} ${srctop} "${dstfilename}-${tag}"

}



###############################################################################
# Set the destination rotation and pass it on.
#
function SetDstRotAndBlit() {
  local src1filecmd=$1
  local dstfmt=$2
  local dstfilename=$3

  local dstrots=( 0 90 180 270 )
  local dstrot=
  local dstfilecmd=
  local destfilename=
  local idx=0

  while [ "x${dstrots[idx]}" != "x" ]
  do
    dstrot=${dstrots[idx]}
    dstfilecmd="-dst ${dstfmt} ${src_w}x${src_h} -dstangle ${dstrot}"
    destfilename=${dstfilename}-to-${dstfmt}-${dstrot}
    SetSrcXYandBlit "${src1filecmd}" "${dstfilecmd}" "${destfilename}"

    idx=$(($idx + 1))
  done

}


###############################################################################
# Cycle through all source rotations.
#
function SetSrcRotAndBlit() {
  local srcfmt=$1
  local srcstride=$2     # 1, 2, 3, or 4
  local dstfmt=$3

  local srcrots=( 0 90 180 270 )
  local idx=0
  local srcrot=
  local src1filename=
  local src1filecmd=
  local stride=    # Actual stride. 320, 640, 1280, etc
  local srcdim=
  local dstfilename=

  if [ "$vb" = "1" ]; then
    echo Converting all source rotations for source format $srcfmt
  fi

  while [ "x${srcrots[idx]}" != "x" ]
  do
    srcrot=${srcrots[idx]}

    # files stored in rotated format may have a different stride.
    if [ "$srcrot" = "90" ] ; then
        stride=$((${src_h} * ${srcstride}))
        srcdim=${src_h}x${src_w}
    elif [ "$srcrot" = "270" ] ; then
        stride=$((${src_h} * ${srcstride}))
        srcdim=${src_h}x${src_w}
    else
        stride=$((${src_w} * ${srcstride}))
        srcdim=${src_w}x${src_h}
    fi

    src1filename=${quote}${inputpath}/input-rot${srcrot}-${srcfmt}-${srcdim}\(${stride}\).raw${quote}
    src1filecmd="-src1file ${src1filename} -src1angle ${srcrot}"
    dstfilename=rotscalexlate-${srcfmt}-${srcrot}

    # If this source file doesn't exist,then the driver can't rotate it.
    # Bail now and save some time, instead of running a bunch of tests that
    # we know will fail.
    Test-For-File ${src1filename}
    if [ "$retcode" = "0" ]; then
      if [ "$vb" = "1" ]; then
        echo "ERROR: ${src1filename} doesn't exist. Skipping this test."
      fi
    else
      SetDstRotAndBlit "${src1filecmd}" ${dstfmt} ${dstfilename}
    fi

    idx=$(($idx + 1))
  done

}

###############################################################################
# Cycle through the source formats, and launch the tests for each one.
#
function RotAndScaleTests()
{
  local srcfmts=(${!1})
  local srcstrides=(${!2})
  local destfmt=$3
  local idx=0
  local dstfmt=

  echo First three source formats are ${srcfmts[0]} ${srcfmts[1]} ${srcfmts[2]}
  echo First three source strides are ${srcstrides[0]} ${srcstrides[1]} ${srcstrides[2]}
  echo destfmt is ${destfmt}

  if [ "$vb" = "1" ]; then
    echo Converting all source formats to destination format \"${destfmt}\"
  fi


  while [ "x${srcfmts[idx]}" != "x" ]
  do
    srcfmt=${srcfmts[idx]}
    srcstride=${srcstrides[idx]}

    if [ "${destfmt}" == "same" ]; then
      dstfmt=${srcfmt}
    else
      dstfmt=${destfmt}
    fi

    if [ "$vb" = "1" ]; then
      echo Converting $srcfmt with stride $srcstride to $dstfmt
    fi
    SetSrcRotAndBlit $srcfmt $srcstride $dstfmt

    idx=$(( $idx + 1 ))
  done
}


###############################################################################
#  Cycle through the destination formats, and launch the tests for each one.
#
function RotAndScaleAndXlateTests()
{
  local srcfmts=(${!1})
  local srcstrides=(${!2})
  local dstfmts=(${!3})
  local idx=0
  local dstfmt=

  #echo First three source formats are ${srcfmts[0]} ${srcfmts[1]} ${srcfmts[2]}
  #echo First three source strides are ${srcstrides[0]} ${srcstrides[1]} ${srcstrides[2]}
  #echo First three dest formats are ${dstfmts[0]} ${dstfmts[1]} ${dstfmts[2]}

  while [ "x${dstfmts[idx]}" != "x" ]
  do
    dstfmt=${dstfmts[idx]}

    echo "Calling RotAndScaleTests"
    RotAndScaleTests srcfmts[@] srcstrides[@] $dstfmt

    idx=$(( $idx + 1 ))
  done
}


###############################################################################
#
# Setup test parameters
#

# Define formats to be tested for same-format rot+scale
fmts=(    BGRA24 BGR124 RGB16 RGBA24 RGB124  NV12  YV12 )
strides=(      4      4     2      4      4     1     1 )

# Now create rotated versions of all the source files we will need.
Create-Rotated-Sources fmts[@] strides[@]

# Run rotate+scale tests without format conversion
#    Skip this. We are only interested in format conversion for now.
# RotAndScaleTests fmts[@] strides[@] same


# Run rotate+scale tests with format conversion
srcfmts=(    BGRA24 BGR124 RGB16 RGBA24 RGB124  NV12  YV12 )
srcstrides=(      4      4     2      4      4     1     1 )
dstfmts=( BGRA24 BGR124 RGB16 RGBA24 RGB124)
RotAndScaleAndXlateTests srcfmts[@] srcstrides[@] dstfmts[@]

Fetch-Results


# For Reference
#
#fmts=(    MONO8 LUT8 RGB12 RGB15 RGB16 RGB24 BGR24 xRGB24 0RGB24 xBGR24 0BGR24 RGBx24 RGB024 BGRx24 BGR024 ARGB24 ABGR24 RGBA24 BGRA24 nRGBA24 nBGRA24 nARGB24 nABGR24 YUYV UYVY YV12 IYUV NV12 )
#strides=( 1     1    2     2     2     3     3     3      4      4      4      4      4      4      4      4      4      4      4      4       4       4       4       2    2    1    1    1    )




