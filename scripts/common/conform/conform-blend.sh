#!/bin/bash
#
# conform-blend.sh
# Bltsville conformance tests: blend
#
# Tests executed:
#     Clear
#     Src1
#     Src2
#     Src1over
#     global alpha
#     remote alpha
#
# Currently this test uses the same format for src1, src2, and dst
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#
#


###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done



###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
outputdir=blend-results
targetdir=.
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
maskpath=../../../images/
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
here=.
touch="touch "

###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect='\>'
  push="adb push "
  pull="adb pull "
  here=
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin

  # Push the mask images to the device
  adb push ${maskpath} ${inputpath}
  maskpath=${inputpath}

fi

${make_dir} ${targetdir} 
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null

###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# mask file dimensions
mask_w=800
mask_h=600
mask_dim=${mask_w}x${mask_h}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
half_width=160
half_height=128
qtr_width=80
qtr_height=64

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}

vb=0
verbose=
no_tga=0


#########################################################################
# Parse the command line
for arg in "$@"
do
  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi
  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi
done




#########################################################################
#
function Blend-Images() {
  dstidx=0
  while [ "x${dstfmts[dstidx]}" != "x" ]
  do
    # There is always a destination
    dstfmt=${dstfmts[dstidx]}
    dstfile=${outputpath}/blend-${blendop}-${tag}-${dstfmt}-${dst_dim}

    # Do we need a src1?
    # If so, use same format at dest.
    src1file=
    if [ "${src1filecmd}" == "-src1file" ]
    then
      src1fmt=${dstfmt}
      src1dim=${src_dim}
      src1stride=${dststrides[dstidx]}
      src1filename=${quote}${inputpath}/input-${src1fmt}-${src1dim}\(${src1stride}\).raw${quote}
      src1file="${src1filename} -src1rect ${src1_rc}"
    fi

    # Do we need a src2?
    # If so, use same format at dest.
    src2file=
    if [ "${src2filecmd}" == "-src2file" ]
    then
      src2fmt=${dstfmt}
      src2dim=${src_dim}
      src2stride=${dststrides[dstidx]}
      src2filename=${quote}${inputpath}/input2-${src2fmt}-${src2dim}\(${src2stride}\).raw${quote}
      src2file="${src2filename} -src2rect ${src2_rc}"
    fi

    # Do we need a mask?
    # If so, use ALPHA8.
    maskfile=
    if [ "${maskfilecmd}" == "-maskfile" ]
    then
      maskfmt=ALPHA8
      maskdim=${mask_dim}
      maskstride=${mask_w}
      maskfilename=${quote}${maskpath}/input-${maskfmt}-${maskdim}\(${maskstride}\).raw${quote}
      maskfile="${maskfilename} -maskrect ${mask_rc}"
    fi

    # remove previous test results if they exist
    if [ -f "${dstfile}.tga" ]
    then
       ${remove} ${dstfile}.tga
    fi
    if [ -f "${dstfile}-unsupported.tga" ]
    then
      ${remove} ${dstfile}-unsupported.tga
    fi
    if [ -f "${dstfile}-segfault.tga" ]
    then
       ${remove} ${dstfile}-segfault.tga
    fi
    if [ -f "${dstfile}.crc" ]
    then
       ${remove} ${dstfile}.crc
    fi

    echo Blending to ${dstfmt} using ${blendop}
    if [ "$vb" = "1" ]
    then
      echo ${bvt} ${blendcmd} ${blendop} ${alpha} ${src1filecmd} ${src1file}  ${src2filecmd} ${src2file} ${maskfilecmd} ${maskfile} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} ${clipcmd} ${clip_rc} -dstfile ${dstfile}.tga ${bvt_ret}
    fi
    ${bvt} ${blendcmd} ${blendop} ${alpha} ${src1filecmd} ${src1file}  ${src2filecmd} ${src2file} ${maskfilecmd} ${maskfile} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} ${clipcmd} ${clip_rc} -dstfile ${dstfile}.tga ${bvt_ret}

    # If blend failed, print a warning. Also create a dummy file, so as
    # to immediately call attention to the error, to whatever person (or
    # script) is examining the output.
    retcode=$?
    if [ "$android" = "1" ]; then
      retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
    fi
    # Check for android system error
    if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
    fi

    if [ "$retcode" = "139" ]
    then
          echo ERROR: Segfault during blend to ${dstfmt} using ${blendop}.
          ${touch} ${dstfile}-segfault.tga
    else
      if [ "$retcode" -ne "0" ]; then
          echo ERROR: Cannot blend to ${dstfmt} using ${blendop}.
          ${touch} ${dstfile}-unsupported.tga
      fi
    fi

    # Also create a CRC for the file we just wrote.
    if [ "$android" = "1" ]
    then
        ${crc} ${dstfile}.tga \>  ${dstfile}.crc
    else
        ${crc} ${dstfile}.tga > ${dstfile}.crc
    fi

    if [ "$no_tga" = "1" ]
    then
      ${remove} ${dstfile}*.tga
    fi

    dstidx=$(( $dstidx + 1 ))
  done
}


###########################################################################
#
function Clip-Blend() {

    # No clipping.
    tag=no_clip
    dest_rc=${dst_rc}
    src1_rc=${src_rc}
    src2_rc=${src_rc}
    mask_rc=0,210-${src_dim}
    clipcmd=
    clip_rc=
    Blend-Images

    # Clip a destination rectangle that is larger than the dest surface down to the
    # dest surf boundaries.
    tag=clip_to_full
    clipcmd=-cliprect
    clip_rc=${dst_rc}
    dest_rc=${oversize_rc}
    src1_rc=${oversize_rc}
    src2_rc=${oversize_rc}
    mask_rc=0,210-${double_width}x${double_height}
    Blend-Images

    # Clip a destination rectangle that is in the center of the destination surface
    tag=clip_to_center
    clipcmd=-cliprect
    clip_rc=${middle_rc}
    Blend-Images

    # Clip to a destination rectangle that is in the upper left
    tag=clip_to_ul
    clip_rc=${ul_rc}
    Blend-Images

    # Clip to a destination rectangle that is in the upper right
    tag=clip_to_ur
    clip_rc=${ur_rc}
    Blend-Images

    # Clip to a destination rectangle that is in the lower left
    tag=clip_to_ll
    clip_rc=${ll_rc}
    Blend-Images

    # Clip to a destination rectangle that is in the lower right
    tag=clip_to_lr
    clip_rc=${lr_rc}
    Blend-Images

    # Clip to outside dest.
    tag=clip_outside_dest
    clip_rc=${outside_rc}
    Blend-Images

    # Clip away entirely
    tag=clip_all
    dest_rc=${ul_rc}
    src1_rc=${ul_rc}
    src2_rc=${ul_rc}
    mask_rc=0,210-${half_width}x${half_height}
    clip_rc=${lr_rc}
    Blend-Images

}


dstfmts=(    RGB16 RGB24 BGR24 RGBx24 RGB024 BGRx24 BGR024 RGBA24 BGRA24 )
dststrides=( 640   960   960   1280   1280   1280   1280   1280   1280   )


############################################
# Start with a simple "clear" test
#
blendcmd=-blendtype
blendop=clear
src1filecmd=
src2filecmd=
maskfilecmd=
alpha=
Clip-Blend

############################################
# Src 1 test
#
blendcmd=-blendtype
blendop=src1
src1filecmd=-src1file
src2filecmd=
maskfilecmd=
alpha=
Clip-Blend


############################################
# Src 2 test
#
blendcmd=-blendtype
blendop=src2
src1filecmd=
src2filecmd=-src2file
maskfilecmd=
alpha=
Clip-Blend


############################################
# Src1Over with global alpha
#
blendcmd=-blendtype
blendop=src1over+global
src1filecmd=-src1file
src2filecmd=-src2file
maskfilecmd=
alpha='-alpha 80'
Clip-Blend

############################################
# Src1Over with mask
#
blendcmd=-blendtype
blendop=src1over+remote
src1filecmd=-src1file
src2filecmd=-src2file
maskfilecmd=-maskfile
alpha=
Clip-Blend




# for reference...
#srcfmts=(    MONO8 LUT8 RGB12 RGB15 RGB16 RGB24 BGR24 xRGB24 0RGB24 xBGR24 0BGR24 RGBx24 RGB024 BGRx24 BGR024 ARGB24 ABGR24 RGBA24 BGRA24 nRGBA24 nBGRA24 nARGB24 nABGR24 YUYV UYVY YV12 IYUV NV12 )
#srcstrides=( 320   320  640   640   640   960   960   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280    1280    1280    1280    640  640  320  320  320  )


##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi
