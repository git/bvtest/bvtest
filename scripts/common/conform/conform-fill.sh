#!/bin/bash
#
# conform-fill.sh
# Bltsville conformance tests: filling
#
# Tests executed:
#   Fill entire destination with color
#   Fill rectangle
#   Fill clipped rectangle
#   Fill rectangle in same surface
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#
#


###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done



###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
targetdir=.
outputdir=fill-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
here=.
touch="touch "

###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect='\>'
  push="adb push "
  pull="adb pull "
  here=
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin

fi

echo creating directory ${targetdir}
${make_dir} ${targetdir}
echo creating directory ${targetdir}/this_run
${make_dir} ${targetdir}/this_run
echo creating directory ${outputpath}
${make_dir} ${outputpath}

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null

###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

dst_w=${src_w}
dst_h=${src_h}
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
thrqtr_width=240
thrqtr_height=192
half_width=160
half_height=128
qtr_width=80
qtr_height=64
thrqtr_dim=${thrqtr_width}x${thrqtr_height}

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}

# source rectangles to extract the desired color from the source image.
red='10,190-1x1'
green='10,100-1x1'
blue='10,220-1x1'
yellow='10,30-1x1'
cyan='10,60-1x1'
magenta='10,150-1x1'

# Default destination rotation is 0
dstrot=0

##############################################
# Fill the destination
# fmts is the array of formats to use.
# strides is a corrosponding array of how big the pixel size is for each format
# color is either "red", "green" or "blue"
#
function Fill-Image() {
  srcidx=0
  while [ "x${fmts[srcidx]}" != "x" ]
  do
    srcfmt=${fmts[srcidx]}
    srcstride=${strides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}
    dstfmt=${srcfmt}

    dstfile=${outputpath}/fill-${color}-${tag}-${dstfmt}-${dst_dim}

    # remove previous test results if they exist
    if [ -f "${dstfile}.tga" ]
    then
       ${remove} ${dstfile}.tga
    fi
    if [ -f "${dstfile}-unsupported.tga" ]
    then
      ${remove} ${dstfile}-unsupported.tga
    fi
    if [ -f "${dstfile}-segfault.tga" ]
    then
       ${remove} ${dstfile}-segfault.tga
    fi
    if [ -f "${dstfile}.crc" ]
    then
       ${remove} ${dstfile}.crc
    fi

    echo Filling ${dstfmt}-${dst_dim} using ${color}.
    if [ "$vb" = "1" ]
    then
        echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -dstangle ${dstrot} -cliprect ${clip_rc} -dstfile ${dstfile}.tga  -rop CCCC ${bvt_ret}
    fi
    ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -dstangle ${dstrot} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -rop CCCC ${bvt_ret}

    # If fill failed, print a warning. Also create a dummy file, so as
    # to immediately call attention to the error, to whatever person (or
    # script) is examining the output.
    retcode=$?
    if [ "$android" = "1" ]
    then
      retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
    fi
    # Check for android system error
    if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
    fi

    if [ "$retcode" = "139" ]
    then
      echo ERROR: Segfault in fill to ${dstfmt}
      ${touch} ${dstfile}-segfault.tga
    else
      if [ "$retcode" -ne "0" ]
      then
        echo ERROR: Cannot fill to ${dstfmt}
        ${touch} ${dstfile}-unsupported.tga
      fi
    fi

    # Also create a CRC for the file we just wrote.
    if [ "$android" = "1" ]
    then
        ${crc} ${dstfile}.tga \>  ${dstfile}.crc
    else
        ${crc} ${dstfile}.tga > ${dstfile}.crc
    fi

    if [ "$no_tga" = "1" ]
    then
      ${remove} ${dstfile}*.tga
    fi

    srcidx=$(( $srcidx + 1 ))
  done
}

function Fill-All-Colors() {
color=red
src1_rc=${red}
Fill-Image

color=green
src1_rc=${green}
Fill-Image

color=blue
src1_rc=${blue}
Fill-Image

color=yellow
src1_rc=${yellow}
Fill-Image

color=cyan
src1_rc=${cyan}
Fill-Image

color=magenta
src1_rc=${magenta}
Fill-Image

}



##############################################
# Do a fill each of these formats
fmts=(     LUT8  RGB16 RGB24 BGR24 xRGB24 0RGB24 xBGR24 0BGR24 RGBx24 RGB024 BGRx24 BGR024 ARGB24 ABGR24 RGBA24 BGRA24 nRGBA24 nBGRA24 nARGB24 nABGR24 )
strides=(  320   640   960   960   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280    1280    1280    1280    )

##############################################
# Fill the entire surface.
tag=full-surface
dest_rc=${dst_rc}
clip_rc=${dst_rc}
Fill-All-Colors

##############################################
# Only fill the center
tag=center
dest_rc=${middle_rc}
clip_rc=${dst_rc}
Fill-All-Colors

##############################################
# Extend dest rect beyond the boundaries of the dest surface
# set clip rect to boundaries of the dest surface.
tag=clip-to-dest
dest_rc=${oversize_rc}
clip_rc=${dst_rc}
Fill-All-Colors

##############################################
# Extend dest rect beyond lower right edges of destination surface
# extend clip rect to the upper left edges of destination surface
# intersection will be a rectangle in the center of the image
tag=clip-out-of-bounds
dest_rc=${qtr_width},${qtr_height}-${dst_dim}
clip_rc=0,0-${thrqtr_dim}
Fill-All-Colors

##############################################
# Clip it away entirely.
# Dest and clip do not intersect.
tag=clip-all
dest_rc=${ul_rc}
clip_rc=${lr_rc}
Fill-All-Colors

##############################################
# Fill center of same surface
tag=same-surf
dest_rc=${middle_rc}
clip_rc=${dst_rc}
dst_dim=src1
Fill-All-Colors


##############################################
# Fill rotated surfaces
dest_rc=${half_width},${qtr_height}-${half_width}x${half_height}
clip_rc=${dst_rc}
dst_dim=${dst_w}x${dst_h}
tag=rot90
dstrot=90
Fill-All-Colors

tag=rot180
dstrot=180
Fill-All-Colors

tag=rot270
dstrot=270
Fill-All-Colors


##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi
