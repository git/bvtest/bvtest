#!/bin/bash
#
# conform-dither.sh
# Bltsville conformance tests: Dithering
#
# This test attempts to do dithered source copies between pixel
# formats
#
# Two arrays of pixel format names are suppled: the "from" array and the
# "to" array.
#
# Also, an array of dither types is supplied.
#
# Output images are named "dithered-<FMT>-to-<FMT>-<type>.tga". The contents of
# all the output images should look the same.
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#



###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done



###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
targetdir=.
outputdir=dither-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
touch="touch "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect='\>'
  push="adb push "
  pull="adb pull "
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin
fi

# Create the directory to store the results in
${make_dir} ${targetdir}  
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null


###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
half_width=160
half_height=128
qtr_width=80
qtr_height=64

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}

##############################################
# Function to convert from source to destination pixel formats
# srcfmt is the array of source formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
# dstfmt is the array of destination formats to create.
#
function Dither-Files() {
  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcstride=${srcstrides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}

    dstidx=0
    while [ "x${dstfmts[dstidx]}" != "x" ]
    do
      dstfmt=${dstfmts[dstidx]}

      ditheridx=0
      while [ "x${dithertypes[ditheridx]}" != "x" ]
      do
        dithertype=${dithertypes[ditheridx]}

        dstfile=${outputpath}/dither-${srcfmt}-to-${dstfmt}-${dithertype}-${tag}

        echo Dithering from ${srcfmt} to ${dstfmt} using ${dithertype}
        if [ "$vb" = "1" ]
        then
            echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc}  -cliprect ${clip_rc} -dstfile ${dstfile}.tga -dither ${dithertype} -rop CCCC ${bvt_ret}
        fi
        ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc}  -cliprect ${clip_rc} -dstfile ${dstfile}.tga  -dither ${dithertype} -rop CCCC ${bvt_ret}

        # If dither failed, print a warning. Also create a dummy file, so as
        # to immediately call attention to the error, to whatever person (or
        # script) is examining the output.
        retcode=$?
        if [ "$android" = "1" ]
        then
          retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
        fi
        # Check for android system error
        if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
        fi

        if [ "$retcode" = "139" ]
        then
          echo ERROR: Segfault in dither from ${srcfmt} to ${dstfmt}
          ${touch} ${dstfile}-segfault.tga
        else
          if [ "$retcode" -ne "0" ]
          then
            echo ERROR: Cannot dither from ${srcfmt} to ${dstfmt}
            ${touch} ${dstfile}-unsupported.tga
          fi
        fi

        # Also create a CRC for the file we just wrote.
        if [ "$android" = "1" ]
        then
            ${crc} ${dstfile}.tga \>  ${dstfile}.crc
        else
            ${crc} ${dstfile}.tga > ${dstfile}.crc
        fi

        if [ "$no_tga" = "1" ]
        then
          ${remove} ${dstfile}*.tga
        fi

        ditheridx=$(( $ditheridx + 1 ))
      done

      dstidx=$(( $dstidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done
}


##############################################
# Now go through all the formats we support and do the dither
# These formats were hand picked based on what the bltsville software-engine
# can do.  Add or remove formats as required for your implementation.
#
# Source and destination color formats being tested (followed by stride info)

# libbltsville_cpu.so only supports dithering for a very few formats.
# Add additional formats here if your implementation does more.
# Don't forget to add stride infomation as well.

srcfmts=(     BGR024 BGRx24 RGB024 RGBx24 )
srcstrides=(  1280   1280   1280   1280   )

dstfmts=(  RGB16 )

#Types of dithering to try.
dithertypes=( none  fastest  best )

# Dither
tag=full
dest_rc=${dst_rc}
src1_rc=${src_rc}
clip_rc=${dst_rc}
Dither-Files

#######################################
# Test clipping with dither
#######################################

# Set up a destination rectangle that exceeds the size of the surface
dest_rc=${oversize_rc}
src1_rc=${oversize_rc}

# set up a clip rectangle that clips that to the surface boundaries.
tag=clip-to-dest
clip_rc=${dst_rc}
Dither-Files

# Clip to the upper left corner
tag=clip-to-ul
clip_rc=${ul_rc}
Dither-Files

# Clip to the upper right corner
tag=clip-to-ur
clip_rc=${ur_rc}
Dither-Files

# Clip to the lower left corner
tag=clip-to-ll
clip_rc=${ll_rc}
Dither-Files

# Clip to the lower right corner
tag=clip-to-lr
clip_rc=${lr_rc}
Dither-Files

# Clip to the center
tag=clip-to-center
clip_rc=${middle_rc}
Dither-Files


# Now run through the full list of supported dither types, with just a singel
# basic clipping test for each
tag=full
dest_rc=${dst_rc}
src1_rc=${src_rc}
clip_rc=${dst_rc}
#Types of dithering to try.
dithertypes=( fastest fastest_on fastest_random fastest_ordered fastest_diffused fastest_photo fastest_drawing )
Dither-Files
dithertypes=( good    good_on    good_random    good_ordered    good_diffused    good_photo    good_drawing )
Dither-Files
dithertypes=( better  better_on  better_random  better_ordered  better_diffused  better_photo  better_drawing )
Dither-Files
dithertypes=( best    best_on    best_random    best_ordered    best_diffused    best_photo    best_drawing )
Dither-Files
dithertypes=( none ordered2x2 ordered_4x4 ordered_2x2_4x4 )
Dither-Files


##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi
