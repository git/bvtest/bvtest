#!/bin/bash
#
# conform-callback.sh
# Bltsville conformance tests:
#     Callback test
#     Map test
#     Batch test
#
# This test verifies the bltsville callback mechanism is working.
# Two calls are made to bltsville, one we know should be successful, one we
# know should fail.  bv_test will verify that thec callback mechanism is
# activated for the successful blit, and not activated for the failed blit.
#
# This test verifies the bv_map and bv_unmap functions
# a single blit is performed (one which we expect to pass). The destination
# buffer is mapped before the blit and unmapped after the blit.
#
# This test verifies the batch commands are working.  A single blit is sent
# as a one-blt batch. bvtest will verify that the batch pointers and flags
# are all set properly, and terminate the batch with a BATCH_ENDNOP command.
#
# Output images are named "cb-<FMT>-to-<FMT>.tga" and should all look identical.
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#

###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done

###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
outputdir=callback-results
targetdir=.
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
touch="touch "
echo="echo "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  quote=\"
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  and_ref="../../android/conform/bin/libbltsville_ref.so"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=" > callback.log; echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect=\>
  push="adb push "
  pull="adb pull "
  touch="adb shell touch "
  echo="adb shell \" echo "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin
  adb push ${and_ref} /system/vendor/lib

fi

# Create the directory to store the results in
${make_dir} ${targetdir} 
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null

tag=

# input file dimensions
w=320
h=256
dim=${w}x${h}
rc=0,0-${dim}

##############################################
# Function to convert from source to destination pixel formats
# srcfmt is the array of source formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
# dstfmt is the array of destination formats to create.
#
function Callback-Test() {
  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}
    srcstride=${srcstrides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${dim}\(${srcstride}\).raw${quote}

    dstidx=0
    while [ "x${dstfmts[dstidx]}" != "x" ]
    do

      dstfmt=${dstfmts[dstidx]}
      dstfile=${outputpath}/from-${srcfmt}-to-${dstfmt}-${tag}

      echo Testing callback with ${srcfmt} to ${dstfmt}.
      if [ "$vb" = "1" ]; then
          echo ${bvt} -src1file ${src1file} -src1rect ${rc} -dst ${dstfmt} ${dim} -dstrect ${rc} -dstfile ${dstfile}.tga -rop CCCC -callback -map ${maptest} -batch ${bvt_ret} >callback.log
      fi
      ${bvt} -src1file ${src1file} -src1rect ${rc} -dst ${dstfmt} ${dim} -dstrect ${rc} -dstfile ${dstfile}.tga -rop CCCC -callback -map  ${maptest} -batch  ${bvt_ret} >callback.log

      retcode=$?
      if [ "$android" = "1" ]; then
        retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
      fi
      # Check for android system error
      if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
      fi

      if [ "$retcode" = "139" ]; then
          # Test should never ever segfault.

          echo ERROR: Segfault during blit from ${srcfmt} to ${dstfmt}.
          ${touch} ${dstfile}-segfault.tga
          if [ "$android" = "1" ]; then
            adb shell "echo 0xFFFFFFFF > ${dstfile}.crc"
          else
            echo 0xFFFFFFFF > ${dstfile}.crc
          fi

      else
          # For this test, some of the requested operations will pass
          # and some will fail intentionally.  We are only concerned with the
          # messages that are printed out.

          # Fetch the output file
          if [ "$android" = "1" ]; then
            rm ${targetdir}/callback.log 2> /dev/null
            ${pull} ${targetdir}/callback.log
          fi

          # Look for a failure
          grep -q FAIL callback.log
          if [ "$?" = "0" ]; then
            # At least one test failed
            if [ "$android" = "1" ]; then
              adb shell "echo 0xFFFFFFFF > ${dstfile}.crc"
            else
              echo 0xFFFFFFFF > ${dstfile}.crc
            fi
          else
            # all tests passed.
            if [ "$android" = "1" ]; then
              adb shell "echo 0 > ${dstfile}.crc"
            else
              echo 0 > ${dstfile}.crc
            fi
          fi

          echo ++++++++++++++++++++++++++++++++++++
          cat callback.log
          echo 

          rm callback.log
          if [ "$android" = "1" ]; then
            ${remove} ${targetdir}/callback.log
          fi

      fi

      if [ "$no_tga" = "1" ]; then
        rm ${dstfile}*.tga
      fi

      dstidx=$(( $dstidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done
}


##############################################
# Source and destination color formats being tested (followed by stride info)

srcfmts=(    RGB16 )
srcstrides=( 640 )
dstfmts=(    RGB24 YV12 )


# Do the blits.
# the RGB16 to RGB24 should succeed, the RGB16 to NV12 should fail.
# Look for error messages from bvtest about the callback function.
# Both tests should return a message saying "Callback function test PASSED."
echo
echo testing callback and mapping
echo
maptest=test
tag=maptest
Callback-Test
echo Failure above is intentional and can be ignored. All other tests must say \"PASSED\"
echo However, failure should not be a segfault.

echo
echo testing crossmap
echo
maptest=cross
tag=crossmaptest
Callback-Test
echo Failure above is intentional and can be ignored. All other tests must say \"PASSED\"
echo However, failure should not be a segfault.

##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi


