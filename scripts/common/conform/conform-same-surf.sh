#!/bin/bash
#
# conform-scale.sh
# Bltsville conformance tests: same surface blits
#
# Tests executed:
#     Copy rectangle in same surface
#     blend
#
# This test does a blit from a portion of a surface to a different part of
# the same surface.  
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#


###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done



###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
targetdir=.
outputdir=same-surf-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
here=.
touch="touch "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect='\>'
  push="adb push "
  pull="adb pull "
  here=
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin

fi

${make_dir} ${targetdir} 
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null



###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
src_rc=0,0-${src_dim}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
threeqtr_width=240
threeqtr_height=192
half_width=160
half_height=128
third_width=96
third_height=80
qtr_width=80
qtr_height=64

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}



##############################################
# Function to copy from one part of a surface to another
# srcfmt is the array of formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
#
function Copy-Image() {
  srcidx=0
  while [ "x${fmts[srcidx]}" != "x" ]
  do
    srcfmt=${fmts[srcidx]}
    srcstride=${strides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}
    dstfmt=${srcfmt}

      dstfile=${outputpath}/copy-${srcfmt}-${tag}

      # remove previous test results if they exist
      if [ -f "${dstfile}.tga" ]
      then
         ${remove} ${dstfile}.tga
      fi
      if [ -f "${dstfile}-unsupported.tga" ]
      then
        ${remove} ${dstfile}-unsupported.tga
      fi
      if [ -f "${dstfile}-segfault.tga" ]
      then
         ${remove} ${dstfile}-segfault.tga
      fi
      if [ -f "${dstfile}.crc" ]
      then
         ${remove} ${dstfile}.crc
      fi

      echo Same-surface copy for ${srcfmt}
      if [ "$vb" = "1" ]
      then
          echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${srcfmt} src1 -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -rop CCCC ${bvt_ret}
      fi
      ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${srcfmt} src1 -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -rop CCCC ${bvt_ret}

      # If copy failed, print a warning. Also create a dummy file, so as
      # to immediately call attention to the error, to whatever person (or
      # script) is examining the output.
      retcode=$?
      if [ "$android" = "1" ]
      then
        retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
      fi

      # Check for android system error
      if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
      fi

      if [ "$retcode" = "139" ]
      then
        echo ERROR: Segault during same-surface blit for ${srcfmt}
        ${touch} ${dstfile}-segfault.tga
      else
        if [ "$retcode" -ne "0" ]
        then
          echo ERROR: Same-surface copy failed for ${srcfmt}
          ${touch} ${dstfile}-unsupported.tga
        fi
      fi

      # Also create a CRC for the file we just wrote.
      if [ "$android" = "1" ]
      then
          ${crc} ${dstfile}.tga \>  ${dstfile}.crc
      else
          ${crc} ${dstfile}.tga > ${dstfile}.crc
      fi

      if [ "$no_tga" = "1" ]
      then
        ${remove} ${dstfile}*.tga
      fi

    srcidx=$(( $srcidx + 1 ))
  done

}

##############################################
# Fill the destination
# fmts is the array of formats to use.
# strides is a corrosponding array of how big the pixel size is for each format
# color is either "red", "green" or "blue"
#
function Fill-Image() {
  srcidx=0
  while [ "x${fmts[srcidx]}" != "x" ]
  do
    srcfmt=${fmts[srcidx]}
    srcstride=${strides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}
    dstfmt=${srcfmt}

    dstfile=${outputpath}/fill-${color}-${tag}-${dstfmt}-${dst_dim}

    # remove previous test results if they exist
    if [ -f "${dstfile}.tga" ]
    then
       ${remove} ${dstfile}.tga
    fi
    if [ -f "${dstfile}-unsupported.tga" ]
    then
      ${remove} ${dstfile}-unsupported.tga
    fi
    if [ -f "${dstfile}-segfault.tga" ]
    then
       ${remove} ${dstfile}-segfault.tga
    fi
    if [ -f "${dstfile}.crc" ]
    then
       ${remove} ${dstfile}.crc
    fi

    echo Filling ${dstfmt}-${dst_dim} using ${color}.
    if [ "$vb" = "1" ]
    then
        echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga  -rop CCCC ${bvt_ret}
    fi
    ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -dst ${dstfmt} ${dst_dim} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga -rop CCCC ${bvt_ret}

    # If fill failed, print a warning. Also create a dummy file, so as
    # to immediately call attention to the error, to whatever person (or
    # script) is examining the output.
    retcode=$?
    if [ "$android" = "1" ]
    then
      retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
    fi
    # Check for android system error
    if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
    fi

    if [ "$retcode" = "139" ]
    then
      echo ERROR: Segfault during fill to ${dstfmt}
      ${touch} ${dstfile}-segfault.tga
    else
      if [ "$retcode" -ne "0" ]
      then
        echo ERROR: Cannot fill to ${dstfmt}
        ${touch} ${dstfile}-unsupported.tga
      fi
    fi

    # Also create a CRC for the file we just wrote.
    if [ "$android" = "1" ]
    then
        ${crc} ${dstfile}.tga \>  ${dstfile}.crc
    else
        ${crc} ${dstfile}.tga > ${dstfile}.crc
    fi

    if [ "$no_tga" = "1" ]
    then
      ${remove} ${dstfile}*.tga
    fi

    srcidx=$(( $srcidx + 1 ))
  done
}





##############################################
# Test same-format copy.  Copy a rectangle from the upper left to lower right
# of the same surface.

fmts=(    LUT8 RGB16 RGB24 BGR24 xRGB24 0RGB24 xBGR24 0BGR24 RGBx24 RGB024 BGRx24 BGR024 ARGB24 ABGR24 RGBA24 BGRA24 nRGBA24 nBGRA24 nARGB24 nABGR24 YUYV UYVY YV12 IYUV NV12 )
strides=( 320  640   960   960   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280   1280    1280    1280    1280    640  640  320  320  320  )

# Do a non-overlapped copy
# Source is upper left quardrant
# Dest is lower right quadrant
tag=copy
src1_rc=0,0-${half_width}x${half_height}
dest_rc=${half_width},${half_height}-${half_width}x${half_height}
clip_rc=${dest_rc}
Copy-Image




##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi
