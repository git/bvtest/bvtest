#!/bin/bash
#
# conform-rotate.sh
# Bltsville conformance tests: Rotation
#
# Tests executed:
#    Rotate same format
#    Rotate from src orientation X to dest orientation Y
#    Rotate full image
#    Rotate partial image
#    Rotate with clip to full destination
#    Rotate with clip to part of destination
#    Rotate with clip away entirely
#
#
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used for each test.
#
#


###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done



###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
targetdir=.
outputdir=rotate-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
here=.
touch="touch "

###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect='\>'
  push="adb push "
  pull="adb pull "
  here=
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin

fi

${make_dir} ${targetdir} 
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null


###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_dim=${src_w}x${src_h}
rot_src_dim=${src_h}x${src_w}
src_rc=0,0-${src_dim}

# dest file dimensions
dst_w=320
dst_h=256
dst_dim=${dst_w}x${dst_h}
dst_rc=0,0-${dst_dim}

# useful dimensions
double_width=640
double_height=512
thrqtr_width=224
thrqtr_height=192
half_width=160
half_height=128
qtr_width=64
qtr_height=64
thrqtr_dim=${thrqtr_width}x${thrqtr_height}

# a rectangle that exceeds the dest on all sides
oversize_rc=-${half_width},-${half_height}-${double_width}x${double_height}

# a rectangle in the center of the dest
middle_rc=${qtr_width},${qtr_height}-${half_width}x${half_height}

# a rectangle in the upper left quadrant
ul_rc=0,0-${half_width}x${half_height}

# a rectangle in the upper right quadrant
ur_rc=${half_width},0-${half_width}x${half_height}

# a rectangle in the lower left quadrant
ll_rc=0,${half_height}-${half_width}x${half_height}

# a rectangle in the lower right quadrant
lr_rc=${half_width},${half_height}-${half_width}x${half_height}

# a rectangle outside the dest
outside_rc=-${half_width},-${half_height}-${half_width}x${half_height}


##############################################
# Create rotated source helper function
function Create-Rotated-File() {

  dstfile=${inputpath}/input-rot${dstrot}

  if [ "$vb" = "1" ]
  then
    echo ${bvt} -src1file ${src1file} -src1rect ${src_rc} -dst ${dstfmt} ${dst_dim} -dstangle ${dstrot} -dstrect ${dst_rc} -dstfile ${dstfile}.raw  -rop CCCC ${bvt_ret}
  fi
  ${bvt} -src1file ${src1file} -src1rect ${src_rc} -dst ${dstfmt} ${dst_dim} -dstangle ${dstrot} -dstrect ${dst_rc} -dstfile ${dstfile}.raw  -rop CCCC ${bvt_ret}

  retcode=$?
  if [ "$android" = "1" ]
  then
    retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
  fi

  if [ "$retcode" -ne "0" ]
  then
    echo ERROR: Cannot create source image ${dstfile}-${dstfmt}
  fi
}

##############################################
# Function to create new rotated sources from each source image
# fmts is the array of formats to use.
# strides is a corrosponding array of the stride for each format
#
function Create-Rotated-Sources() {
  srcidx=0
  while [ "x${fmts[srcidx]}" != "x" ]
  do
    srcfmt=${fmts[srcidx]}
    srcstride=${strides[srcidx]}
    src1file=${quote}${inputpath}/input-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}
    dstfmt=${srcfmt}

    dstrot=0
    Create-Rotated-File

    dstrot=90
    Create-Rotated-File

    dstrot=180
    Create-Rotated-File

    dstrot=270
    Create-Rotated-File

    srcidx=$(( $srcidx + 1 ))
  done
}




##############################################
# Function to rotate from a source format to the same destination pixel formats
# srcfmt is the array of formats to use.
# srcsize is a corrosponding array of how big the pixel size is for each format
#
function Rotate-Image() {
  srcidx=0
  while [ "x${fmts[srcidx]}" != "x" ]
  do
    srcfmt=${fmts[srcidx]}
    dstfmt=${srcfmt}

    srcrotidx=0
    while [ "x${srcrots[srcrotidx]}" != "x" ]
    do
      srcrot=${srcrots[srcrotidx]}
      srcstride=${strides[srcidx]}
      rotsrcstride=${rotstrides[srcidx]}

      src1file=${quote}${inputpath}/input-rot${srcrot}-${srcfmt}-${src_dim}\(${srcstride}\).raw${quote}

      # For sources
      if [ "$srcrot" = "90" ]
      then
        src1file=${quote}${inputpath}/input-rot${srcrot}-${srcfmt}-${rot_src_dim}\(${rotsrcstride}\).raw${quote}
      fi
      if [ "$srcrot" = "270" ]
      then
        src1file=${quote}${inputpath}/input-rot${srcrot}-${srcfmt}-${rot_src_dim}\(${rotsrcstride}\).raw${quote}
      fi

      dstrotidx=0
      while [ "x${dstrots[dstrotidx]}" != "x" ]
      do
        dstrot=${dstrots[dstrotidx]}

        dstfile=${outputpath}/rotate-${tag}-${srcfmt}-${srcrot}-to-${dstfmt}-${dstrot}-${dst_dim}

        # remove previous test results if they exist
        if [ -f "${dstfile}.tga" ]
        then
           ${remove} ${dstfile}.tga
        fi
        if [ -f "${dstfile}-unsupported.tga" ]
        then
          ${remove} ${dstfile}-unsupported.tga
        fi
        if [ -f "${dstfile}-segfault.tga" ]
        then
           ${remove} ${dstfile}-segfault.tga
        fi
        if [ -f "${dstfile}.crc" ]
        then
           ${remove} ${dstfile}.crc
        fi

        echo Rotating from ${srcfmt} ${srcrot} to ${dstfmt} ${dstrot} ${dst_dim}
        if [ "$vb" = "1" ]
        then
            echo ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -src1angle ${srcrot} -dst ${dstfmt} ${dst_dim} -dstangle ${dstrot} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga  -rop CCCC ${bvt_ret}
        fi
        ${bvt} -src1file ${src1file} -src1rect ${src1_rc} -src1angle ${srcrot} -dst ${dstfmt} ${dst_dim} -dstangle ${dstrot} -dstrect ${dest_rc} -cliprect ${clip_rc} -dstfile ${dstfile}.tga  -rop CCCC ${bvt_ret}

        # If rotate failed, print a warning. Also create a dummy file, so as
        # to immediately call attention to the error, to whatever person (or
        # script) is examining the output.
        retcode=$?
        if [ "$android" = "1" ]
        then
          retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
        fi
        # Check for android system error
        if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
        fi

        if [ "$retcode" = "139" ]
        then
              echo ERROR: Segfault during rotate from ${srcfmt}-${srcrot} to ${dstfmt}-${dstrot}.
          ${touch} ${dstfile}-segfault.tga
        else
          if [ "$retcode" -ne "0" ]
          then
              echo ERROR: Cannot rotate from ${srcfmt}-${srcrot} to ${dstfmt}-${dstrot}.
            ${touch} ${dstfile}-unsupported.tga
          fi
        fi

        # Also create a CRC for the file we just wrote.
        if [ "$android" = "1" ]
        then
            ${crc} ${dstfile}.tga \>  ${dstfile}.crc
        else
            ${crc} ${dstfile}.tga > ${dstfile}.crc
        fi

        if [ "$no_tga" = "1" ]
        then
          ${remove} ${dstfile}*.tga
        fi

        dstrotidx=$(($dstrotidx + 1))
      done

      srcrotidx=$(( $srcrotidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done
}


##############################################
#  Create Rotated Sources
#  For each of the source formats, rotate it to 90, 180, and 270 degrees
fmts=(       LUT8 RGB16 YUYV UYVY IYUV NV12 )
strides=(    320  640   640  640  320  320  )
rotstrides=( 256  512   512  512  256  256  )
Create-Rotated-Sources

# Even though we can rotate all the above sources, we can't convert them to
# RGB to view the results of the rotation.
fmts=(       LUT8 RGB16  UYVY NV12 )
strides=(    320  640    640  320 )
rotstrides=( 256  512    512  256 )


##############################################
# here are the types of rotation to try for each copy.
dstrots=( 0  90  180  270  )
srcrots=( 0  90  180  270  )



##############################################
#  Rotation full image
tag=full-image
src1_rc=${src_rc}
dest_rc=${dst_rc}
clip_rc=${dst_rc}
Rotate-Image


##############################################
#  Rotate upper left quadrant
tag=upper-left
src1_rc=${ul_rc}
dest_rc=${ul_rc}
Rotate-Image

##############################################
#  Rotate lower right quadrant
tag=lower-right
src1_rc=${lr_rc}
dest_rc=${lr_rc}
Rotate-Image


##############################################
#  Rotate center to upper right
tag=center-to-ur
src1_rc=${middle_rc}
dest_rc=${ur_rc}
Rotate-Image


##############################################
#  Create src and dest rectangles that exceed the surfaces
#  Create a clip rectangle that clips to dest surf
tag=clip-to-dest

src1_rc=${oversize_rc}
dest_rc=${oversize_rc}
clip_rc=${dst_rc}
Rotate-Image


##############################################
# Rotate a rectangle from the center of the source to the upper left corner of the destination.
# have the destination rectangle extend off the top and left of the destination surface
# clip to the destination surface.

tag=clip-to-ul
src1_rc=${middle_rc}
dest_rc=-${qtr_width},-${qtr_height}-${half_width}x${half_height}
clip_rc=${dst_rc}
Rotate-Image

##############################################
# Rotate a rectangle from the lower left of the source to the lower left of the destination.
# have the clip rectangle extend off the top and right of the destination surface

tag=clip-to-lr
src1_rc=${lr_rc}
dest_rc=${lr_rc}
clip_rc=0,0-${thrqtr_dim}
Rotate-Image


##############################################
# Dest and clip rect do not intersect.

tag=clip-all
src1_rc=${ul_rc}
dest_rc=${ul_rc}
clip_rc=${lr_rc}
Rotate-Image





##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir}
  ${pull} ${outputpath}
  popd

  # Delete the CRC files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
  fi

fi
