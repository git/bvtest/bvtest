#!/bin/bash
#
# conform-make-inputs.sh
# Bltsville conformance tests: Source Image Creation
#
# This test creates input files for the rest of the tests.
# For the rest of the tests, we will need these as input file of each possible
# pixel format supported.
#
# Our source image is a very large PNG file.  Since we will be running
# thousands of blts, we would rather use a small file. So our first step is to
# scale and convert the PNG file to a 320x256 image.
#
# Using the smaller source image, do a full surface blit 
# to a new surface of each possible format, and save that resulting image
# in a file.
#
# Files created will have the file name format:
#    input-<FMT>-<width>x<height>(stride).raw
#
# Possible source formats can be:
#
#    ALPHA1 ALPHA2 ALPHA4 ALPHA8 ALPHA4x1 ALPHA3x8 ALPHA4x8
#    MONO1 MONO2 MONO4 MONO8
#    LUT1 LUT2 LUT4 LUT8
#    RGB12 1RGB12 xRGB12 0RGB12 ARGB12 nARGB12
#    RGB15 ARGB15 nARGB15
#    RGB16 ARGB16 RGBA16 nARGB16 nRGBA16
#    RGB24 BGR24
#    xRGB24 0RGB24 1RGB24
#    xBGR24 0BGR24 1BGR24
#    RGBx24 RGB024 RGB124
#    BGRx24 BGR024 BGR124
#    ARGB24 ABGR24 RGBA24 BGRA24
#    nARGB24 nABGR24 nRGBA24 nBGRA24
#    UYVY Y422 VYUY YUYV YUY2 YVYU YV12
#    IYUV IMC1 IMC2 IMC3 IMC4
#    NV12 NV21 NV16 NV61
#    ARGB12_NON_PREMULT ARGB15_NON_PREMULT ARGB16_NON_PREMULT RGBA16_NON_PREMULT
#    ARGB24_NON_PREMULT ABGR24_NON_PREMULT RGBA24_NON_PREMULT BGRA24_NON_PREMULT
#
# Not all implementations support all formats, so there may be some errors creating inputs.
#
# The -verbose command line argument will cause this test to print the bvtest
# command line used to create each image.
#

# Paths to things we need
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
preferredlib=
outpath=./input-images
imagespath=../../../images
pngfile=${imagespath}/test-pattern.png
pngdim=1920x1080
fruitfile=${imagespath}/Fruit-2048x1536.jpg
fruitdim=2048x1536

# The size we want all our test images to be.
w=320
h=256
dim=${w}x${h}
rc=0,0-${dim}

# Defaults for command line parameters
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0

##############################################
# Parse the command line
for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

done


##############################################
# Setup for Linux
#
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
targetdir=.
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
push="cp "
make_dir="mkdir -p "

##############################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  targetdir=/sdcard/TI-conform
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  make_dir="adb shell mkdir -p "
  pngfile=test-pattern.png
  fruitfile=Fruit-2048x1536.jpg
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  push="adb push "

  echo Creating target directory ${targetdir}
  ${make_dir} ${targetdir}
  adb push ${and_bvt} /system/bin
  adb push ../../../images/Fruit-2048x1536.jpg ${targetdir}
  adb push ../../../images/kings_1_bg_051802.jpg ${targetdir}
  adb push ../../../images/test-pattern.png ${targetdir}

fi

echo Creating input images directory ${targetdir}/input-images
${make_dir} ${targetdir}/input-images



##############################################
# Function to create files from an array of source formats.
# srcfmt is the array of source formats to create.
#
function Create-Files() {
  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    srcfmt=${srcfmts[srcidx]}

    echo Creating ${srcfmt} input file.
    if [ "$vb" = "1" ]
    then
        echo ${bvt} ${verbose}  -src1file ${src1file} -src1rect ${rc} -dst ${srcfmt} ${dim} -dstrect ${rc} -dstfile ${outpath}/${dstname}.raw -rop CCCC
    fi
    ${bvt} ${verbose}  -src1file ${src1file} -src1rect ${rc} -dst ${srcfmt} ${dim} -dstrect ${rc} -dstfile ${outpath}/${dstname}.raw -rop CCCC

    srcidx=$(( $srcidx + 1 ))
  done
}

##############################################
# First convert the giant PNG file to a RGBA24 file
if [ "$vb" = "1" ]
then
  echo ${bvt}  ${verbose} -src1file ${pngfile}   -src1 RGBA24 ${pngdim}   -src1rect 0,0-${pngdim}   -dst RGBA24 ${pngdim}   -dstrect 0,0-${pngdim}   -dstfile ${outpath}/test-pattern.raw -rop CCCC
fi
${bvt}  ${verbose} -src1file ${pngfile}   -src1 RGBA24 ${pngdim}   -src1rect 0,0-${pngdim}   -dst RGBA24 ${pngdim}   -dstrect 0,0-${pngdim}   -dstfile ${outpath}/test-pattern.raw -rop CCCC

if [ "$vb" = "1" ]
then
  echo ${bvt}  ${verbose} -src1file ${fruitfile} -src1 RGBA24 ${fruitdim} -src1rect 0,0-${fruitdim} -dst RGBA24 ${fruitdim} -dstrect 0,0-${fruitdim} -dstfile ${outpath}/fruit.raw        -rop CCCC
fi
${bvt}  ${verbose} -src1file ${fruitfile} -src1 RGBA24 ${fruitdim} -src1rect 0,0-${fruitdim} -dst RGBA24 ${fruitdim} -dstrect 0,0-${fruitdim} -dstfile ${outpath}/fruit.raw        -rop CCCC

# Since we can't scale an RGBA24 yet (or convert it to RGB24), treat this
# as an RGB124 file instead.

${remove} ${quote}${outpath}/test-pattern-RGB124-${pngdim}\(7680\).raw${quote}
${remove} ${quote}${outpath}/fruit-RGB124-${fruitdim}\(8192\).raw${quote}
${move}   ${quote}${outpath}/test-pattern-RGBA24-${pngdim}\(7680\).raw${quote} ${quote}${outpath}/test-pattern-RGB124-${pngdim}\(7680\).raw${quote}
${move}   ${quote}${outpath}/fruit-RGBA24-${fruitdim}\(8192\).raw${quote}      ${quote}${outpath}/fruit-RGB124-${fruitdim}\(8192\).raw${quote}

# now scale it down.
if [ "$vb" = "1" ]
then
  echo ${bvt}  ${verbose} -src1file ${quote}${outpath}/test-pattern-RGB124-${pngdim}\(7680\).raw${quote}  -src1rect 0,0-${pngdim}   -dst RGB124 ${dim} -dstrect 0,0-${dim} -dstfile ${outpath}/test-pattern.raw -rop CCCC
fi
${bvt}  ${verbose} -src1file ${quote}${outpath}/test-pattern-RGB124-${pngdim}\(7680\).raw${quote}  -src1rect 0,0-${pngdim}   -dst RGB124 ${dim} -dstrect 0,0-${dim} -dstfile ${outpath}/test-pattern.raw -rop CCCC


if [ "$vb" = "1" ]
then
  echo ${bvt}  ${verbose} -src1file ${quote}${outpath}/fruit-RGB124-${fruitdim}\(8192\).raw${quote}       -src1rect 0,0-${fruitdim} -dst RGB124 ${dim} -dstrect 0,0-${dim} -dstfile ${outpath}/fruit.raw        -rop CCCC
fi
${bvt}  ${verbose} -src1file ${quote}${outpath}/fruit-RGB124-${fruitdim}\(8192\).raw${quote}       -src1rect 0,0-${fruitdim} -dst RGB124 ${dim} -dstrect 0,0-${dim} -dstfile ${outpath}/fruit.raw        -rop CCCC

# and convert it back to RGBA24
${copy} ${quote}${outpath}/test-pattern-RGBx24-320x256\(1280\).raw${quote} ${to} ${quote}${outpath}/test-pattern-RGBA24-320x256\(1280\).raw${quote}
${copy} ${quote}${outpath}/fruit-RGBx24-320x256\(1280\).raw${quote}        ${to} ${quote}${outpath}/fruit-RGBA24-320x256\(1280\).raw${quote}

##############################################
# Now go through all the source formats we support and create as many as we can.
# We do this in groups simply because it makes it easier to read and modify
# the test code
#

# Start by using the scaled down RGBA image, and make as many as possible.
srcfmts=( ALPHA1 ALPHA2 ALPHA4 ALPHA8 ALPHA4x1 ALPHA3x8 ALPHA4x8  MONO1 MONO2 MONO4 MONO8 LUT1 LUT2 LUT4 LUT8  RGB12 1RGB12 xRGB12 0RGB12 ARGB12 nARGB12  RGB15 ARGB15 nARGB15 RGB16 ARGB16 RGBA16 nARGB16 nRGBA16 RGB24 BGR24 xRGB24 0RGB24 1RGB24 xBGR24 0BGR24 1BGR24 RGBx24 RGB024 RGB124 BGRx24 BGR024 BGR124  ARGB24 ABGR24 RGBA24 BGRA24 nARGB24 nABGR24 nRGBA24 nBGRA24  UYVY Y422 VYUY YUYV YUY2 YVYU YV12  IYUV IMC1 IMC2 IMC3 IMC4 NV12 NV21 NV16 NV61 )
src1file=${quote}${outpath}/test-pattern-RGBA24-320x256\(1280\).raw${quote}
dstname=input
Create-Files
src1file=${quote}${outpath}/fruit-RGBA24-320x256\(1280\).raw${quote}
dstname=input2
Create-Files

# The above only made a few RGBA formats. But it should have made at least one
# non-premultiplied format.
# Use that to create more.
srcfmts=( ALPHA1 ALPHA2 ALPHA4 ALPHA8 ALPHA4x1 ALPHA3x8 ALPHA4x8  MONO1 MONO2 MONO4 MONO8 LUT1 LUT2 LUT4 LUT8  RGB12 1RGB12 xRGB12 0RGB12 ARGB12 nARGB12  RGB15 ARGB15 nARGB15 RGB16 ARGB16 RGBA16 nARGB16 nRGBA16 RGB24 BGR24 xRGB24 0RGB24 1RGB24 xBGR24 0BGR24 1BGR24 RGBx24 RGB024 RGB124 BGRx24 BGR024 BGR124 nARGB24 nABGR24 nBGRA24  UYVY Y422 VYUY YUYV YUY2 YVYU YV12  IYUV IMC1 IMC2 IMC3 IMC4 NV12 NV21 NV16 NV61 )
src1file=${quote}${outpath}/input-nRGBA24-320x256\(1280\).raw${quote}
dstname=input
Create-Files
src1file=${quote}${outpath}/input2-nRGBA24-320x256\(1280\).raw${quote}
dstname=input2
Create-Files

# that should have created all the RGB+A formats.  Now to make the step down
# to 3 comp formats.  Do this by copying an RGBA24 to RGB124
${copy} ${quote}${outpath}/input-BGRA24-320x256\(1280\).raw${quote}  ${to} ${quote}${outpath}/input-BGRx24-320x256\(1280\).raw${quote}
${copy} ${quote}${outpath}/input2-BGRA24-320x256\(1280\).raw${quote} ${to} ${quote}${outpath}/input2-BGRx24-320x256\(1280\).raw${quote}
srcfmts=( ALPHA1 ALPHA2 ALPHA4 ALPHA8 ALPHA4x1 ALPHA3x8 ALPHA4x8  MONO1 MONO2 MONO4 MONO8 LUT1 LUT2 LUT4 LUT8  RGB12 1RGB12 xRGB12 0RGB12 ARGB12 nARGB12  RGB15 ARGB15 nARGB15 RGB16 ARGB16 RGBA16 nARGB16 nRGBA16 RGB24 BGR24 xRGB24 0RGB24 1RGB24 xBGR24 0BGR24 1BGR24 RGBx24 RGB024 RGB124 BGR024 UYVY Y422 VYUY YUYV YUY2 YVYU YV12  IYUV IMC1 IMC2 IMC3 IMC4 NV12 NV21 NV16 NV61 )
src1file=${quote}${outpath}/input-BGRx24-320x256\(1280\).raw${quote}
dstname=input
Create-Files
src1file=${quote}${outpath}/input2-BGRx24-320x256\(1280\).raw${quote}
dstname=input2
Create-Files


# That should have created the rest of the formats that we can create.
# the following formats still can't be created.
#srcfmts=( ALPHA1 ALPHA2 ALPHA4 ALPHA8 ALPHA4x1 ALPHA3x8 ALPHA4x8  MONO1 MONO2 MONO4 MONO8 LUT1 LUT2 LUT4 LUT8 RGB12 1RGB12 xRGB12 0RGB12 ARGB12 nARGB12  RGB15 ARGB15 nARGB15 ARGB16 RGBA16 nARGB16 nRGBA16 Y422 VYUY YUYV YUY2 YVYU YV12  IYUV IMC1 IMC2 IMC3 IMC4 NV21 NV16 NV61 )


# Copy down pre-made copies of these
${push} ${imagespath}/input-LUT8-320x256\(320\).raw  ${targetdir}/${outpath}
${push} ${imagespath}/input-YV12-320x256\(320\).raw  ${targetdir}/${outpath}
${push} ${imagespath}/input-IYUV-320x256\(320\).raw  ${targetdir}/${outpath}
${push} ${imagespath}/input-YUYV-320x256\(640\).raw  ${targetdir}/${outpath}

${push} ${imagespath}/input2-LUT8-320x256\(320\).raw  ${targetdir}/${outpath}
${push} ${imagespath}/input2-YV12-320x256\(320\).raw  ${targetdir}/${outpath}
${push} ${imagespath}/input2-IYUV-320x256\(320\).raw  ${targetdir}/${outpath}
${push} ${imagespath}/input2-YUYV-320x256\(640\).raw  ${targetdir}/${outpath}


