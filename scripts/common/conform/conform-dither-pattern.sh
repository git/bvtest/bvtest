#!/bin/bash
#
# conform-dither-pattern.sh
# Bltsville conformance tests: Dithering pattern.
# This test confirms that the dithering pattern is not affected buy destination
# address or clip rectangles.
#
# This test performs dithered operations to different locations with different
# clip rectangles, and verifies that the resulting images are always the same.
#
# Two arrays of pixel format names are suppled: the "from" array and the
# "to" array.
#
# both surface format conversion and blending are tested.
#

###########################################################################
#
# Parse command line
#
vb=0
verbose=
android=0
preferredlib="-imp cpu"
get_lib=0
no_tga=0

for arg in "$@"
do
  if [ "$get_lib" = "1" ]
  then
    preferredlib="-imp "$arg
    get_lib=0
  fi

  if [ "$arg" = "-imp" ]
  then
    get_lib=1
  fi

  if [ "$arg" = "-verbose" ]
  then
    vb=1
    verbose=-verbose
  fi

  if [ "$arg" = "-linux" ]
  then
    android=0
  fi

  if [ "$arg" = "-android" ]
  then
    android=1
  fi

  if [ "$arg" = "-no_tga" ]
  then
    no_tga=1
  fi

done



###########################################################################
# Setup for Linux
#
export LD_LIBRARY_PATH=`pwd`/../../../../ti-bltsville/lib/linux
targetdir=.
outputdir=dither-pattern-results
inputpath=${targetdir}/input-images
outputpath=${targetdir}/this_run/${outputdir}
bvt="../../../src/bvtest/bvtest ${preferredlib}"
bvt_ret=
crc="../../../src/crc32/crc32 -q -file "
remove="rm -rf "
move="mv "
quote=
copy="cp "
to=
redirect='>'
push="cp "
make_dir="mkdir -p    "
pull="cp "
here=.
touch="touch "


###########################################################################
# Setup for Android
#
if [ "$android" = "1" ]
then
  targetdir=/sdcard/TI-conform
  inputpath=${targetdir}/input-images
  outputpath=${targetdir}/this_run/${outputdir}
  and_bvt="../../android/conform/bin/bvtest"
  and_crc="../../android/conform/bin/crc32"
  bvt="adb shell cd ${targetdir};bvtest ${preferredlib}"
  bvt_ret=";echo \$? > ${targetdir}/retval"
  crc="adb shell cd ${targetdir};crc32 -q -file "
  make_dir="adb shell mkdir -p    "
  remove="adb shell cd ${targetdir};rm "
  move="adb shell cd ${targetdir};mv "
  quote=\"
  copy="adb shell cd ${targetdir};cat "
  to=\>
  redirect='\>'
  push="adb push "
  pull="adb pull "
  here=./this_run
  touch="adb shell touch "

  adb push ${and_bvt} /system/bin
  adb push ${and_crc} /system/bin
fi

${make_dir} ${targetdir}  
${make_dir} ${targetdir}/this_run 
${make_dir} ${outputpath} 

#if we are running on android, we will also need a local directory for pulling results to.
if [ "$android" = "1" ] ; then
  mkdir -p this_run
  mkdir -p this_run/${outputdir}
fi

# Clean it out from any previous results
${remove} ${outputpath}/*.crc > /dev/null
${remove} ${outputpath}/*.tga > /dev/null


###########################################################################
#
# Setup default test parameters
#
tag=

# input file dimensions
src_w=320
src_h=256
src_file_dim=${src_w}x${src_h}
dest_file_dim=${src_w}x${src_h}


# Rectangles to use for scaling
# Destination file needs to be slightly larger than the destination rectangle
# since src x,y may be up to 8,8

#no scale
dest_file_dim_no_scale=352x288

# scale up
rc_dim_scale_up=480x384
file_dim_scale_up=496x400

# 0.5x-1.0x
rc_dim_scale_A=288x224
file_dim_scale_A=304x240

# 0.33x-0.5x
rc_dim_scale_B=144x112
file_dim_scale_B=160x128

# 0.25x-0.33x
rc_dim_scale_C=96x80
file_dim_scale_C=112x96

# under 0.25x
rc_dim_scale_D=64x48
file_dim_scale_D=80x64


#############################################################################
#############################################################################
#############################################################################
#
#
#         Helper functions for setting up dither tests
#
#
#############################################################################
#############################################################################
#############################################################################


#############################################################################
#
# Test the dithered file against the reference file
# The reference and dithered files should have the same image, but the
# dithered file should have that image shifted slightly.
# The offset may be a clip offset or a dest offset, depending on if
# "validate_type" is set to "dest" or not
#
#
function Validate-File() {

  # Create a CRC for the file we just wrote.
  if [ "$android" = "1" ]
  then
      ${crc} ${destfile}.tga \>  ${destfile}.crc
  else
      ${crc} ${destfile}.tga > ${destfile}.crc
  fi

  # Before we can compare them, we need to extract equivelent rectangles
  # from both so we are comparing the same thing.  We do this by
  # copying a rectangle from the file to be tested, or from the reference
  # file to a temporary file, and comparing with that.

  if [ "${validate_type}" = "dest" ]
  then
    # For this test, we extract a rectangle from the file we just rendered, which we
    # expect to match the reference file.

    # Blit the destination rectangle from the destination image to its own file.
    test_cmd="${bvt} -src1 ${destfmt} ${dest_file_dim} -src1file ${destfile}.tga -src1rect ${dest_x},${dest_y}-300x220  ${dest_format} -dstrect 0,0-300x220 -dstfile ${destfile}-temp.tga -rop CCCC ${bvt_ret}"
  else
    # For this test, we extract a rectangle from the reference file, which we
    # expect to match the file we just rendered.

    # Clip out an identical rectangle from the reference image to its own file.
    test_cmd="${bvt} -src1 ${destfmt} ${dest_file_dim} -src1file ${outputpath}/dither-${src1fmt}-to-${destfmt}-${dithertype}-ref.tga ${src1_rect} ${dest_format} ${dest_rect} -dstfile ${destfile}-temp.tga ${clip_cmd} -rop CCCC  ${bvt_ret}"
  fi

  if [ "$vb" = "1" ]
  then
      echo Creating validation image
      echo ${test_cmd}
  fi
  ${test_cmd}


  # Test against the reference file.
  if [ "${validate_type}" = "dest" ]
  then
    file1=${destfile}-temp.tga
    file2=${outputpath}/dither-${src1fmt}-to-${destfmt}-${dithertype}-ref.tga
  else
    file1=${destfile}-temp.tga
    file2=${destfile}.tga
  fi

  # if we are running on Android, we need to pull the files to the host for testing.
  if [ "${android}" = "1" ]
  then
    pushd this_run/${outputdir} > /dev/null
    ${pull} ${file1} 2> /dev/null
    ${pull} ${file2} 2> /dev/null
    popd > /dev/null
  fi

  diff -q this_run/${outputdir}/$(basename "$file1") this_run/${outputdir}/$(basename "$file2")
  retcode=$?

  # if it failed, rename the destination file in such a way that we can see
  # there was a failure.
  if [ "$retcode" -ne "0" ]
  then
    echo ERROR: Dither Error for ${destfile}.tga
    if [ "${android}" = "1" ] ; then
        ${move} ${destfile}.tga ${destfile}-compare-failed.tga
    else
       mv ${here}/${destfile}.tga ${here}/${destfile}-compare-failed.tga
    fi
  else
    # remove the temporary file to save disk space.
    if [ "${android}" = "1" ] ; then
      ${remove} ${destfile}-temp.tga
    else
      rm ${here}/${destfile}-temp.tga
    fi
  fi

  if [ "$no_tga" = "1" ]; then
    ${remove} ${destfile}*.tga
  fi

}


##############################################################################
#
# Execute the dither command.
# By this time all the parameters are set up.
#
function Dither-File() {

  src1file=${quote}${inputpath}/input-${src1fmt}-${src_file_dim}\(${src1stride}\).raw${quote}
  destfile=${outputpath}/dither-${src1fmt}-to-${destfmt}-${dithertype}-${tag}

  # remove previous test results if they exist
  if [ "${android}" = "1" ]
  then
    "${remove} ${destfile}.tga" 2> /dev/null
    "${remove} ${destfile}-unsupported.tga"  2> /dev/null
    "${remove} ${destfile}-segfault.tga"  2> /dev/null
    "${remove} ${destfile}.crc"  2> /dev/null
  fi
  rm ${here}/${destfile}.tga 2> /dev/null
  rm ${here}/${destfile}-unsupported.tga  2> /dev/null
  rm ${here}/${destfile}-segfault.tga  2> /dev/null
  rm ${here}/${destfile}.crc  2> /dev/null

  echo Dithering from ${src1fmt} to ${destfmt} using ${dithertype} for ${tag}

  test_cmd="${bvt} -src1file ${src1file} ${src1_rect} ${dest_format} ${dest_rect} -dstfile ${destfile}.tga ${clip_cmd} ${dither_cmd} -rop CCCC  ${bvt_ret}"

  if [ "$vb" = "1" ]
  then
      echo ${test_cmd}
  fi
  ${test_cmd}

  # If dither failed, print a warning. Also create a dummy file, so as
  # to immediately call attention to the error, to whatever person (or
  # script) is examining the output.
  retcode=$?
  if [ "$android" = "1" ]
  then
    retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
  fi
  # Check for android system error
  if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
  fi

  if [ "$retcode" = "139" ]
  then
    echo ERROR: Segfault in dither from ${src1fmt} to ${destfmt}
    ${touch} ${destfile}-segfault.tga
  else
    if [ "$retcode" -ne "0" ]
    then
      echo ERROR: Cannot dither from ${src1fmt} to ${destfmt}
      ${touch} ${destfile}-unsupported.tga
    fi
  fi

  # Now validate the file we just made.
  if [ "$do_validate" = "yes" ]
  then
   Validate-File
  fi


}


#############################################################################
#
# Set up the source and destination surface types for the dither test.
#
function Do-Dither-Surfaces() {

  srcfmts=(     BGR024 BGRx24 RGB024 RGBx24 )
  srcstrides=(  1280   1280   1280   1280   )

  destfmts=(  RGB16 )

  srcidx=0
  while [ "x${srcfmts[srcidx]}" != "x" ]
  do
    src1fmt=${srcfmts[srcidx]}
    src1stride=${srcstrides[srcidx]}

    destidx=0
    while [ "x${destfmts[destidx]}" != "x" ]
    do
      destfmt=${destfmts[destidx]}
      dest_format="-dst ${destfmt} ${dest_file_dim}"

      Dither-File

      destidx=$(( $destidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done
}

#############################################################################
#
#  Set up the type of dithering to do for the test.
#
function Do-Dither-Types() {

  #Types of dithering we could try.
  #dithertypes=( fastest fastest_on fastest_random fastest_ordered fastest_diffused fastest_photo fastest_drawing )
  #dithertypes=( good    good_on    good_random    good_ordered    good_diffused    good_photo    good_drawing )
  #dithertypes=( better  better_on  better_random  better_ordered  better_diffused  better_photo  better_drawing )
  #dithertypes=( best    best_on    best_random    best_ordered    best_diffused    best_photo    best_drawing )
  #dithertypes=( none    ordered2x2 ordered_4x4    ordered_2x2_4x4 )

  # CPU lib only supports this dithering.
  dithertypes=( none ordered_2x2_4x4 )

  dither_idx=0
  while [ "x${dithertypes[dither_idx]}" != "x" ]
  do
      dithertype=${dithertypes[dither_idx]}

      dither_cmd="-dither ${dithertype}"

      Do-Dither-Surfaces

      dither_idx=$(( $dither_idx + 1 ))
  done
}


###############################################################################
#
#  Set up the source and destination rectangles for the dither test.
#
function Do-Destination-Tests() {

  # Set source and destination rectangles to the same size, so there is no scaling.
  src1_rc_dim=310x230
  dest_rc_dim=${src1_rc_dim}
  dest_file_dim=${src_file_dim}

  # Use the exact same source rectangle for every test.
  src1_rect="-src1rect 0,0-${src1_rc_dim}"

  # No clipping used for destination offset validation
  clip_cmd=

  # Vary the destination rectange over an 8x8 grid.
  dest_x_array=( 0 1 2 3 )
  dest_y_array=( 0 1 2 3 )

  y_idx=0
  while [ "x${dest_y_array[y_idx]}" != "x" ]
  do
    dest_y=${dest_y_array[y_idx]}

    x_idx=0
    while [ "x${dest_x_array[x_idx]}" != "x" ]
    do
      dest_x=${dest_x_array[x_idx]}

      dest_rect="-dstrect ${dest_x},${dest_y}-${dest_rc_dim}"

      # Describe what this test will be testing
      tag=dest-offset-${dest_x}-${dest_y}

      Do-Dither-Types

      x_idx=$(( $x_idx + 1 ))
    done

    y_idx=$(( $y_idx + 1 ))
  done
}


###############################################################################
#
function Do-Clip-Tests() {

  # Set source and destination rectangles to the same size, so there is no scaling.
  src1_rc_dim=300x220
  dest_rc_dim=${src1_rc_dim}
  dest_file_dim=${src_file_dim}

  # Use the exact same source and dest rectangle for every test.
  src1_rect="-src1rect 0,0-${src1_rc_dim}"
  dest_rect="-dstrect 0,0-${dest_rc_dim}"


  # Vary the destination rectange over an 8x8 grid.
  clp_x_array=( 0 1 2 3 )
  clp_y_array=( 0 1 2 3 )

  y_idx=0
  while [ "x${clp_y_array[y_idx]}" != "x" ]
  do
    clp_y=${clp_y_array[y_idx]}

    x_idx=0
    while [ "x${clp_x_array[x_idx]}" != "x" ]
    do
      clp_x=${clp_x_array[x_idx]}

      clip_cmd="-cliprect ${clp_x},${clp_y}-310x230"

      # Describe what this test will be testing
      tag=clip-offset-${clp_x}-${clp_y}

      Do-Dither-Types

      x_idx=$(( $x_idx + 1 ))
    done

    y_idx=$(( $y_idx + 1 ))
  done
}


###############################################################################
#
# Create the reference images to comare the rest of the images against.
#
function Create-Reference-Images() {

  # Do a full surface dither from source to the reference image
  src1_rc_dim=300x220
  dest_rc_dim=${src1_rc_dim}

  # Use the exact same source rectangle for every test.
  src1_rect="-src1rect 0,0-${src1_rc_dim}"
  dest_rect="-dstrect  0,0-${dest_rc_dim}"

  # No clipping.
  clip_cmd=

  # Describe what these images are.
  tag=ref

  # Generate the reference files.
  Do-Dither-Types

}


###############################################################################
###############################################################################
###############################################################################
#
#
#      Helper functions for doing the Blend-Dither tests
#
#
###############################################################################
###############################################################################
###############################################################################



#############################################################################
#
# Test the dithered file against the reference file
#
function Validate-Blend-File() {

  # Create a CRC for the file we just wrote.
  if [ "$android" = "1" ]
  then
      ${crc} ${destfile}.tga \>  ${destfile}.crc
  else
      ${crc} ${destfile}.tga > ${destfile}.crc
  fi

  # Before we can compare them, we need to extract equivelent rectangles
  # from both so we are comparing the same thing. 

  if [ "${validate_type}" = "dest" ]
  then
    # For this test, we extract a rectangle from the destination file, which we
    # expect to match the reference file.

    # Blit the destination rectangle from the destination image to its own file.
    test_cmd="${bvt} -src1 ${destfmt} ${dest_file_dim} -src1file ${destfile}.tga -src1rect ${dest_x},${dest_y}-${dest_rc_dim}  -dst ${destfmt} ${dest_file_dim} -dstrect 0,0-${dest_rc_dim} -dstfile ${destfile}-temp.tga  ${clip_cmd} -rop CCCC  ${bvt_ret}"
  else
    # For this test, we extract a rectangle from the reference file, which we
    # expect to match the file we are testing

    # Clip out an identical rectangle from the reference image to its own file.
    test_cmd="${bvt} -src1 ${destfmt} ${dest_file_dim} -src1file ${outputpath}/blend-${blendop}-dither-${dithertype}-ref-${tag2}-${src1fmt}-to-${destfmt}-${dest_file_dim}.tga -src1rect ${dest_x},${dest_y}-${dest_rc_dim} -dst ${destfmt} ${dest_file_dim} ${dest_rect}  ${clip_cmd} -dstfile ${destfile}-temp.tga ${clip_cmd} -rop CCCC  ${bvt_ret}"
  fi

  if [ "$vb" = "1" ]
  then
      echo Creating validation image
      echo ${test_cmd}
  fi
  ${test_cmd}

  # Test against the reference file.
  if [ "${validate_type}" = "dest" ]
  then
    file1=${destfile}-temp.tga
    file2=${outputpath}/blend-${blendop}-dither-${dithertype}-ref-${tag2}-${src1fmt}-to-${destfmt}-${dest_file_dim}.tga
  else
    file1=${destfile}-temp.tga
    file2=${destfile}.tga
  fi

  # if we are running on Android, we need to pull the files to the host for testing.
  if [ "${android}" = "1" ]
  then
    pushd this_run/${outputdir} > /dev/null
    ${pull} ${file1} 2> /dev/null
    ${pull} ${file2} 2> /dev/null
    popd > /dev/null
  fi

  diff -q this_run/${outputdir}/$(basename "$file1") this_run/${outputdir}/$(basename "$file2")
  retcode=$?

  # if it failed, rename the destination file in such a way that we can see
  # there was a failure.
  if [ "$retcode" -ne "0" ]
  then
    echo ERROR: Blend Error for ${destfile}.tga
    ${move} ${destfile}.tga ${destfile}-compare-failed.tga
  fi

  # remove the temporary file to save disk space.
  ${remove} ${destfile}-temp.tga
  if [ "${android}" = "1" ]; then
      # remove our local copy as well
      rm this_run/${outputdir}/*-temp.tga
  fi

  if [ "$no_tga" = "1" ]; then
    ${remove} ${destfile}*.tga
  fi

}


###############################################################################
# Do the blend
# All parameters are set up.  Build the command strings and issue the command
function Blend-File() {

  # set up src 1
  src1_rect="-src1rect ${src1_x},${src1_y}-${src1_rc_dim}"
  src1filename=${quote}${inputpath}/input-${src1fmt}-${src_file_dim}\(${src1stride}\).raw${quote}
  src1file="${src1filename} ${src1_rect}"
  src1filecmd="-src1file ${src1file}"
  
  # src2 is the same as the dest.
  src2file=
  src2filecmd="-src2 ${destfmt} dst"

  # no mask
  maskfilecmd=

  # set up the dest 
  dest_rect="-dstrect ${dest_x},${dest_y}-${dest_rc_dim}"
  destfile=${outputpath}/blend-${blendop}-dither-${dithertype}-${tag}-${tag2}-${src1fmt}-to-${destfmt}-${dest_file_dim}
  destfilecmd="-dst ${destfmt} ${dest_file_dim} ${dest_rect}  -dstfile ${destfile}.tga"

  # remove previous test results if they exist
  if [ "${android}" = "1" ]
  then
    "${remove} ${destfile}.tga" 2> /dev/null
    "${remove} ${destfile}-unsupported.tga"  2> /dev/null
    "${remove} ${destfile}-segfault.tga"  2> /dev/null
    "${remove} ${destfile}.crc"  2> /dev/null
  fi
  rm ${here}/${destfile}.tga 2> /dev/null
  rm ${here}/${destfile}-unsupported.tga  2> /dev/null
  rm ${here}/${destfile}-segfault.tga  2> /dev/null
  rm ${here}/${destfile}.crc  2> /dev/null

  echo Blending with dither ${dithertype} from ${src1fmt} to ${destfmt} for ${tag}-${tag2}

  testcmd="${bvt} ${blendcmd} ${dithercmd} ${src1filecmd} ${src2filecmd} ${maskfilecmd} ${destfilecmd}  ${clip_cmd}  ${bvt_ret}"
  if [ "$vb" = "1" ]
  then
      echo ${testcmd}
  fi
  ${testcmd}

  # If dither failed, print a warning. Also create a dummy file, so as
  # to immediately call attention to the error, to whatever person (or
  # script) is examining the output.
  retcode=$?
  if [ "$android" = "1" ]
  then
    retcode=`adb shell cat -v ${targetdir}/retval | sed -e s/[^0-9]//g `
  fi
  # Check for android system error
  if [ "$retcode" = "" ]; then
        echo
        echo Device has gone offline. Waiting for device to return.
        echo
        retcode=255
        adb wait-for-device
        adb root
        adb wait-for-device
        adb remount
  fi

  if [ "$retcode" = "139" ]
  then
    echo ERROR: Segfault in dither from ${src1fmt} to ${destfmt}
    ${touch} ${destfile}-segfault.tga
  else
    if [ "$retcode" -ne "0" ]
    then
      echo ERROR: Cannot dither from ${src1fmt} to ${destfmt}
      ${touch} ${destfile}-unsupported.tga
    fi
  fi

  # Now validate the file we just made.
  if [ "$do_validate" = "yes" ]
  then
      Validate-Blend-File
  fi

}

###############################################################################
# Already set up: clipping, src rect, dest rect. Dither. Blend op.
# This function sets up: dest fmt, src fmt, src stride
# Next function in the chain: Do the blend
function Setup-Blend-Surface-Formats-Helper() {

  srcidx=0
  while [ "x${src1fmts[srcidx]}" != "x" ]
  do
    src1fmt=${src1fmts[srcidx]}
    src1stride=${src1strides[srcidx]}
    destidx=0
    while [ "x${destfmts[destidx]}" != "x" ]
    do
      destfmt=${destfmts[destidx]}

      Blend-File

      destidx=$(( $destidx + 1 ))
    done

    srcidx=$(( $srcidx + 1 ))
  done
}
function Setup-Blend-Surface-Formats() {

  src1fmts=(  RGBA24 )
  src1strides=( 1280  )
  destfmts=(  RGBx24 RGBA24  )
  Setup-Blend-Surface-Formats-Helper

  src1fmts=(    RGBx24 RGBA24 )
  src1strides=( 1280   1280   )
  destfmts=( RGB16  )
  Setup-Blend-Surface-Formats-Helper

}



#############################################################################
# Already set up: clipping, src rect, dest rect, blend operation
# This function sets up: dither
# Next function in the chain: Surface formats
function Setup-Blend-Dither() {

  #Types of dithering we could try.
  #dithertypes=( fastest fastest_on fastest_random fastest_ordered fastest_diffused fastest_photo fastest_drawing )
  #dithertypes=( good    good_on    good_random    good_ordered    good_diffused    good_photo    good_drawing )
  #dithertypes=( better  better_on  better_random  better_ordered  better_diffused  better_photo  better_drawing )
  #dithertypes=( best    best_on    best_random    best_ordered    best_diffused    best_photo    best_drawing )
  #dithertypes=( none    ordered2x2 ordered_4x4    ordered_2x2_4x4 )

  # CPU lib only supports this dithering.
  dithertypes=( none ordered_2x2_4x4 )

  dither_idx=0
  while [ "x${dithertypes[dither_idx]}" != "x" ]
  do
      dithertype=${dithertypes[dither_idx]}

      dithercmd="-dither ${dithertype}"

      Setup-Blend-Surface-Formats

      dither_idx=$(( $dither_idx + 1 ))
  done
}


###############################################################################
# Already set up: clipping, src rect, dest rect.
# This function sets up: blend operation
# Next function in the chain: dither
#
function Setup-Blend-Op() {

  # set up the blend type
  blendop=src1over
  alpha=
  blendcmd="-blendtype ${blendop} ${alpha}"

  Setup-Blend-Dither

}

###############################################################################
# Already set up: clipping, src x-y, dest x-y
# This function sets up: src and dest rectangles
# Next function in the chain: Blend op
#
function Setup-Blend-Scaling() {

  # Source rectangles are all the size of the source images
  src1_rc_dim=${src_file_dim}

  # there are 6 levels of scaling to test.

  # No scaling, dest file same size as dest rect.
  dest_rc_dim=${src1_rc_dim}
  dest_file_dim=${dest_file_dim_no_scale}
  tag2=no_scale
  Setup-Blend-Op

  # scale up
  dest_rc_dim=${rc_dim_scale_up}
  dest_file_dim=${file_dim_scale_up}
  tag2=scale_up
  Setup-Blend-Op

  # 0.5x-1.0x
  dest_rc_dim=${rc_dim_scale_A}
  dest_file_dim=${file_dim_scale_A}
  tag2=scale_up_A
  Setup-Blend-Op

  # 0.33x-0.5x
  dest_rc_dim=${rc_dim_scale_B}
  dest_file_dim=${file_dim_scale_B}
  tag2=scale_up_B
  Setup-Blend-Op

  # 0.25x-0.33x
  dest_rc_dim=${rc_dim_scale_C}
  dest_file_dim=${file_dim_scale_C}
  tag2=scale_up_C
  Setup-Blend-Op

  # under 0.25x
  dest_rc_dim=${rc_dim_scale_D}
  dest_file_dim=${file_dim_scale_D}
  tag2=scale_up_D
  Setup-Blend-Op

}

###############################################################################
# Entry point for destination offset tests.
# Sets Up: clipping, Source and destination points.
# Passes control to:  scaling (source and dest rect) setup.
#
function Do-Blend-Dest-Tests () {

  # No clipping used for destination offset validation
  clip_cmd=

  # Always use source point 0,0
  src1_x=0
  src1_y=0

  # cut back on the test range a bit to speed things up.
  dest_x_array=( 0 1 2 3 )
  dest_y_array=( 0 1 2 3 )

  y_idx=0
  while [ "x${dest_y_array[y_idx]}" != "x" ]
  do
    dest_y=${dest_y_array[y_idx]}

    x_idx=0
    while [ "x${dest_x_array[x_idx]}" != "x" ]
    do
      dest_x=${dest_x_array[x_idx]}

      dest_rect="-dstrect ${dest_x},${dest_y}-${dest_rc_dim}"

      # Describe what this test will be testing
      tag=dest-offset-${dest_x}-${dest_y}

      Setup-Blend-Scaling

      x_idx=$(( $x_idx + 1 ))
    done

    y_idx=$(( $y_idx + 1 ))
  done

}




###############################################################################
#
#  Vary the clipping rectangle across the destination.
#
function Do-Blend-Clip-Tests () {

  # Always use source point 0,0
  src1_x=0
  src1_y=0

  # No dest offset
  dest_x=0
  dest_y=0

  # cut back on the test range a bit to speed things up.
  clp_x_array=( 0 1 2 3 )
  clp_y_array=( 0 1 2 3 )


  y_idx=0
  while [ "x${clp_y_array[y_idx]}" != "x" ]
  do
    clp_y=${clp_y_array[y_idx]}

    x_idx=0
    while [ "x${clp_x_array[x_idx]}" != "x" ]
    do
      clp_x=${clp_x_array[x_idx]}

      clip_cmd="-cliprect ${clp_x},${clp_y}-310x230"

      # Describe what this test will be testing
      tag=clip-offset-${clp_x}-${clp_y}

      Setup-Blend-Scaling

      x_idx=$(( $x_idx + 1 ))
    done

    y_idx=$(( $y_idx + 1 ))
  done

}


###############################################################################
#
# Create the reference images to comare the rest of the images against.
#
function Create-Blend-Reference-Images() {

  # No source offset
  src1_x=0
  src1_y=0

  # No dest offset
  dest_x=0
  dest_y=0

  # No clipping.
  clip_cmd=

  # Describe what these images are.
  tag=ref

  # Generate the reference files.
  Setup-Blend-Scaling

}




###############################################################################
###############################################################################
###############################################################################
#
#
#                   Setup and execute the tests
#
#
###############################################################################
###############################################################################
###############################################################################

# Create the reference images.  No need (or way) to validate them, so turn
# off validation.
echo
echo Creating reference files.
do_validate=no
Create-Reference-Images
Create-Blend-Reference-Images

# Turn validation back on.  Now images generated will be compared to the
# reference images.
do_validate=yes

# The validate function needs to know what type of validation to do:
# We can do destination offset testing or clip offset testing.
# We are going to run the destination offset tests first.
validate_type=dest

# run the destination offset tests
echo
echo Executing destination offset tests.
Do-Destination-Tests

echo
echo Executing blend destination offset tests.
Do-Blend-Dest-Tests

# set up and run the clipping tests.
validate_type=clip

echo
echo Executing clip offset tests.
Do-Clip-Tests

echo
echo Executing blendclip offset tests.
Do-Blend-Clip-Tests


echo Tests complete.
echo




##########################################################################
# Retrieve the results
#
if [ "$android" = "1" ]
then
  # Create a local directory to store the results
  mkdir -p    this_run
  mkdir -p    this_run/${outputdir}

  # Remove results from previous tests
  rm this_run/${outputdir}/*

  # Pull the results from the device to here.
  pushd this_run/${outputdir} > /dev/null
  ${pull} ${outputpath}
  popd > /dev/null

  # Delete the CRC and reference files on the device since we don't need them anymore.
  if [ "$no_tga" = "1" ]; then
      adb shell rm ${outputpath}/*.crc
      adb shell rm ${outputpath}/*-ref-*.tga
  fi

fi

pushd ${here}/${outputpath} > /dev/null
  rm *ref*
popd > /dev/null

