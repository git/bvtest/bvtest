// Test BLTsville text

#include "bvtext.h"

extern "C" void BVDump(const char* prefix, const char* tab, const struct bvbltparams* parms);
extern "C" void BVGeomDump(char*               cmdline,
                           const char*         surf,
                           const char*         prefix,
                           const char*         tab,
                           struct bvsurfgeom*  pgeom,
                           const struct bvbltparams* parms);

template<class T>
  const T& max(const T& a,
               const T& b)
  {
    return((a > b) ? a : b);
  }

template<class T>
  const T& min(const T& a,
               const T& b)
  {
    return((a < b) ? a : b);
  }

template<class T>
  const T abs(const T& v)
  {
    if(v < 0)
      return(-v);
    else
      return(v);
  }

#define DODS (void) // ODS
// #define SIMULATE
// #define OFFSCREEN32

int ghscrn = -1;
const char DEFSCRNDEV[] = "/dev/fb0";
const char* scrndev = NULL;

#define BGRx32toRGB16(bgrx32) (static_cast<unsigned short>(bgrx32 >> 8) & 0xF800) | \
                              (static_cast<unsigned short>(bgrx32 >> 5) & 0x07E0) | \
                              (static_cast<unsigned short>(bgrx32 >> 3) & 0x001F)

const char* fontfile = NULL;
const char* bkgndfile = NULL;
const char* textfile = NULL;

void PrintVersion()
{
  ODS("bvtext - version 1.0");
}

struct bvbuffdesc scrnbuff = { sizeof(struct bvbuffdesc), 0 };
struct bvbuffdesc fontbuff = { sizeof(struct bvbuffdesc), 0 };
struct bvbuffdesc bkgndbuff = { sizeof(struct bvbuffdesc), 0 };
struct bvsurfgeom scrngeom = { sizeof(struct bvsurfgeom), OCDFMT_UNKNOWN, 0 };
struct bvsurfgeom fontgeom = { sizeof(struct bvsurfgeom), OCDFMT_UNKNOWN, 0 };
struct bvsurfgeom bkgndgeom = { sizeof(struct bvsurfgeom), OCDFMT_UNKNOWN, 0 };

bool verbose = false;

int offscrn = 0; // 0 = onscreen; 16 = 16 bpp offscreen; 32 = 32 bpp offscreen; etc.

#define VODS(...) if(!verbose) ; else ODS(__VA_ARGS__)

#define SAFEFREE(ptr) if(ptr) { free(ptr); ptr = 0; }
#define SAFESTBIFREE(ptr) if(ptr) { stbi_image_free(ptr); ptr = 0; }

timespec res_realtime = {0};
timespec res_monotonic = {0};
timespec res_process_cputime = {0};
timespec res_thread_cputime = {0};

struct bltsvillelib
{
  const char* libname;
  void* hlib;
  BVFN_MAP bv_map;
  BVFN_BLT bv_blt;
  BVFN_UNMAP bv_unmap;
};

struct bltsvillelib bvlib[] =
{
  { "bltsville_cpu", 0 },
  { "bltsville_2d", 0 }
};
#define NUMLIBS (sizeof(bvlib) / sizeof(struct bltsvillelib))

enum bvlibid
{
  BV_CPU = 0,
  BV_2D = 1
};

void Usage(const char* szapp)
{
  PrintVersion();
  ODS("USAGE:  %s", szapp);
}

#if 0
bool InitBackgroundColorSurface()
{
  DODS("%s() entry.\n", __func__);
  bkgndclrsurf.structsize = sizeof(bkgndclrsurf);
  bkgndclrsurf.format = scrnsurf.format;
  bkgndclrsurf.width = bkgndclrsurf.height = 1;
//  bkgndclrrgb16 = BGRx32toRGB16(bkgndclr);
  bkgndclrsurf.virtptr = (bkgndclrsurf.format == TIBLT_FMT_RGB16) ?
                         static_cast<void*>(&bkgndclrrgb16) :
                         const_cast<void*>(static_cast<const void*>(&bkgndclr));
  DODS("Background color = 0x%08X, 16bpp = 0x%04X", bkgndclr, bkgndclrrgb16);
  DODS("%s() exit.\n", __func__);
  return(true);
}

bool InitScreenSurface()
{
  bool success = false;

  if(offscrn)
  {
    scrnsurf.structsize = sizeof(scrnsurf);
    switch(offscrn)
    {
      case 16:
        scrnsurf.format = TIBLT_FMT_RGB16;
        scrnsurf.virtstride = 800 * 2;
        break;
        
      case 32:
        scrnsurf.format = TIBLT_FMT_BGR132;
        scrnsurf.virtstride = 800 * 4;
        break;

      default:
        ODS("Unsupported offscreen pixel depth.\n");
        goto Error;
    }
    scrnsurf.width = 800;
    scrnsurf.height = 480;
    scrnsurf.virtptr = malloc(scrnsurf.virtstride * scrnsurf.height);
    if(!scrnsurf.virtptr)
    {
      ODS("%s(): Error allocating offscreen framebuffer memory.\n", __func__);
      goto Error;
    }
    VODS("Framebuffer successfully allocated (0x%08X).\n", scrnsurf.virtptr);

    scrnrect.left = scrnrect.top = 0;
    scrnrect.width = scrnsurf.width;
    scrnrect.height = scrnsurf.height;
  }
  else
  {
    if(!scrndev)
    {
      VODS("Device not specified on command line, checking FRAMEBUFFER environment variable.\n");
      scrndev = getenv("FRAMEBUFFER");
    }

    if(!scrndev)
    {
      VODS("Device not specified in FRAMEBUFFER environment variable, using default: %s.\n", DEFSCRNDEV);
      scrndev = DEFSCRNDEV;
    }

    VODS("Attempting to open framebuffer device: %s.\n", scrndev);
#ifndef SIMULATE
    ghscrn = open(scrndev, O_RDWR);
#else
    ghscrn = 4;
#endif // SIMULATE
    if(ghscrn == -1)
    {
      ODS("%s(): Error opening screen device (%s): %d.  Please provide the correct one with the -device argument.\n",
          __func__,
          scrndev,
          errno);
      goto Error;
    }
    VODS("Framebuffer device %s successfully opened.\n", scrndev);

    struct fb_fix_screeninfo fb_fixinfo;
    struct fb_var_screeninfo fb_varinfo;

    VODS("Attempting to get framebuffer device info.\n");
#ifndef SIMULATE
    if(ioctl(ghscrn, FBIOGET_FSCREENINFO, &fb_fixinfo))
    {
      ODS("%s(): Error getting fb_fixinfo: %d.\n", __func__, errno);
      goto Error;
    }

    if(ioctl(ghscrn, FBIOGET_VSCREENINFO, &fb_varinfo))
    {
      ODS("%s(): Error gettting fb_varinfo: %d.\n", __func__, errno);
      goto Error;
    }
#else
    fb_varinfo.bits_per_pixel = 16;
    fb_varinfo.xres_virtual = 800;
    fb_varinfo.yres_virtual = 480;
    fb_fixinfo.line_length = 1600;
#endif // SIMULATE
    VODS("Successfully obtained framebuffer info:");
    VODS("fb_fix_screeninfo:");
    VODS("\tsmem_start = 0x%08X", fb_fixinfo.smem_start);
    VODS("\tsmem_len = %d", fb_fixinfo.smem_len);
    VODS("\ttype = %s (%d)", (fb_fixinfo.type == FB_TYPE_PACKED_PIXELS) ? "packed pixels" :
                              ((fb_fixinfo.type == FB_TYPE_PLANES) ? "non-interleaved planes" :
                              ((fb_fixinfo.type == FB_TYPE_INTERLEAVED_PLANES) ? "interleaved planes" :
                              ((fb_fixinfo.type == FB_TYPE_TEXT) ? "text" :
                              ((fb_fixinfo.type == FB_TYPE_VGA_PLANES) ? "EGA/VGA planes" : "unknown")))),
                              fb_fixinfo.type);
    VODS("\ttype_aux = %d", fb_fixinfo.type_aux);
    VODS("\tvisual = %s (%d)", (fb_fixinfo.visual == FB_VISUAL_MONO01) ? "reverse monochrome" :
                                ((fb_fixinfo.visual == FB_VISUAL_MONO10) ? "monochrome" :
                                ((fb_fixinfo.visual == FB_VISUAL_TRUECOLOR) ? "true color" :
                                ((fb_fixinfo.visual == FB_VISUAL_PSEUDOCOLOR) ? "pseudo color" :
                                ((fb_fixinfo.visual == FB_VISUAL_DIRECTCOLOR) ? "direct color" :
                                ((fb_fixinfo.visual == FB_VISUAL_STATIC_PSEUDOCOLOR) ? "read-only pseudo color" : "unknown"))))),
                                fb_fixinfo.visual);
    VODS("\txpanstep = %d", fb_fixinfo.xpanstep);
    VODS("\typanstep = %d", fb_fixinfo.ypanstep);
    VODS("\tywrapstep = %d", fb_fixinfo.ywrapstep);
    VODS("\tline_length = %d", fb_fixinfo.line_length);
    VODS("\tmmio_start = 0x%08X", fb_fixinfo.mmio_start);
    VODS("\tmmio_len = %d", fb_fixinfo.mmio_len);
    VODS("\taccel = %d", fb_fixinfo.accel);
    VODS("fb_var_screeninfo:");
    VODS("\txres, yres: %d, %d", fb_varinfo.xres, fb_varinfo.yres);
    VODS("\txres_virtual, yres_virtual: %d, %d", fb_varinfo.xres_virtual, fb_varinfo.yres_virtual);
    VODS("\txoffset, yoffset: %d, %d", fb_varinfo.xoffset, fb_varinfo.yoffset);
    VODS("\tbits_per_pixel: %d", fb_varinfo.bits_per_pixel);
    VODS("\tgrayscale: %s", (fb_varinfo.grayscale == 0) ? "false" : "true");
    VODS("\tmore...\n");

    scrnsurf.structsize = sizeof(scrnsurf);
    switch(fb_varinfo.bits_per_pixel)
    {
      case 16:
        scrnsurf.format = TIBLT_FMT_RGB16;
        break;

      case 24:
        scrnsurf.format = TIBLT_FMT_BGR24;
        break;

      case 32:
        scrnsurf.format = TIBLT_FMT_BGR132;
        break;

      default:
        ODS("%s(): Unsupported screen depth (%d).\n", __func__, fb_varinfo.bits_per_pixel);
        goto Error;
    }
    scrnsurf.width = fb_varinfo.xres_virtual;
    scrnsurf.height = fb_varinfo.yres_virtual;
    scrnsurf.virtstride = fb_fixinfo.line_length;
    VODS("Mapping framebuffer into user space.\n");
#ifndef SIMULATE
    scrnsurf.virtptr = mmap(0,
                            scrnsurf.virtstride * scrnsurf.height,
                            PROT_WRITE | PROT_READ,
                            MAP_SHARED,
                            ghscrn,
                            0);
#else
    scrnsurf.virtptr = (void*)4;
#endif // SIMULATE
    if(scrnsurf.virtptr == MAP_FAILED)
    {
      ODS("%s(): Error mapping framebuffer into user space: %d.\n", __func__, errno);
      goto Error;
    }
    VODS("Framebuffer successfully mapped into user space (0x%08X).\n", scrnsurf.virtptr);

    scrnrect.left = scrnrect.top = 0;
    scrnrect.width = scrnsurf.width;
    scrnrect.height = scrnsurf.height;
  }
  
  success = true;
Error:
  if(!success)
  {
    if(offscrn)
    {
      if(scrnsurf.virtptr)
        free(scrnsurf.virtptr);
    }
    else
    {
      if(scrnsurf.virtptr)
        munmap(scrnsurf.virtptr, scrnsurf.virtstride * scrnsurf.height);

      if(ghscrn)
        close(ghscrn);
    }
  }
  return(success);
}

bool LoadAndConvertFile(TIBLTSURF&  surf,
                        const char* file)
{
  VODS("%s(): Loading %s.\n", __func__, file);

  bool success = false;
  TIBLTSURF tmpsurf = {0};
  TIBLTPARAMS newparms = {0};
  
  int comp, width, height;
  unsigned char* ptr = stbi_load(file, &width, &height, &comp, 0);
  tmpsurf.virtptr = reinterpret_cast<void*>(ptr);
  if(!tmpsurf.virtptr)
  {
    ODS("%s(): Error loading %s: %s", __func__, file, stbi_failure_reason());
    goto Error;
  }
  DODS("%s(): Image file buffer: 0x%08X.\n", __func__, tmpsurf.virtptr);
  tmpsurf.width = width;
  tmpsurf.height = height;
  VODS("Image file %s is %d x %d x %d", file, tmpsurf.width, tmpsurf.height, comp);
  
  switch(comp)
  {
    case 1: tmpsurf.format = TIBLT_FMT_MONO8; break;
    case 2: tmpsurf.format = TIBLT_FMT_RGB16; break; // @@@ Not the right format!
    case 3: tmpsurf.format = TIBLT_FMT_RGB24; break;
    // If alpha is involved, assume the premultiply type of the file matches the caller's format
    case 4:
      if(((surf.format & TIBLTDEF_FMT_CS_MASK) == TIBLTDEF_FMT_RGB) &&
         (surf.format & TIBLTDEF_FMT_ALPHA) &&
         (surf.format & TIBLTDEF_FMT_NONPREMULT))
        tmpsurf.format = TIBLT_FMT_RGBA32_NONPREMULT;
      else
        tmpsurf.format = TIBLT_FMT_RGBA32;
      break;
  }

  tmpsurf.virtstride = tmpsurf.width * comp;

  surf.width = tmpsurf.width;
  surf.height = tmpsurf.height;
  surf.virtstride = surf.width * (((surf.format & TIBLTDEF_FMT_CONTAINER_MASK) >> TIBLTDEF_FMT_CONTAINER_SHIFT) + 1);
  surf.virtptr = malloc(surf.height * surf.virtstride);
  if(!surf.virtptr)
  {
    ODS("%s(): Error allocating surface memory.\n", __func__);
    goto Error;
  }
  DODS("%s(): Image buffer allocated: 0x%08X.\n", __func__, surf.virtptr);

  newparms.structsize = sizeof(newparms);
  newparms.flags = TIBLT_FLAG_ROP;
#ifdef LIBFLAG
  newparms.flags = TIBLT_FLAG_ROP | LIBFLAG;
#endif
  newparms.ROP = 0xCCCC;
  newparms.pdst = &surf;
  newparms.dstrect.left = newparms.dstrect.top = 0;
  newparms.dstrect.width = surf.width;
  newparms.dstrect.height = surf.height;
  newparms.psrc = &tmpsurf;
  newparms.srcrect = newparms.dstrect;

  VODS("Copying %s file contents to TIBLT surface.\n", file);
//  DumpParams(&parms);
  {
#ifndef SIMULATE
    TIBLTERROR err = TIBLT(&newparms);
#else
    TIBLTERROR err = TIBLT_ERR_NONE;
#endif // SIMULATE
    if(err != TIBLT_ERR_NONE)
    {
      const char* errorstring;
      ErrorToText(errorstring, err);
      ODS("%s(): Error copying source image from file %s to surface: %s.\n",
          __func__,
          file,
          errorstring);
      goto Error;
    }
  }
  VODS("%s(): %s successfully copied to surface.\n", __func__, file);
  
  success = true;
Error:
  SAFESTBIFREE(tmpsurf.virtptr);
  if(!success)
  {
    SAFEFREE(surf.virtptr);      
  }
  return(success);
}

bool InitBackgroundSurface()
{
  DODS("%s() entry.\n", __func__);
  bool success;
  bkgndsurf.structsize = sizeof(bkgndsurf);
  bkgndsurf.format = TIBLT_FMT_BGR132;
  success = LoadAndConvertFile(bkgndsurf, bkgndfile);
//  bkgndsurf.format = TIBLT_FMT_BGRA32;

  bkgndsrcrect.left = 0;
  bkgndsrcrect.top = 0;
  bkgndsrcrect.width = scrnsurf.width;
  bkgndsrcrect.height = scrnsurf.height;

  if((bkgndsurf.width < scrnsurf.width) ||
     (bkgndsurf.height < scrnsurf.height))
  {
    ODS("Background image is smaller than the screen.  Please use a larger background.\n");
    success = false;
    goto Error;
  }

Error:
  DODS("%s() exit.\n", __func__);
  return(success);
}

bool InitForegroundSurface()
{
  DODS("%s() entry.\n", __func__);
  bool success;
  foregndsurf.structsize = sizeof(foregndsurf);
  foregndsurf.format = TIBLT_FMT_BGRA32_NONPREMULT;
  success = LoadAndConvertFile(foregndsurf, foregndfile);
  DODS("%s() exit.\n", __func__);
  return(success);
}

bool InitImageSurfaces()
{
  DODS("%s() entry.\n", __func__);
  bool success;
  for(int i = 0; i < IMAGES; i++)
  {
    imagesurf[i].structsize = sizeof(imagesurf[0]);
    imagesurf[i].format = TIBLT_FMT_BGRA32_NONPREMULT;
    success = LoadAndConvertFile(imagesurf[i], imagefile[i]);
    if(!success)
      break;
    if(i == 0)
    {
      imagedstrect[i].left = imagedstrect[i].top = 0;
    }
    else
    {
      imagedstrect[i].left = scrnsurf.width / 2l;
      imagedstrect[i].top = scrnsurf.height / 2l;
    }
    imagedstrect[i].width = imagesurf[i].width;
    imagedstrect[i].height = imagesurf[i].height;
  }
  DODS("%s() exit.\n", __func__);
  return(success);
}

bool Init()
{
  DODS("%s() entry.\n", __func__);
  bool success = false;

#ifndef SIMULATE
  ghTICPUBLT = OSLoadLibrary("TICPUBLT");
#else
  ghTICPUBLT = (TIBLTLIBHANDLE)4;
#endif // SIMULATE
  if(!ghTICPUBLT)
  {
    ODS("%s(): Error opening TICPUBLT library.\n", __FUNCTION__);
    goto Error;
  }

#ifndef SIMULATE
  TIBLT = (PTIBLT)OSGetProcAddress(ghTICPUBLT, "TIBLT");
#else
  TIBLT = (PTIBLT)4;
#endif // SIMULATE
  if(!TIBLT)
  {
    ODS("%s(): Error importing TIBLT entry point from TICPUBLT library.\n", __FUNCTION__);
    goto Error;
  }

clock_getres(CLOCK_REALTIME, &res_realtime);
ODS("%s(): res_realtime.tv_sec = %d, res_realtime.tv_nsec = %d.\n", __FUNCTION__, res_realtime.tv_sec, res_realtime.tv_nsec);
clock_getres(CLOCK_REALTIME, &res_monotonic);
VODS("Clock precision is %d nS.\n", res_monotonic.tv_nsec);
//ODS("%s(): res_monotonic.tv_sec = %d, res_monotonic.tv_nsec = %d.\n", __FUNCTION__, res_monotonic.tv_sec, res_monotonic.tv_nsec);
//clock_getres(CLOCK_REALTIME, &res_process_cputime);
//ODS("%s(): res_process_cputime.tv_sec = %d, res_process_cputime.tv_nsec = %d.\n", __FUNCTION__, res_process_cputime.tv_sec, res_process_cputime.tv_nsec);
//clock_getres(CLOCK_REALTIME, &res_thread_cputime);
//ODS("%s(): res_thread_cputime.tv_sec = %d, res_thread_cputime.tv_nsec = %d.\n", __FUNCTION__, res_thread_cputime.tv_sec, res_thread_cputime.tv_nsec);

  success = true;
Error:
  DODS("%s() exit.\n", __func__);
  return(success);
}

void Cleanup()
{
  if(ghTICPUBLT)
    OSUnloadLibrary(ghTICPUBLT);
}

int testnum = 0;
const int NUMTESTS = 2;
#endif

bool ParseCmdLine(int         argc,
                  const char* argv[])
{
  bool success = false;
  
  for(int arg = 1; arg < argc;)
  {
    if(!stricmp(argv[arg], "-help"))
    {
      Usage(argv[0]);
      goto Error;
    }
    else if(!stricmp(argv[arg], "-verbose"))
    {
      verbose = true;
      VODS("-verbose");
      arg++;
    }
    else if(!stricmp(argv[arg], "-device"))
    {
      arg++;
      if(arg < argc)
      {
        scrndev = argv[arg];
        arg++;
        VODS("-device %s", scrndev);
      }
      else
      {
        ODS("No device specified with -device option.\n");
        goto Error;
      }
    }
#if 0
    else if(!stricmp(argv[arg], "-offscreen"))
    {
      arg++;
      if(arg < argc)
      {
        offscrn = atoi(argv[arg]);
        arg++;
        VODS("-offscreen %d", offscrn);
      }
      else
      {
        ODS("No test number specified with -offscreen option.\n");
      }
    }
    else if(!stricmp(argv[arg], "-test"))
    {
      arg++;
      if(arg < argc)
      {
        testnum = atoi(argv[arg]);
        arg++;
        VODS("-test %d", testnum);
      }
      else
      {
        ODS("No test number specified with -test option.\n");
        goto Error;
      }
    }
#endif
    else
    {
      ODS("Unrecognized option %s", argv[arg]);
      goto Error;
    }
  }
  
  success = true;
Error:
  return(success);
}

#if 0
int PaletteSize(TIBLTFMT format)
{
  int palsize;
  
  switch(format)
  {
    case TIBLT_FMT_LUT1:
    case TIBLT_FMT_LUT1_SYMBIAN:
      palsize = 2;
      break;
      
    case TIBLT_FMT_LUT2:
    case TIBLT_FMT_LUT2_SYMBIAN:
      palsize = 4;
      break;
      
    case TIBLT_FMT_LUT4:
    case TIBLT_FMT_LUT4_SYMBIAN:
      palsize = 16;
      break;
      
    case TIBLT_FMT_LUT8:
      palsize = 256;
      break;

    default:
      palsize = 0;
  }
  return(palsize);
}

int bkgndxmove = -2;
int imagexmove[IMAGES] = {1, -1};

bool CalcRects()
{
#if 1
  int leftbumper = abs(bkgndxmove);
  int rightbumper = bkgndsurf.width - scrnrect.width;

//ODS("%s(): leftbumper = %d, rightbumper = %d.\n", __func__, leftbumper, rightbumper);

//ODS("%s(): bkgndsrcrect = (%d, %d) - %d x %d.\n", __func__, bkgndsrcrect.left, bkgndsrcrect.top, bkgndsrcrect.width, bkgndsrcrect.height);

  if(bkgndsrcrect.left < leftbumper)
  {
//    ODS("%s(): Hit left bumper.\n", __func__);
    bkgndxmove = abs(bkgndxmove);
  }
  else if(bkgndsrcrect.left >= rightbumper)
  {
//    ODS("%s(): Hit right bumper.\n", __func__);
    bkgndxmove = -abs(bkgndxmove);
  }

  bkgndsrcrect.left += bkgndxmove;
//ODS("%s(): bkgndsrcrect = (%d, %d) - %d x %d.\n", __func__, bkgndsrcrect.left, bkgndsrcrect.top, bkgndsrcrect.width, bkgndsrcrect.height);

#else
  int rightbumper = scrnsurf.width / 2;
  int leftbumper = -rightbumper;
  
//ODS("bkgndxmove = %d, bkgnddstrect.left = %d, leftbumper = %d, rightbumper = %d.\n",
//    bkgndxmove, bkgnddstrect.left, leftbumper, rightbumper);

  bkgnddstrect.left += bkgndxmove;
  
  if((bkgnddstrect.left < leftbumper) ||
     (bkgnddstrect.left >= rightbumper))
    bkgndxmove = -bkgndxmove;
#endif

  for(int i = 0; i < IMAGES; i++)
  {
    imagedstrect[i].left += imagexmove[i];
    if(imagedstrect[i].left <= 0)
    {
      imagedstrect[i].left = 0;
      imagexmove[i] = -imagexmove[i];
    }
    if((imagedstrect[i].left + imagedstrect[i].width) >= scrnsurf.width)
    {
      imagedstrect[i].left = scrnsurf.width - imagedstrect[i].width;
      imagexmove[i] = -imagexmove[i];
    }
  }
}

//typedef std::list<TIBLTPARAMS> BLTLIST;
typedef simplelist<TIBLTPARAMS> BLTLIST;

BLTLIST bltlist;

bool Intersection(TIBLTRECT&       rect,
                  const TIBLTRECT& rect1,
                  const TIBLTRECT& rect2)
{
  int rect1right = rect1.left + rect1.width;
  int rect1bottom = rect1.top + rect1.height;
  int rect2right = rect2.left + rect2.width;
  int rect2bottom = rect2.top + rect2.height;

  rect.left = max(rect1.left, rect2.left);
  rect.top = max(rect1.top, rect2.top);
  int rectright = min(rect1right, rect2right);
  int rectbottom = min(rect1bottom, rect2bottom);
  rect.width = rectright - rect.left;
  rect.height = rectbottom - rect.top;

  return((rect.width > 0) && (rect.height > 0));
}

typedef TIBLTRECT* SPLITRECTS[4];

void SplitRect(SPLITRECTS& outrects,
               TIBLTRECT&  oldrect,
               TIBLTRECT&  intersectrect)
{
  DODS("%s() entry.\n", __func__);
  SPLITRECTS tmprects = {outrects[0], outrects[1], outrects[2], outrects[3]};

DODS("oldrect = %d , %d - %d x %d.\n", oldrect.left, oldrect.top, oldrect.width, oldrect.height);
DODS("intersectrect = %d , %d - %d x %d.\n", intersectrect.left, intersectrect.top, intersectrect.width, intersectrect.height);

  int oldrectright = oldrect.left + oldrect.width;
  int oldrectbottom = oldrect.top + oldrect.height;
  int intersectrectright = intersectrect.left + intersectrect.width;
  int intersectrectbottom = intersectrect.top + intersectrect.height;

DODS("oldrectright = %d, oldrectbottom = %d.\n", oldrectright, oldrectbottom);
DODS("intersectrectright = %d, intersectrectbottom = %d.\n", intersectrectright, intersectrectbottom);

  outrects[0] = 0;
  outrects[1] = 0;
  outrects[2] = 0;
  outrects[3] = 0;

  if(oldrect.top < intersectrect.top)
  {
DODS("Top present.\n");
    // top present
    outrects[0] = tmprects[0];
    outrects[0]->left = oldrect.left;
    outrects[0]->top = oldrect.top;
    outrects[0]->width = oldrect.width;
    outrects[0]->height = intersectrect.top - oldrect.top;
DODS("Top rect = %d , %d - %d x %d.\n", outrects[0]->left, outrects[0]->top, outrects[0]->width, outrects[0]->height);
  }

  if(oldrect.left < intersectrect.left)
  {
DODS("Left present.\n");
    // left present
    outrects[1] = tmprects[1];
    outrects[1]->left = oldrect.left;
    outrects[1]->top = (oldrect.top > intersectrect.top) ? oldrect.top : intersectrect.top;
    outrects[1]->width = intersectrect.left - oldrect.left;
    int rectbottom = (oldrectbottom < intersectrectbottom) ? oldrectbottom : intersectrectbottom;
    outrects[1]->height = rectbottom - outrects[1]->top;
DODS("Left rect = %d , %d - %d x %d.\n", outrects[1]->left, outrects[1]->top, outrects[1]->width, outrects[1]->height);
  }

  if(oldrectright > intersectrectright)
  {
DODS("Right present.\n");
    // right present
    outrects[2] = tmprects[2];
    outrects[2]->left = intersectrectright;
    outrects[2]->top = (oldrect.top > intersectrect.top) ? oldrect.top : intersectrect.top;
    outrects[2]->width = oldrectright - intersectrectright;
    int rectbottom = (oldrectbottom < intersectrectbottom) ? oldrectbottom : intersectrectbottom;
    outrects[2]->height = rectbottom - outrects[2]->top;
DODS("Right rect = %d , %d - %d x %d.\n", outrects[2]->left, outrects[2]->top, outrects[2]->width, outrects[2]->height);
  }

  if(oldrectbottom > intersectrectbottom)
  {
DODS("Bottom present.\n");
    // bottom present
    outrects[3] = tmprects[3];
    outrects[3]->left = oldrect.left;
    outrects[3]->top = intersectrectbottom;
    outrects[3]->width = oldrect.width;
    outrects[3]->height =  oldrectbottom - intersectrectbottom;
DODS("Bottom rect = %d , %d - %d x %d.\n", outrects[3]->left, outrects[3]->top, outrects[3]->width, outrects[3]->height);
  }

  DODS("%s() exit.\n", __func__);
}

void DumpList()
{
  DODS("bltlist.size() = %d.\n", bltlist.size());
  int idx = 0;
  for(BLTLIST::iterator i = bltlist.begin(); i != bltlist.end(); i++)
  {
    DODS("Entry %d:", idx);
    DumpParams(&(*i));
    idx++;
  }
}

void AddBackgroundColor()
{
  DODS("%s() entry.\n", __func__);
  bltlist.clear();
  TIBLTPARAMS newparms = {0};
  newparms.structsize = sizeof(newparms);
  newparms.flags = TIBLT_FLAG_ROP;
#ifdef LIBFLAG
  newparms.flags = TIBLT_FLAG_ROP | LIBFLAG;
#endif
  newparms.ROP = 0xF0F0;
  newparms.pdst = &scrnsurf;
  newparms.dstrect = scrnrect;
  newparms.pbrush = &bkgndclrsurf;
  newparms.brushrect.left = 0;
  newparms.brushrect.top = 0;
  newparms.brushrect.width = 1;
  newparms.brushrect.height = 1;
  bltlist.push_front(newparms);
  if(verbose)
    DumpList();
  DODS("%s() exit.\n", __func__);
}

void AddBackground()
{
  DODS("%s() entry.\n", __func__);
  // Get iterator to background color parameter in list (no need to search)
  BLTLIST::iterator i = bltlist.begin();
  // Copy parameters to use in other rectangles
  TIBLTPARAMS bkgndcolorparms = *i;
  // Remove the original rectangle from the list
  bltlist.erase(i);

  TIBLTRECT rects[4];
  SPLITRECTS newrects = {&rects[0], &rects[1], &rects[2], &rects[3]};

  // Split the old rectangle into 1 to 4 rectangles, depending on where the
  // background rectangle sits
  SplitRect(newrects, bkgndcolorparms.dstrect, scrnrect);
  // Put all resulting rectangles into the list
  for(int i = 0; i < 4; i++)
  {
    if(newrects[i])
    {
      bkgndcolorparms.dstrect = *(newrects[i]);
      bltlist.push_front(bkgndcolorparms);
    }
  }
  
  // Add the background rectangle itself
  TIBLTPARAMS newparms = {0};
  newparms.structsize = sizeof(newparms);
  newparms.flags = TIBLT_FLAG_ROP; // | TIBLT_FLAG_CLIP;
#ifdef LIBFLAG
  newparms.flags = TIBLT_FLAG_ROP | LIBFLAG;
#endif
  newparms.ROP = 0xCCCC;
  newparms.pdst = &scrnsurf;
  newparms.dstrect = scrnrect;
  newparms.psrc = &bkgndsurf;
  newparms.srcrect = bkgndsrcrect;
  bltlist.push_front(newparms);

  if(verbose)
    DumpList();
  DODS("%s() exit.\n", __func__);
}

void AddImage(TIBLTPARAMS& imageparms)
{
  DODS("%s() entry.\n", __func__);
  BLTLIST newbltlist;
  
  for(BLTLIST::iterator it = bltlist.begin(); it != bltlist.end(); it++)
  {
    TIBLTRECT intersectrect;
    DODS("%s(): Checking rectangles (%d, %d) - %d x %d vs. (%d, %d) - %d x %d.\n", __func__,
          (*it).dstrect.left, (*it).dstrect.top, (*it).dstrect.width, (*it).dstrect.height,
          imageparms.dstrect.left, imageparms.dstrect.top, imageparms.dstrect.width, imageparms.dstrect.height);
    if(Intersection(intersectrect, (*it).dstrect, imageparms.dstrect))
    {
      DODS("%s(): Intersection found.\n", __func__);
      
      TIBLTRECT rects[4];
      SPLITRECTS splitrects = {&rects[0], &rects[1], &rects[2], &rects[3]};
      TIBLTPARAMS oldBLTparms = *it;
      bltlist.erase(it);

      SplitRect(splitrects, oldBLTparms.dstrect, intersectrect);

      for(int i = 0; i < 4; i++)
      {
        if(splitrects[i])
        {
          bltlist.push_front(oldBLTparms);
//newbltlist.push_front(oldBLTparms);
          TIBLTPARAMS& newparms = bltlist.front();
//TIBLTPARAMS& newparms = newbltlist.front();
          newparms.dstrect = rects[i];
          newparms.srcrect.left = oldBLTparms.srcrect.left + (newparms.dstrect.left - oldBLTparms.dstrect.left);
          newparms.srcrect.top = rects[i].top + newparms.dstrect.top - oldBLTparms.dstrect.top;
          newparms.srcrect.width = newparms.dstrect.width;
          newparms.srcrect.height = newparms.dstrect.height;
        }
      }

imageparms.src2rect.left += bkgndsrcrect.left;
imageparms.src2rect.top += bkgndsrcrect.top;
      bltlist.push_front(imageparms);
//newbltlist.push_front(imageparms);
    }
  }

#if 0
bltlist.erase(it);
for(BLTLIST::iterator it = newbltlist.begin(); it != newbltlist.end(); it++)
{
  bltlist.push_back(*it);
}
#endif

  if(verbose)
    DumpList();
  DODS("%s() exit.\n", __func__);
}

time_t then;

bool PerformBLT()
{
  DODS("%s() entry.\n", __func__);
  TIBLTERROR err = TIBLT_ERR_UNKNOWN;
  
  static unsigned int framecount = 0;
  framecount++;

  time_t now = time(0);
  if(now != then)
  {
    ODS("fps = %d", framecount);
    then = now;
    framecount = 0;
  }

  switch(testnum)
  {
    case 0:
      {
        CalcRects();
        AddBackgroundColor();
        AddBackground();
        TIBLTPARAMS imageparms = {0};
        imageparms.structsize = sizeof(imageparms);
        imageparms.flags = TIBLT_FLAG_BLEND;
#ifdef LIBFLAG
        imageparms.flags = TIBLT_FLAG_ROP | LIBFLAG;
#endif
        imageparms.blendtype = TIBLT_BLEND_SRCOVER;
        imageparms.pdst = &scrnsurf;
        for(int i = 0; i < IMAGES; i++)
        {
          imageparms.dstrect = imagedstrect[i];
          imageparms.psrc = &(imagesurf[i]);
          imageparms.srcrect.left = imageparms.srcrect.top = 0;
          imageparms.srcrect.width = imagesurf[i].width;
          imageparms.srcrect.height = imagesurf[i].height;
          imageparms.psrc2 = &bkgndsurf;
          imageparms.src2rect = imageparms.dstrect;
          AddImage(imageparms);
        }
  
        if(bltlist.empty())
          DODS("Bad sign...the list of BLTs is empty.\n");

        for(BLTLIST::iterator i = bltlist.begin(); i != bltlist.end(); i++)
        {
          DODS("%s(): Performing BLT (0x%08X):", __func__, &(*i));
          // DumpParams(&(*i));
#ifndef SIMULATE
          err = TIBLT(&(*i));
#else
          err = TIBLT_ERR_NONE;
#endif // SIMULATE
          if(err != TIBLT_ERR_NONE)
          {
            const char* errorstring;
            ErrorToText(errorstring, err);
            ODS("%s(): Error performing BLT: %s",
                __func__,
                errorstring);
            DumpParams(&(*i));
            break;
          }
          DODS("%s(): BLT done.\n", __func__);
        }
      }
      break;

    case 1:
      {
        static char alpha = 0x00;
        static char deltaalpha = 1;

static TIBLTRECT bkgndsrcrect = {0, 0, scrnsurf.width, scrnsurf.height};
static TIBLTRECT foregndsrcrect = {0, 0, scrnsurf.width, scrnsurf.height};
static int xbgmove = 1;
static int ybgmove = 2;
static int xfgmove = 2;
static int yfgmove = 1;

  int leftbumper = abs(bkgndxmove);
  int rightbumper = bkgndsurf.width - scrnrect.width;

  if(bkgndsrcrect.left < abs(xbgmove))
    xbgmove = abs(xbgmove);
  else if(bkgndsrcrect.left >= (bkgndsurf.width - scrnrect.width))
    xbgmove = -abs(xbgmove);

  if(bkgndsrcrect.top < abs(ybgmove))
    ybgmove = abs(ybgmove);
  else if(bkgndsrcrect.top >= (bkgndsurf.height - scrnrect.height))
    ybgmove = -abs(ybgmove);

  bkgndsrcrect.left += xbgmove;
  bkgndsrcrect.top += ybgmove;

  if(foregndsrcrect.left < abs(xfgmove))
    xfgmove = abs(xfgmove);
  else if(foregndsrcrect.left >= (foregndsurf.width - scrnrect.width))
    xfgmove = -abs(xfgmove);

  if(foregndsrcrect.top < abs(yfgmove))
    yfgmove = abs(yfgmove);
  else if(foregndsrcrect.top >= (foregndsurf.height - scrnrect.height))
    yfgmove = -abs(yfgmove);

  foregndsrcrect.left += xfgmove;
  foregndsrcrect.top += yfgmove;

        static TIBLTPARAMS imageparms = {0};
        imageparms.structsize = sizeof(imageparms);
        imageparms.flags = TIBLT_FLAG_BLEND;
#ifdef LIBFLAG
        imageparms.flags = TIBLT_FLAG_ROP | LIBFLAG;
#endif
        imageparms.blendtype = TIBLT_BLEND_SRCOVER | TIBLT_BLEND_GLOBAL;
        imageparms.globalalpha = alpha;
        imageparms.pdst = &scrnsurf;
        imageparms.dstrect.left = 0;
        imageparms.dstrect.top = 0;
        imageparms.dstrect.width = scrnsurf.width;
        imageparms.dstrect.height = scrnsurf.height;
        imageparms.psrc = &foregndsurf;
        imageparms.srcrect = foregndsrcrect;
        imageparms.psrc2 = &bkgndsurf;
        imageparms.src2rect = bkgndsrcrect;
        err = TIBLT(&imageparms);
        if(err != TIBLT_ERR_NONE)
        {
          const char* errorstring;
          ErrorToText(errorstring, err);
          ODS("%s(): Error performing BLT: %s",
              __func__,
              errorstring);
          DumpParams(&imageparms);
          break;
        }

        alpha += deltaalpha;
        if(alpha == 0xFF)
          deltaalpha = 0xFF;
        else if(alpha == 1)
          deltaalpha = 1;
      }
      break;
  }

Error:
  DODS("%s() exit.\n", __func__);
  return(err == TIBLT_ERR_NONE);
}

void DestroySurfaces()
{
  if(scrnsurf.virtptr)
  {
    munmap(scrnsurf.virtptr, scrnsurf.virtstride * scrnsurf.height);
  }
  if(ghscrn)
  {
    close(ghscrn);
  }
}
#endif

enum surfcreator
{
  SC_STBI,
  SC_FB,
  SC_MALLOC,
  SC_STATIC
};

struct surf
{
  enum surfcreator creator;
  union
  {
    struct bvbuffdesc buffdesc;
    struct bvtileparams tileparams;
  };
  struct bvsurfgeom surfgeom;
};

struct surf g_bkgnd;
struct surf g_font;
struct surf g_fgcolor;
struct surf g_dest;

bool LoadImage(struct surf& surf,
               const char*  filename)
{
  bool success = false;

  int comp;
  memset(&surf, 0, sizeof(struct surf));
  surf.creator = SC_STBI;
  surf.buffdesc.structsize = sizeof(struct bvbuffdesc);
  surf.surfgeom.structsize = sizeof(struct bvsurfgeom);
  surf.buffdesc.virtaddr = stbi_load(filename,
                                     (int*)(&surf.surfgeom.width),
                                     (int*)(&surf.surfgeom.height),
                                     &comp,
                                     0);
  if(!surf.buffdesc.virtaddr)
  {
    ODS("%s(): Error loading %s: %s\n", __func__,
      filename,
      stbi_failure_reason());
    goto Error;
  }
  VODS("%s(): %s is %d x %d x %d\n", __func__,
    filename,
    surf.surfgeom.width,
    surf.surfgeom.height,
    comp);
  switch(comp)
  {
    case 1: surf.surfgeom.format = OCDFMT_ALPHA8; break;
    case 2: surf.surfgeom.format = OCDFMT_RGB16; break; // @@@ Not the right format!
    case 3: surf.surfgeom.format = OCDFMT_RGB24; break;
    case 4: surf.surfgeom.format = OCDFMT_RGBx24; break;
  }

  surf.surfgeom.virtstride = surf.surfgeom.width *
                         comp;
  surf.buffdesc.length = surf.surfgeom.virtstride *
                         surf.surfgeom.height;

  success = true;
Error:
  return(success);
}

bool SetColor(struct surf&   surf,
              unsigned long& rgba)
{
  memset(&surf, 0, sizeof(struct surf));
  surf.creator = SC_STBI;
  surf.buffdesc.structsize = sizeof(struct bvbuffdesc);
  surf.surfgeom.structsize = sizeof(struct bvsurfgeom);
  surf.buffdesc.virtaddr = &rgba;
  surf.buffdesc.length = sizeof(unsigned long);
  surf.surfgeom.width = 1;
  surf.surfgeom.height = 1;
  surf.surfgeom.format = OCDFMT_RGBx24;

  return(true);
}

bool bmdest = false;

bool MapFramebuffer(struct surf& surf)
{
  bool success = false;

  memset(&surf, 0, sizeof(struct surf));
  surf.buffdesc.structsize = sizeof(struct bvbuffdesc);
  surf.surfgeom.structsize = sizeof(struct bvsurfgeom);

  if(bmdest)
  {
    surf.creator = SC_MALLOC;
    surf.surfgeom.width = 800;
    surf.surfgeom.height = 480;
    surf.surfgeom.virtstride = surf.surfgeom.width * 4;
    surf.buffdesc.length = surf.surfgeom.height * surf.surfgeom.virtstride;
    surf.buffdesc.virtaddr = malloc(surf.buffdesc.length);
    if(!surf.buffdesc.virtaddr)
    {
      ODS("Unable to allocate memory for destination.\n");
      goto Error;
    }
    surf.surfgeom.format = OCDFMT_RGB124;
  }
  else
  {
    surf.creator = SC_FB;
    if(!scrndev)
    {
      VODS("Device not specified on command line, checking FRAMEBUFFER environment variable.\n");
      scrndev = getenv("FRAMEBUFFER");
    }

    if(!scrndev)
    {
      VODS("Device not specified in FRAMEBUFFER environment variable, using default: %s.\n", DEFSCRNDEV);
      scrndev = DEFSCRNDEV;
    }

    VODS("Attempting to open framebuffer device: %s.\n", scrndev);
    ghscrn = open(scrndev, O_RDWR);
    if(ghscrn == -1)
    {
      ODS("%s(): Error opening screen device (%s): %d.  Please provide the correct one with the -device argument.\n",
          __func__,
          scrndev,
          errno);
      goto Error;
    }
    VODS("Framebuffer device %s successfully opened.\n", scrndev);

    struct fb_fix_screeninfo fb_fixinfo;
    struct fb_var_screeninfo fb_varinfo;

    VODS("Attempting to get framebuffer device info.\n");
    if(ioctl(ghscrn, FBIOGET_FSCREENINFO, &fb_fixinfo))
    {
      ODS("%s(): Error getting fb_fixinfo: %d.\n", __func__, errno);
      goto Error;
    }

    if(ioctl(ghscrn, FBIOGET_VSCREENINFO, &fb_varinfo))
    {
      ODS("%s(): Error gettting fb_varinfo: %d.\n", __func__, errno);
      goto Error;
    }
    VODS("Successfully obtained framebuffer info:\n");
    VODS("fb_fix_screeninfo:\n");
    VODS("\tsmem_start = 0x%08X\n", fb_fixinfo.smem_start);
    VODS("\tsmem_len = %d\n", fb_fixinfo.smem_len);
    VODS("\ttype = %s (%d)\n", (fb_fixinfo.type == FB_TYPE_PACKED_PIXELS) ? "packed pixels" :
                              ((fb_fixinfo.type == FB_TYPE_PLANES) ? "non-interleaved planes" :
                              ((fb_fixinfo.type == FB_TYPE_INTERLEAVED_PLANES) ? "interleaved planes" :
                              ((fb_fixinfo.type == FB_TYPE_TEXT) ? "text" :
                              ((fb_fixinfo.type == FB_TYPE_VGA_PLANES) ? "EGA/VGA planes" : "unknown")))),
                              fb_fixinfo.type);
    VODS("\ttype_aux = %d\n", fb_fixinfo.type_aux);
    VODS("\tvisual = %s (%d)\n", (fb_fixinfo.visual == FB_VISUAL_MONO01) ? "reverse monochrome" :
                                ((fb_fixinfo.visual == FB_VISUAL_MONO10) ? "monochrome" :
                                ((fb_fixinfo.visual == FB_VISUAL_TRUECOLOR) ? "true color" :
                                ((fb_fixinfo.visual == FB_VISUAL_PSEUDOCOLOR) ? "pseudo color" :
                                ((fb_fixinfo.visual == FB_VISUAL_DIRECTCOLOR) ? "direct color" :
                                ((fb_fixinfo.visual == FB_VISUAL_STATIC_PSEUDOCOLOR) ? "read-only pseudo color" : "unknown"))))),
                                fb_fixinfo.visual);
    VODS("\txpanstep = %d\n", fb_fixinfo.xpanstep);
    VODS("\typanstep = %d\n", fb_fixinfo.ypanstep);
    VODS("\tywrapstep = %d\n", fb_fixinfo.ywrapstep);
    VODS("\tline_length = %d\n", fb_fixinfo.line_length);
    VODS("\tmmio_start = 0x%08X\n", fb_fixinfo.mmio_start);
    VODS("\tmmio_len = %d\n", fb_fixinfo.mmio_len);
    VODS("\taccel = %d\n", fb_fixinfo.accel);
    VODS("fb_var_screeninfo:\n");
    VODS("\txres, yres: %d, %d\n", fb_varinfo.xres, fb_varinfo.yres);
    VODS("\txres_virtual, yres_virtual: %d, %d\n", fb_varinfo.xres_virtual, fb_varinfo.yres_virtual);
    VODS("\txoffset, yoffset: %d, %d\n", fb_varinfo.xoffset, fb_varinfo.yoffset);
    VODS("\tbits_per_pixel: %d\n", fb_varinfo.bits_per_pixel);
    VODS("\tgrayscale: %s\n", (fb_varinfo.grayscale == 0) ? "false" : "true");
    VODS("\tmore...\n");

    switch(fb_varinfo.bits_per_pixel)
    {
      case 16:
        surf.surfgeom.format = OCDFMT_RGB16;
        break;

      case 24:
        surf.surfgeom.format = OCDFMT_RGB24;
        break;

      case 32:
        surf.surfgeom.format = OCDFMT_RGB124;
        break;

      default:
        ODS("%s(): Unsupported screen depth (%d).\n", __func__, fb_varinfo.bits_per_pixel);
        goto Error;
    }
    surf.surfgeom.width = fb_varinfo.xres_virtual;
    surf.surfgeom.height = fb_varinfo.yres_virtual;
    surf.surfgeom.virtstride = fb_fixinfo.line_length;
    surf.buffdesc.length = surf.surfgeom.height * surf.surfgeom.virtstride;
    VODS("Mapping framebuffer into user space.\n");
    surf.buffdesc.virtaddr = mmap(0,
                                  surf.buffdesc.length,
                                  PROT_WRITE | PROT_READ,
                                  MAP_SHARED,
                                  ghscrn,
                                  0);
    if(surf.buffdesc.virtaddr == MAP_FAILED)
    {
      ODS("%s(): Error mapping framebuffer into user space: %d.\n", __func__, errno);
      goto Error;
    }
    VODS("Framebuffer successfully mapped into user space (0x%08X).\n", surf.buffdesc.virtaddr);
  }

  success = true;
Error:
  return(success);
}

bool Copy(struct surf& dest,
          struct surf& src)
{
  struct bvbltparams parms;
  memset(&parms, 0, sizeof(struct bvbltparams));
  parms.structsize = sizeof(bvbltparams);
  parms.dstdesc = &dest.buffdesc;
  parms.dstgeom = &dest.surfgeom;
  parms.dstrect.width = dest.surfgeom.width;
  parms.dstrect.height = dest.surfgeom.height;
  parms.src1.desc = &src.buffdesc;
  parms.src1geom = &src.surfgeom;
  parms.src1rect.width = src.surfgeom.width;
  parms.src1rect.height = src.surfgeom.height;
  parms.flags = BVFLAG_ROP;
  parms.op.rop = 0xCCCC;
  parms.dithermode = BVDITHER_BEST;
  enum bverror bverr = bvlib[BV_CPU].bv_blt(&parms);
  if(bverr != BVERR_NONE)
  {
    ODS("%s(): BV error = %d, %s\n", __func__, bverr, parms.errdesc);
    BVDump("", "\t", &parms);
  }
}

bool InitBV(void)
{
  bool success = false;  

  for(int i = 0; i < NUMLIBS; i++)
  {
    bvlib[i].hlib = OSLoadLibrary(bvlib[i].libname);

    if(!bvlib[i].hlib)
    {
      ODS("%s(): Error opening BLTsville library lib%s.so.\n", __FUNCTION__, bvlib[i].libname);
    }
    else
    {
      bvlib[i].bv_map = (BVFN_MAP)OSGetProcAddress(bvlib[i].hlib, "bv_map");
      if(!bvlib[i].bv_map)
      {
        ODS("%s(): Error importing bv_map entry point from BLTsville library lib%s.so.\n", __FUNCTION__, bvlib[i].libname);
        goto BadLib;
      }

      bvlib[i].bv_blt = (BVFN_BLT)OSGetProcAddress(bvlib[i].hlib, "bv_blt");
      if(!bvlib[i].bv_blt)
      {
        ODS("%s(): Error importing bv_blt entry point from BLTsville library lib%s.so.\n", __FUNCTION__, bvlib[i].libname);
        goto BadLib;
      }

      bvlib[i].bv_unmap = (BVFN_UNMAP)OSGetProcAddress(bvlib[i].hlib, "bv_unmap");
      if(!bvlib[i].bv_unmap)
      {
        ODS("%s(): Error importing bv_unmap entry point from BLTsville library lib%s.so.\n", __FUNCTION__, bvlib[i].libname);
      }

      success = true;

BadLib:
      if(!bvlib[i].bv_unmap)
      {
        OSUnloadLibrary(bvlib[i].hlib);
        memset(&bvlib[i], 0, sizeof(bvlib[i]));
      }
    }

  }

  return(success);
}

#if 0
bool SaveImage(const char* szDst)
{
  bool success = false;
  
  struct bvbltparams tmpparms;
  int containersize;
  int true_width, true_height;

  if (parms.dstgeom->orientation == 0 || parms.dstgeom->orientation == 180)
  {
    true_width = parms.dstgeom->width;
    true_height = parms.dstgeom->height;
  }
  else if (parms.dstgeom->orientation == 90 || parms.dstgeom->orientation == 270)
  {
    true_width = parms.dstgeom->height;
    true_height = parms.dstgeom->width;
  }
  else
  {
      VODS("Cannot save destination file. Orientation %d is invalid.\n", parms.dstgeom->orientation);
      goto Error;
  }

  VODS("Destination file specified...saving.\n");
  if(parms.dstgeom->format == (ocdformat)-1)
  {
    ODS("Destination format not specified.  Unable to save results.\n");
    goto Error;
  }

  int comp;

  dstfile = *(parms.dstgeom);

  switch(parms.dstgeom->format & OCDFMTDEF_CS_MASK)
  {
    case OCDFMTDEF_CS_LUT:
      VODS("dst format is LUT. comp = 4. dstfile.format = nRGBA24\n");
      comp = 4;
      dstfile.format = OCDFMT_RGBA24_NON_PREMULT;
      break;
        
    case OCDFMTDEF_CS_RGB:
      VODS("dst format is RGB. ");
      if(parms.dstgeom->format & OCDFMTDEF_ALPHA)
      {
        VODS("dst format has alpha. comp = 4. ");
        comp = 4;
        if(parms.dstgeom->format & OCDFMTDEF_NON_PREMULT)
          VODS("dst format is non-premultiplied. dstfile.format = nRGBA24.\n");
        else
          VODS("dst format is premultiplied. dstfile.format = RGBA24.\n");
        dstfile.format = (parms.dstgeom->format & OCDFMTDEF_NON_PREMULT) ?
                         OCDFMT_RGBA24_NON_PREMULT :
                         OCDFMT_RGBA24;
      }
      else
      {
        VODS("dst format does not have alpha. comp = 3. dstfile.format = RGB24.\n");
        comp = 3;
        dstfile.format = OCDFMT_RGB24;
      }
      break;
      
    case OCDFMTDEF_CS_MONO:
    case OCDFMTDEF_CS_YCbCr:
      VODS("dst format is YUV. comp = 3; dstfile.foramt = RGB24.\n");
      comp = 3;
      dstfile.format = OCDFMT_RGB24;
      break;
  }

  FormatToContainerSize(containersize, dstfile.format);
  dstfile.virtstride = ((true_width * containersize) + 7) / 8;
  unsigned long size = SurfaceSize(dstfile.virtstride, dstfile.format, true_width, true_height);
  dstfiledesc.length = size;
  dstfiledesc.virtaddr = malloc(size);
  if(!dstfiledesc.virtaddr)
  {
    ODS("Error allocating memory for destination file surface.\n");
    dstfiledesc.length = 0;
    goto Error;
  }

  VODS("Copying destination surface to destination file surface.\n");

  memset(&tmpparms, 0, sizeof(tmpparms));
  tmpparms.structsize = sizeof(tmpparms);
  tmpparms.flags = BVFLAG_ROP;
  tmpparms.op.rop = 0xCCCC;
  tmpparms.dstgeom = &dstfile;
  tmpparms.src1geom = parms.dstgeom;
  tmpparms.src1.desc = parms.dstdesc;

  tmpparms.dstgeom = &dstfile;
  tmpparms.dstdesc = &dstfiledesc;

  tmpparms.dstrect.left = 0;
  tmpparms.dstrect.top = 0;
  tmpparms.dstrect.width = dstfile.width;
  tmpparms.dstrect.height = dstfile.height;
  tmpparms.src1rect.left = 0;
  tmpparms.src1rect.top = 0;
  tmpparms.src1rect.width = parms.dstgeom->width;
  tmpparms.src1rect.height = parms.dstgeom->height;

  {
    //bverror err = bvlib[0].bv_blt(&tmpparms); // MWI TODO Check all params
  bverror err = blt(&tmpparms, lib);
    if(err != BVERR_NONE)
    {
      const char* szErr;
      ErrorToText(szErr, err);
      ODS("Error copying destination surface image to destination file surface: (0x%08X) %s\n", err, szErr);
      goto Error;
    }
  }

  VODS("Destination file %s is %d x %d x %d\n", szDst, true_width, true_height, comp);
  int success;
  if(IsFileTGA(szDst))
    success = stbi_write_tga(szDst, true_width, true_height, comp, dstfiledesc.virtaddr);
  else
  {
    if (comp == 4)
    {
      ODS("bvtest: Four component files must be saved to TGA.\n");
    }
    else
        success = stbi_write_bmp(szDst, true_width, true_height, comp, dstfiledesc.virtaddr);
  }

  if(!success)
  {
    ODS("Error writing destination file.\n");
    goto Error;
  }
  
  success = true;
Error:
  return(success);
}
#endif

int main(int         argc,
         const char* argv[])
{
  int retval = EXIT_FAILURE;

//  unsigned long rgba = 0xFF000000;

  ParseCmdLine(argc, argv);
  InitBV();
  LoadImage(g_bkgnd, "bkgnd.png");
  LoadImage(g_font, "font.png");
BVGeomDump(0, "font", "", "\t", &g_font.surfgeom, 0);
//  SetColor(g_fgcolor, rgba);
  MapFramebuffer(g_dest);
  Copy(g_dest, g_bkgnd);
//  DrawText(g_dest, g_fgcolor, g_font, g_bkgnd, 0, 0, "This is a test");

#if 0
  PrintVersion();
      
  if(Init())
  {
    if(ParseCmdLine(argc, argv))
    {
      if(InitScreenSurface())
      {
        switch(testnum)
        {
          case 0:
            if(InitBackgroundColorSurface())
            {
              if(InitBackgroundSurface())
              {
                if(InitImageSurfaces())
                {
                  then = time(0);
                  while(true)
                    PerformBLT();
                }
              }
            }
            break;

          case 1:
            if(InitBackgroundSurface())
            {
              if(InitForegroundSurface())
              {
                then = time(0);
                while(true)
                  PerformBLT();
              }
            }
            break;

          default:
            ODS("Unknown test number.\n");
            goto Error;
        }

        retval = EXIT_SUCCESS;
      }
    }
    Cleanup();
  }

//ODS("TIBLTTest returns %d", retval);

Error:
#endif
  return(retval);
}


