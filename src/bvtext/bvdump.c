#include <string.h>
#include <stdio.h>

#include "os.h"

#include <bltsville.h>

const char* bvops[] =
{
  "unknown", "ROP", "blend", "unknown", "filter", "unknown", "unknown", "unknown",
  "unknown", "unknown", "unknown", "unknown", "unknown", "unknown", "unknown", "unknown"
};

struct bvflagdesc
{
  unsigned long mask;
  unsigned long val;
  const char* name;
};

#define BVPF(def) { def, def, #def }
struct bvflagdesc bvparamflags[] =
{
  { BVFLAG_OP_MASK, BVFLAG_ROP, "op = BVFLAG_ROP" },
  { BVFLAG_OP_MASK, BVFLAG_BLEND, "op = BVFLAG_BLEND" },
  { BVFLAG_OP_MASK, BVFLAG_FILTER, "op = BVFLAG_FILTER" },
  BVPF(BVFLAG_KEY_SRC),
  BVPF(BVFLAG_KEY_DST),
  BVPF(BVFLAG_CLIP),
  BVPF(BVFLAG_SRCMASK),
  BVPF(BVFLAG_ASYNC),
  BVPF(BVFLAG_TILE_SRC1),
  BVPF(BVFLAG_TILE_SRC2),
  BVPF(BVFLAG_TILE_MASK),
  { BVFLAG_BATCH_MASK, BVFLAG_BATCH_BEGIN, "BVFLAG_BATCH_BEGIN" },
  { BVFLAG_BATCH_MASK, BVFLAG_BATCH_CONTINUE, "BVFLAG_BATCH_CONTINUE" },
  { BVFLAG_BATCH_MASK, BVFLAG_BATCH_END, "BVFLAG_BATCH_END" },
  BVPF(BVFLAG_HORZ_FLIP_SRC1),
  BVPF(BVFLAG_VERT_FLIP_SRC1),
  BVPF(BVFLAG_HORZ_FLIP_SRC2),
  BVPF(BVFLAG_VERT_FLIP_SRC2),
  BVPF(BVFLAG_HORZ_FLIP_MASK),
  BVPF(BVFLAG_VERT_FLIP_MASK),
  BVPF(BVFLAG_SCALE_RETURN),
  BVPF(BVFLAG_DITHER_RETURN)
};
#define NUMBVPARAMFLAGS (sizeof(bvparamflags) / sizeof(struct bvflagdesc))

enum bverror DetermineSourcesUsedFromBlendConstant(unsigned long k,
                                                   int*          psrc1used,
                                                   int*          psrc2used)
{
  enum bverror err = BVERR_NONE;

#if 1
  switch(k)
  {
    case 0x01:
    case 0x04:
    case 0x06:
    case 0x09:
    case 0x10:
    case 0x11:
    case 0x14:
    case 0x15:
    case 0x20:
    case 0x21:
    case 0x24:
    case 0x25:
    case 0x31:
    case 0x33:
    case 0x34:
    case 0x3C:
      *psrc1used = !0;
      break;

    case 0x12:
    case 0x13:
    case 0x16:
    case 0x17:
    case 0x18:
    case 0x19:
    case 0x1C:
    case 0x1D:
    case 0x22:
    case 0x23:
    case 0x26:
    case 0x27:
    case 0x28:
    case 0x29:
    case 0x2C:
    case 0x2D:
      *psrc1used = !0;
    // fall through
    case 0x03:
    case 0x0B:
    case 0x0C:
    case 0x0E:
    case 0x1A:
    case 0x1B:
    case 0x1E:
    case 0x1F:
    case 0x2A:
    case 0x2B:
    case 0x2E:
    case 0x2F:
    case 0x36:
    case 0x39:
    case 0x3B:
    case 0x3E:
      *psrc2used = !0;
    // fall through
    default:
      break;
  }

#else
  if(k)
  {
    if(k != 0x3F)
    {
      switch(k >> 4)
      {
        case 0:
          switch(k & 0xF)
          {
            case 0x1: // 00 00 01: A1 (preferred)
            case 0x4: // 00 01 00: 1-A1 (preferred)
            case 0x6: // 00 01 10: 1-A1
            case 0x9: // 00 10 01: A1
              *psrc1used = !0;
              break;

            case 0x3: // 00 00 11: A2 (preferred)
            case 0xB: // 00 10 11: A2
            case 0xC: // 00 11 00: 1-A2 (preferred)
            case 0xE: // 00 11 10: 1-A2
              *psrc2used = !0;
              break;

            default:
              err = BVERR_BLEND;
              goto Error;
              // undefined
          }
          break;

        case 1:
        case 2:
          switch(k & 0xA)
          {
            case 0x0: // 01/10 0x 0x
              *psrc1used = !0;
              break;

            case 0x2: // 01/10 0x 1x
            case 0x8: // 01/10 1x 0x
              *psrc1used = !0;
            case 0xA: // 01/10 1x 1x
              *psrc2used = !0;
              break;
          }
          break;

        case 3:
          switch(k & 0xF)
          {
            case 0x1: // 11 00 01: 1-C1
            case 0x3: // 11 00 11: 1-C1 (preferred)
            case 0x4: // 11 01 00: C1
            case 0xC: // 11 11 00: C1 (preferred)
              *psrc1used = !0;
              break;

            case 0x6:
            case 0x9:
            case 0xB:
            case 0xE:
              *psrc2used = !0;
              break;

            default:
              err = BVERR_BLEND;
              goto Error;
          }
          break;
      }
    }
  }
#endif

Error:
  return(err);
}

enum bverror BVDetermineSurfaceUse(int*          psrc1used,
                                   int*          psrc2used,
                                   int*          pmaskused,
                                   char* const*  errdesc,
                                   unsigned long bltflags,
                                   union bvop*   pop)
{
  enum bverror err = BVERR_NONE;

  switch((bltflags & BVFLAG_OP_MASK) >> BVFLAG_OP_SHIFT)
  {
    case (BVFLAG_ROP >> BVFLAG_OP_SHIFT):
      {
        unsigned short rop = pop->rop;
        *psrc1used = ((rop & 0xCCCC) >> 2) ^ (rop & 0x3333);
        *psrc2used = ((rop & 0xF0F0) >> 4) ^ (rop & 0x0F0F);
        *pmaskused = ((rop & 0xFF00) >> 8) ^ (rop & 0x00FF);
      }
      break;

    case (BVFLAG_BLEND >> BVFLAG_OP_SHIFT):
      {
        enum bvblend blend = pop->blend;

        switch((pop->blend & BVBLENDDEF_FORMAT_MASK) >> BVBLENDDEF_FORMAT_SHIFT)
        {
          case (BVBLENDDEF_FORMAT_CLASSIC >> BVBLENDDEF_FORMAT_SHIFT):
            *pmaskused = blend & BVBLENDDEF_REMOTE;

            // Any non-zero constant means the source is used
            *psrc1used = blend & (BVBLENDDEF_K1_MASK | BVBLENDDEF_K3_MASK); // C1 & A1
            *psrc2used = blend & (BVBLENDDEF_K2_MASK | BVBLENDDEF_K4_MASK); // C2 & A2

            // Any constant can contain part of a source, too
            err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K1_MASK) >> BVBLENDDEF_K1_SHIFT,
                                                        psrc1used,
                                                        psrc2used);
            if(err != BVERR_NONE)
              err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K2_MASK) >> BVBLENDDEF_K2_SHIFT,
                                                          psrc1used,
                                                          psrc2used);
            if(err != BVERR_NONE)
              err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K3_MASK) >> BVBLENDDEF_K3_SHIFT,
                                                          psrc1used,
                                                          psrc2used);
            if(err != BVERR_NONE)
              err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K4_MASK) >> BVBLENDDEF_K4_SHIFT,
                                                          psrc1used,
                                                          psrc2used);
            if(err != BVERR_NONE)
            {
              errdesc = (char* const*)"undefined blend";
              goto Error;
            }
            break;

          default:
            err = BVERR_BLEND;
            errdesc = (char* const*)"unrecognized blend";
            goto Error;
        }
      }
      break;

    default:
      err = BVERR_OP;
      errdesc = (char* const*)"unrecognized operation";
      goto Error;
  }

Error:
  return(err);
}

void BVFlagDump(char*               cmdline,
                const char*         prefix,
                const char*         tab,
                const struct bvbltparams* parms)
{
  unsigned long flags = parms->flags;

  for(int i = 0; i < NUMBVPARAMFLAGS; i++)
  {
    if((flags & bvparamflags[i].mask) == bvparamflags[i].val)
    {
      ODS("%s%s%s\n", prefix, tab, bvparamflags[i].name);
      flags &= ~bvparamflags[i].mask;
    }
  }
  if(flags)
    ODS("%s%s??? Some unknown flags (0x%08X) ???\n", prefix, tab, flags);
  if(cmdline)
  {
    if(flags & BVFLAG_CLIP)
    {
      char tmp[256];
      sprintf(tmp, " -cliprect %d,%d-%dx%d", parms->cliprect.left, parms->cliprect.top, parms->cliprect.width, parms->cliprect.height);
      strcat(cmdline, tmp);
    }
  }
}

struct bvblendname
{
  enum bvblend blend;
  const char* name;
};
#define BVBN(def) { def, #def }

struct bvblendname bvcbnames[] =
{
  BVBN(BVBLEND_CLEAR),
  BVBN(BVBLEND_SRC1),
  BVBN(BVBLEND_SRC2),
  BVBN(BVBLEND_SRC1OVER),
  BVBN(BVBLEND_SRC2OVER),
  BVBN(BVBLEND_SRC1IN),
  BVBN(BVBLEND_SRC2IN),
  BVBN(BVBLEND_SRC1ATOP),
  BVBN(BVBLEND_SRC2ATOP),
  BVBN(BVBLEND_XOR),
  BVBN(BVBLEND_PLUS)
};
#define NUMBVCLASSICBLENDS (sizeof(bvcbnames) / sizeof(struct bvblendname))

struct bvblendname bvebnames[] =
{
  BVBN(BVBLEND_NORMAL),
  BVBN(BVBLEND_LIGHTEN),
  BVBN(BVBLEND_DARKEN),
  BVBN(BVBLEND_MULTIPLY),
  BVBN(BVBLEND_AVERAGE),
  BVBN(BVBLEND_ADD),
  BVBN(BVBLEND_LINEAR_DODGE),
  BVBN(BVBLEND_SUBTRACT),
  BVBN(BVBLEND_LINEAR_BURN),
  BVBN(BVBLEND_DIFFERENCE),
  BVBN(BVBLEND_NEGATE),
  BVBN(BVBLEND_SCREEN),
  BVBN(BVBLEND_EXCLUSION),
  BVBN(BVBLEND_OVERLAY),
  BVBN(BVBLEND_SOFT_LIGHT),
  BVBN(BVBLEND_HARD_LIGHT),
  BVBN(BVBLEND_COLOR_DODGE),
  BVBN(BVBLEND_COLOR_BURN),
  BVBN(BVBLEND_LINEAR_LIGHT),
  BVBN(BVBLEND_VIVID_LIGHT),
  BVBN(BVBLEND_PIN_LIGHT),
  BVBN(BVBLEND_HARD_MIX),
  BVBN(BVBLEND_REFLECT),
  BVBN(BVBLEND_GLOW),
  BVBN(BVBLEND_PHOENIX)
};
#define NUMBVESSENTIALBLENDS (sizeof(bvebnames) / sizeof(struct bvblendname))

void BVClassicBlendDump(char*                     cmdline,
                        const char*               prefix,
                        const char*               tab,
                        const struct bvbltparams* parms)
{
  enum bvblend blend = parms->op.blend;

  int i;
  for(i = 0; i < NUMBVCLASSICBLENDS; i++)
  {
    if((blend & BVBLENDDEF_CLASSIC_EQUATION_MASK) == bvcbnames[i].blend)
    {
      ODS("%s%s%s", prefix, tab, bvcbnames[i].name);
      if(cmdline)
      {
        strcat(cmdline, " -blend ");
        strcat(cmdline, &bvcbnames[i].name[8]);
      }
      break;
    }
  }
  if(i >= NUMBVCLASSICBLENDS)
  {
    ODS("%s%sUnrecognized classic blend: 0x%08X", prefix, tab, blend & BVBLENDDEF_CLASSIC_EQUATION_MASK);
  }
  if(blend & BVBLENDDEF_REMOTE) ODS(" + BVBLENDDEF_REMOTE");
  switch((blend & BVBLENDDEF_GLOBAL_MASK) >> BVBLENDDEF_GLOBAL_SHIFT)
  {
    case 0:
      ODS("\n");
      break;

    case (BVBLENDDEF_GLOBAL_UCHAR >> BVBLENDDEF_GLOBAL_SHIFT):
      ODS(" + BVBLENDEF_GLOBAL_UCHAR\n");
      if(cmdline)
      {
        char tmp[256];
        sprintf(tmp, "+global -alpha %02X", parms->globalalpha.size8);
        strcat(cmdline, tmp);
      }
      break;

    case (BVBLENDDEF_GLOBAL_FLOAT >> BVBLENDDEF_GLOBAL_SHIFT):
      ODS(" + BVBLENDEF_GLOBAL_FLOAT\n");
      if(cmdline)
      {
        char tmp[256];
        sprintf(tmp, "+global -alpha %02X", (unsigned char)(parms->globalalpha.fp * 255.0));
        strcat(cmdline, tmp);
      }
      break;

    default:
      ODS(" + unrecognized global format\n");
  }
}

void BVEssentialBlendDump(char*                     cmdline,
                          const char*               prefix,
                          const char*               tab,
                          const struct bvbltparams* parms)
{
  enum bvblend blend = parms->op.blend;

  int i;
  for(i = 0; i < NUMBVESSENTIALBLENDS; i++)
  {
    if(blend == bvebnames[i].blend)
    {
      ODS("%s%s%s\n", prefix, tab, bvebnames[i].name);
      if(cmdline)
      {
        char tmp[256];
        sprintf(tmp, "+global -alpha %02X", parms->globalalpha.size8);
        strcat(cmdline, tmp);
      }
      break;
    }
  }
  if(i >= NUMBVESSENTIALBLENDS)
  {
    ODS("%s%sUnrecognized essential blend: 0x%08X\n", prefix, tab, blend);
  }
}

void BVFilterDump(char*                     cmdline,
                  const char*               prefix,
                  const char*               tab,
                  const struct bvbltparams* parms)
{
  struct bvfilter* filter = parms->op.filter;
  ODS("%s%sUnrecognized filter: 0x%08X\n", prefix, tab, filter->filter);
}

void BVBlendDump(char*                     cmdline,
                 const char*               prefix,
                 const char*               tab,
                 const struct bvbltparams* parms)
{
  enum bvblend blend = parms->op.blend;
  switch((blend & BVBLENDDEF_FORMAT_MASK) >> BVBLENDDEF_FORMAT_SHIFT)
  {
    case BVBLENDDEF_FORMAT_CLASSIC: BVClassicBlendDump(cmdline, prefix, tab, parms); break;
    case BVBLENDDEF_FORMAT_ESSENTIAL: BVEssentialBlendDump(cmdline, prefix, tab, parms); break;
    default: ODS("%sop.blend unrecognized (0x%08X)\n", prefix, blend);
  }
}

struct bvscalename
{
  enum bvscalemode scale;
  const char* name;
};
#define BVSN(def) { def, #def }

struct bvscalename bvscalenames[] =
{
  BVSN(BVSCALE_FASTEST),
  BVSN(BVSCALE_FASTEST_NOT_NEAREST_NEIGHBOR),
  BVSN(BVSCALE_FASTEST_POINT_SAMPLE),
  BVSN(BVSCALE_FASTEST_INTERPOLATED),
  BVSN(BVSCALE_FASTEST_PHOTO),
  BVSN(BVSCALE_FASTEST_DRAWING),
  BVSN(BVSCALE_GOOD),
  BVSN(BVSCALE_GOOD_POINT_SAMPLE),
  BVSN(BVSCALE_GOOD_INTERPOLATED),
  BVSN(BVSCALE_GOOD_PHOTO),
  BVSN(BVSCALE_GOOD_DRAWING),
  BVSN(BVSCALE_BETTER),
  BVSN(BVSCALE_BETTER_POINT_SAMPLE),
  BVSN(BVSCALE_BETTER_INTERPOLATED),
  BVSN(BVSCALE_BETTER_PHOTO),
  BVSN(BVSCALE_BETTER_DRAWING),
  BVSN(BVSCALE_BEST),
  BVSN(BVSCALE_BEST_POINT_SAMPLE),
  BVSN(BVSCALE_BEST_INTERPOLATED),
  BVSN(BVSCALE_BEST_PHOTO),
  BVSN(BVSCALE_BEST_DRAWING),
  BVSN(BVSCALE_NEAREST_NEIGHBOR),
  BVSN(BVSCALE_BILINEAR),
  BVSN(BVSCALE_BICUBIC),
  BVSN(BVSCALE_3x3_TAP),
  BVSN(BVSCALE_5x5_TAP),
  BVSN(BVSCALE_7x7_TAP),
  BVSN(BVSCALE_9x9_TAP)
};
#define NUMBVSCALEMODES (sizeof(bvscalenames) / sizeof(struct bvscalename))

void BVScaleDump(char*                     cmdline,
                 const char*               prefix,
                 const char*               tab,
                 const struct bvbltparams* parms)
{
  enum bvscalemode scale = parms->scalemode;
  int i;
  for(i = 0; i < NUMBVSCALEMODES; i++)
  {
    if(scale == bvscalenames[i].scale)
    {
      if(scale != BVSCALE_FASTEST)
      {
        ODS("%sscalemode = %s\n", prefix, bvscalenames[i].name);
        if(cmdline)
        {
          strcat(cmdline, " -scale ");
          strcat(cmdline, &bvscalenames[i].name[8]);
        }
      }
      break;
    }
  }
  if(i >= NUMBVSCALEMODES)
  {
    ODS("%sscalemode = (unknown) 0x%08X\n", prefix, scale);
  }
}

struct bvdithername
{
  enum bvdithermode dither;
  const char* name;
};
#define BVDN(def) { def, #def }

struct bvdithername bvdithernames[] =
{
  BVDN(BVDITHER_FASTEST),
  BVDN(BVDITHER_FASTEST_ON),
  BVDN(BVDITHER_FASTEST_RANDOM),
  BVDN(BVDITHER_FASTEST_ORDERED),
  BVDN(BVDITHER_FASTEST_DIFFUSED),
  BVDN(BVDITHER_FASTEST_PHOTO),
  BVDN(BVDITHER_FASTEST_DRAWING),
  BVDN(BVDITHER_GOOD),
  BVDN(BVDITHER_GOOD_RANDOM),
  BVDN(BVDITHER_GOOD_ORDERED),
  BVDN(BVDITHER_GOOD_DIFFUSED),
  BVDN(BVDITHER_GOOD_PHOTO),
  BVDN(BVDITHER_GOOD_DRAWING),
  BVDN(BVDITHER_BETTER),
  BVDN(BVDITHER_BETTER_RANDOM),
  BVDN(BVDITHER_BETTER_ORDERED),
  BVDN(BVDITHER_BETTER_DIFFUSED),
  BVDN(BVDITHER_BETTER_PHOTO),
  BVDN(BVDITHER_BETTER_DRAWING),
  BVDN(BVDITHER_BEST),
  BVDN(BVDITHER_BEST_RANDOM),
  BVDN(BVDITHER_BEST_ORDERED),
  BVDN(BVDITHER_BEST_DIFFUSED),
  BVDN(BVDITHER_BEST_PHOTO),
  BVDN(BVDITHER_BEST_DRAWING),
  BVDN(BVDITHER_NONE),
  BVDN(BVDITHER_ORDERED_2x2),
  BVDN(BVDITHER_ORDERED_2x2_4x4),
  BVDN(BVDITHER_ORDERED_4x4)
};
#define NUMBVDITHERMODES (sizeof(bvdithernames) / sizeof(struct bvdithername))

void BVDitherDump(char*                     cmdline,
                  const char*               prefix,
                  const char*               tab,
                  const struct bvbltparams* parms)
{
  enum bvdithermode dither = parms->dithermode;

  int i;
  for(i = 0; i < NUMBVDITHERMODES; i++)
  {
    if(dither == bvdithernames[i].dither)
    {
      if(dither != BVDITHER_FASTEST)
      {
        ODS("%sdithermode = %s\n", prefix, bvdithernames[i].name);
        if(cmdline)
        {
          strcat(cmdline, " -dither ");
          strcat(cmdline, &bvdithernames[i].name[9]);
        }
      }
      break;
    }
  }
  if(i >= NUMBVDITHERMODES)
  {
    ODS("%sdithermode = (unknown) 0x%08X\n", prefix, dither);
  }
}

void BVDescDump(char*                     cmdline,
                const char*               surf,
                const char*               prefix,
                const char*               tab,
                struct bvbuffdesc*        pdesc,
                const struct bvbltparams* parms)
{
  ODS("%s%sdesc:\n", prefix, surf);
  ODS("%s%sstructsize = %d\n", prefix, tab, pdesc->structsize);
  ODS("%s%svirtaddr = 0x%04X\n", prefix, tab, pdesc->virtaddr);
  ODS("%s%slength = %d\n", prefix, tab, pdesc->length);
  ODS("%s%smap = 0x%08X\n", prefix, tab, pdesc->map);
}

struct ocdformatname
{
  enum ocdformat format;
  const char* name;
};
#define OCDFN(def) { def, #def }

struct ocdformatname ocdformatnames[] =
{
  OCDFN(OCDFMT_UNKNOWN),
  OCDFN(OCDFMT_NONE),
  OCDFN(OCDFMT_ALPHA1),
  OCDFN(OCDFMT_ALPHA2),
  OCDFN(OCDFMT_ALPHA4),
  OCDFN(OCDFMT_ALPHA8),
  OCDFN(OCDFMT_ALPHA4x1),
  OCDFN(OCDFMT_ALPHA3x8),
  OCDFN(OCDFMT_ALPHA4x8),
  OCDFN(OCDFMT_MONO1),
  OCDFN(OCDFMT_MONO2),
  OCDFN(OCDFMT_MONO4),
  OCDFN(OCDFMT_MONO8),
  OCDFN(OCDFMT_LUT1),
  OCDFN(OCDFMT_LUT2),
  OCDFN(OCDFMT_LUT4),
  OCDFN(OCDFMT_LUT8),
  OCDFN(OCDFMT_RGB12),
  OCDFN(OCDFMT_0RGB12),
  OCDFN(OCDFMT_BGR12),
  OCDFN(OCDFMT_0BGR12),
  OCDFN(OCDFMT_RGBx12),
  OCDFN(OCDFMT_RGB012),
  OCDFN(OCDFMT_BGRx12),
  OCDFN(OCDFMT_BGR012),
  OCDFN(OCDFMT_RGB15),
  OCDFN(OCDFMT_0RGB15),
  OCDFN(OCDFMT_BGR15),
  OCDFN(OCDFMT_0BGR15),
  OCDFN(OCDFMT_RGBx15),
  OCDFN(OCDFMT_RGB015),
  OCDFN(OCDFMT_BGRx15),
  OCDFN(OCDFMT_BGR015),
  OCDFN(OCDFMT_RGB16),
  OCDFN(OCDFMT_BGR16),
  OCDFN(OCDFMT_RGB24),
  OCDFN(OCDFMT_BGR24),
  OCDFN(OCDFMT_xRGB16),
  OCDFN(OCDFMT_0RGB16),
  OCDFN(OCDFMT_xBGR16),
  OCDFN(OCDFMT_0BGR16),
  OCDFN(OCDFMT_RGBx16),
  OCDFN(OCDFMT_RGB016),
  OCDFN(OCDFMT_BGRx16),
  OCDFN(OCDFMT_BGR016),
  OCDFN(OCDFMT_xRGB24),
  OCDFN(OCDFMT_0RGB24),
  OCDFN(OCDFMT_xBGR24),
  OCDFN(OCDFMT_0BGR24),
  OCDFN(OCDFMT_RGBx24),
  OCDFN(OCDFMT_RGB024),
  OCDFN(OCDFMT_BGRx24),
  OCDFN(OCDFMT_BGR024),
  OCDFN(OCDFMT_ARGB12),
  OCDFN(OCDFMT_ABGR12),
  OCDFN(OCDFMT_RGBA12),
  OCDFN(OCDFMT_BGRA12),
  OCDFN(OCDFMT_ARGB16),
  OCDFN(OCDFMT_ABGR16),
  OCDFN(OCDFMT_RGBA16),
  OCDFN(OCDFMT_BGRA16),
  OCDFN(OCDFMT_ARGB24),
  OCDFN(OCDFMT_ABGR24),
  OCDFN(OCDFMT_RGBA24),
  OCDFN(OCDFMT_BGRA24),
  OCDFN(OCDFMT_nARGB12),
  OCDFN(OCDFMT_nABGR12),
  OCDFN(OCDFMT_nRGBA12),
  OCDFN(OCDFMT_nBGRA12),
  OCDFN(OCDFMT_ARGB15),
  OCDFN(OCDFMT_nARGB15),
  OCDFN(OCDFMT_ABGR15),
  OCDFN(OCDFMT_nABGR15),
  OCDFN(OCDFMT_RGBA15),
  OCDFN(OCDFMT_nRGBA15),
  OCDFN(OCDFMT_BGRA15),
  OCDFN(OCDFMT_nBGRA15),
  OCDFN(OCDFMT_nARGB16),
  OCDFN(OCDFMT_nABGR16),
  OCDFN(OCDFMT_nRGBA16),
  OCDFN(OCDFMT_nBGRA16),
  OCDFN(OCDFMT_nARGB24),
  OCDFN(OCDFMT_nABGR24),
  OCDFN(OCDFMT_nRGBA24),
  OCDFN(OCDFMT_nBGRA24),
  OCDFN(OCDFMT_UYVY),
  OCDFN(OCDFMT_UYVY_709),
  OCDFN(OCDFMT_VYUY),
  OCDFN(OCDFMT_VYUY_709),
  OCDFN(OCDFMT_YUY2),
  OCDFN(OCDFMT_YUY2_709),
  OCDFN(OCDFMT_YVYU),
  OCDFN(OCDFMT_YVYU_709),
  OCDFN(OCDFMT_YV16),
  OCDFN(OCDFMT_YV16_709),
  OCDFN(OCDFMT_IYUV),
  OCDFN(OCDFMT_IYUV_709),
  OCDFN(OCDFMT_YV12),
  OCDFN(OCDFMT_YV12_709),
  OCDFN(OCDFMT_IMC3),
  OCDFN(OCDFMT_IMC3_709),
  OCDFN(OCDFMT_IMC1),
  OCDFN(OCDFMT_IMC1_709),
  OCDFN(OCDFMT_IMC4),
  OCDFN(OCDFMT_IMC4_709),
  OCDFN(OCDFMT_IMC2),
  OCDFN(OCDFMT_IMC2_709),
  OCDFN(OCDFMT_NV16),
  OCDFN(OCDFMT_NV16_709),
  OCDFN(OCDFMT_NV61),
  OCDFN(OCDFMT_NV61_709),
  OCDFN(OCDFMT_NV12),
  OCDFN(OCDFMT_NV12_709),
  OCDFN(OCDFMT_NV21),
  OCDFN(OCDFMT_NV21_709)
};
#define NUMOCDFORMATS (sizeof(ocdformatnames) / sizeof(struct ocdformatname))

const char* OCDFormat(enum ocdformat format)
{
  const char* szfmt = "(unknown)";

  int i;
  for(i = 0; i < NUMOCDFORMATS; i++)
  {
    if(format == ocdformatnames[i].format)
    {
      szfmt = ocdformatnames[i].name;
      break;
    }
  }

  return(szfmt);
}

void BVGeomDump(char*               cmdline,
                const char*         surf,
                const char*         prefix,
                const char*         tab,
                struct bvsurfgeom*  pgeom,
                const struct bvbltparams* parms)
{
  ODS("%s%sgeom:\n", prefix, surf);
  ODS("%s%sstructsize = %d\n", prefix, tab, pgeom->structsize);
  const char* szfmt = OCDFormat(pgeom->format);
  ODS("%s%sformat = %s\n", prefix, tab, szfmt);
  ODS("%s%swidth = %d\n", prefix, tab, pgeom->width);
  ODS("%s%sheight = %d\n", prefix, tab, pgeom->height);
  ODS("%s%sorientation = %d\n", prefix, tab, pgeom->orientation);
  ODS("%s%svirtstride = %d\n", prefix, tab, pgeom->virtstride);
  switch((pgeom->format & OCDFMTDEF_VENDOR_MASK) == OCDFMTDEF_VENDOR_ALL)
  {
    case (OCDFMTDEF_VENDOR_ALL >> OCDFMTDEF_VENDOR_SHIFT):
      if((pgeom->format & OCDFMTDEF_CS_MASK) != OCDFMTDEF_CS_LUT)
        break;
    // fall through
    case (OCDFMTDEF_VENDOR_TI >> OCDFMTDEF_VENDOR_SHIFT):
    default:
      if(pgeom->palette)
      {
        ODS("%s%spaletteformat = %s\n", prefix, tab, OCDFormat(pgeom->paletteformat));
        ODS("%s%spalette = 0x%08X\n", prefix, tab, pgeom->palette);
      }
  }
  if(cmdline)
  {
    const char* szsurf;
    if(pgeom == parms->dstgeom)
      szsurf = "dst";
    else if(pgeom == parms->src1geom)
      szsurf = "src1";
    else if(pgeom == parms->src2geom)
      szsurf = "src2";
    else if(pgeom == parms->maskgeom)
      szsurf = "mask";
    else
      szsurf = 0;

    char tmp[256];
    sprintf(tmp, " -%s %s %dx%d", szsurf, &szfmt[7], pgeom->width, pgeom->height);
    strcat(cmdline, tmp);

    if(pgeom->orientation != 0)
    {
      sprintf(tmp, " -%sangle %d", szsurf, pgeom->orientation);
      strcat(cmdline, tmp);
    }
  }
}

void BVTileDump(const char* surf, const char* prefix, const char* tab, const struct bvtileparams* tileparms)
{
  ODS("%s%stileparams:\n", prefix, surf);
  ODS("%s%sstructsize = %d\n", prefix, tab, tileparms->structsize);
  ODS("%s%sflags = 0x%08X", prefix, tab, tileparms->flags);
  ODS("%s%svirtaddr = 0x%08X\n", prefix, tab, tileparms->virtaddr);
  ODS("%s%sdstleft = %d\n", prefix, tab, tileparms->dstleft);
  ODS("%s%sdsttop = %d\n", prefix, tab, tileparms->dsttop);
  ODS("%s%ssrcwidth = %d\n", prefix, tab, tileparms->srcwidth);
  ODS("%s%ssrcheight = %d\n", prefix, tab, tileparms->srcheight);
}

void BVDump(const char* prefix, const char* tab, const struct bvbltparams* parms)
{
#ifndef BVDUMP_NOCMDLINE
  char cmdline[512] = "bvtest";
  char tmp[256];
#else
  char* cmdline = 0;
  char* tmp = 0;
#endif /* BVDUMP_NOCMDLINE */

  ODS("%sstructsize = %d\n", prefix, parms->structsize);
  const char* errdesc = parms->errdesc;
  if(!errdesc) errdesc = "NULL";
  ODS("%serrdesc = %s\n", prefix, errdesc);
  ODS("%simplementation = 0x%08X\n", prefix, parms->implementation);
  ODS("%sflags = 0x%08X\n", prefix, parms->flags);
  BVFlagDump(cmdline, prefix, tab, parms);
  switch((parms->flags & BVFLAG_OP_MASK) >> BVFLAG_OP_SHIFT)
  {
    case BVFLAG_ROP:
      ODS("%sop.rop = 0x%04X\n", prefix, parms->op.rop);
      if(cmdline)
      {
        sprintf(tmp, " -rop %04X", parms->op.rop);
        strcat(cmdline, tmp);
      }
      break;

    case BVFLAG_BLEND:
      BVBlendDump(cmdline, prefix, tab, parms);
      break;

    case BVFLAG_FILTER:
      BVFilterDump(cmdline, prefix, tab, parms);
      break;

    default:
      ODS("%sop.??? = 0x%08X\n", prefix, (unsigned long)parms->op.filter);
  }
  if(parms->flags & (BVFLAG_KEY_DST | BVFLAG_KEY_SRC))
  {
    ODS("%s0x%08X\n", *((unsigned long*)(parms->colorkey)));
    if(cmdline)
    {
      /* @@@ modify when added to bvtest */
    }
  }
  if((parms->flags & BVFLAG_OP_MASK) == BVFLAG_BLEND)
  {
    switch((parms->op.blend & BVBLENDDEF_GLOBAL_MASK) >> BVBLENDDEF_GLOBAL_SHIFT)
    {
      case 0: break;
      case (BVBLENDDEF_GLOBAL_UCHAR >> BVBLENDDEF_GLOBAL_SHIFT): ODS("%sglobalalpha = 0x%02X\n", parms->globalalpha.size8); break;
      case (BVBLENDDEF_GLOBAL_FLOAT >> BVBLENDDEF_GLOBAL_SHIFT): ODS("%sglobalalpha = %f\n", parms->globalalpha.fp); break;
      default: ODS("%s globalalpha = 0x%08X (unknown type)\n");
    }
  }
  BVScaleDump(cmdline, prefix, tab, parms);
  BVDitherDump(cmdline, prefix, tab, parms);
  BVDescDump(cmdline, "dst", prefix, tab, parms->dstdesc, parms);
  BVGeomDump(cmdline, "dst", prefix, tab, parms->dstgeom, parms);
  ODS("%sdstrect = (%d, %d)-(%d, %d)\n", prefix, parms->dstrect.left, parms->dstrect.top, parms->dstrect.width, parms->dstrect.height);
  if(cmdline)
  {
    sprintf(tmp, " -dstrect %d,%d-%dx%d", parms->dstrect.left, parms->dstrect.top, parms->dstrect.width, parms->dstrect.height);
    strcat(cmdline, tmp);
  }
  int src1used, src2used, maskused;
  if(BVDetermineSurfaceUse(&src1used,
                           &src2used,
                           &maskused,
                           &(((struct bvbltparams*)parms)->errdesc),
                           parms->flags,
                           &(((struct bvbltparams*)parms)->op)) == BVERR_NONE)
  {
    if(src1used)
    {
      if(parms->flags & BVFLAG_TILE_SRC1)
        BVTileDump("src1.", prefix, tab, parms->src1.tileparams);
      else
        BVDescDump(cmdline, "src1.", prefix, tab, parms->src1.desc, parms);
      BVGeomDump(cmdline, "src1", prefix, tab, parms->src1geom, parms);
      ODS("%ssrc1rect = (%d, %d)-(%d, %d)\n", prefix, parms->src1rect.left, parms->src1rect.top, parms->src1rect.width, parms->src1rect.height);
      if(cmdline)
      {
        sprintf(tmp, " -src1rect %d,%d-%dx%d", parms->src1rect.left, parms->src1rect.top, parms->src1rect.width, parms->src1rect.height);
        strcat(cmdline, tmp);
      }
    }
    if(src2used)
    {
      if(parms->flags & BVFLAG_TILE_SRC2)
        BVTileDump(prefix, "src2.", tab, parms->src2.tileparams);
      else
        BVDescDump(cmdline, "src2.", prefix, tab, parms->src2.desc, parms);
      BVGeomDump(cmdline, "src2", prefix, tab, parms->src2geom, parms);
      ODS("%ssrc2rect = (%d, %d)-(%d, %d)\n", prefix, parms->src2rect.left, parms->src2rect.top, parms->src2rect.width, parms->src2rect.height);
      if(cmdline)
      {
        sprintf(tmp, " -src2rect %d,%d-%dx%d", parms->src2rect.left, parms->src2rect.top, parms->src2rect.width, parms->src2rect.height);
        strcat(cmdline, tmp);
      }
    }
    if(maskused)
    {
      if(parms->flags & BVFLAG_TILE_MASK)
        BVTileDump(prefix, "mask.", tab, parms->mask.tileparams);
      else
        BVDescDump(cmdline, "mask.", prefix, tab, parms->mask.desc, parms);
      BVGeomDump(cmdline, "mask", prefix, tab, parms->maskgeom, parms);
      ODS("%smaskrect = (%d, %d)-(%d, %d)\n", prefix, parms->maskrect.left, parms->maskrect.top, parms->maskrect.width, parms->maskrect.height);
      if(cmdline)
      {
        sprintf(tmp, " -maskrect %d,%d-%dx%d", parms->maskrect.left, parms->maskrect.top, parms->maskrect.width, parms->maskrect.height);
        strcat(cmdline, tmp);
      }
    }

    if(parms->flags & BVFLAG_CLIP)
    {
      ODS("%scliprect = (%d, %d)-(%d, %d)\n", prefix, parms->cliprect.left, parms->cliprect.top, parms->cliprect.width, parms->cliprect.height);
      if(cmdline)
      {
        sprintf(tmp, " -cliprect %d,%d-%dx%d", parms->cliprect.left, parms->cliprect.top, parms->cliprect.width, parms->cliprect.height);
        strcat(cmdline, tmp);
      }
    }

    if(cmdline) ODS("BVTEST CMDLINE: %s\n", cmdline);
  }
}

