#ifndef BVTEXT_H
#define BVTEXT_H

#include "os.h"
#include <bltsville.h>

#define STBI_HEADER_FILE_ONLY
#define STBI_FAILURE_USERMSG
#include "stb_image.c"

#include <stdlib.h>
#include <string.h>
#define stricmp strcasecmp

#include <time.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <new>
// #include <list>
// #include <simplelist.h>
// #include <algorithm>

#include <unistd.h>

#include <string.h>
//#include <iostream>
//#include <map>
//#include <utility>

using namespace std;

#endif // BVTEXT_H
