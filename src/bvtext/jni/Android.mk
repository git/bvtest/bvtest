LOCAL_PATH := $(call my-dir)/..

include $(CLEAR_VARS)

LOCAL_CFLAGS := -std=c++0x

LOCAL_MODULE := bvtext

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := bvtext.cpp \
                   oslinux.c \
                   common.c \
                   stb_image.c \
                   bvdump.c

LOCAL_C_INCLUDES := $(LOCAL_PATH) \
		$(LOCAL_PATH)/../../../bltsville/include \
		$(LOCAL_PATH)/../../../ocd/include \

LOCAL_LDLIBS := -ldl

include $(BUILD_EXECUTABLE)

