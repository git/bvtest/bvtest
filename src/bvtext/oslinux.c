#include "os.h"

#include <stdio.h>

extern int verbose;
#define VODS(...) if(!verbose) ; else ODS(__VA_ARGS__)

void OSDebugPrint(const char* szMsg)
{
  printf("%s", szMsg);
}

BVLIBHANDLE OSLoadLibrary(const char* szLibPrefix)
{
  BVLIBHANDLE hLib = 0;
  char szBuf[256];
  sprintf(szBuf, "lib%s.so", szLibPrefix);
  VODS("%s(): Attempting to open %s\n", __func__, szBuf);
  hLib = dlopen(szBuf, RTLD_LOCAL | RTLD_LAZY);
  VODS("%s(): Library %s %s\n", __func__, szBuf, hLib ? "opened" : "failed to open");
  return(hLib);
}

void* OSGetProcAddress(BVLIBHANDLE hLib,
                       const char* szFn)
{
  void* pFn = 0;
  pFn = dlsym(hLib, szFn);
  return(pFn);
}

void OSUnloadLibrary(BVLIBHANDLE hLib)
{
  dlclose(hLib);
}
