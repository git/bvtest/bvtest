LOCAL_PATH := $(call my-dir)/..

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := tests

LOCAL_MODULE := crc32

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := crc32.c

LOCAL_C_INCLUDES := $(LOCAL_PATH) \
		$(LOCAL_PATH)/../../../bltsville/include \
		$(LOCAL_PATH)/../../../ocd/include \

LOCAL_LDLIBS := -ldl

include $(BUILD_EXECUTABLE)

