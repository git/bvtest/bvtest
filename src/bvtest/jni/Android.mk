LOCAL_PATH := $(call my-dir)/..

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := tests

LOCAL_MODULE := bvtest

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := bvtest.cpp \
                   oslinux.c \
                   common.c \
                   stb_image.c \
                   bvdump.c

LOCAL_C_INCLUDES := $(LOCAL_PATH) \
	external/bltsville/bltsville/include \
	external/bltsville/ocd/include

LOCAL_SHARED_LIBRARIES := libdl

include $(BUILD_EXECUTABLE)

