#include <string.h>
#include <stdio.h>

#include "os.h"

#include <bltsville.h>

//extern "C"
enum bverror BVDetermineSurfaceUse(int*              psrc1used,
                                   int*              psrc2used,
                                   int*              pmaskused,
                                   char**            errdesc,
                                   unsigned long     bltflags,
                                   const union bvop* pop);


const char* bvops[] =
{
  "unknown", "ROP", "blend", "unknown", "filter", "unknown", "unknown", "unknown",
  "unknown", "unknown", "unknown", "unknown", "unknown", "unknown", "unknown", "unknown"
};

struct bvflagdesc
{
  unsigned long mask;
  unsigned long val;
  const char* name;
};

#define BVPF(def) { def, def, #def }
struct bvflagdesc bvparamflags[] =
{
  { BVFLAG_OP_MASK, BVFLAG_ROP, "op = BVFLAG_ROP" },
  { BVFLAG_OP_MASK, BVFLAG_BLEND, "op = BVFLAG_BLEND" },
  { BVFLAG_OP_MASK, BVFLAG_FILTER, "op = BVFLAG_FILTER" },
  BVPF(BVFLAG_KEY_SRC),
  BVPF(BVFLAG_KEY_DST),
  BVPF(BVFLAG_CLIP),
  BVPF(BVFLAG_SRCMASK),
  BVPF(BVFLAG_ASYNC),
  BVPF(BVFLAG_TILE_SRC1),
  BVPF(BVFLAG_TILE_SRC2),
  BVPF(BVFLAG_TILE_MASK),
  { BVFLAG_BATCH_MASK, BVFLAG_BATCH_BEGIN, "BVFLAG_BATCH_BEGIN" },
  { BVFLAG_BATCH_MASK, BVFLAG_BATCH_CONTINUE, "BVFLAG_BATCH_CONTINUE" },
  { BVFLAG_BATCH_MASK, BVFLAG_BATCH_END, "BVFLAG_BATCH_END" },
  BVPF(BVFLAG_HORZ_FLIP_SRC1),
  BVPF(BVFLAG_VERT_FLIP_SRC1),
  BVPF(BVFLAG_HORZ_FLIP_SRC2),
  BVPF(BVFLAG_VERT_FLIP_SRC2),
  BVPF(BVFLAG_HORZ_FLIP_MASK),
  BVPF(BVFLAG_VERT_FLIP_MASK),
  BVPF(BVFLAG_SCALE_RETURN),
  BVPF(BVFLAG_DITHER_RETURN)
};
#define NUMBVPARAMFLAGS (sizeof(bvparamflags) / sizeof(struct bvflagdesc))

void BVFlagDump(char*               cmdline,
                const char*         prefix,
                const char*         tab,
                const struct bvbltparams* parms)
{
  unsigned long flags = parms->flags;

  unsigned int i;
  for(i = 0; i < NUMBVPARAMFLAGS; i++)
  {
    if((flags & bvparamflags[i].mask) == bvparamflags[i].val)
    {
      ODS("%s%s%s\n", prefix, tab, bvparamflags[i].name);
      flags &= ~bvparamflags[i].mask;
    }
  }
  if(flags)
    ODS("%s%s??? Some unknown flags (0x%08X) ???\n", prefix, tab, flags);
  if(cmdline)
  {
    if(flags & BVFLAG_CLIP)
    {
      char tmp[256];
      sprintf(tmp, " -cliprect %d,%d-%dx%d", parms->cliprect.left, parms->cliprect.top, parms->cliprect.width, parms->cliprect.height);
      strcat(cmdline, tmp);
    }
  }
}

struct bvblendname
{
  enum bvblend blend;
  const char* name;
};
#define BVBN(def) { def, #def }

struct bvblendname bvcbnames[] =
{
  BVBN(BVBLEND_CLEAR),
  BVBN(BVBLEND_SRC1),
  BVBN(BVBLEND_SRC2),
  BVBN(BVBLEND_SRC1OVER),
  BVBN(BVBLEND_SRC2OVER),
  BVBN(BVBLEND_SRC1IN),
  BVBN(BVBLEND_SRC2IN),
  BVBN(BVBLEND_SRC1ATOP),
  BVBN(BVBLEND_SRC2ATOP),
  BVBN(BVBLEND_XOR),
  BVBN(BVBLEND_PLUS)
};
#define NUMBVCLASSICBLENDS (sizeof(bvcbnames) / sizeof(struct bvblendname))

struct bvblendname bvebnames[] =
{
  BVBN(BVBLEND_NORMAL),
  BVBN(BVBLEND_LIGHTEN),
  BVBN(BVBLEND_DARKEN),
  BVBN(BVBLEND_MULTIPLY),
  BVBN(BVBLEND_AVERAGE),
  BVBN(BVBLEND_ADD),
  BVBN(BVBLEND_LINEAR_DODGE),
  BVBN(BVBLEND_SUBTRACT),
  BVBN(BVBLEND_LINEAR_BURN),
  BVBN(BVBLEND_DIFFERENCE),
  BVBN(BVBLEND_NEGATE),
  BVBN(BVBLEND_SCREEN),
  BVBN(BVBLEND_EXCLUSION),
  BVBN(BVBLEND_OVERLAY),
  BVBN(BVBLEND_SOFT_LIGHT),
  BVBN(BVBLEND_HARD_LIGHT),
  BVBN(BVBLEND_COLOR_DODGE),
  BVBN(BVBLEND_COLOR_BURN),
  BVBN(BVBLEND_LINEAR_LIGHT),
  BVBN(BVBLEND_VIVID_LIGHT),
  BVBN(BVBLEND_PIN_LIGHT),
  BVBN(BVBLEND_HARD_MIX),
  BVBN(BVBLEND_REFLECT),
  BVBN(BVBLEND_GLOW),
  BVBN(BVBLEND_PHOENIX)
};
#define NUMBVESSENTIALBLENDS (sizeof(bvebnames) / sizeof(struct bvblendname))

void BVClassicBlendDump(char*                     cmdline,
                        const char*               prefix,
                        const char*               tab,
                        const struct bvbltparams* parms)
{
  enum bvblend blend = parms->op.blend;

  unsigned int i;
  for(i = 0; i < NUMBVCLASSICBLENDS; i++)
  {
    if((blend & BVBLENDDEF_CLASSIC_EQUATION_MASK) == bvcbnames[i].blend)
    {
      ODS("%sop.blend = %s", prefix, bvcbnames[i].name);
      if(cmdline)
      {
        strcat(cmdline, " -blend ");
        strcat(cmdline, &bvcbnames[i].name[8]);
      }
      break;
    }
  }
  if(i >= NUMBVCLASSICBLENDS)
  {
    ODS("%sop.blend = (unknown) 0x%08X", prefix, blend & BVBLENDDEF_CLASSIC_EQUATION_MASK);
  }
  if(blend & BVBLENDDEF_REMOTE) ODS(" + BVBLENDDEF_REMOTE");
  switch((blend & BVBLENDDEF_GLOBAL_MASK) >> BVBLENDDEF_GLOBAL_SHIFT)
  {
    case 0:
      ODS("\n");
      break;

    case (BVBLENDDEF_GLOBAL_UCHAR >> BVBLENDDEF_GLOBAL_SHIFT):
      ODS(" + BVBLENDEF_GLOBAL_UCHAR\n");
      if(cmdline)
      {
        char tmp[256];
        sprintf(tmp, "+global -alpha %02X", parms->globalalpha.size8);
        strcat(cmdline, tmp);
      }
      break;

    case (BVBLENDDEF_GLOBAL_FLOAT >> BVBLENDDEF_GLOBAL_SHIFT):
      ODS(" + BVBLENDEF_GLOBAL_FLOAT\n");
      if(cmdline)
      {
        char tmp[256];
        sprintf(tmp, "+global -alpha %02X", (unsigned char)(parms->globalalpha.fp * 255.0));
        strcat(cmdline, tmp);
      }
      break;

    default:
      ODS(" + unrecognized global format\n");
  }
}

void BVEssentialBlendDump(char*                     cmdline,
                          const char*               prefix,
                          const char*               tab,
                          const struct bvbltparams* parms)
{
  enum bvblend blend = parms->op.blend;

  unsigned int i;
  for(i = 0; i < NUMBVESSENTIALBLENDS; i++)
  {
    if(blend == bvebnames[i].blend)
    {
      ODS("%sop.blend = %s\n", prefix, bvebnames[i].name);
      if(cmdline)
      {
        char tmp[256];
        sprintf(tmp, "+global -alpha %02X", parms->globalalpha.size8);
        strcat(cmdline, tmp);
      }
      break;
    }
  }
  if(i >= NUMBVESSENTIALBLENDS)
  {
    ODS("%sop.blend = (unknown) 0x%08X\n", prefix, blend);
  }
}

void BVFilterDump(char*                     cmdline,
                  const char*               prefix,
                  const char*               tab,
                  const struct bvbltparams* parms)
{
  struct bvfilter* filter = parms->op.filter;
  ODS("%s%sUnrecognized filter: 0x%08X\n", prefix, tab, filter->filter);
}

void BVBlendDump(char*                     cmdline,
                 const char*               prefix,
                 const char*               tab,
                 const struct bvbltparams* parms)
{
  enum bvblend blend = parms->op.blend;
  switch((blend & BVBLENDDEF_FORMAT_MASK) >> BVBLENDDEF_FORMAT_SHIFT)
  {
    case BVBLENDDEF_FORMAT_CLASSIC: BVClassicBlendDump(cmdline, prefix, tab, parms); break;
    case BVBLENDDEF_FORMAT_ESSENTIAL: BVEssentialBlendDump(cmdline, prefix, tab, parms); break;
    default: ODS("%sop.blend unrecognized (0x%08X)\n", prefix, blend);
  }
}

struct bvscalename
{
  enum bvscalemode scale;
  const char* name;
};
#define BVSN(def) { def, #def }

struct bvscalename bvscalenames[] =
{
  BVSN(BVSCALE_FASTEST),
  BVSN(BVSCALE_FASTEST_NOT_NEAREST_NEIGHBOR),
  BVSN(BVSCALE_FASTEST_POINT_SAMPLE),
  BVSN(BVSCALE_FASTEST_INTERPOLATED),
  BVSN(BVSCALE_FASTEST_PHOTO),
  BVSN(BVSCALE_FASTEST_DRAWING),
  BVSN(BVSCALE_GOOD),
  BVSN(BVSCALE_GOOD_POINT_SAMPLE),
  BVSN(BVSCALE_GOOD_INTERPOLATED),
  BVSN(BVSCALE_GOOD_PHOTO),
  BVSN(BVSCALE_GOOD_DRAWING),
  BVSN(BVSCALE_BETTER),
  BVSN(BVSCALE_BETTER_POINT_SAMPLE),
  BVSN(BVSCALE_BETTER_INTERPOLATED),
  BVSN(BVSCALE_BETTER_PHOTO),
  BVSN(BVSCALE_BETTER_DRAWING),
  BVSN(BVSCALE_BEST),
  BVSN(BVSCALE_BEST_POINT_SAMPLE),
  BVSN(BVSCALE_BEST_INTERPOLATED),
  BVSN(BVSCALE_BEST_PHOTO),
  BVSN(BVSCALE_BEST_DRAWING),
  BVSN(BVSCALE_NEAREST_NEIGHBOR),
  BVSN(BVSCALE_BILINEAR),
  BVSN(BVSCALE_BICUBIC),
  BVSN(BVSCALE_3x3_TAP),
  BVSN(BVSCALE_5x5_TAP),
  BVSN(BVSCALE_7x7_TAP),
  BVSN(BVSCALE_9x9_TAP)
};
#define NUMBVSCALEMODES (sizeof(bvscalenames) / sizeof(struct bvscalename))

void BVScaleDump(char*                     cmdline,
                 const char*               prefix,
                 const char*               tab,
                 const struct bvbltparams* parms)
{
  enum bvscalemode scale = parms->scalemode;
  unsigned int i;
  for(i = 0; i < NUMBVSCALEMODES; i++)
  {
    if(scale == bvscalenames[i].scale)
    {
      if(scale != BVSCALE_FASTEST)
      {
        ODS("%sscalemode = %s\n", prefix, bvscalenames[i].name);
        if(cmdline)
        {
          strcat(cmdline, " -scale ");
          strcat(cmdline, &bvscalenames[i].name[8]);
        }
      }
      break;
    }
  }
  if(i >= NUMBVSCALEMODES)
  {
    ODS("%sscalemode = (unknown) 0x%08X\n", prefix, scale);
  }
}

struct bvdithername
{
  enum bvdithermode dither;
  const char* name;
};
#define BVDN(def) { def, #def }

struct bvdithername bvdithernames[] =
{
  BVDN(BVDITHER_FASTEST),
  BVDN(BVDITHER_FASTEST_ON),
  BVDN(BVDITHER_FASTEST_RANDOM),
  BVDN(BVDITHER_FASTEST_ORDERED),
  BVDN(BVDITHER_FASTEST_DIFFUSED),
  BVDN(BVDITHER_FASTEST_PHOTO),
  BVDN(BVDITHER_FASTEST_DRAWING),
  BVDN(BVDITHER_GOOD),
  BVDN(BVDITHER_GOOD_RANDOM),
  BVDN(BVDITHER_GOOD_ORDERED),
  BVDN(BVDITHER_GOOD_DIFFUSED),
  BVDN(BVDITHER_GOOD_PHOTO),
  BVDN(BVDITHER_GOOD_DRAWING),
  BVDN(BVDITHER_BETTER),
  BVDN(BVDITHER_BETTER_RANDOM),
  BVDN(BVDITHER_BETTER_ORDERED),
  BVDN(BVDITHER_BETTER_DIFFUSED),
  BVDN(BVDITHER_BETTER_PHOTO),
  BVDN(BVDITHER_BETTER_DRAWING),
  BVDN(BVDITHER_BEST),
  BVDN(BVDITHER_BEST_RANDOM),
  BVDN(BVDITHER_BEST_ORDERED),
  BVDN(BVDITHER_BEST_DIFFUSED),
  BVDN(BVDITHER_BEST_PHOTO),
  BVDN(BVDITHER_BEST_DRAWING),
  BVDN(BVDITHER_NONE),
  BVDN(BVDITHER_ORDERED_2x2),
  BVDN(BVDITHER_ORDERED_2x2_4x4),
  BVDN(BVDITHER_ORDERED_4x4)
};
#define NUMBVDITHERMODES (sizeof(bvdithernames) / sizeof(struct bvdithername))

void BVDitherDump(char*                     cmdline,
                  const char*               prefix,
                  const char*               tab,
                  const struct bvbltparams* parms)
{
  enum bvdithermode dither = parms->dithermode;

  unsigned int i;
  for(i = 0; i < NUMBVDITHERMODES; i++)
  {
    if(dither == bvdithernames[i].dither)
    {
      if(dither != BVDITHER_FASTEST)
      {
        ODS("%sdithermode = %s\n", prefix, bvdithernames[i].name);
        if(cmdline)
        {
          strcat(cmdline, " -dither ");
          strcat(cmdline, &bvdithernames[i].name[9]);
        }
      }
      break;
    }
  }
  if(i >= NUMBVDITHERMODES)
  {
    ODS("%sdithermode = (unknown) 0x%08X\n", prefix, dither);
  }
}

void BVDescDump(char*                     cmdline,
                const char*               surf,
                const char*               prefix,
                const char*               tab,
                struct bvbuffdesc*        pdesc,
                const struct bvbltparams* parms)
{
  ODS("%s%sdesc:\n", prefix, surf);
  ODS("%s%sstructsize = %d\n", prefix, tab, pdesc->structsize);
  ODS("%s%svirtaddr = 0x%04X\n", prefix, tab, pdesc->virtaddr);
  ODS("%s%slength = %d\n", prefix, tab, pdesc->length);
  ODS("%s%smap = 0x%08X\n", prefix, tab, pdesc->map);
}

struct ocdformatname
{
  enum ocdformat format;
  const char* name;
};
#define OCDFN(def) { def, #def }

struct ocdformatname ocdformatnames[] =
{
  OCDFN(OCDFMT_UNKNOWN),
  OCDFN(OCDFMT_NONE),
  OCDFN(OCDFMT_ALPHA1),
  OCDFN(OCDFMT_ALPHA2),
  OCDFN(OCDFMT_ALPHA4),
  OCDFN(OCDFMT_ALPHA8),
  OCDFN(OCDFMT_ALPHA4x1),
  OCDFN(OCDFMT_ALPHA3x8),
  OCDFN(OCDFMT_ALPHA4x8),
  OCDFN(OCDFMT_MONO1),
  OCDFN(OCDFMT_MONO2),
  OCDFN(OCDFMT_MONO4),
  OCDFN(OCDFMT_MONO8),
  OCDFN(OCDFMT_LUT1),
  OCDFN(OCDFMT_LUT2),
  OCDFN(OCDFMT_LUT4),
  OCDFN(OCDFMT_LUT8),
  OCDFN(OCDFMT_RGB12),
  OCDFN(OCDFMT_0RGB12),
  OCDFN(OCDFMT_BGR12),
  OCDFN(OCDFMT_0BGR12),
  OCDFN(OCDFMT_RGBx12),
  OCDFN(OCDFMT_RGB012),
  OCDFN(OCDFMT_BGRx12),
  OCDFN(OCDFMT_BGR012),
  OCDFN(OCDFMT_RGB15),
  OCDFN(OCDFMT_0RGB15),
  OCDFN(OCDFMT_BGR15),
  OCDFN(OCDFMT_0BGR15),
  OCDFN(OCDFMT_RGBx15),
  OCDFN(OCDFMT_RGB015),
  OCDFN(OCDFMT_BGRx15),
  OCDFN(OCDFMT_BGR015),
  OCDFN(OCDFMT_RGB16),
  OCDFN(OCDFMT_BGR16),
  OCDFN(OCDFMT_RGB24),
  OCDFN(OCDFMT_BGR24),
  OCDFN(OCDFMT_xRGB16),
  OCDFN(OCDFMT_0RGB16),
  OCDFN(OCDFMT_xBGR16),
  OCDFN(OCDFMT_0BGR16),
  OCDFN(OCDFMT_RGBx16),
  OCDFN(OCDFMT_RGB016),
  OCDFN(OCDFMT_BGRx16),
  OCDFN(OCDFMT_BGR016),
  OCDFN(OCDFMT_xRGB24),
  OCDFN(OCDFMT_0RGB24),
  OCDFN(OCDFMT_xBGR24),
  OCDFN(OCDFMT_0BGR24),
  OCDFN(OCDFMT_RGBx24),
  OCDFN(OCDFMT_RGB024),
  OCDFN(OCDFMT_BGRx24),
  OCDFN(OCDFMT_BGR024),
  OCDFN(OCDFMT_ARGB12),
  OCDFN(OCDFMT_ABGR12),
  OCDFN(OCDFMT_RGBA12),
  OCDFN(OCDFMT_BGRA12),
  OCDFN(OCDFMT_ARGB16),
  OCDFN(OCDFMT_ABGR16),
  OCDFN(OCDFMT_RGBA16),
  OCDFN(OCDFMT_BGRA16),
  OCDFN(OCDFMT_ARGB24),
  OCDFN(OCDFMT_ABGR24),
  OCDFN(OCDFMT_RGBA24),
  OCDFN(OCDFMT_BGRA24),
  OCDFN(OCDFMT_nARGB12),
  OCDFN(OCDFMT_nABGR12),
  OCDFN(OCDFMT_nRGBA12),
  OCDFN(OCDFMT_nBGRA12),
  OCDFN(OCDFMT_ARGB15),
  OCDFN(OCDFMT_nARGB15),
  OCDFN(OCDFMT_ABGR15),
  OCDFN(OCDFMT_nABGR15),
  OCDFN(OCDFMT_RGBA15),
  OCDFN(OCDFMT_nRGBA15),
  OCDFN(OCDFMT_BGRA15),
  OCDFN(OCDFMT_nBGRA15),
  OCDFN(OCDFMT_nARGB16),
  OCDFN(OCDFMT_nABGR16),
  OCDFN(OCDFMT_nRGBA16),
  OCDFN(OCDFMT_nBGRA16),
  OCDFN(OCDFMT_nARGB24),
  OCDFN(OCDFMT_nABGR24),
  OCDFN(OCDFMT_nRGBA24),
  OCDFN(OCDFMT_nBGRA24),
  OCDFN(OCDFMT_UYVY),
  OCDFN(OCDFMT_UYVY_709),
  OCDFN(OCDFMT_VYUY),
  OCDFN(OCDFMT_VYUY_709),
  OCDFN(OCDFMT_YUY2),
  OCDFN(OCDFMT_YUY2_709),
  OCDFN(OCDFMT_YVYU),
  OCDFN(OCDFMT_YVYU_709),
  OCDFN(OCDFMT_YV16),
  OCDFN(OCDFMT_YV16_709),
  OCDFN(OCDFMT_IYUV),
  OCDFN(OCDFMT_IYUV_709),
  OCDFN(OCDFMT_YV12),
  OCDFN(OCDFMT_YV12_709),
  OCDFN(OCDFMT_IMC3),
  OCDFN(OCDFMT_IMC3_709),
  OCDFN(OCDFMT_IMC1),
  OCDFN(OCDFMT_IMC1_709),
  OCDFN(OCDFMT_IMC4),
  OCDFN(OCDFMT_IMC4_709),
  OCDFN(OCDFMT_IMC2),
  OCDFN(OCDFMT_IMC2_709),
  OCDFN(OCDFMT_NV16),
  OCDFN(OCDFMT_NV16_709),
  OCDFN(OCDFMT_NV61),
  OCDFN(OCDFMT_NV61_709),
  OCDFN(OCDFMT_NV12),
  OCDFN(OCDFMT_NV12_709),
  OCDFN(OCDFMT_NV21),
  OCDFN(OCDFMT_NV21_709)
};
#define NUMOCDFORMATS (sizeof(ocdformatnames) / sizeof(struct ocdformatname))

const char* OCDFormat(enum ocdformat format)
{
  const char* szfmt = "(unknown)";

  unsigned int i;
  for(i = 0; i < NUMOCDFORMATS; i++)
  {
    if(format == ocdformatnames[i].format)
    {
      szfmt = ocdformatnames[i].name;
      break;
    }
  }

  return(szfmt);
}

void BVGeomDump(char*               cmdline,
                const char*         surf,
                const char*         prefix,
                const char*         tab,
                struct bvsurfgeom*  pgeom,
                const struct bvbltparams* parms)
{
  ODS("%s%sgeom:\n", prefix, surf);
  ODS("%s%sstructsize = %d\n", prefix, tab, pgeom->structsize);
  const char* szfmt = OCDFormat(pgeom->format);
  ODS("%s%sformat = %s\n", prefix, tab, szfmt);
  ODS("%s%swidth = %d\n", prefix, tab, pgeom->width);
  ODS("%s%sheight = %d\n", prefix, tab, pgeom->height);
  ODS("%s%sorientation = %d\n", prefix, tab, pgeom->orientation);
  ODS("%s%svirtstride = %d\n", prefix, tab, pgeom->virtstride);
  switch((pgeom->format & OCDFMTDEF_VENDOR_MASK) == OCDFMTDEF_VENDOR_ALL)
  {
    case (OCDFMTDEF_VENDOR_ALL >> OCDFMTDEF_VENDOR_SHIFT):
      if((pgeom->format & OCDFMTDEF_CS_MASK) != OCDFMTDEF_CS_LUT)
        break;
    // fall through
    case (OCDFMTDEF_VENDOR_TI >> OCDFMTDEF_VENDOR_SHIFT):
    default:
      if(pgeom->palette)
      {
        ODS("%s%spaletteformat = %s\n", prefix, tab, OCDFormat(pgeom->paletteformat));
        ODS("%s%spalette = 0x%08X\n", prefix, tab, pgeom->palette);
      }
  }
  if(cmdline)
  {
    const char* szsurf = surf;
    char* szdup = NULL;
    char tmp[256];

    /*
     We have to account for the possibility that the same surface is being
     used for two parameters. This presents a difficulty in that it is
     impossible to know what order the parameters were set up in.  Did the user
     set up the destination and then say use the destination as src1?  Or did
     the user set up src1 and then say use src1 as the destination?
     We will assume the order of the parameters are src1, src2, mask, dest

     Additionally, even though this function parses the geometry, we can't
     actually test the geometry when testing for surface equality.  This is
     because it is technically possible for two "identical" surfaces to have
     different surface formats, to allow for in-place conversion of the
     surface format.
    */

    //
    // We are parsing src1
    //
    if (strcmp(surf,"src1") == 0)
    {
      // Nothing to do.
      // If src1 is the same as anything else, we will catch it when we parse
      // that parameter.
      ;
    }

    //
    // We are parsing src2
    //
    else if (strcmp(surf,"src2") == 0)
    {
      // See if src2 is the same as src1
      if ( (!(parms->flags & BVFLAG_TILE_SRC2)) && (!(parms->flags & BVFLAG_TILE_SRC1)) )
      {
          // if they are both not tiled, compare their buffer desc.
          if (parms->src2.desc == parms->src1.desc)
            szdup = "src1";
      }
      else if ( ((parms->flags & BVFLAG_TILE_SRC2)) && ((parms->flags & BVFLAG_TILE_SRC1)) )
      {
          // If they both are tiled, compare their tileparams.
          if (parms->src2.tileparams == parms->src1.tileparams)
            szdup = "src1";
      }

    }

    //
    // We are parsing the mask
    //
    else if (strcmp(surf,"mask") == 0)
    {
      // Check if mask is the same as src2
      if ( (!(parms->flags & BVFLAG_TILE_MASK)) && (!(parms->flags & BVFLAG_TILE_SRC2)) )
      {
          if (parms->mask.desc == parms->src2.desc)
            szdup = "src2";
      }
      else if ( (parms->flags & BVFLAG_TILE_MASK) && (parms->flags & BVFLAG_TILE_SRC2) )
      {
          if (parms->mask.tileparams == parms->src2.tileparams)
            szdup = "src2";
      }

      // Check if mask is the same as src1
      if ( (!(parms->flags & BVFLAG_TILE_MASK)) && (!(parms->flags & BVFLAG_TILE_SRC1)) )
      {
          if (parms->mask.desc == parms->src1.desc)
            szdup = "src1";
      }
      else if ( ((parms->flags & BVFLAG_TILE_MASK)) && ((parms->flags & BVFLAG_TILE_SRC1)) )
      {
          if (parms->mask.tileparams == parms->src1.tileparams)
            szdup = "src1";
      }
    }

    //
    // We are parsing the destination.
    //
    else if (strcmp(surf,"dst") == 0)
      {
      // No dest tiling, so just compare buffer desc
      if (parms->src1.desc == parms->dstdesc)
        szdup = "src1";
      else if (parms->src2.desc == parms->dstdesc)
            szdup = "src2";
      else if (parms->mask.desc == parms->dstdesc)
        szdup = "mask";
    }

    if (szdup)
        sprintf(tmp, " -%s %s %s", szsurf, &szfmt[7], szdup);
    else
        sprintf(tmp, " -%s %s %dx%d", szsurf, &szfmt[7], pgeom->width, pgeom->height);

    strcat(cmdline, tmp);

    // Question:  If src1 is set to be the same as dest, and dest is rotated, is
    // src1 automatically set up for rotation? or do we still need to supply
    // the angle command? guess it doesn't hurt to, so play it safe.
    if(pgeom->orientation != 0)
    {
      sprintf(tmp, " -%sangle %d", szsurf, pgeom->orientation);
      strcat(cmdline, tmp);
    }
  }
}

void BVTileDump(const char* surf, const char* prefix, const char* tab, const struct bvtileparams* tileparms)
{
  ODS("%s%stileparams:\n", prefix, surf);
  ODS("%s%sstructsize = %d\n", prefix, tab, tileparms->structsize);
  ODS("%s%sflags = 0x%08X", prefix, tab, tileparms->flags);
  ODS("%s%svirtaddr = 0x%08X\n", prefix, tab, tileparms->virtaddr);
  ODS("%s%sdstleft = %d\n", prefix, tab, tileparms->dstleft);
  ODS("%s%sdsttop = %d\n", prefix, tab, tileparms->dsttop);
  ODS("%s%ssrcwidth = %d\n", prefix, tab, tileparms->srcwidth);
  ODS("%s%ssrcheight = %d\n", prefix, tab, tileparms->srcheight);
}

void BVDump(const char* prefix, const char* tab, const struct bvbltparams* parms)
{
  char *cmdline = NULL;
  char *tmp = NULL;
#ifndef BVDUMP_NOCMDLINE
  char cmdlinebuf[512] = "bvtest";
  char tmpbuf[256];
  cmdline = cmdlinebuf;
  tmp = tmpbuf;
#endif /* BVDUMP_NOCMDLINE */

  ODS("%sstructsize = %d\n", prefix, parms->structsize);
  const char* errdesc = parms->errdesc;
  if(!errdesc) errdesc = "NULL";
  ODS("%serrdesc = %s\n", prefix, errdesc);
  ODS("%simplementation = 0x%08X\n", prefix, parms->implementation);
  ODS("%sflags = 0x%08X\n", prefix, parms->flags);
  BVFlagDump(cmdline, prefix, tab, parms);
  switch((parms->flags & BVFLAG_OP_MASK) >> BVFLAG_OP_SHIFT)
  {
    case BVFLAG_ROP:
      ODS("%sop.rop = 0x%04X\n", prefix, parms->op.rop);
      if(cmdline)
      {
        sprintf(tmp, " -rop %04X", parms->op.rop);
        strcat(cmdline, tmp);
      }
      break;

    case BVFLAG_BLEND:
      BVBlendDump(cmdline, prefix, tab, parms);
      break;

    case BVFLAG_FILTER:
      BVFilterDump(cmdline, prefix, tab, parms);
      break;

    default:
      ODS("%sop.??? = 0x%08X\n", prefix, (unsigned long)parms->op.filter);
  }
  if(parms->flags & (BVFLAG_KEY_DST | BVFLAG_KEY_SRC))
  {
    ODS("%s0x%08X\n", *((unsigned long*)(parms->colorkey)));
    if(cmdline)
    {
      /* @@@ modify when added to bvtest */
    }
  }
  if((parms->flags & BVFLAG_OP_MASK) == BVFLAG_BLEND)
  {
    switch((parms->op.blend & BVBLENDDEF_GLOBAL_MASK) >> BVBLENDDEF_GLOBAL_SHIFT)
    {
      case 0: break;
      case (BVBLENDDEF_GLOBAL_UCHAR >> BVBLENDDEF_GLOBAL_SHIFT): ODS("%sglobalalpha = 0x%02X\n", parms->globalalpha.size8); break;
      case (BVBLENDDEF_GLOBAL_FLOAT >> BVBLENDDEF_GLOBAL_SHIFT): ODS("%sglobalalpha = %f\n", parms->globalalpha.fp); break;
      default: ODS("%s globalalpha = 0x%08X (unknown type)\n");
    }
  }
  BVScaleDump(cmdline, prefix, tab, parms);
  BVDitherDump(cmdline, prefix, tab, parms);
  BVDescDump(cmdline, "dst", prefix, tab, parms->dstdesc, parms);
  BVGeomDump(cmdline, "dst", prefix, tab, parms->dstgeom, parms);
  ODS("%sdstrect = (%d, %d)-(%d, %d)\n", prefix, parms->dstrect.left, parms->dstrect.top, parms->dstrect.width, parms->dstrect.height);
  if(cmdline)
  {
    sprintf(tmp, " -dstrect %d,%d-%dx%d", parms->dstrect.left, parms->dstrect.top, parms->dstrect.width, parms->dstrect.height);
    strcat(cmdline, tmp);
  }
  int src1used, src2used, maskused;
  if(BVDetermineSurfaceUse(&src1used,
                           &src2used,
                           &maskused,
                           &(((struct bvbltparams*)parms)->errdesc),
                           parms->flags,
                           &parms->op) == BVERR_NONE)
  {
    if(src1used)
    {
      if(parms->flags & BVFLAG_TILE_SRC1)
        BVTileDump("src1.", prefix, tab, parms->src1.tileparams);
      else
        BVDescDump(cmdline, "src1.", prefix, tab, parms->src1.desc, parms);
      BVGeomDump(cmdline, "src1", prefix, tab, parms->src1geom, parms);
      ODS("%ssrc1rect = (%d, %d)-(%d, %d)\n", prefix, parms->src1rect.left, parms->src1rect.top, parms->src1rect.width, parms->src1rect.height);
      if(cmdline)
      {
        sprintf(tmp, " -src1rect %d,%d-%dx%d", parms->src1rect.left, parms->src1rect.top, parms->src1rect.width, parms->src1rect.height);
        strcat(cmdline, tmp);
      }
    }
    if(src2used)
    {
      if(parms->flags & BVFLAG_TILE_SRC2)
        BVTileDump(prefix, "src2.", tab, parms->src2.tileparams);
      else
        BVDescDump(cmdline, "src2.", prefix, tab, parms->src2.desc, parms);
      BVGeomDump(cmdline, "src2", prefix, tab, parms->src2geom, parms);
      ODS("%ssrc2rect = (%d, %d)-(%d, %d)\n", prefix, parms->src2rect.left, parms->src2rect.top, parms->src2rect.width, parms->src2rect.height);
      if(cmdline)
      {
        sprintf(tmp, " -src2rect %d,%d-%dx%d", parms->src2rect.left, parms->src2rect.top, parms->src2rect.width, parms->src2rect.height);
        strcat(cmdline, tmp);
      }
    }
    if(maskused)
    {
      if(parms->flags & BVFLAG_TILE_MASK)
        BVTileDump(prefix, "mask.", tab, parms->mask.tileparams);
      else
        BVDescDump(cmdline, "mask.", prefix, tab, parms->mask.desc, parms);
      BVGeomDump(cmdline, "mask", prefix, tab, parms->maskgeom, parms);
      ODS("%smaskrect = (%d, %d)-(%d, %d)\n", prefix, parms->maskrect.left, parms->maskrect.top, parms->maskrect.width, parms->maskrect.height);
      if(cmdline)
      {
        sprintf(tmp, " -maskrect %d,%d-%dx%d", parms->maskrect.left, parms->maskrect.top, parms->maskrect.width, parms->maskrect.height);
        strcat(cmdline, tmp);
      }
    }

    if(parms->flags & BVFLAG_CLIP)
    {
      ODS("%scliprect = (%d, %d)-(%d, %d)\n", prefix, parms->cliprect.left, parms->cliprect.top, parms->cliprect.width, parms->cliprect.height);
      if(cmdline)
      {
        sprintf(tmp, " -cliprect %d,%d-%dx%d", parms->cliprect.left, parms->cliprect.top, parms->cliprect.width, parms->cliprect.height);
        strcat(cmdline, tmp);
      }
    }

    if(cmdline) ODS("BVTEST CMDLINE: %s\n", cmdline);
  }
}

