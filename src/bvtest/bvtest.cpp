/*
 * ======================================================================
 *             Texas Instruments OMAP(TM) Platform Software
 *
 * This Software is provided "as-is," without warranty of any kind,
 * express or implied, including but not limited to the warranties of
 * merchantability, fitness for a particular purpose and noninfringement.
 * In no event shall Texas Instruments, Inc. be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or
 * otherwise, arising from, out of or in connection with the software or
 * the use or other dealings in the Software.
 *
 * Except as contained in this notice, the name of Texas Instruments,
 * Inc. shall not be used in advertising or otherwise to promote the
 * sale, use or other dealings in this Software without prior written
 * authorization from Texas Instruments, Inc.
 *
 * Use at your own risk.
 *=======================================================================
 */

#include "bvtest.h"
#include <malloc.h>


enum SurfOrigin
{
  SO_UNKNOWN,
  SO_DST,
  SO_SRC1,
  SO_SRC2,
  SO_MASK
};

#ifdef ENABLE_OMAPDRM
extern "C" {
#include <omap/omap_drm.h>
#include <omap_drmif.h>
}

struct dmabuf {
  unsigned int *mem;
  struct omap_bo *bo;
  unsigned long handle;
};

static int drmFd = 0;
static omap_device *drmDev = NULL;
struct dmabuf dmabufDst={0};
struct dmabuf dmabufSrc1={0};
struct dmabuf dmabufSrc2={0};
struct dmabuf dmabufMask={0};

#endif //ENABLE_OMAPDRM


extern "C" void BVDump(const char*, const char*, const struct bvbltparams*);

static void gc_print_generic(unsigned char *data,
			     unsigned int datasize)
{
	char buffer[128];
	unsigned int i, len = 0;

	/* Print the title. */
	ODS("BUFFER @ 0x%08X\n", (unsigned int) data);

	/* Print the buffer. */
	for (i = 0; i < datasize; i += 4) {
		if ((i % 32) == 0) {
			if (i != 0) {
				/* Print the string. */
				ODS("%s\n", buffer);

				/* Reset the line. */
				len = 0;
			}

			len += snprintf(buffer + len, sizeof(buffer) - len,
					"0x%08X: ", (unsigned int) data + i);
		}

		/* Append the data value. */
		len += snprintf(buffer + len, sizeof(buffer) - len,
				" 0x%08X", *(unsigned int *) (data + i));
	}

	/* Print the last partial string. */
	if ((i % 32) != 0)
		ODS("%s\n", buffer);
}

static int gc_dump_enable = 0;
static int gc_dump_before = 0;
static int gc_dump_after = 0;

static void gc_dump_surf(struct bvbltparams *bltparms, int before)
{
	unsigned int op, size;
	unsigned short rop;
	bool src1used, src2used;

	if (!gc_dump_enable)
		return;

	if (before && !gc_dump_before)
		return;

	if (!before && !gc_dump_after)
		return;

	if (before)
		ODS("======= BEFORE OP ========\n");
	else
		ODS("======= AFTER OP ========\n");

	op = bltparms->flags & BVFLAG_OP_MASK;
	if (op == BVFLAG_ROP) {
		rop = bltparms->op.rop;
		src1used = ((((rop & 0xCCCC)  >> 2)
			 ^    (rop & 0x3333)) != 0);
		src2used = ((((rop & 0xF0F0)  >> 4)
			 ^    (rop & 0x0F0F)) != 0);
	} else if (op == BVFLAG_BLEND) {
		return;
	} else {
		return;
	}

	if (src1used) {
		ODS("*** SRC1:\n");
		size = (bltparms->src1.desc->length < 512)
		     ? bltparms->src1.desc->length
		     : 512;
		gc_print_generic((unsigned char *) bltparms->src1.desc->virtaddr, size);
	}

	if (src2used) {
		ODS("*** SRC1:\n");
		size = (bltparms->src2.desc->length < 512)
		     ? bltparms->src2.desc->length
		     : 512;
		gc_print_generic((unsigned char *) bltparms->src1.desc->virtaddr, size);
	}

	ODS("*** DST:\n");
	size = (bltparms->dstdesc->length < 512)
	     ? bltparms->dstdesc->length
	     : 512;
	gc_print_generic((unsigned char *) bltparms->dstdesc->virtaddr, size);
}

void PrintVersion()
{
  ODS("bvtest - version 2.0.07\n");
}

bool ErrorToText(const char*& szErr, bverror error);
enum bverror DetermineSourcesUsedFromBlendConstant(unsigned long k,
                                                   int*          psrc1used,
                                                   int*          psrc2used)
{
  enum bverror err = BVERR_NONE;

#if 1
  switch(k)
  {
    case 0x01:
    case 0x04:
    case 0x06:
    case 0x09:
    case 0x10:
    case 0x11:
    case 0x14:
    case 0x15:
    case 0x20:
    case 0x21:
    case 0x24:
    case 0x25:
    case 0x31:
    case 0x33:
    case 0x34:
    case 0x3C:
      *psrc1used = !0;
      break;

    case 0x12:
    case 0x13:
    case 0x16:
    case 0x17:
    case 0x18:
    case 0x19:
    case 0x1C:
    case 0x1D:
    case 0x22:
    case 0x23:
    case 0x26:
    case 0x27:
    case 0x28:
    case 0x29:
    case 0x2C:
    case 0x2D:
      *psrc1used = !0;
    // fall through
    case 0x03:
    case 0x0B:
    case 0x0C:
    case 0x0E:
    case 0x1A:
    case 0x1B:
    case 0x1E:
    case 0x1F:
    case 0x2A:
    case 0x2B:
    case 0x2E:
    case 0x2F:
    case 0x36:
    case 0x39:
    case 0x3B:
    case 0x3E:
      *psrc2used = !0;
    // fall through
    default:
      break;
  }

#else
  if(k)
  {
    if(k != 0x3F)
    {
      switch(k >> 4)
      {
        case 0:
          switch(k & 0xF)
          {
            case 0x1: // 00 00 01: A1 (preferred)
            case 0x4: // 00 01 00: 1-A1 (preferred)
            case 0x6: // 00 01 10: 1-A1
            case 0x9: // 00 10 01: A1
              *psrc1used = !0;
              break;

            case 0x3: // 00 00 11: A2 (preferred)
            case 0xB: // 00 10 11: A2
            case 0xC: // 00 11 00: 1-A2 (preferred)
            case 0xE: // 00 11 10: 1-A2
              *psrc2used = !0;
              break;

            default:
              err = BVERR_BLEND;
              goto Error;
              // undefined
          }
          break;

        case 1:
        case 2:
          switch(k & 0xA)
          {
            case 0x0: // 01/10 0x 0x
              *psrc1used = !0;
              break;

            case 0x2: // 01/10 0x 1x
            case 0x8: // 01/10 1x 0x
              *psrc1used = !0;
            case 0xA: // 01/10 1x 1x
              *psrc2used = !0;
              break;
          }
          break;

        case 3:
          switch(k & 0xF)
          {
            case 0x1: // 11 00 01: 1-C1
            case 0x3: // 11 00 11: 1-C1 (preferred)
            case 0x4: // 11 01 00: C1
            case 0xC: // 11 11 00: C1 (preferred)
              *psrc1used = !0;
              break;

            case 0x6:
            case 0x9:
            case 0xB:
            case 0xE:
              *psrc2used = !0;
              break;

            default:
              err = BVERR_BLEND;
              goto Error;
          }
          break;
      }
    }
  }
#endif

Error:
  return(err);
}

extern "C"
enum bverror BVDetermineSurfaceUse(int*          psrc1used,
                                   int*          psrc2used,
                                   int*          pmaskused,
                                   int*          pdstusedassrc,
                                   char*         errdesc,
                                   unsigned long bltflags,
                                   union bvop*   pop)
{
  enum bverror err = BVERR_NONE;

  switch((bltflags & BVFLAG_OP_MASK) >> BVFLAG_OP_SHIFT)
  {
    case (BVFLAG_ROP >> BVFLAG_OP_SHIFT):
      {
        unsigned short rop = pop->rop;
        *pdstusedassrc = ((rop & 0xAAAA) >> 1) ^ (rop & 0x5555);
        *psrc1used = ((rop & 0xCCCC) >> 2) ^ (rop & 0x3333);
        *psrc2used = ((rop & 0xF0F0) >> 4) ^ (rop & 0x0F0F);
        *pmaskused = ((rop & 0xFF00) >> 8) ^ (rop & 0x00FF);
      }
      break;

    case (BVFLAG_BLEND >> BVFLAG_OP_SHIFT):
      {
        enum bvblend blend = pop->blend;

        switch((pop->blend & BVBLENDDEF_FORMAT_MASK) >> BVBLENDDEF_FORMAT_SHIFT)
        {
          case (BVBLENDDEF_FORMAT_CLASSIC >> BVBLENDDEF_FORMAT_SHIFT):
            *pmaskused = blend & BVBLENDDEF_REMOTE;

            // Any non-zero constant means the source is used
            *psrc1used = blend & (BVBLENDDEF_K1_MASK | BVBLENDDEF_K3_MASK); // C1 & A1
            *psrc2used = blend & (BVBLENDDEF_K2_MASK | BVBLENDDEF_K4_MASK); // C2 & A2

            // Any constant can contain part of a source, too
            err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K1_MASK) >> BVBLENDDEF_K1_SHIFT,
                                                        psrc1used,
                                                        psrc2used);
            if(err != BVERR_NONE)
              err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K2_MASK) >> BVBLENDDEF_K2_SHIFT,
                                                          psrc1used,
                                                          psrc2used);
            if(err != BVERR_NONE)
              err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K3_MASK) >> BVBLENDDEF_K3_SHIFT,
                                                          psrc1used,
                                                          psrc2used);
            if(err != BVERR_NONE)
              err = DetermineSourcesUsedFromBlendConstant((blend & BVBLENDDEF_K4_MASK) >> BVBLENDDEF_K4_SHIFT,
                                                          psrc1used,
                                                          psrc2used);
            if(err != BVERR_NONE)
            {
              errdesc = (char*)"undefined blend";
              goto Error;
            }
            break;

          default:
            err = BVERR_BLEND;
            errdesc = (char*)"unrecognized blend";
            goto Error;
        }
      }
      break;

    default:
      err = BVERR_OP;
      errdesc = (char*)"unrecognized operation";
      goto Error;
  }

Error:
  return(err);
}

struct ocdformatTEXT
{
  const char* szFmt;
  ocdformat fmt;
  int containersize;
};

ocdformatTEXT ocdformats[] =
{
  { "ALPHA1", OCDFMT_ALPHA1, 1 },
  { "ALPHA2", OCDFMT_ALPHA2, 2 },
  { "ALPHA4", OCDFMT_ALPHA4, 4 },
  { "ALPHA8", OCDFMT_ALPHA8, 8 },
  { "ALPHA4x1", OCDFMT_ALPHA4x1, 1 },
  { "ALPHA3x8", OCDFMT_ALPHA3x8, 2 },
  { "ALPHA4x8", OCDFMT_ALPHA4x8, 4 },
  { "MONO1", OCDFMT_MONO1, 1 },
  { "MONO2", OCDFMT_MONO2, 2 },
  { "MONO4", OCDFMT_MONO4, 4 },
  { "MONO8", OCDFMT_MONO8, 8 },
  { "LUT1", OCDFMT_LUT1, 1 },
  { "LUT2", OCDFMT_LUT2, 2 },
  { "LUT4", OCDFMT_LUT4, 4 },
  { "LUT8", OCDFMT_LUT8, 8 },
  { "RGB12", OCDFMT_RGB12, 16 },
  { "1RGB12", OCDFMT_1RGB12, 16 },
  { "xRGB12", OCDFMT_xRGB12, 16 },
  { "0RGB12", OCDFMT_0RGB12, 16 },
  { "RGB15", OCDFMT_RGB15, 16 },
  { "RGB16", OCDFMT_RGB16, 16 },
  { "RGB24", OCDFMT_RGB24, 24 },
  { "BGR24", OCDFMT_BGR24, 24 },
  { "xRGB24", OCDFMT_xRGB24, 32 },
  { "0RGB24", OCDFMT_0RGB24, 32 },
  { "1RGB24", OCDFMT_1RGB24, 32 },
  { "xBGR24", OCDFMT_xBGR24, 32 },
  { "0BGR24", OCDFMT_0BGR24, 32 },
  { "1BGR24", OCDFMT_1BGR24, 32 },
  { "RGBx24", OCDFMT_RGBx24, 32 },
  { "RGB024", OCDFMT_RGB024, 32 },
  { "RGB124", OCDFMT_RGB124, 32 },
  { "BGRx24", OCDFMT_BGRx24, 32 },
  { "BGR024", OCDFMT_BGR024, 32 },
  { "BGR124", OCDFMT_BGR124, 32 },
  { "ARGB12", OCDFMT_ARGB12, 16 },
  { "ARGB16", OCDFMT_ARGB16, 24 },
  { "RGBA16", OCDFMT_RGBA16, 24 },
  { "ARGB24", OCDFMT_ARGB24, 32 },
  { "ABGR24", OCDFMT_ABGR24, 32 },
  { "RGBA24", OCDFMT_RGBA24, 32 },
  { "BGRA24", OCDFMT_BGRA24, 32 },
  { "nARGB12", OCDFMT_ARGB12_NON_PREMULT, 16 },
  { "ARGB12_NON_PREMULT", OCDFMT_ARGB12_NON_PREMULT, 16 },
  { "ARGB15", OCDFMT_ARGB15, 16 },
  { "nARGB15", OCDFMT_ARGB15_NON_PREMULT, 16 },
  { "ARGB15_NON_PREMULT", OCDFMT_ARGB15_NON_PREMULT, 16 },
  { "nARGB16", OCDFMT_ARGB16_NON_PREMULT, 24 },
  { "ARGB16_NON_PREMULT", OCDFMT_ARGB16_NON_PREMULT, 24 },
  { "nRGBA16", OCDFMT_RGBA16_NON_PREMULT, 24 },
  { "RGBA16_NON_PREMULT", OCDFMT_RGBA16_NON_PREMULT, 24 },
  { "nARGB24", OCDFMT_ARGB24_NON_PREMULT, 32 },
  { "ARGB24_NON_PREMULT", OCDFMT_ARGB24_NON_PREMULT, 32 },
  { "nABGR24", OCDFMT_ABGR24_NON_PREMULT, 32 },
  { "ABGR24_NON_PREMULT", OCDFMT_ABGR24_NON_PREMULT, 32 },
  { "nRGBA24", OCDFMT_RGBA24_NON_PREMULT, 32 },
  { "RGBA24_NON_PREMULT", OCDFMT_RGBA24_NON_PREMULT, 32 },
  { "nBGRA24", OCDFMT_BGRA24_NON_PREMULT, 32 },
  { "BGRA24_NON_PREMULT", OCDFMT_BGRA24_NON_PREMULT, 32 },
  { "UYVY", OCDFMT_UYVY, 16 },
  { "Y422", OCDFMT_Y422, 16 },
  { "VYUY", OCDFMT_VYUY, 16 },
  { "YUYV", OCDFMT_YUYV, 16 },
  { "YUY2", OCDFMT_YUY2, 16 },
  { "YVYU", OCDFMT_YVYU, 16 },
  { "YV12", OCDFMT_YV12, 8 },
  { "IYUV", OCDFMT_I420, 8 },
  { "I420", OCDFMT_I420, 8 },
  { "IMC1", OCDFMT_IMC1, 8 },
  { "IMC2", OCDFMT_IMC2, 8 },
  { "IMC3", OCDFMT_IMC3, 8 },
  { "IMC4", OCDFMT_IMC4, 8 },
  { "NV12", OCDFMT_NV12, 8 },
  { "NV21", OCDFMT_NV21, 8 },
  { "NV16", OCDFMT_NV16, 8 },
  { "NV61", OCDFMT_NV61, 8 },

  // These formats are fake, and are used to intentionally feed bltsville
  // bad data.
  { "f_MONO8",   (enum ocdformat)((OCDFMT_MONO8)  | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 8 },
  { "f_ALPHA8",  (enum ocdformat)((OCDFMT_ALPHA8) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 8 },
  { "f_LUT8",    (enum ocdformat)((OCDFMT_LUT8)   | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 8 },
  { "f_RGB16",   (enum ocdformat)((OCDFMT_RGB16)  | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 16 },
  { "f_RGB24",   (enum ocdformat)((OCDFMT_RGB24)  | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 24 },
  { "f_BGR24",   (enum ocdformat)((OCDFMT_BGR24)  | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 24 },
  { "f_xRGB24",  (enum ocdformat)((OCDFMT_xRGB24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_0RGB24",  (enum ocdformat)((OCDFMT_0RGB24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_xBGR24",  (enum ocdformat)((OCDFMT_xBGR24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_0BGR24",  (enum ocdformat)((OCDFMT_0BGR24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_RGBx24",  (enum ocdformat)((OCDFMT_RGBx24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_RGB024",  (enum ocdformat)((OCDFMT_RGB024) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_BGRx24",  (enum ocdformat)((OCDFMT_BGRx24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_BGR024",  (enum ocdformat)((OCDFMT_BGR024) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_ARGB24",  (enum ocdformat)((OCDFMT_ARGB24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_ABGR24",  (enum ocdformat)((OCDFMT_ABGR24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_RGBA24",  (enum ocdformat)((OCDFMT_RGBA24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_BGRA24",  (enum ocdformat)((OCDFMT_BGRA24) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_nARGB24", (enum ocdformat)((OCDFMT_ARGB24_NON_PREMULT) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_nABGR24", (enum ocdformat)((OCDFMT_ABGR24_NON_PREMULT) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_nRGBA24", (enum ocdformat)((OCDFMT_RGBA24_NON_PREMULT) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_nBGRA24", (enum ocdformat)((OCDFMT_BGRA24_NON_PREMULT) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 32 },
  { "f_UYVY",    (enum ocdformat)((OCDFMT_UYVY) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 16 },
  { "f_YUYV",    (enum ocdformat)((OCDFMT_YUYV) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 16 },
  { "f_YV12",    (enum ocdformat)((OCDFMT_YV12) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 8 },
  { "f_IYUV",    (enum ocdformat)((OCDFMT_I420) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 8 },
  { "f_NV12",    (enum ocdformat)((OCDFMT_NV12) | (0xBD << OCDFMTDEF_VENDOR_SHIFT)), 8 }


};
const int ocdformatsnum = sizeof(ocdformats) / sizeof(ocdformatTEXT);

bvsurfgeom dst = {0};
bvbuffdesc dstdesc = {0};
bvsurfgeom src1 = {0};
bvbuffdesc src1desc = {0};
bvsurfgeom src2 = {0};
bvbuffdesc src2desc = {0};
bvsurfgeom mask = {0};
bvbuffdesc maskdesc = {0};
bvbltparams parms;
bvcopparams copparams;
bool verbose = false;
const char* szDst = 0;
bvsurfgeom dstfile = {0};
bvbuffdesc dstfiledesc = {0};
const char* szSrc1 = 0;
bvsurfgeom src1file = {0};
bvbuffdesc src1filedesc = {0};
const char* szMask = 0;
bvsurfgeom maskfile = {0};
bvbuffdesc maskfiledesc = {0};
const char* szSrc2 = 0;
bvsurfgeom src2file = {0};
bvbuffdesc src2filedesc = {0};
bool profile = false;
const char* szProfout = 0;
int pretouch = 0;
bool preseed = false;
unsigned long colorkey = 0;
bool maptest = false;
bool crossmap = false;
bool batchtest = false;
bool premap = false;

#define VODS(...) if(!verbose) ; else ODS(__VA_ARGS__)
bool priorityFlag2D = false;
bool priorityFlagCPU = true; //Default to CPU priority
bool libLoaded[2] = {false, false};

char* bv_imp = 0;
const char* lib_prefix = "bltsville_";

int loop_times = 1;

SurfOrigin dstorigin = SO_DST;
SurfOrigin src1origin = SO_SRC1;
SurfOrigin src2origin = SO_SRC2;
SurfOrigin maskorigin = SO_MASK;

struct bvsurfgeom* GetGeomOrigin(SurfOrigin origin)
{
  struct bvsurfgeom* pgeom;

  switch(origin)
  {
    case SO_DST: pgeom = &dst; break;
    case SO_SRC1: pgeom = &src1; break;
    case SO_SRC2: pgeom = &src2; break;
    case SO_MASK: pgeom = &mask; break;
  }

  return(pgeom);
}

struct bvbuffdesc* GetDescOrigin(SurfOrigin origin)
{
  struct bvbuffdesc* pBuf;

  switch(origin)
  {
    case SO_DST: pBuf = &dstdesc; break;
    case SO_SRC1: pBuf = &src1desc; break;
    case SO_SRC2: pBuf = &src2desc; break;
    case SO_MASK: pBuf = &maskdesc; break;
  }

  return(pBuf);
}

struct bverrordesc
{
  bverror error;
  const char* szErr;
};

bverrordesc bverrordescs[] =
{
  { BVERR_NONE, "NONE" },
  { BVERR_UNK, "UNKNOWN" },
  { BVERR_OOM, "NOT_ENOUGH_MEMORY" },
  { BVERR_RSRC, "REQUIRED RESOURCE UNAVAILABLE" },
  { BVERR_VIRTADDR, "VIRTADDR IS BAD" },
  { BVERR_BUFFERDESC, "INVALID BVBUFFERDESC" },
  { BVERR_BUFFERDESC_VERS, "bvbuffdesc.structsize too small" },
  { BVERR_BUFFERDESC_VIRTADDR, "bad bvbuffdesc.virtaddr" },
  { BVERR_BUFFERDESC_LEN, "bvbuffdesc.length not supported" },
 // { BVERR_BUFFERDESC_ALIGNMENT , "unsupported buffer base address" },
  { BVERR_BLTPARAMS_VERS, "bvbltparams.structsize too small" },
  { BVERR_IMPLEMENTATION, "bvbltparams.implementation unsupported" },
  { BVERR_FLAGS, "bvbltparams.flags unsupported" },
  { BVERR_OP, "unsupported operation" },
  { BVERR_KEY, "type of color key not supported" },
  { BVERR_SRC1_TILE, "src1 tiling not supported" },
  { BVERR_SRC2_TILE, "src2 tiling not supported" },
  { BVERR_MASK_TILE, "mask tiling not supported" },
  { BVERR_FLIP, "flipping not supported" },
  { BVERR_ROP, "ROP code not supported" },
  { BVERR_BLEND, "blend not supported" },
  { BVERR_GLOBAL_ALPHA, "type of global alpha not supported" },
  { BVERR_FILTER, "filter type not supported" },
  { BVERR_FILTER_PARAMS_VERS, "filter parameter structsize too small" },
  { BVERR_FILTER_PARAMS, "filter parameters not supported" },
  { BVERR_SCALE_MODE, "bvbltparams.scalemode not supported" },
  { BVERR_DITHER_MODE, "bvbltparams.dithermode not supported" },
  { BVERR_DSTDESC, "invalid bvbltparams.dstdesc" },
  { BVERR_DSTDESC_VERS, "bvbuffdesc.structsize too small" },
  { BVERR_DSTDESC_VIRTADDR, "bad bvbuffdesc.virtaddr" },
  { BVERR_DSTDESC_LEN, "bvbuffdesc.length not supported" },
  { BVERR_DST_ALIGNMENT, "unsupported buffer base address" },
  { BVERR_DSTGEOM, " invalid bvbltparams.dstgeom" },
  { BVERR_DSTGEOM_VERS, "dstgeom.structsize too small" },
  { BVERR_DSTGEOM_FORMAT, "bltparams.dstgeom.format not supported" },
  { BVERR_DSTGEOM_STRIDE, "bltparams.dstgeom.stride not supported" },
  { BVERR_DSTRECT, "bvbltparams.dstrect not supported" },
  { BVERR_SRC1DESC, "invalid bvbltparams.src1.desc" },
  { BVERR_SRC1DESC_VERS, "bvbuffdesc.structsize too small" },
  { BVERR_SRC1DESC_VIRTADDR, "bad bvbuffdesc.virtaddr" },
  { BVERR_SRC1DESC_LEN, "bvbuffdesc.length not supported" },
  { BVERR_SRC1DESC_ALIGNMENT, "unsupported buffer base address" },
  { BVERR_SRC1GEOM, " invalid bvbltparams.src1geom" },
  { BVERR_SRC1GEOM_VERS, "src1geom.structsize too small" },
  { BVERR_SRC1GEOM_FORMAT, "bltparams.src1geom.format not supported" },
  { BVERR_SRC1GEOM_STRIDE, "bltparams.src1geom.stride not supported" },
  { BVERR_SRC1RECT, "bvbltparams.src1rect not supported" },
  { BVERR_SRC1_HORZSCALE, "horz scale for src1->dst not supported" },
  { BVERR_SRC1_VERTSCALE, "vert scale for src1->dst not supported" },
  { BVERR_SRC1_ROT, "src1->dst rotation angle not supported" },
  { BVERR_SRC1_TILE_VERS, "src1.tileparams.structsize too small" },
  { BVERR_SRC1_TILE_FLAGS, "tileparams.flags not supported" },
  { BVERR_SRC1_TILE_ORIGIN, "tileparams.left or .top not supported" },
  { BVERR_SRC1_TILE_SIZE, "tileparams.width or .height not supported" },
  { BVERR_SRC2DESC, "invalid bvbltparams.src2.desc" },
  { BVERR_SRC2DESC_VERS, "bvbuffdesc.structsize too small" },
  { BVERR_SRC2DESC_VIRTADDR, "bad bvbuffdesc.virtaddr" },
  { BVERR_SRC2DESC_LEN, "bvbuffdesc.length not supported" },
  { BVERR_SRC2DESC_ALIGNMENT, "unsupported buffer base address" },
  { BVERR_SRC2GEOM, " invalid bvbltparams.src2geom" },
  { BVERR_SRC2GEOM_VERS, "src1geom.structsize too small" },
  { BVERR_SRC2GEOM_FORMAT, "bltparams.src2geom.format not supported" },
  { BVERR_SRC2GEOM_STRIDE, "bltparams.src2geom.stride not supported" },
  { BVERR_SRC2RECT, "bvbltparams.src2rect not supported" },
  { BVERR_SRC2_HORZSCALE, "horz scale for src2->dst not supported" },
  { BVERR_SRC2_VERTSCALE, "vert scale for src2->dst not supported" },
  { BVERR_SRC2_ROT, "src2->dst rotation angle not supported" },
  { BVERR_SRC2_TILE_VERS, "src2.tileparams.structsize too small" },
  { BVERR_SRC2_TILE_FLAGS, "tileparams.flags not supported" },
  { BVERR_SRC2_TILE_ORIGIN, "tileparams.left or .top not supported" },
  { BVERR_SRC2_TILE_SIZE, "tileparams.width or .height not supported" },
  { BVERR_MASKDESC, "invalid bvbltparams.mask.desc" },
  { BVERR_MASKDESC_VERS, "bvbuffdesc.structsize too small" },
  { BVERR_MASKDESC_VIRTADDR, "bad bvbuffdesc.virtaddr" },
  { BVERR_MASKDESC_LEN, "bvbuffdesc.length not supported" },
  { BVERR_MASKDESC_ALIGNMENT, "unsupported buffer base address" },
  { BVERR_MASKGEOM, " invalid bvbltparams.src1geom" },
  { BVERR_MASKGEOM_VERS, "maskgeom.structsize too small" },
  { BVERR_MASKGEOM_FORMAT, "bltparams.maskgeom.format not supported" },
  { BVERR_MASKGEOM_STRIDE, "bltparams.maskgeom.stride not supported" },
  { BVERR_MASKRECT, "bvbltparams.maskrect not supported" },
  { BVERR_MASK_HORZSCALE, "horz scale for mask->dst not supported" },
  { BVERR_MASK_VERTSCALE, "vert scale for mask->dst not supported" },
  { BVERR_MASK_ROT, "mask->dst rotation angle not supported" },
  { BVERR_MASK_TILE_VERS, "mask.tileparams.structsize too small" },
  { BVERR_MASK_TILE_FLAGS, "tileparams.flags not supported" },
  { BVERR_MASK_TILE_ORIGIN, "tileparams.left or .top not supported" },
  { BVERR_MASK_TILE_SIZE, "tileparams.width or .height not supported" },
  { BVERR_CLIP_RECT, "bvbltparams.cliprect not supported" },
  { BVERR_BATCH_FLAGS, "bvbltparams.batchflags not supported" },
  { BVERR_BATCH, "bvbltparams.batch not valid" },
  { BVERR_OP_FAILED, "async operation failed to start" },
  { BVERR_OP_INCOMPLETE, "async operation failed mid-way" },
  { BVERR_MEMORY_ERROR, "async operation triggered memory error" },
  { BVERR_DSTGEOM_PALETTE, "dest palette not supported" },
  { BVERR_SRC1GEOM_PALETTE, "src1 palette not supported" },
  { BVERR_SRC2GEOM_PALETTE, "src2 palette not supported" },
  { BVERR_MASKGEOM_PALETTE, "mask palette not supported" },
};

const int bverrorS = sizeof(bverrordescs) / sizeof(bverrordesc);

#ifdef ENABLE_OMAPDRM
static int gc_init_dmabuf()
{
  if(drmFd > 0)
  {
    VODS("Already Opened OMAP FD\n");
    return 0;
  }

  VODS("Opening OMAP FD - ");
  drmFd = drmOpen("omapdrm", "platform:omapdrm:00");
  if(drmFd < 0)
  {
    ODS("Invalid drm Handle %d\n", drmFd);
    return -1;
  }
  else
  {
    VODS(" 0x%x Successful\n", drmFd);
    VODS("Opening DRM device - ");
    if(!drmDev)
    {
        drmDev = omap_device_new(drmFd);
    }
    if(drmDev == NULL)
    {
      ODS("Unable to open DRM device -> Error:0x%x\n", drmDev);
      return -1;
    }
    else
      VODS("Successful\n");
  }

  return 0;
}

static void gc_deinit_dmabuf(void)
{
    drmFd = 0;
    drmDev = NULL;
}

static int gc_alloc_dmabuf(SurfOrigin origin, unsigned int len)
{
  struct dmabuf* pDMABuf;

  switch(origin)
  {
    case SO_DST:  pDMABuf = &dmabufDst; break;
    case SO_SRC1: pDMABuf = &dmabufSrc1; break;
    case SO_SRC2: pDMABuf = &dmabufSrc2; break;
    case SO_MASK: pDMABuf = &dmabufMask; break;
  }

  VODS("Allocating %d bytes in omap_drm..", len);
  pDMABuf->bo = omap_bo_new(drmDev, len, OMAP_BO_WC);
  if(pDMABuf->bo)
    VODS("done\n");
  else
  {
    ODS("fail\n");
    return -1;
  }
  pDMABuf->mem = (unsigned int*) omap_bo_map(pDMABuf->bo);
  pDMABuf->handle = omap_bo_dmabuf(pDMABuf->bo);
  VODS("getting dma_buf for %d bo = 0x%p, fd = %d\n", origin, pDMABuf->bo, pDMABuf->handle);

  return 0;
}

static void gc_dealloc_dmabuf(SurfOrigin origin)
{
  struct dmabuf* pDMABuf;

  switch(origin)
  {
    case SO_DST:  pDMABuf = &dmabufDst; break;
    case SO_SRC1: pDMABuf = &dmabufSrc1; break;
    case SO_SRC2: pDMABuf = &dmabufSrc2; break;
    case SO_MASK: pDMABuf = &dmabufMask; break;
  }

  if (pDMABuf->bo != NULL)
  {
    VODS("freeing memory bo=0x%p\n", pDMABuf->bo);
    omap_bo_del(pDMABuf->bo);
    pDMABuf->bo = NULL;
  }

  pDMABuf->mem = NULL;
  pDMABuf->handle = 0;
}

#endif //ENABLE_OMAPDRM
void Usage(const char* szapp)
{
  PrintVersion();
  ODS("USAGE:  %s\n", szapp);
  ODS("-version\n"
      "  Print out version number and exit immediately.\n");
  ODS("-verbose\n"
      "  Print out more verbose messages while parsing the command line.\n");
  ODS("-imp |cpu|cpupriority|2d|2dpriority|tidma|ref\n"
    " Choose an implementation of the bltsville library.\n "
    "If -imp flag is not set, default to cpupriority\n");
  ODS("-rop XXXX\n"
      "  Specify a quaternary raster operation in hexadecimal format.\n");
  ODS("-dst <format> ###x### OR -dst <format> src1|src2\n"
      "  Specify a destination surface format (from list below) and size.\n");
  ODS("-dstrect ###,###-###x###\n"
      "  Specify a rectangle within the destination surface which receives the BLT.\n");
  ODS("-dstfile <file.>bmp|tga|raw\n"
      "  Specify a file into which to save the results.  Must be either BMP or TGA.\n");
  ODS("-dstangle ###\n"
      "  Specify orientation of destination surface in degrees.\n");
  ODS("-src1 <format> ###x### OR -src1 <format> dst|src2\n"
      "  Specify a source surface format (from list below) and size.\n");
  ODS("-src1rect ###,###-###x###\n"
      "  Specify a rectangle within the source surface used as the source for the BLT.\n");
  ODS("-src1file <file.>jpg|bmp|tga|png|psd|hdr|raw\n"
      "  Specify a file from which the source surface is initialized.  The image is\n"
      "  placed in the upper left corner of the source surface and clipped if\n"
      "  necessary.\n");
  ODS("-src1angle ###\n"
      "  Specify orientation of source surface in degrees.\n");
  ODS("-mask <format> ###x###\n"
      "  Specify a mask surface format (from list below) and size.\n");
  ODS("-maskrect ###,###-###x###\n"
      "  Specify a rectangle within the mask surface used as the mask for\n"
      "  the BLT.\n");
  ODS("-maskfile <file.>jpg|bmp|tga|png|psd|hdr\n"
      "  Specify a file from which the mask surface is initialized.  The image\n"
      "  is placed in the upper left corner of the mask surface and clipped if\n"
      "  necessary.\n");
  ODS("-maskangle ###\n"
      "  Specify orientation of mask surface in degrees.\n");
  ODS("-alpha <AA>\n"
      "  Specify the global alpha value as a hexadecimal value between 00\n"
      "  (transparent) to FF (opaque).\n");
  ODS("-src2 <format> ###x### OR -src2 <format> dst|src1\n"
      "  Specify a source2 surface format (from list below) and size.\n");
  ODS("-src2rect ###,###-###x###\n"
      "  Specify a rectangle within the source2 surface used as the\n"
      "  source2 for the BLT.\n");
  ODS("-src2file <file.>jpg|bmp|tga|png|psd|hdr\n"
      "  Specify a file from which the source2 surface is initialized.  The\n"
      "  image is placed in the upper left corner of the source2 surface and\n"
      "  clipped if necessary.\n");
  ODS("-src2angle ###\n"
      "  Specify orientation of source2 in degrees.\n");
  ODS("-dither none|fastest|best|ordered2x2 etc \n"
      "  Specify the type of dithering to use.\n");
  ODS("-srcmask\n"
      "  Specify that the mask is applied to the source prior to scaling.  The\n"
      "  default is to apply the mask to the destination after scaling.\n");
  ODS("-scale best|fastest|point|bilinear|bicubic|nearest_neighbor|3tap|5tap|7tap|9tap \n"
      "  Specify the type of scaling to use.\n");
  ODS("-srckey <RRGGBB|YYUUVV> <RRGGBB|YYUUVV>\n"
      "  Specify the range of colors to use as a source color key in hexadecimal\n"
      "  format.\n");
  ODS("-dstkey <RRGGBB|YYUUVV> <RRGGBB|YYUUVV>\n"
      "  Specify the range of colors to use as a destination color key in hexadecimal\n"
      "  format.\n");
  ODS("-blend clear|src1|src2|src1over|src2over|src1in|src2in|src1out|src2out|src1atop|src2atop|xor|plus|remote|global\n"
      "  Specify the type of blend to use. Multiple -blend parameters can be specified.\n");
  ODS("-cliprect ###,###-###x###\n"
      "  Specify a rectangle which is used to indicate the area in the destination\n"
      "  which is written, and outside of which the destination is not modified.\n");
  ODS("-profile\n"
       " Enable measurement of time to perform BLT.\n");
  ODS("-profout <file>\n"
      " Output (append) profile measurement to specified files.\n");
  ODS("-pretouch <step>\n"
      " For each input and output buffer, read or write one byte every <step> bytes\n"
      " to ensure on-demand paging is not part of the profiling measurement.\n");
  ODS("-preseed\n"
      " Perform a mini version of the specified BLT in order to allow dynamic\n"
      " implementations to load without affecting profiling.\n");
  ODS("-callback\n"
      " Test the callback functionality.\n");
  ODS("-map test|cross\n"
      " 'test' will test the bv_map and bv_unmap funtions.\n"
      " 'cross' will map the surface to cpuref instead of the main bltsville library.");
  ODS("-batch\n"
      " Test batch blt functionality.\n");

  ODS("\nFORMATS\n");
  int i;
  for(i = 0; i < ocdformatsnum; i++)
  {
    ODS("  %s", ocdformats[i].szFmt);
    while((i < (ocdformatsnum - 1)) &&
          (ocdformats[i].fmt == ocdformats[i + 1].fmt))
    {
      i++;
      ODS(" | %s", ocdformats[i].szFmt);
    }
    if(i < (ocdformatsnum - 1))
      ODS(", ");
    else
      ODS("\n");
  }
}

struct TypeTrans
{
  const char* szType;
  unsigned long type;
};

TypeTrans dithertypes[] =
{
  { "fastest",         (unsigned long) BVDITHER_FASTEST },
  { "fastest_on",      (unsigned long) BVDITHER_FASTEST_ON },
  { "fastest_random",  (unsigned long) BVDITHER_FASTEST_RANDOM },
  { "fastest_ordered", (unsigned long) BVDITHER_FASTEST_ORDERED },
  { "fastest_diffused",(unsigned long) BVDITHER_FASTEST_DIFFUSED },
  { "fastest_photo",   (unsigned long) BVDITHER_FASTEST_PHOTO },
  { "fastest_drawing", (unsigned long) BVDITHER_FASTEST_DRAWING },
  { "good",            (unsigned long) BVDITHER_GOOD },
  { "good_on",         (unsigned long) BVDITHER_GOOD_ON },
  { "good_random",     (unsigned long) BVDITHER_GOOD_RANDOM },
  { "good_ordered",    (unsigned long) BVDITHER_GOOD_ORDERED },
  { "good_diffused",   (unsigned long) BVDITHER_GOOD_DIFFUSED },
  { "good_photo",      (unsigned long) BVDITHER_GOOD_PHOTO },
  { "good_drawing",    (unsigned long) BVDITHER_GOOD_DRAWING },
  { "better",          (unsigned long) BVDITHER_BETTER },
  { "better_on",       (unsigned long) BVDITHER_BETTER_ON },
  { "better_random",   (unsigned long) BVDITHER_BETTER_RANDOM },
  { "better_ordered",  (unsigned long) BVDITHER_BETTER_ORDERED },
  { "better_diffused", (unsigned long) BVDITHER_BETTER_DIFFUSED },
  { "better_photo",    (unsigned long) BVDITHER_BETTER_PHOTO },
  { "better_drawing",  (unsigned long) BVDITHER_BETTER_DRAWING },
  { "best",            (unsigned long) BVDITHER_BEST },
  { "best_on",         (unsigned long) BVDITHER_BEST_ON },
  { "best_random",     (unsigned long) BVDITHER_BEST_RANDOM },
  { "best_ordered",    (unsigned long) BVDITHER_BEST_ORDERED },
  { "best_diffused",   (unsigned long) BVDITHER_BEST_DIFFUSED },
  { "best_photo",      (unsigned long) BVDITHER_BEST_PHOTO },
  { "best_drawing",    (unsigned long) BVDITHER_BEST_DRAWING },
  { "none",            (unsigned long) BVDITHER_NONE },
  { "ordered2x2",      (unsigned long) BVDITHER_ORDERED_2x2 },
  { "ordered_4x4",     (unsigned long) BVDITHER_ORDERED_4x4 },
  { "ordered_2x2_4x4", (unsigned long) BVDITHER_ORDERED_2x2_4x4 },
};
const int DITHERTYPES = sizeof(dithertypes) / sizeof(dithertypes[0]);

TypeTrans scaletypes[] =
{
  { "fastest",          (unsigned long)BVSCALE_FASTEST},
  { "best",             (unsigned long)BVSCALE_BEST },
  { "point",            (unsigned long)BVSCALE_FASTEST_POINT_SAMPLE },
  { "nearest_neighbor", (unsigned long)BVSCALE_NEAREST_NEIGHBOR },
  { "bilinear",         (unsigned long)BVSCALE_BILINEAR },
  { "bicubic",          (unsigned long)BVSCALE_BICUBIC },
  { "3tap",             (unsigned long)BVSCALE_3x3_TAP },
  { "5tap",             (unsigned long)BVSCALE_5x5_TAP },
  { "7tap",             (unsigned long)BVSCALE_7x7_TAP },
  { "9tap",             (unsigned long)BVSCALE_9x9_TAP },

};
const int SCALETYPES = sizeof(scaletypes) / sizeof(scaletypes[0]);

TypeTrans blendtypes[] =
{
  { "clear", (unsigned long)BVBLEND_CLEAR },
  { "src1", (unsigned long)BVBLEND_SRC1 },
  { "src2", (unsigned long)BVBLEND_SRC2 },
  { "src1over", (unsigned long)BVBLEND_SRC1OVER },
  { "src2over", (unsigned long)BVBLEND_SRC2OVER },
  { "src1in", (unsigned long)BVBLEND_SRC1IN },
  { "src2in", (unsigned long)BVBLEND_SRC2IN },
  { "src1out", (unsigned long)BVBLEND_SRC1OUT },
  { "src2out", (unsigned long)BVBLEND_SRC2OUT },
  { "src1atop", (unsigned long)BVBLEND_SRC1ATOP },
  { "src2atop", (unsigned long)BVBLEND_SRC2ATOP },
  { "xor", (unsigned long)BVBLEND_XOR },
  { "plus", (unsigned long)BVBLEND_PLUS },
  { "remote", (unsigned long)BVBLENDDEF_REMOTE },
  { "global", (unsigned long)BVBLENDDEF_GLOBAL_UCHAR }
};
const int BLENDTYPES = sizeof(blendtypes) / sizeof(blendtypes[0]);

timespec res_realtime = {0};
timespec res_monotonic = {0};
timespec res_process_cputime = {0};
timespec res_thread_cputime = {0};

struct bltsvillelib
{
  const char* libname;
  void* hlib;
  BVFN_MAP bv_map;
  BVFN_BLT bv_blt;
  BVFN_UNMAP bv_unmap;
  BVFN_CACHE bv_cache;
};

//Used for targeted BLT
typedef struct bltsvillelib bltsvillelib;

bltsvillelib bvlib =
{
	"bltsville_cpu", 0
};

bltsvillelib bvreflib =
{
	"bltsville_ref", 0
};

//Used for pre and post BLT
bltsvillelib prebvlib[2] =
{
  {"bltsville_cpu", 0},
  {"bltsville_2d", 0}
};

#define NUMLIBS (sizeof(prebvlib) / sizeof(struct bltsvillelib))

#define BV_CACHE(bvcache, copparams)\
if (bvcache) {\
	err = bvcache(copparams);\
}

bool ImportFunction(struct bltsvillelib *bvlib)
{

  bool success = false;
  //No need to Load library again if it's already loaded.
  if(!bvlib->hlib)
  {
    VODS("%s(): BLTsville library lib%s.so has not been loaded, attempting to load.\n", __FUNCTION__, bvlib->libname);
    bvlib->hlib = OSLoadLibrary(bvlib->libname);
    if(!bvlib->hlib)
    {
      ODS("%s(): Error opening BLTsville library lib%s.so.\n", __FUNCTION__, bvlib->libname);
      goto Error;
    }
  }
  VODS("%s(): BLTsville library lib%s.so opened successfully\n", __FUNCTION__, bvlib->libname);
  bvlib->bv_map = (BVFN_MAP)dlsym(bvlib->hlib, "bv_map");
  if(!bvlib->bv_map)
  {
    ODS("%s(): Error importing bv_map entry point from BLTsville library lib%s.so.\n", __FUNCTION__, bvlib->libname);
    goto BadLib;
  }

  bvlib->bv_blt = (BVFN_BLT)dlsym(bvlib->hlib, "bv_blt");
  if(!bvlib->bv_blt)
  {
    ODS("%s(): Error importing bv_blt entry point from BLTsville library lib%s.so.\n", __FUNCTION__, bvlib->libname);
    goto BadLib;
  }

  bvlib->bv_unmap = (BVFN_UNMAP)dlsym(bvlib->hlib, "bv_unmap");
  if(!bvlib->bv_unmap)
  {
    ODS("%s(): Error importing bv_unmap entry point from BLTsville library lib%s.so.\n", __FUNCTION__, bvlib->libname);
  }

  bvlib->bv_cache = (BVFN_CACHE)dlsym(bvlib->hlib, "bv_cache");
  if(!bvlib->bv_unmap)
  {
    ODS("%s(): Error importing bv_unmap entry point from BLTsville library lib%s.so.\n", __FUNCTION__, bvlib->libname);
  }

BadLib:
  if(!bvlib->bv_unmap)
  {
    OSUnloadLibrary(bvlib->hlib);
    memset(&bvlib, 0, sizeof(bvlib));
    goto Error;
  }

  success = true;
Error:
  return(success);
}

//Wrapper function used to perform Pre and Post BLT
bverror pblt(struct bvbltparams *bltparms)
{
  bverror err = BVERR_UNK;
  for(int i = 0; i < sizeof(prebvlib) / sizeof(struct bltsvillelib); i++)
  {
    if(prebvlib[i].hlib)
    {
      if(ImportFunction(&prebvlib[i]))
      {
        libLoaded[i] = true;
      }
    }
  }
  for(int i = 0; i < sizeof(prebvlib) / sizeof(struct bltsvillelib); i++)
  {
    if(libLoaded[i])
    {
      gc_dump_surf(bltparms, 1);
      err = prebvlib[i].bv_blt(bltparms);
      if(err == BVERR_NONE) {
        gc_dump_surf(bltparms, 0);
        break;
      }
    }
    else
      err = BVERR_RSRC;
  }
Error:
  return err;
}

//Wrapper function used to perform targeted BLT
bverror blt(struct bvbltparams *bltparms)
{
  bverror err = BVERR_RSRC;
  if(priorityFlagCPU)
  {
    for(int i = 0; i < sizeof(prebvlib) / sizeof(struct bltsvillelib); i++)
    {
      if(libLoaded[i])
      {
        gc_dump_surf(bltparms, 1);
        err = prebvlib[i].bv_blt(bltparms);
        if(err == BVERR_NONE) {
          gc_dump_surf(bltparms, 0);
          break;
	}
        else
        {
          ODS("Library %s failed. Trying next library.\n", prebvlib[i].libname);
        }
      }
      else
      {
        const char* szErr;
        ErrorToText(szErr, err);
        ODS("%s(): Library '%s' not loaded. Trying next library in the list.\n",
             __FUNCTION__, prebvlib[i].libname);
      }
    }
    goto Error;
  }
  else if(priorityFlag2D)
  {
    for(int i = NUMLIBS-1; i >= 0; i--)
    {
      if(libLoaded[i])
      {
        gc_dump_surf(bltparms, 1);
        err = prebvlib[i].bv_blt(bltparms);
        if(err == BVERR_NONE) {
          gc_dump_surf(bltparms, 0);
          break;
	}
        else
        {
          ODS("Library %s failed. Trying next library.\n", prebvlib[i].libname);
        }
      }
      else
      {
          const char* szErr;
          ErrorToText(szErr, err);
          ODS("%s(): Library '%s' not loaded. Trying next library in the list.\n",
               __FUNCTION__, prebvlib[i].libname);
      }
    }
    goto Error;
  }
  else
  {
    gc_dump_surf(bltparms, 1);
    err = bvlib.bv_blt(bltparms);
    if(err == BVERR_NONE) {
      gc_dump_surf(bltparms, 0);
    }
  }
Error:
  return err;
}

// This code is to test the callback function from bv_blt
unsigned long our_callback_data = 0x12345678; // callback will modify this

// Our callback function.  bv_blt() will call this when the blt is complete.
// It is only called if the blt is successful.
void bvtest_cb_fn(struct bvcallbackerror* err, unsigned long callbackdata)
{
  VODS("bv_blt() called our callback function.\n");
  if (!callbackdata)
  {
    ODS("ERROR: bv_blt() called our callback function with null data!\n");
  }
  else
  {
      // Modify our callback data to make sure bv_blt gave us a pointer
      // to the right thing.
      // PerformBLT() will test the modified data and make sure it is correct.

      unsigned long *ourdata = (unsigned long *)callbackdata;
      *ourdata = 0xAAAAAAAA;

      // Extract the error code from the callback data structure.
      // Unless there has been some kind of unexpected hardware error, this
      // should be NULL
      if (err)
      {
        ODS("ERROR: bv_blt() called our callback function with and error code!\n");
      }
  }
}

bool Init()
{
  bool success = false;

  //Loading libraries for pre and post BLTs
  for(int i = 0; i < sizeof(prebvlib) / sizeof(struct bltsvillelib); i++)
    prebvlib[i].hlib = OSLoadLibrary(prebvlib[i].libname);

  //Loading libraries for reference library
  bvreflib.hlib = OSLoadLibrary(bvreflib.libname);
  if (bvreflib.hlib)
     ImportFunction(&(bvreflib));

  memset(&dst, 0, sizeof(dst));
  dst.structsize = sizeof(dst);
  dst.format = (ocdformat)-1;
  dstdesc.structsize = sizeof(dstdesc);
  dstfiledesc.structsize = sizeof(dstfiledesc);

  memset(&src1, 0, sizeof(src1));
  src1.structsize = sizeof(src1);
  src1.format = (ocdformat)-1;
  src1desc.structsize = sizeof(src1desc);
  src1filedesc.structsize = sizeof(src1filedesc);

  memset(&src2, 0, sizeof(src2));
  src2.structsize = sizeof(src2);
  src2.format = (ocdformat)-1;
  src2desc.structsize = sizeof(src2desc);
  src2filedesc.structsize = sizeof(src2filedesc);

  memset(&mask, 0, sizeof(mask));
  mask.structsize = sizeof(mask);
  mask.format = (ocdformat)-1;
  maskdesc.structsize = sizeof(maskdesc);
  maskfiledesc.structsize = sizeof(maskfiledesc);

  memset(&dstfile, 0, sizeof(dstfile));
  dstfile.structsize = sizeof(dstfile);

  memset(&src1file, 0, sizeof(src1file));
  src1file.structsize = sizeof(src1file);

  memset(&src2file, 0, sizeof(src2file));
  src2file.structsize = sizeof(src2file);

  memset(&maskfile, 0, sizeof(maskfile));
  maskfile.structsize = sizeof(maskfile);

  memset(&parms, 0, sizeof(parms));
  parms.structsize = sizeof(parms);
  parms.dstgeom = &dst;
  parms.dstdesc = &dstdesc;

#ifdef ENABLE_OMAPDRM
  if (gc_init_dmabuf() < 0) {
      return false;
  }
#endif

  pblt(&parms);
//clock_getres(CLOCK_REALTIME, &res_realtime);
//ODS("%s(): res_realtime.tv_sec = %d, res_realtime.tv_nsec = %d.\n", __FUNCTION__, res_realtime.tv_sec, res_realtime.tv_nsec);
clock_getres(CLOCK_REALTIME, &res_monotonic);
VODS("Clock precision is %d nS.\n", res_monotonic.tv_nsec);
//ODS("%s(): res_monotonic.tv_sec = %d, res_monotonic.tv_nsec = %d.\n", __FUNCTION__, res_monotonic.tv_sec, res_monotonic.tv_nsec);
//clock_getres(CLOCK_REALTIME, &res_process_cputime);
//ODS("%s(): res_process_cputime.tv_sec = %d, res_process_cputime.tv_nsec = %d.\n", __FUNCTION__, res_process_cputime.tv_sec, res_process_cputime.tv_nsec);
//clock_getres(CLOCK_REALTIME, &res_thread_cputime);
//ODS("%s(): res_thread_cputime.tv_sec = %d, res_thread_cputime.tv_nsec = %d.\n", __FUNCTION__, res_thread_cputime.tv_sec, res_thread_cputime.tv_nsec);

pblt(&parms);

  success = true;
Error:
  return(success);
}

void Cleanup()
{
  for(int i = 0; i < sizeof(prebvlib) / sizeof(struct bltsvillelib); i++)
  {
    if(prebvlib[i].hlib)
      OSUnloadLibrary(prebvlib[i].hlib);
  }

  if(bvlib.hlib)
    OSUnloadLibrary(bvlib.hlib);

#ifdef ENABLE_OMAPDRM
  gc_deinit_dmabuf();
#endif
}

const char* HexToUL(unsigned long& val,
                    const char*    p)
{
  const char* szErr = 0;

  val = 0;

  char c = *p;
  while(c)
  {
    if((c >= '0') && (c <= '9'))
      val = (val * 16) + (c - '0');
    else if((c >= 'A') && (c <= 'F'))
      val = (val * 16) + (10 + c - 'A');
    else if((c >= 'a') && (c <= 'f'))
      val = (val * 16) + (10 + c - 'a');
    else
    {
      szErr = "Invalid hex value";
      break;
    }
    p++;
    c = *p;
  }

  return(szErr);
}

const char* TextToFormat(ocdformat&   fmt,
                         const char* szFmt)
{
  const char* szErr = 0;

  int i;
  for(i = 0; i < ocdformatsnum; i++)
  {
    if(!stricmp(ocdformats[i].szFmt, szFmt))
      break;
  }

  if(i < ocdformatsnum)
    fmt = ocdformats[i].fmt;
  else
  {
    unsigned long val;
    VODS("Unrecognized format (%s), checking for hex value.\n", szFmt);
    if(HexToUL(val, szFmt))
    {
      szErr = "Unrecognized format";
      goto Error;
    }

    fmt = (ocdformat)val;
  }

Error:
  return(szErr);
}

const char* FormatToText(ocdformat fmt)
{
  int i;

  for(i = 0; i < ocdformatsnum; i++)
  {
    if(fmt == ocdformats[i].fmt)
      break;
  }

  return(ocdformats[i].szFmt);
}

bool FormatToContainerSize(int&      containersize,
                           ocdformat& fmt)
{
  bool success = false;
  int i;
  for(i = 0; i < ocdformatsnum; i++)
  {
    if(ocdformats[i].fmt == fmt)
      break;
  }

  if(i < ocdformatsnum)
    containersize = ocdformats[i].containersize;

  success = true;
Error:
  return(success);
}

bool ErrorToText(const char*& szErr,
                 bverror   error)
{
  int i;
  for(i = 0; i < bverrorS; i++)
  {
    if(bverrordescs[i].error == error)
      break;
  }

  if(i < bverrorS)
    szErr = bverrordescs[i].szErr;
  else
    szErr = bverrordescs[1].szErr; // unknown

  return(i < bverrorS);
}

bool TextToInt(int&         val,
               const char*& p,
               const char   end)
{
  bool success = true;
  int v = 0;
  bool vneg = false;
  if(*p == '-')
  {
    vneg = true;
    p++;
  }

  do
  {
    if((*p <'0') || (*p >'9'))
    {
      success = false;
      goto Error;
    }

    v = (v * 10) + (*p - '0');

    p++;
  }while(*p != end);

  val = vneg ? -v : v;
Error:
  return(success);
}

bool TextToValPair(int& val1,
                   int& val2,
                   const char*& p,
                   const char   sep,
                   const char   end)
{
  bool success;
  const char* szVals = p;
  if(!(success = TextToInt(val1, p, sep)))
  {
    ODS("Invalid first value of value pair: %s\n", szVals);
  }
  else
  {
    p++;
    if(!(success = TextToInt(val2, p, end)))
    {
      ODS("Invalid second value of value pair: %s\n", szVals);
    }
  }

  return(success);
}

const char* GetRect(bvrect&  rect, const char* p)
{
  const char* szErr = 0;

  if(!TextToValPair((int&)rect.left, (int&)rect.top, p, ',', '-'))
  {
    szErr = "Invalid upper left coordinate";
    goto Error;
  }

  if(*p)
    p++;

  if(!TextToValPair((int&)rect.width, (int&)rect.height, p, 'x', '\0'))
  {
    szErr = "Invalid size";
    goto Error;
  }

Error:
  return(szErr);
}

const char* GetDimensions(unsigned int& w,
                          unsigned int& h,
                          const char*   p)
{
  const char* szErr = 0;
  int width, height;

  if(!TextToValPair(width, height, p, 'x', '\0'))
  {
    szErr = "Invalid size specified";
    goto Error;
  }

  if((width <= 0) || (height <= 0))
  {
    szErr = "Invalid surface size";
    goto Error;
  }

  w = (unsigned int)width;
  h = (unsigned int)height;

Error:
  return(szErr);
}

//==============================================================================
// Parse types from the command line.
//
// The user has supplied one or more type descriptors on the command line,
// separated by plus signs such as "white+yellow+red".
// Validate each of the requested type descriptors against the list of allowed
// types supplied in the array "types".
// For each of the validated type descriptors, look up it's type value and
// OR all the type values together, and return their combined value.
// =============================================================================
bool ParseType(unsigned long& type,
               TypeTrans*     types,
               int            maxtypes,
               int            argc,
               const char*    argv[],
               int            arg)
{
  bool success = false;
  int i,j;
  char *ptr;

  // These hold the list of requested types, copied from the command line.
  // We are limited to 64 items taking up no more than 255 chars.
  char requested_type_list[256]; // Holds the entire list of all types requested
  char *requested_types[64] = {0}; // Pointers to each of the items in the list.

  // Make sure there is an arg to parse
  if((arg + 1) >= argc)
  {
    ODS("No type provided with %s option (ex. %s type).\n", argv[arg], argv[arg]);
    goto Error;
  }

  if (strlen(argv[arg + 1]) > 255) // type list too long to parse
  {
    ODS("List of types provided with %s option is too long to parse.\n", argv[arg]);
    goto Error;
  }

  // Copy the command line arg to our local list, so we can manipulate it.
  strcpy(requested_type_list, argv[arg + 1]);

   // walk the list and convert it to an array of strings.
  // Convert the "+" signs to NULLs
  // Fill in the array of pointers to the types.
  // Count how many requested types there are.
  i = 0;
  ptr = requested_type_list;
  while (1)
  {
    // store pointer to the next requested type string.
    requested_types[i++] = ptr;

    // Find the "+" sign that separates this one from the next one.
    while ( (*ptr != 0)  && (*ptr != '|') && (*ptr != '+') )
      ++ ptr;

    if (*ptr == 0) // End of list
      break;

    // Replace the "+" with a NULL
    *ptr = 0;

    // Advance to next requested type.
    if (i > 64) // List is full...
    {
      ODS("List of types provided with %s option has more than 64 types in it.\n", argv[arg]);
      goto Error;
    }
    ++ptr;
  }

  // requested_types[] is now an array of strings of the requested types
  // go through it (in reverse) and process each string.
  // i is the number of items in this string
  type = 0;
  while (i > 0)
  {
    --i;

    // Locate this requested type in the list of allowed types
    bool found = false;
    for(j = 0; j < maxtypes; j++)
    {
      if(!stricmp(types[j].szType, requested_types[i]))
      {
        // Found it. OR it into the final value.
        found = true;
        type |= types[j].type;
      }
    }

    if (!found) // User requested an invalid type
    {
      ODS("Type '%s' not allowed for argument '%s'.\n", requested_types[i], argv[arg]);
      goto Error;
    }
  }

  success = true;

Error:
  return(success);
}

bool ParseROP(unsigned short& ROP,
              int             argc,
              const char*     argv[],
              int&            arg)
{
  bool success = false;
  const char* szErr;
  unsigned long rop;

  if((arg + 1) >= argc)
  {
    ODS("No ROP code provided with -rop option.\n");
    goto Error;
  }

  szErr = HexToUL(rop, argv[arg + 1]);
  if(szErr)
  {
    ODS("%s with option -rop.\n", szErr);
    goto Error;
  }

  if(rop > 0xFFFF)
  {
    ODS("Invalid ROP code provided for -rop option: %s (ex. -rop CCCC).\n", argv[arg + 1]);
    goto Error;
  }
  parms.op.rop = rop;

  VODS("-rop %04X\n", parms.op.rop);
  arg += 2;

  VODS("Setting BVFLAG_ROP in parms.flags.\n");
  parms.flags |= BVFLAG_ROP;

  success = true;
Error:
  return(success);
}

bool ParseImp(int         argc,
              const char* argv[],
              int&        arg)
{
  bool success = false;
  const char* szErr;
  unsigned long alpha;
  char libname[50];

  if((arg + 1) >= argc)
  {
    ODS("No implementation choice provided with -imp option.\n");
    goto Error;
  }
  bv_imp = (char*) argv[arg+1];
  arg += 2;

  if(!stricmp(bv_imp, "2dpriority"))
  {
    priorityFlag2D = true;
    priorityFlagCPU = false;
    success = true;
    goto Error;
  }
  else if(!stricmp(bv_imp, "cpupriority"))
  {
    priorityFlag2D = false;
    priorityFlagCPU = true;
    success = true;
    goto Error;
  }
  else
  {
    priorityFlag2D = false;
    priorityFlagCPU = false;
  }

  strcpy(libname, lib_prefix);
  strcat(libname, bv_imp);

  bvlib.libname = libname;
  success = ImportFunction(&bvlib);
Error:
  return(success);
}

bool ParseMapTests(int         argc,
                   const char* argv[],
                   int&        arg)
{
  bool success = false;
  const char* szErr;
  char *test_type;
  unsigned long alpha;
  char libname[50];
  if((arg + 1) >= argc)
  {
    ODS("No test command provided with -map option.\n");
    goto Error;
  }

  test_type = (char*) argv[arg+1];
  arg += 2;

  maptest = false;
  crossmap = false;

  if(!stricmp(test_type, "test"))
  {
    maptest = true;
    success = true;
    goto Error;
  }
  else if(!stricmp(test_type, "cross"))
  {
    crossmap = true;
    success = true;
    goto Error;
  }

Error:
  return(success);
}


SurfOrigin SurfaceToOrigin(const char* szSurf)
{
  SurfOrigin surforigin;

  if(!stricmp(szSurf, "dst"))
    surforigin = SO_DST;
  else if(!stricmp(szSurf, "src1"))
    surforigin = SO_SRC1;
  else if(!stricmp(szSurf, "src2"))
    surforigin = SO_SRC2;
  else if(!stricmp(szSurf, "mask"))
    surforigin = SO_MASK;
  else
    surforigin = SO_UNKNOWN;

  return(surforigin);
}

bool ParseSurf(struct bvsurfgeom&  geom,
               SurfOrigin& surforigin,
               int         argc,
               const char* argv[],
               int&        arg)
{
  bool success = false;
  const char* szErr = NULL;

  // -src1|dst|mask|src2 <format> ###x###
  // or -src1|mask|src2 <format> <geom>

  if((arg + 2) >= argc)
  {
    ODS("Details not provided with %s option.\n", argv[arg]);
    goto Error;
  }

  szErr = TextToFormat(geom.format, argv[arg + 1]);
  if(szErr)
  {
    ODS("%s with option %s.\n", szErr, argv[arg]);
    goto Error;
  }

  // Next param may be dimensions, or it may be be a command to use another surface.
  // Only calculate dimensions if the next char is a digit.
  if ( (argv[arg + 2][0] >= '0') && (argv[arg + 2][0] <= '9') )
  {
    szErr = GetDimensions(geom.width, geom.height, argv[arg + 2]);
    if (szErr)
    {
      VODS("Error '%s' while getting surface dimensions\n",szErr);
      arg += 3;
      success = false;
      goto Error;
    }
    VODS("%s %s (%08X) %dx%d\n", argv[arg], argv[arg + 1], geom.format, geom.width, geom.height);
  }

  else //surface origin command
  {
    SurfOrigin origin = SurfaceToOrigin(argv[arg + 2]);
    if(origin != SO_UNKNOWN)
    {
      VODS("%s %s %s\n", argv[arg], argv[arg + 1], argv[arg + 2]);
      surforigin = origin;
    }
    else
    {
      ODS("invalid parameter '%s' with option %s.\n", argv[arg + 2], argv[arg]);
      goto Error;
    }
  }

  arg += 3;

  success = true;

Error:
  return(success);
}

bool ParseFile(const char*& szFile,
               int          argc,
               const char*  argv[],
               int&         arg)
{
  bool success = false;
  if((arg + 1) >= argc)
  {
    ODS("No filename provided with %s option (ex. %s image.bmp).\n", argv[arg], argv[arg]);
    goto Error;
  }

  szFile = argv[arg + 1];
  VODS("%s %s\n", argv[arg], szFile);
  arg += 2;

  success = true;
Error:
  return(success);
}

bool ParseKeys(int         argc,
               const char* argv[],
               int&        arg)
{
  bool success = false;
  const char* szErr;

  szErr = HexToUL(colorkey, argv[arg + 1]);
  if(szErr)
  {
    ODS("%s with option %s.\n", szErr, argv[arg]);
    goto Error;
  }

  parms.colorkey = &colorkey;

  VODS("%s %04X\n", argv[arg], parms.op.rop);
  arg += 3;

  if((argv[arg][1] == 'd') || (argv[arg][1] == 'D'))
  {
    VODS("Setting BVFLAG_KEY_DST in parms.flags.\n");
    parms.flags |= BVFLAG_KEY_DST;
  }
  else
  {
    VODS("Setting BVFLAG_KEY_SRC in parms.flags.\n");
    parms.flags |= BVFLAG_KEY_SRC;
  }

  success = true;
Error:
  return(success);
}

bool ParseAlpha(int         argc,
                const char* argv[],
                int&        arg)
{
  bool success = false;
  const char* szErr;
  unsigned long alpha;

  szErr = HexToUL(alpha, argv[arg + 1]);
  if(szErr)
  {
    ODS("%s with -alpha option.\n", szErr);
    goto Error;
  }

  if(alpha > 0xFF)
  {
    ODS("Invalid alpha value provided for -alpha option: %s (ex. -rop 80).\n", argv[arg + 1]);
    goto Error;
  }

  parms.globalalpha.size8 = (unsigned char)alpha;
  VODS("-alpha %02X\n", parms.globalalpha.size8);
  arg += 2;

  VODS("Setting BVFLAG_BLEND in parms.flags.\n");
  parms.flags |= BVFLAG_BLEND;

  success = true;
Error:
  return(success);
}

bool ParseRect(struct bvrect&  rect,
               int         argc,
               const char* argv[],
               int&        arg)
{
  const char* szErr = 0;

  if((arg + 1) >= argc)
  {
    ODS("No rectangle provided with %s option (ex. %s 0,0-640x480)\n", argv[arg], argv[arg]);
    goto Error;
  }

  szErr = GetRect(rect, argv[arg + 1]);
  if(szErr)
  {
    ODS("%s with option %s (ex. %s 0,0-640x480)\n", szErr, argv[arg], argv[arg]);
    goto Error;
  }
  VODS("%s %d,%d-%dx%d\n", argv[arg], rect.left, rect.top, rect.width, rect.height);
  arg += 2;

Error:
  return(szErr == 0);
}

bool ParseCmdLine(int         argc,
                  const char* argv[])
{
  bool success = false;

  for(int arg = 1; arg < argc;)
  {
    if(!stricmp(argv[arg], "-imp"))
    {
      if(!ParseImp(argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-dst"))
    {
      if(!ParseSurf(dst, dstorigin, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-dstfile"))
    {
      if(!ParseFile(szDst, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-dstrect"))
    {
      if(!ParseRect(parms.dstrect, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-dstangle"))
    {
      if(arg + 1 >= argc)
      {
        ODS("No angle provided with -dstangle option.\n");
        goto Error;
      }
      if(!TextToInt(dst.orientation, argv[arg + 1], '\0'))
        goto Error;
      arg += 2;
    }
    else if(!stricmp(argv[arg], "-src1"))
    {
      if(!ParseSurf(src1, src1origin, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-src1rect"))
    {
      if(!ParseRect(parms.src1rect, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-src1file"))
    {
      if(!ParseFile(szSrc1, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-src1angle"))
    {
      if(arg + 1 >= argc)
      {
        ODS("No angle provided with -src1angle option.\n");
        goto Error;
      }
      if(!TextToInt(src1.orientation, argv[arg + 1], '\0'))
        goto Error;
      arg += 2;
    }
    else if(!stricmp(argv[arg], "-src2"))
    {
      if(!ParseSurf(src2, src2origin, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-src2file"))
    {
      if(!ParseFile(szSrc2, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-src2rect"))
    {
      if(!ParseRect(parms.src2rect, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-src2angle"))
    {
      if(arg + 1 >= argc)
      {
        ODS("No angle provided with -src2angle option.\n");
        goto Error;
      }
      if(!TextToInt(src2.orientation, argv[arg + 1], '\0'))
        goto Error;
      arg += 2;
    }
    else if(!stricmp(argv[arg], "-mask"))
    {
      if(!ParseSurf(mask, maskorigin, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-maskfile"))
    {
      if(!ParseFile(szMask, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-maskrect"))
    {
      if(!ParseRect(parms.maskrect, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-maskangle"))
    {
      if(arg + 1 >= argc)
      {
        ODS("No angle provided with -maskangle option.\n");
        goto Error;
      }
      if(!TextToInt(mask.orientation, argv[arg + 1], '\0'))
        goto Error;
      arg += 2;
    }
    else if(!stricmp(argv[arg], "-cliprect"))
    {
      if(!ParseRect(parms.cliprect, argc, argv, arg))
        goto Error;
      VODS("Setting the BVFLAG_CLIP in parms.flags.\n");
      parms.flags |= BVFLAG_CLIP;
    }
    else if(!stricmp(argv[arg], "-rop"))
    {
      if(!ParseROP(parms.op.rop, argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-blend") ||
            !stricmp(argv[arg], "-blendtype"))
    {
      unsigned long type;
      if(!ParseType(type, blendtypes, BLENDTYPES, argc, argv, arg))
        goto Error;

      parms.op.blend =  (bvblend)((unsigned long)parms.op.blend  | type);
      VODS("Setting the BVFLAG_BLEND in parms.flags.\n");
      parms.flags |= BVFLAG_BLEND;
      VODS("-blend %s\n", argv[arg + 1]);
      arg += 2;
    }
    else if(!stricmp(argv[arg], "-dither"))
    {
      unsigned long type;
      if(!ParseType(type, dithertypes, DITHERTYPES, argc, argv, arg))
        goto Error;

      parms.dithermode = (bvdithermode)((unsigned long)parms.dithermode | type);
      VODS("-dither %s\n", argv[arg + 1]);

      // Set the dither return flag. We don't have any way to predict
      // what the return value will be, but at least we can verify that
      // the library accepts the flag.
      VODS("Setting the BVFLAG_DITHER_RETURN in parms.flags.\n");
      parms.flags |= BVFLAG_DITHER_RETURN;

      arg += 2;
    }
    else if(!stricmp(argv[arg], "-scale"))
    {
      unsigned long type;
      if(!ParseType(type, scaletypes, SCALETYPES, argc, argv, arg))
        goto Error;

      parms.scalemode = (bvscalemode)type;
      VODS("-scale %s\n", argv[arg + 1]);

      // Set the scale return flag. We don't have any way to predict
      // what the return value will be, but at least we can verify that
      // the library accepts the flag.
      VODS("Setting the BVFLAG_SCALE_RETURN in parms.flags.\n");
      parms.flags |= BVFLAG_SCALE_RETURN;

      arg += 2;
    }
    else if(!stricmp(argv[arg], "-srckey") ||
            !stricmp(argv[arg], "-dstkey"))
    {
      if(!ParseKeys(argc, argv, arg))
        goto Error;
    }
    ///////////////////////////////
    else if(!stricmp(argv[arg], "-alpha") ||
            !stricmp(argv[arg], "-globalalpha"))
    {
      if(!ParseAlpha(argc, argv, arg))
        goto Error;
    }
    ///////////////////////////////
    else if(!stricmp(argv[arg], "-srcmask"))
    {
      VODS("-srcmask\n");
      arg++;
      VODS("Setting BVFLAG_SRCMASK in parms.flags.\n");
      parms.flags |= BVFLAG_SRCMASK;
    }
    ///////////////////////////////
    else if(!stricmp(argv[arg], "-profile"))
    {
      profile = true;
      VODS("-profile\n");
      arg++;
    }
    ///////////////////////////////
    else if(!stricmp(argv[arg], "-profout"))
    {
      if(!ParseFile(szProfout, argc, argv, arg))
        goto Error;
    }
    ///////////////////////////////
    else if(!stricmp(argv[arg], "-preseed"))
    {
      preseed = true;
      VODS("-preseed\n");
      arg++;
    }
    ///////////////////////////////
    else if(!stricmp(argv[arg], "-pretouch"))
    {
      if(arg + 1 >= argc)
      {
        ODS("No step size provided with -pretouch option.\n");
        goto Error;
      }
      if(!TextToInt(pretouch, argv[arg + 1], '\0'))
        goto Error;
      VODS("-pretouch %d\n", pretouch);
      arg += 2;
    }
    ///////////////////////////////
    else if(!stricmp(argv[arg], "-version"))
    {
      PrintVersion();
      exit(0);
    }
    else if(!stricmp(argv[arg], "-verbose"))
    {
      verbose = true;
      VODS("-verbose\n");
      arg++;
    }
    else if(!stricmp(argv[arg], "-premap"))
    {
      premap = true;
      VODS("-premap\n");
      arg++;
    }
    else if(!stricmp(argv[arg], "-callback"))
    {
      VODS("-callback\n");
      VODS("Setting the BVFLAG_ASYNC in parms.flags.\n");
      parms.flags |= BVFLAG_ASYNC;
      arg++;
    }
    else if(!stricmp(argv[arg], "-map"))
    {
      VODS("-map\n");
      if(!ParseMapTests(argc, argv, arg))
        goto Error;
    }
    else if(!stricmp(argv[arg], "-batch"))
    {
      VODS("-batch\n");
      batchtest = true;
      arg++;
    }
    ////////////////////////////////////////////
    else if(!stricmp(argv[arg], "-loops"))
    {
      if(arg + 1 >= argc)
      {
        ODS("No loop times provided with -loops option.\n");
        goto Error;
      }
      if(!TextToInt(loop_times, argv[arg + 1], '\0'))
        goto Error;
      VODS("-loops %d\n", loop_times);
      arg += 2;
    }
    else
    {
      ODS("Unrecognized option %s\n", argv[arg]);
      goto Error;
    }
    ///////////////////////////////
  }

  success = true;
Error:
  return(success);
}

//
// Calculate how much memory this surface needs.
//
unsigned long SurfaceSize(long&    stride,
                          enum ocdformat format,
                          int      width,
                          int      height)
{
  unsigned long size,
                size_y_plane,
                size_uv_plane,
                aligned_y_width,
                bytes_per_pel;

  if ( (format & OCDFMTDEF_CS_MASK) == OCDFMTDEF_CS_YCbCr)
  {
    // We are not concerned with the layout of the YUV data, just how
    // much space it takes up.  Doesn't matter if it is planar, interleave, whatever.
    // The exception to this is layouts which are left justified, in which case
    // we need to add in space for the padding on the right of the UV plane.

    if ((format & OCDFMTDEF_SUBSAMPLE_MASK) == OCDFMTDEF_SUBSAMPLE_422_YCbCr)
    {
      // Assume Y values are 8 bit, one per pixel
      // and pad to 32 bit boundary
      aligned_y_width = ((width + 3) / 4) * 4;
	    size_y_plane = aligned_y_width  * height;
      size_uv_plane = size_y_plane;
    }
    else if ((format & OCDFMTDEF_SUBSAMPLE_MASK) == OCDFMTDEF_SUBSAMPLE_420_YCbCr)
    {
      // Assume Y values are 8 bit, one per pixel
      // and pad to 32 bit boundary
      aligned_y_width = ((width + 3) / 4) * 4;
	    size_y_plane = aligned_y_width  * height;
      size_uv_plane = size_y_plane / 2;
    }
    else if ((format & OCDFMTDEF_SUBSAMPLE_MASK) == OCDFMTDEF_SUBSAMPLE_411_YCbCr)
    {
      // Assume Y values are 8 bit, one per pixel
      // and pad to 32 bit boundary
      aligned_y_width = ((width + 3) / 4) * 4;
	    size_y_plane = aligned_y_width  * height;
      size_uv_plane = size_y_plane / 2;
    }
    else
    {
      // Undefined planar format.
      ODS("ERROR SurfaceSize(): Unrecognized YUV subsample\n");
      return 0;
    }

    // Calculate the stride based on packed or planar.
    if ( ((format & OCDFMTDEF_LAYOUT_MASK) ==  OCDFMTDEF_PACKED) ||
         ((format & OCDFMTDEF_LAYOUT_MASK) ==  OCDFMTDEF_DISTRIBUTED) )
    {
      stride = aligned_y_width * 2;
    }
    else // Planar
    {
      stride = aligned_y_width;
      // If it is planar and the u and v planes are justified to the left,
      // then there is an equal sized block of empty space to the right.
   	  if ( format & OCDFMTDEF_LEFT_JUSTIFIED )
  	  {
  		  size_uv_plane += size_uv_plane;
  	  }
    }

    size = size_y_plane + size_uv_plane;
  }

  else // Non-YUV
  {
    // Pick the pixel size out of the format code.
    bytes_per_pel = ((format & OCDFMTDEF_CONTAINER_MASK) >> OCDFMTDEF_CONTAINER_SHIFT) + 1;

    // Calculate width in bytes, 32 bit aligned.
    stride = (((width * bytes_per_pel) + 3) / 4) * 4;

    // Calculate surface size
    size = height * stride;
  }

  return(size);
}

bool CreateSurface(const char*        szType,
                   struct bvsurfgeom& geom,
                   struct bvbuffdesc& desc)
{
  bool success = false;

  unsigned long size;
  struct bvbltparams tmpparms;
  int true_width, true_height;

  VODS("Creating %d x %d %s surface with orientation %d.\n", geom.width, geom.height, szType, geom.orientation);

  if (geom.orientation == 0 || geom.orientation == 180)
  {
    true_width = geom.width;
    true_height = geom.height;
  }
  else if (geom.orientation == 90 || geom.orientation == 270)
  {
    true_width = geom.height;
    true_height = geom.width;
  }
  else
  {
      ODS("Cannot create surface. Orientation %d is invalid.\n", geom.orientation);
      goto Error;
  }

  VODS("True surface size is %d x %d.\n", true_width, true_height);

  size = SurfaceSize(geom.virtstride, geom.format, true_width, true_height);
  VODS("%s size = %d, %s.virtstride = %d.\n", szType, size, szType, geom.virtstride);

  desc.length = size;
  desc.virtaddr = memalign(64,size);
  memset(desc.virtaddr,0,size);
  if(!desc.virtaddr)
  {
    ODS("Error allocating memory for %s surface.\n", szType);
    desc.length = 0;
    goto Error;
  }

  // Allocat palette when necessary
  switch(geom.format)
  {
    case OCDFMT_LUT1:
    case OCDFMT_LUT2:
    case OCDFMT_LUT4:
    case OCDFMT_LUT8:
      {
        int containersize;
        FormatToContainerSize(containersize, geom.format);
        geom.palette = (unsigned long*)malloc(sizeof(unsigned long) * (1 << containersize));
        if(!geom.palette)
        {
          ODS("Error allocating memory for %s palette.\n", szType);
          if(desc.virtaddr)
          {
//ODS("%s(): Freeing 0x%08X\n", __FUNCTION__, geom.virtaddr);
            free(desc.virtaddr);
            desc.virtaddr = 0;
            desc.length = 0;
          }
          goto Error;
        }
      }
      break;

    default:
      // Do nothing
      break;
  }
  VODS("%s surface successfully created (0x%08X).\n", szType, desc.virtaddr);

  success = true;
Error:
  return(success);
}

bool CreateSurfaces()
{
  bool success = false;

  if((GetGeomOrigin(src1origin) == &src1) &&
     (src1.format != (ocdformat)-1))
  {
    if(!CreateSurface("source", src1, src1desc))
      goto Error;
  }

  if((GetGeomOrigin(maskorigin) == &mask) &&
     (mask.format != (ocdformat)-1))
  {
    if(!CreateSurface("mask", mask, maskdesc))
      goto Error;
  }

  if((GetGeomOrigin(src2origin) == &src2) &&
     (src2.format != (ocdformat)-1))
  {
    if(!CreateSurface("src2", src2, src2desc))
      goto Error;
  }

  if(GetGeomOrigin(dstorigin) == &dst)
  {
    if(dst.format != (ocdformat)-1)
    {
      if(!CreateSurface("destination", dst, dstdesc))
        goto Error;
    }
    else
    {
      ODS("ERROR: No destination specified.  Use -dst option.\n");
      goto Error;
    }
  }

  success = true;
Error:
  return(success);
}

bool LoadRawFile(const char* szType,
                 const char* szFile,
                 struct bvsurfgeom&  geom,
         bvbuffdesc& desc)
{
  bool success = false;

  int containersize;
  FILE* hFile = fopen(szFile, "rb");
  int len = strlen(szFile);

  char* ptype = (char*)(&(szFile[len-3]));

  if(desc.virtaddr != 0)
  {
    ODS("ERROR: Do not specify surface format (-src1) for raw files.  It will be obtained from filename.\n");
    goto Error;
  }

  const char* pdesc;
  unsigned long filesize;
  if(!hFile)
  {
    ODS("ERROR: Unable to open %s file.\n", szType);
    goto Error;
  }

  // FOO-%s-%dx%d(%d).raw
  while((int)--ptype >= (int)szFile)
  {
    if(*ptype == '-')
    {
      *ptype = '\0';
      pdesc = ptype + 1;
      break;
    }
  }

  while((int)--ptype >= (int)szFile)
  {
    if(*ptype == '-')
      break;
  }

  if((int)ptype < (int)szFile)
  {
    ODS("ERROR: Filename doesn't have recognizable form (name-type-widthxheight(stride).raw).\n");
    goto Error;
  }

  if(TextToFormat(geom.format, ptype+1))
  {
    ODS("ERROR: Unrecognized format in %s filename: %s\n", szType, ptype+1);
    goto Error;
  }

  if(!TextToValPair(*((int*)(&geom.width)), *((int*)(&geom.height)), pdesc, 'x', '('))
  {
    ODS("ERROR: Unable to parse dimensions from %s filename.\n", szType);
    goto Error;
  }

  pdesc++;

  if(!TextToInt(*((int*)(&geom.virtstride)), pdesc, ')'))
  {
    ODS("ERROR: Unable to parse stride from %s filename.\n", szType);
    goto Error;
  }

  filesize = SurfaceSize(geom.virtstride, geom.format, geom.width, geom.height);
  FormatToContainerSize(containersize, geom.format);

  switch(geom.format)
  {
    // LUTs need to read palette
    case OCDFMT_LUT1:
    case OCDFMT_LUT2:
    case OCDFMT_LUT4:
    case OCDFMT_LUT8:
      geom.palette = (unsigned long*)malloc(sizeof(unsigned long) * (1 << containersize));
      if(!geom.palette)
      {
        ODS("ERROR:  Unable to allocate memory for %s palette.\n", szType);
        goto Error;
      }
      if(fread(geom.palette, sizeof(unsigned long), 1 << containersize, hFile) != (1 << containersize))
      {
        ODS("ERROR reading palette from %s file.\n", szType);
        goto Error;
      }

      // All the LUT8 files we have currently use have this palette format.
      // We will need to change this when we support more input files with
      // different palette formats.
      geom.paletteformat = OCDFMT_BGRx24;
      break;

    default:
      ;
  }

//ODS("%s(): filesize = %d\n", __FUNCTION__, filesize);

  desc.length = filesize;
  desc.virtaddr = memalign(64,filesize);
  if(!desc.virtaddr)
  {
    ODS("ERROR: Unable to allocate %d bytes for %s surface.\n", filesize, szType);
    desc.length = 0;
    goto Error;
  }

  VODS("%s surface successfully created (0x%08X).\n", szType, desc.virtaddr);

  if(fread(desc.virtaddr, filesize, 1, hFile) != 1)
  {
    ODS("ERROR while reading %s file.\n", szType);
    goto Error;
  }

  VODS("%s surface initialized from input file.\n", szType);

  success = true;
Error:
  if(hFile)
    fclose(hFile);
  return(success);
}

bool LoadFormattedFile(const char* szType,
                       const char* szFile,
                       bvsurfgeom&  filesurf,
                       bvbuffdesc&  filebuf)
{
  bool success = false;

  VODS("%s file specified...loading.\n", szType);

  int len = strlen(szFile);
  int comp;
  filebuf.virtaddr = stbi_load(szFile, (int*)(&filesurf.width), (int*)(&filesurf.height), &comp, 0);
  if(!filebuf.virtaddr)
  {
    ODS("Error loading %s: %s\n", szType, stbi_failure_reason());
    goto Error;
  }
  VODS("%s file %s is %d x %d x %d\n", szType, szFile, filesurf.width, filesurf.height, comp);

  switch(comp)
  {
    case 1: filesurf.format = OCDFMT_MONO8; break;
    case 2: filesurf.format = OCDFMT_RGB16; break; // @@@ Not the right format!
    case 3: filesurf.format = OCDFMT_RGB24; break;
    // If alpha is involved, assume the caller is specifying what it will be
    case 4:
      if(((filesurf.format & OCDFMTDEF_CS_MASK) == OCDFMTDEF_CS_RGB) &&
         (filesurf.format & OCDFMTDEF_ALPHA) &&
         (filesurf.format & OCDFMTDEF_NON_PREMULT))
        filesurf.format = OCDFMT_RGBA24_NON_PREMULT;
      else
        filesurf.format = OCDFMT_RGBA24;
      break;
  }

  filesurf.virtstride = filesurf.width * comp;
  filebuf.length = filesurf.virtstride * filesurf.height;   // MWI TODO Check this value

  success = true;
Error:
  return(success);
}

bool CopyAndConvertFileData(const char* szType,
                            struct bvsurfgeom&  filegeom,
                            struct bvbuffdesc*  filedesc,
                            struct bvsurfgeom&  geom,
                            struct bvbuffdesc*  buf)
{
  enum bverror err;

  VODS("Copying %s file contents to %s surface.\n", szType, szType);
  filegeom.orientation = geom.orientation;

  // If we are reading a 3 component source file (such as RGB24) into a
  // 4 component source image (such as RGBA24)  we will need to create an
  // alpha component. We will do this by temporarily changing the format of the
  // RGBA24 surface to RGB124 when we load the file surface into it.  Then we
  // will change the format back to RGBA24.

  // Remember what the source format is supposed to be.
  enum ocdformat saved_format = geom.format;

  // if the file read from the disk does not have an alpha component
  if (!(filegeom.format & OCDFMTDEF_ALPHA))
  {

    // and the source format we need does,
    // then temporarily use an RGB124 format, which creates an alpha
    // of "1", then treat that as RGBA.
    switch (geom.format)
    {
      case OCDFMT_RGBA24:
      case OCDFMT_nRGBA24:
        geom.format = OCDFMT_RGB124;
        break;

      case OCDFMT_BGRA24:
      case OCDFMT_nBGRA24:
        geom.format = OCDFMT_BGR124;
        break;

      case OCDFMT_ARGB24:
      case OCDFMT_nARGB24:
        geom.format = OCDFMT_1RGB24;
        break;

      case OCDFMT_ABGR24:
      case OCDFMT_nABGR24:
        geom.format = OCDFMT_1BGR24;
        break;
    }
  }


  struct bvbltparams tmpparms = {0};
  tmpparms.structsize = sizeof(tmpparms);
  tmpparms.flags = BVFLAG_ROP;
  tmpparms.op.rop = 0xCCCC;
  tmpparms.dstgeom = &geom;
  tmpparms.dstdesc = buf;
  tmpparms.src1geom = &filegeom;
  tmpparms.src1.desc = filedesc;
  tmpparms.dstrect.left = 0;
  tmpparms.dstrect.top = 0;
  tmpparms.dstrect.width = filegeom.width;
  if(geom.width < tmpparms.dstrect.width)
    tmpparms.dstrect.width = geom.width;
  tmpparms.dstrect.height = filegeom.height;
  if(geom.height < tmpparms.dstrect.height)
    tmpparms.dstrect.height = geom.height;
  tmpparms.src1rect = tmpparms.dstrect;

//TODO: Try all libraries
  err = pblt(&tmpparms);
  if(err != BVERR_NONE)
  {
    const char* szErr;
    ErrorToText(szErr, err);
    ODS("Error copying %s file data to %s surface: (0x%08X) %s.\n", szType, szType, err, szErr);
    goto Error;
  }

  // Restore the original source format.
  geom.format = saved_format;

Error:
  return(err == BVERR_NONE);
}

bool IsFileTGA(const char* szFilename)
{
  int len = strlen(szFilename);
  return(stricmp(&(szFilename[len-3]), "tga") == 0);
}

bool IsFileRaw(const char* szFilename)
{
  int len = strlen(szFilename);
  return(stricmp(&(szFilename[len-3]), "raw") == 0);
}

bool LoadFile(const char* szType,              // Type (source, brush, mask, etc)
              const char* szFile,              // File name
              struct bvsurfgeom&  filegeom,    // Intermediate surface for file conversion
              struct bvbuffdesc*  filedesc, // Intermediate surface for file conversion
              struct bvsurfgeom*  pgeom,       // Destination for file contents
              struct bvbuffdesc*  desc)    // Destination for file contents
{
  bool success = false;

  VODS("Loading %s into %s surface.\n", szFile, szType);

  if(IsFileRaw(szFile))
  {
    if(!LoadRawFile(szType, szFile, *pgeom, *desc))
      goto Error;
  }
  else
  {
    filegeom.format = pgeom->format;  // Tell loader whether premultiplied or not
    // Load to intermediate surface.
    if(!LoadFormattedFile(szType, szFile, filegeom, *filedesc))
      goto Error;

    // Copy it to the destination surface (for c/s conv, etc.)
    if(!CopyAndConvertFileData(szType, filegeom, filedesc, *pgeom, desc))
      goto Error;
  }

  // Handle rotation.  The image file has no concept of orientation, so its width
  // and height are absolute values, not relative to surface orientation.
  // Check the orientation provided on the command line, and adjust the
  // surface width and height accordingly.
  if ( (src1.orientation == 90) || (src1.orientation == 270) )
  {
    int temp = pgeom->width;
    pgeom->width = pgeom->height;
    pgeom->height = temp;
  }

  success = true;
Error:
  return(success);
}

bool LoadFiles()
{
  bool success = false;

  if(szSrc1)
  {
    if(!LoadFile("source", szSrc1, src1file, &src1filedesc, GetGeomOrigin(src1origin), GetDescOrigin(src1origin)))
      goto Error;
  }

  if(szSrc2)
  {
    if(!LoadFile("src2", szSrc2, src2file, &src2filedesc, GetGeomOrigin(src2origin), GetDescOrigin(src2origin)))
      goto Error;
  }

  if(szMask)
  {
    if(!LoadFile("mask", szMask, maskfile, &maskfiledesc, GetGeomOrigin(maskorigin), GetDescOrigin(maskorigin)))
      goto Error;
  }

  success = true;
Error:
  return(success);
}

int PaletteSize(ocdformat format)
{
  int palsize;

  switch(format)
  {
    case OCDFMT_LUT1:
      palsize = 2;
      break;

    case OCDFMT_LUT2:
      palsize = 4;
      break;

    case OCDFMT_LUT4:
      palsize = 16;
      break;

    case OCDFMT_LUT8:
      palsize = 256;
      break;

    default:
      palsize = 0;
  }
  return(palsize);
}

void PreReadSurface(const bvsurfgeom& geom, const bvbuffdesc& desc)
{
  long surfsize = geom.virtstride * static_cast<long>(geom.height);
  const volatile char* p = static_cast<const volatile char*>(desc.virtaddr);

  if (p == NULL)
  {
  ODS("Surface pointer is NULL in %s()", __func__);
  return;
  }

  do
  {
    volatile char c = *p;
    p += pretouch;
    surfsize -= pretouch;
  }while(surfsize > 0);
}

void PreWriteSurface(const struct bvsurfgeom& geom, const bvbuffdesc& desc)
{
  long surfsize = geom.virtstride * static_cast<long>(geom.height);
  volatile char* p = static_cast<char*>(desc.virtaddr);

  if (p == NULL)
  {
  ODS("Surface pointer is NULL in %s()", __func__);
  return;
  }

  do
  {
    *p = (volatile char)0;
    p += pretouch;
    surfsize -= pretouch;
  }while(surfsize > 0);
}

void PreTouch()
{
  if(GetGeomOrigin(dstorigin) == &dst)
  {
    PreWriteSurface(*(parms.dstgeom), *(parms.dstdesc));
  }
  if(parms.src1geom)
  {
    if (!szSrc1 && GetGeomOrigin(src1origin) == &src1)
    {
      PreWriteSurface(*(parms.src1geom), *(parms.src1.desc));
    }
    PreReadSurface(*(parms.src1geom), *(parms.src1.desc));
  }
  if(parms.src2geom)
  {
    if (!szSrc2 && GetGeomOrigin(src2origin) == &src2)
    {
      PreWriteSurface(*(parms.src2geom), *(parms.src2.desc));
    }
    PreReadSurface(*(parms.src2geom), *(parms.src2.desc));
  }
  if(parms.maskgeom)
  {
    if (!szMask && GetGeomOrigin(maskorigin) == &mask)
    {
      PreWriteSurface(*(parms.maskgeom), *(parms.mask.desc));
    }
    PreReadSurface(*(parms.maskgeom), *(parms.mask.desc));
  }
}

#define MINIWIDTH 16
#define MINIHEIGHT 16

bool CreateMiniSurface(const char*      szType,
                       struct bvsurfgeom&       surfout,
                       const struct bvsurfgeom& surfin,
                       struct bvbuffdesc&     bufout,
                       const struct bvbuffdesc& bufin)
{
  surfout = surfin;
  surfout.width = MINIWIDTH;
  surfout.height = MINIHEIGHT;
  bufout.virtaddr = 0;
  bufout.length = 0;
  surfout.virtstride = 0;
  return(CreateSurface(szType, surfout, bufout));
}

bool PreSeed()
{
  bool success = true;
  struct bvbltparams miniparms = parms;
  struct bvsurfgeom minidst = {0};
  struct bvbuffdesc minidstdesc = {0};
  struct bvsurfgeom minisrc1 = {0};
  struct bvbuffdesc minisrc1desc = {0};
  struct bvsurfgeom minisrc2 = {0};
  struct bvbuffdesc minisrc2desc = {0};
  struct bvsurfgeom minimask = {0};
  struct bvbuffdesc minimaskdesc = {0};

  minidst.structsize = sizeof(minidst);
  minisrc1.structsize = sizeof(minisrc1);
  minisrc2.structsize = sizeof(minisrc2);
  minimask.structsize = sizeof(minimask);

  minidstdesc.structsize = sizeof(minidstdesc);
  minisrc1desc.structsize = sizeof(minisrc1desc);
  minisrc2desc.structsize = sizeof(minisrc2desc);
  minimaskdesc.structsize = sizeof(minimaskdesc);

  if(parms.dstgeom == parms.maskgeom)
  {
    VODS("%s(): Dst is mask.", __func__);
    miniparms.dstgeom = &minimask;
    miniparms.dstdesc = &minimaskdesc;
  }
  else if(parms.dstgeom == parms.src2geom)
  {
    VODS("%s(): Dst is src2.", __func__);
    miniparms.dstgeom = &minisrc2;
    miniparms.dstdesc = &minisrc2desc;
  }
  else if(parms.dstgeom == parms.src1geom)
  {
    VODS("%s(): Dst is src1.", __func__);
    miniparms.dstgeom = &minisrc1;
    miniparms.dstdesc = &minisrc1desc;
  }
  else
  {
    VODS("%s(): Dst is dst.", __func__);
    miniparms.dstgeom = &minidst;
    miniparms.dstdesc = &minidstdesc;
  }

  if(!miniparms.dstdesc->virtaddr)
  {
    success = CreateMiniSurface("mini dst", *miniparms.dstgeom, *(parms.dstgeom), *miniparms.dstdesc, *(parms.dstdesc));
    if(!success)
      goto Error;
  }

  if(parms.src1geom)
  {
    if(parms.src1geom == parms.maskgeom)
    {
      VODS("%s(): Src is mask.", __func__);
      miniparms.src1geom = &minimask;
      miniparms.src1.desc = &minimaskdesc;
    }
    else if(parms.src1geom == parms.src2geom)
    {
      VODS("%s(): Src is src2.", __func__);
      miniparms.src1geom = &minisrc2;
      miniparms.src1.desc = &minisrc2desc;
    }
    else if(parms.src1geom == parms.dstgeom)
    {
      VODS("%s(): Src is dst.", __func__);
      miniparms.src1geom = &minisrc1;  // pdst already pointing to minisrc1
      miniparms.src1.desc = &minidstdesc;
    }
    else
    {
      VODS("%s(): Src is src1.", __func__);
      miniparms.src1geom = &minisrc1;
      miniparms.src1.desc = &minisrc1desc;
    }

    if(!miniparms.src1.desc->virtaddr)
    {
      success = CreateMiniSurface("mini src1", *miniparms.src1geom, *(parms.src1geom), *miniparms.src1.desc, *(parms.src1.desc));
      if(!success)
        goto Error;
    }
  }

  if(parms.src2geom)
  {
    if(parms.src2geom == parms.maskgeom)
    {
      VODS("%s(): Src2 is mask.", __func__);
      miniparms.src2geom = &minimask;
      miniparms.src2.desc = &minimaskdesc;
    }
    else if(parms.src2geom == parms.src1geom)
    {
      VODS("%s(): Src2 is src1.", __func__);
      miniparms.src2geom = &minisrc2; // psrc already pointing to minisrc2
      miniparms.src2.desc = &minisrc1desc;
    }
    else if(parms.src2geom == parms.dstgeom)
    {
      VODS("%s(): Src2 is dst.", __func__);
      miniparms.src2geom = &minisrc2; // pdst already pointing to minisrc2
      miniparms.src2.desc = &minisrc2desc;
    }
    else
    {
      VODS("%s(): Src2 is src2.", __func__);
      miniparms.src2geom = &minisrc2;
      miniparms.src2.desc = &minisrc2desc;
    }

    if(!miniparms.src2.desc->virtaddr)
    {
      success = CreateMiniSurface("mini src2", *miniparms.src2geom, *(parms.src2geom), *miniparms.src2.desc, *(parms.src2.desc));
      if(!success)
        goto Error;
    }
  }

  if(parms.maskgeom)
  {
    if(parms.maskgeom == parms.src2geom)
    {
      VODS("%s(): Mask is src2.", __func__);
      miniparms.maskgeom = &minimask; // psrc2 already pointing to minimask
      miniparms.mask.desc = &minisrc2desc;
    }
    else if(parms.maskgeom == parms.src1geom)
    {
      VODS("%s(): Mask is src1.", __func__);
      miniparms.maskgeom = &minimask; // psrc already pointing to minimask
      miniparms.mask.desc = &minisrc1desc;
    }
    else if(parms.maskgeom == parms.dstgeom)
    {
      VODS("%s(): Mask is dst.", __func__);
      miniparms.maskgeom = &minimask; // pdst already pointing to minimask
      miniparms.mask.desc = &minidstdesc;
    }
    else
    {
      VODS("%s(): Mask is mask.", __func__);
      miniparms.maskgeom = &minimask;
    }

    if(!miniparms.mask.desc->virtaddr)
    {
      success = CreateMiniSurface("mini mask", *miniparms.maskgeom, *(parms.maskgeom), *miniparms.mask.desc, *(parms.mask.desc));
      if(!success)
        goto Error;
    }
  }

  if(MINIWIDTH < miniparms.dstrect.width)
    miniparms.dstrect.width = MINIWIDTH;
  if(MINIHEIGHT < miniparms.dstrect.height)
    miniparms.dstrect.height = MINIHEIGHT;
  if(MINIWIDTH < miniparms.src1rect.width)
    miniparms.src1rect.width = MINIWIDTH;
  if(MINIHEIGHT < miniparms.src1rect.height)
    miniparms.src1rect.height = MINIHEIGHT;
  if(MINIWIDTH < miniparms.src2rect.width)
    miniparms.src2rect.width = MINIWIDTH;
  if(MINIHEIGHT < miniparms.src2rect.height)
    miniparms.src2rect.height = MINIHEIGHT;
  if(MINIWIDTH < miniparms.maskrect.width)
    miniparms.maskrect.width = MINIWIDTH;
  if(MINIHEIGHT < miniparms.maskrect.height)
    miniparms.maskrect.height = MINIHEIGHT;

  if(verbose)
  {
    VODS("Preseed BLT:\n");
    ODS("miniparms:\n");
    BVDump("\t", "\t", &miniparms);
  }

  {
    //bverror err = bvlib[0].bv_blt(&miniparms);  // MWI TODO Check all parameters
    bverror err = blt(&miniparms);
    if(err != BVERR_NONE)
    {
      const char *szErr;
      ErrorToText(szErr, err);
      ODS("Error performing preseed miniblt: %s (%d).\n", szErr, err);
      goto Error;
    }
  }

  VODS("freeing mini dst\n");
  if(miniparms.dstgeom && minidstdesc.virtaddr)
  {
    free(minidstdesc.virtaddr);
    minidstdesc.virtaddr = 0;
    minidstdesc.length = 0;
  }
  VODS("freeing mini src1\n");
  if(miniparms.src1geom && minisrc1desc.virtaddr)
  {
    free(minisrc1desc.virtaddr);
    minisrc1desc.virtaddr = 0;
    minisrc1desc.length = 0;
  }
  VODS("freeing mini src2\n");
  if(miniparms.src2geom && minisrc2desc.virtaddr)
  {
    free(minisrc2desc.virtaddr);
    minisrc2desc.virtaddr = 0;
    minisrc2desc.length = 0;
  }
  VODS("freeing mini mask\n");
  if(miniparms.maskgeom && minimaskdesc.virtaddr)
  {
    free(minimaskdesc.virtaddr);
    minimaskdesc.virtaddr = 0;
    minimaskdesc.length = 0;
  }

Error:
  return(success);
}

void SmartCopySurf(struct bvsurfgeom*& pgeom,
                   struct bvbuffdesc*& pdesc,
                   SurfOrigin surforigin)
{
  // See where the surface comes from
  struct bvsurfgeom* pRefGeom = GetGeomOrigin(surforigin);
  struct bvbuffdesc* pRefDesc = GetDescOrigin(surforigin);
  ocdformat newfmt;

  // If there is no surface, or if the surface has the same format,
  // just point to the same struct bvsurfgeom structure.
  if(!pgeom ||
     ((newfmt = pgeom->format) == pRefGeom->format))
  {
    pgeom = pRefGeom;
    pdesc = pRefDesc;
  }
  // If there is a surface, but the format is different, assume that
  // the size of the pixels is the same, copy the struct bvsurfgeom structure
  // and restore the new format.
  else
  {
    *pgeom = *pRefGeom;
    pgeom->format = newfmt;
    pdesc = pRefDesc;
  }
}

void SetUpBatch()
{
  // Set up this blit to be a the first in the batch.
  // (The batch will be only one blit long)
  parms.flags = (parms.flags & ~BVFLAG_BATCH_MASK) | BVFLAG_BATCH_BEGIN;

  // Set all the flags in the batchflags.  This is not required to start a blt,
  // but we want to verify that bltsville doesn't touch them
  parms.batchflags = ~(BVBATCH_ENDNOP);

  // Make sure the batch state structure pointer is NULL.  It should come back
  // non-null.
  parms.batch = NULL;

}

void TestBatch()
{
  enum bverror err = BVERR_NONE;
  int success = 1;

  // Make sure that bltsville didn't modify the batchflags
  if (parms.batchflags != ~(BVBATCH_ENDNOP))
  {
    ODS("ERROR: Batchflags were modified!.\n");
    success = 0;
  }

  // Make sure we got a pointer to a batch state structure.  This pointer is
  // opaque, so we can't access it, but it should at least exist.
  if (parms.batch == NULL)
  {
    ODS("ERROR: Batch pointer was NULL after bltsville executed the first blt in the batch.\n");
    success = 0;
  }

  // So far, so good.  Now terminate the batch with a NOP command.
  parms.flags = (parms.flags & ~BVFLAG_BATCH_MASK) | BVFLAG_BATCH_END;
  parms.batchflags = BVBATCH_ENDNOP;
  //err = bvlib[0].bv_blt(&parms);
  err = blt(&parms);

  if (err != BVERR_NONE)
  {
    ODS("ERROR: BVBATCH_ENDNOP command failed.\n");
    success = 0;
  }

  // Make sure that bltsville didn't modify the batchflags
  if (parms.batchflags != BVBATCH_ENDNOP)
  {
    ODS("ERROR: Batchflags were modified!.\n");
    success = 0;
  }

  // Finally, make sure that the batch state pointer went back to NULL
  if (parms.batch)
  {
    ODS("ERROR: Batch pointer was NON-NULL after batch was terminated.\n");
    success = 0;
  }

  if (success)
  {
    ODS("Batch test PASSED.\n");
  }
  else
  {
    ODS("Batch test FAILED.\n");
  }

}

//
// This function maps in the destination surface, but it intentionally maps
// it incorrectly.  Instead of using the main bltsville library for mapping and
// unmapping, will use the ref library to map, then call the main library for
// the blit.
//
void CrossMapSurfaces()
{
  enum bverror err = BVERR_NONE;
  struct bvbuffdesc* pdesc;
  pdesc = parms.dstdesc;

  // Make sure the reference library is loaded
  if (!(bvreflib.hlib))
  {
    ODS("ERROR: Cross mapping test did not successfully load the reference "
        "library. Test results are not valid anymore.\n");
    return;
  }

  // Confirm the current list if mappings is null.
  if (pdesc->map != NULL)
  {
    ODS("ERROR: Destination Buffer Descriptor already had a buffer map before we mapped it.\n");
  }

  // Map the buffer
  err = bvreflib.bv_map(pdesc);

  // Confirm no error.
  if (err != BVERR_NONE)
  {
    ODS("ERROR: ref library bv_map() returned an error: 0x%x.\n", err);
  }
  // Confirm a map was created.
  else if (pdesc->map == NULL)
  {
    ODS("ERROR: No map appeared after mapping destination buffer in reference library.\n");
  }
  else
  {
    // Because the mapping is opaque, all we can do is confirm that a
    // map was actually created.  Which it was.
    ODS("bv_map() reference library successfully mapped the surface.\n");
  }
}

void CrossUnMapSurfaces()
{

  enum bverror err = BVERR_NONE;
  struct bvbuffdesc* pdesc;
  pdesc = parms.dstdesc;

  // Make sure the surface is mapped.
  if (pdesc->map == NULL)
  {
    ODS("ERROR: Destination Buffer Descriptor lost it's map after we mapped it (ref library).\n");
    ODS("bv_unmap() test FAILED.\n");
  }

  // Call the unmap function
  err = bvreflib.bv_unmap(pdesc);

  // Check for error
  if (err != BVERR_NONE)
  {
    ODS("ERROR: ref library bv_unmap() returned an error: 0x%x.\n", err);
    ODS("bv_unmap() test FAILED.\n");
  }

  // Make sure the map is gone.
  else if (pdesc->map != NULL)
  {
    ODS("ERROR: There is still a destination map handle even after calling bv_unmap (ref library)!\n");
    ODS("bv_unmap() test FAILED.\n");
  }
  else
  {
    // Because the is opaque, all we can do is confirm that the map
    // was actually removed.  Which it was.
    ODS("bv_unmap() ref lib unmap successful. Test PASSED.\n");
  }

}

void TestMapping()
{
  enum bverror err = BVERR_NONE;
  struct bvbuffdesc* pdesc;
  pdesc = parms.dstdesc;

  // Confirm the current list if mappings is null.
  if (pdesc->map != NULL)
  {
    ODS("ERROR: Destination Buffer Descriptor already had a buffer map before we mapped it.\n");
  }

  // Map the buffer
  // We need to select the same library that the blit function is going to use.
  // The problem is that we have a list of possibly libraries to use, and if one
  // of the priority flags is set, the blt function will actually try multiple
  // libraries until the blt is successful.
  // We can't know yet which library will be successful, so we will just select
  // the highest priority library.
  // Under real world conditions, this is sub-optimal, but since this is a
  // test program, it is reasonable to expect the highest priority library is
  // the one we are interested in testing.
  if(priorityFlagCPU)
      err = prebvlib[0].bv_map(pdesc);
  else if (priorityFlag2D)
      err = prebvlib[NUMLIBS-1].bv_map(pdesc);
  else
    err = bvlib.bv_map(pdesc);

  // Confirm no error.
  if (err != BVERR_NONE)
  {
    ODS("ERROR: bv_map() returned an error: 0x%x.\n", err);
    ODS("bv_map() test FAILED.\n");
  }
  // Confirm a map was created.
  else if (pdesc->map == NULL)
  {
    ODS("ERROR: No map appeared after mapping destination buffer.\n");
    ODS("bv_map() test FAILED.\n");
  }
  else
  {
    // Because the mapping is opaque, all we can do is confirm that a
    // map was actually created.  Which it was.
    ODS("bv_map() test PASSED.\n");
  }

}

void TestUnmapping()
{

  enum bverror err = BVERR_NONE;
  struct bvbuffdesc* pdesc;
  pdesc = parms.dstdesc;

  // Make sure the surface is mapped.
  if (pdesc->map == NULL)
  {
    ODS("ERROR: Destination Buffer Descriptor lost it's map after we mapped it.\n");
  }

  // Call the unmap function
  // See comments in TestMapping()
  if(priorityFlagCPU)
      err = prebvlib[0].bv_unmap(pdesc);
  else if (priorityFlag2D)
      err = prebvlib[NUMLIBS-1].bv_unmap(pdesc);
  else
    err = bvlib.bv_unmap(pdesc);

  // Check for error
  if (err != BVERR_NONE)
  {
    ODS("ERROR: bv_unmap() returned an error: 0x%x.\n", err);
    ODS("bv_unmap() test FAILED.\n");
  }

  // Make sure the map is gone.
  else if (pdesc->map != NULL)
  {
    ODS("ERROR: There is still a destination map handle even after calling bv_unmap!\n");
    ODS("bv_unmap() test FAILED.\n");
  }
  else
  {
    // Because the is opaque, all we can do is confirm that the map
    // was actually removed.  Which it was.
    ODS("bv_unmap() test PASSED.\n");
  }

}

bool PerformBLT()
{
  enum bverror err = BVERR_UNK;

  int dstpalsize;
  int src1used = 0;
  int src2used = 0;
  int maskused = 0;
  int dstusedassrc = 0;

  SmartCopySurf(parms.dstgeom, parms.dstdesc, dstorigin);

  //
  // Enable sources used based on or blend
  //

  if(parms.flags & BVFLAG_ROP)
  {
    if(((parms.op.rop & 0xCCCC) >> 2) != (parms.op.rop & 0x3333))
    {
      VODS("Setting parms.src1geom.\n");
      SmartCopySurf(parms.src1geom, parms.src1.desc, src1origin);
    }

    if(((parms.op.rop & 0xF0F0) >> 4) != (parms.op.rop & 0x0F0F))
    {
      VODS("Setting parms.src2geom.\n");
      SmartCopySurf(parms.src2geom, parms.src2.desc, src2origin);
    }

    if(((parms.op.rop & 0xFF00) >> 8) != (parms.op.rop & 0x00FF))
    {
      VODS("Setting parms.maskgeom.\n");
      SmartCopySurf(parms.maskgeom, parms.mask.desc, maskorigin);
    }
  }

  if (maptest)
    TestMapping();
  else if (crossmap)
    CrossMapSurfaces();

  if(parms.flags & BVFLAG_BLEND)
  {
    VODS("Setting parms.src1geom.\n");
    parms.src1geom = GetGeomOrigin(src1origin);
    parms.src1.desc = GetDescOrigin(src1origin);

    VODS("Setting parms.src2geom.\n");
    parms.src2geom = GetGeomOrigin(src2origin);
    parms.src2.desc = GetDescOrigin(src2origin);

    if(parms.op.blend & BVBLENDDEF_REMOTE)
    {
      VODS("Settings parms.maskgeom.\n");
      parms.maskgeom = GetGeomOrigin(maskorigin);
      parms.mask.desc = GetDescOrigin(maskorigin);
    }
  }

  dstpalsize = PaletteSize(dst.format);
  if(dstpalsize)
  {
    if(parms.flags & BVFLAG_ROP)
    {
      if(parms.src1geom && parms.src1geom->palette)
      {
        VODS("Initializaing destination palette using source palette.\n");
        memcpy(parms.dstgeom->palette, parms.src1geom->palette, PaletteSize(parms.src1geom->format) * sizeof(unsigned long));
        parms.dstgeom->paletteformat = parms.src1geom->paletteformat;

      }
      else if(parms.src2geom && parms.src2geom->palette)
      {
        VODS("Initializaing destination palette using src2 palette.\n");
        memcpy(parms.dstgeom->palette, parms.src2geom->palette, PaletteSize(parms.src2geom->format) * sizeof(unsigned long));
        parms.dstgeom->paletteformat = parms.src2geom->paletteformat;
      }
      else if(parms.maskgeom && parms.maskgeom->palette)
      {
        VODS("Initializaing destination palette using mask palette.\n");
        memcpy(parms.dstgeom->palette, parms.maskgeom->palette, PaletteSize(parms.maskgeom->format) * sizeof(unsigned long));
        parms.dstgeom->paletteformat = parms.maskgeom->paletteformat;
      }
      else
      {
        ODS("WARNING: No initialization of destination palette.\n");
      }
    }
  }

  if(preseed)
  {
    // Perform mini version of specified BLT
    VODS("Performing mini version (preseed) of specified BLT.\n");
    if(!PreSeed())
      goto Error;
  }

  // Do the BLT specified on the command line
  VODS("Specified BLT:\n");
  if(verbose)
  {
    ODS("bvbltparams:\n");
    BVDump("\t", "\t", &parms);
  }

  if(pretouch)
  {
    VODS("Pre-touching all surfaces.\n");
    PreTouch();
  }

  // Set up callback if needed.
  if(parms.flags & BVFLAG_ASYNC)
  {
    our_callback_data = 0x12345678;
    parms.callbackfn = bvtest_cb_fn;
    parms.callbackdata = (unsigned long)&our_callback_data;
  }

  // If we are testing batch functionality, set up a batch of one blit
  if(batchtest)
    SetUpBatch();

  VODS("Performing specified BLT.\n");

  timespec cgtstart, cgtend;

  err = BVDetermineSurfaceUse(&src1used, &src2used, &maskused, &dstusedassrc, parms.errdesc, parms.flags, &parms.op);
  if(err != BVERR_NONE)
  {
 			ODS( "Error determining surface use 0x%x.\n", err);
      goto Error;
  }

  if(src1used)
  {
    copparams.desc = parms.src1.desc;
    copparams.geom = parms.src1geom;
    copparams.rect = &parms.src1rect;
    copparams.structsize = sizeof(struct bvcopparams);
    copparams.cacheop = BVCACHE_CPU_TO_DEVICE;
    BV_CACHE(bvlib.bv_cache, &copparams);
    if(err != BVERR_NONE)
    {
 			ODS( "Error caching src1 0x%x.\n", err);
      goto Error;
    }
  }
  if(src2used)
  {
    copparams.desc = parms.src2.desc;
    copparams.geom = parms.src2geom;
    copparams.rect = &parms.src2rect;
    copparams.structsize = sizeof(struct bvcopparams);
    copparams.cacheop = BVCACHE_CPU_TO_DEVICE;
    BV_CACHE(bvlib.bv_cache, &copparams);
    if(err != BVERR_NONE)
    {
 			ODS( "Error caching src2 0x%x.\n", err);
      goto Error;
    }
  }
  if(maskused)
  {
    copparams.desc = parms.mask.desc;
    copparams.geom = parms.maskgeom;
    copparams.rect = &parms.maskrect;
    copparams.structsize = sizeof(struct bvcopparams);
    copparams.cacheop = BVCACHE_CPU_TO_DEVICE;
    BV_CACHE(bvlib.bv_cache, &copparams);
    if(err != BVERR_NONE)
    {
 			ODS( "Error caching mask 0x%x.\n", err);
      goto Error;
    }
  }

  /* Need to clean dst rect */
  copparams.desc = parms.dstdesc;
  copparams.geom = parms.dstgeom;
  copparams.rect = &parms.dstrect;
  copparams.structsize = sizeof(struct bvcopparams);
  copparams.cacheop = BVCACHE_CPU_TO_DEVICE;
  BV_CACHE(bvlib.bv_cache, &copparams);
  if(err != BVERR_NONE)
  {
		ODS( "Error caching dst 0x%x.\n", err);
    goto Error;
  }

  if (premap) {
    if (bvlib.bv_map) {
    	if (parms.src1.desc) {
    		VODS( "Mapping src1 buffer\n");
#ifdef ENABLE_OMAPDRM
                if (!gc_alloc_dmabuf(SO_SRC1, parms.src1.desc->length))
                {
                    memcpy(dmabufSrc1.mem, parms.src1.desc->virtaddr, parms.src1.desc->length);
                    src1desc.auxtype = parms.src1.desc->auxtype = BVAT_DMABUF;
                    src1desc.dma_buf_handle = parms.src1.desc->dma_buf_handle = dmabufSrc1.handle;
                }
#endif
    		err = bvlib.bv_map(parms.src1.desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping src1 0x%x\n", err);
    			goto Error;
    		}
                src1desc.map = parms.src1.desc->map;
    	}

    	if (parms.src2.desc) {
		VODS( "Mapping src2 buffer\n");
#ifdef ENABLE_OMAPDRM
                if (!gc_alloc_dmabuf(SO_SRC2, parms.src2.desc->length))
                {
                    memcpy(dmabufSrc2.mem, parms.src2.desc->virtaddr, parms.src2.desc->length);
                    src2desc.auxtype = parms.src2.desc->auxtype = BVAT_DMABUF;
                    src2desc.dma_buf_handle = parms.src2.desc->dma_buf_handle = dmabufSrc2.handle;
                }
#endif
    		err = bvlib.bv_map(parms.src2.desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping src2 0x%x\n", err);
    			goto Error;
    		}
                src2desc.map = parms.src2.desc->map;
		}

		if (parms.dstdesc) {
			VODS( "Mapping dst buffer from bvlib\n");
#ifdef ENABLE_OMAPDRM
                if ((parms.src2.desc && parms.src2.desc==parms.dstdesc) || !gc_alloc_dmabuf(SO_DST, parms.dstdesc->length))
                {
                    dstdesc.auxtype = parms.dstdesc->auxtype = BVAT_DMABUF;
                    dstdesc.dma_buf_handle = parms.dstdesc->dma_buf_handle = dmabufDst.handle;
                }
#endif
    		err = bvlib.bv_map(parms.dstdesc);
    		if (err != BVERR_NONE) {
				ODS( "error mapping dst 0x%x\n", err);
				goto Error;
			}
			dstdesc.map = parms.dstdesc->map;
		}

    	if (parms.mask.desc) {
			VODS( "Mapping mask buffer\n");
#ifdef ENABLE_OMAPDRM
                if (!gc_alloc_dmabuf(SO_MASK, parms.mask.desc->length))
                {
                    memcpy(dmabufMask.mem, parms.mask.desc->virtaddr, parms.mask.desc->length);
                    maskdesc.auxtype = parms.mask.desc->auxtype = BVAT_DMABUF;
                    maskdesc.dma_buf_handle = parms.mask.desc->dma_buf_handle = dmabufMask.handle;
                }
#endif
    		err = bvlib.bv_map(parms.mask.desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping mask 0x%x\n", err);
    			goto Error;
    		}
                maskdesc.map = parms.mask.desc->map;
    	}
    }
  }


{
  int times;
  clock_gettime(CLOCK_MONOTONIC, &cgtstart);

  for (times=0; times<loop_times; times++)
  err = blt(&parms);

  clock_gettime(CLOCK_MONOTONIC, &cgtend);

  {
    unsigned long long end = (cgtend.tv_sec * 1000000000ull) + cgtend.tv_nsec;
    unsigned long long start = (cgtstart.tv_sec * 1000000000ull) + cgtstart.tv_nsec;
    unsigned long long delta = (end - start); // assumes BLT will never take more than 4 seconds
    unsigned long long pps = delta ? (((unsigned long long)parms.dstrect.width * (unsigned long long)parms.dstrect.height * 1000000000ull) * loop_times / delta) : 0;
    if(err != BVERR_NONE)
    {
      const char* szErr;
      ErrorToText(szErr, err);
      if(!parms.errdesc) parms.errdesc = (char*)"no details";
      ODS("Error performing specified BLT: (%d) %s (%s).\n", err, szErr, parms.errdesc);
      delta = pps = 0;
      goto Error;
    }
    else
    {
      copparams.desc = parms.dstdesc;
      copparams.geom = parms.dstgeom;
      copparams.rect = &parms.dstrect;
      copparams.structsize = sizeof(struct bvcopparams);
      copparams.cacheop = BVCACHE_CPU_FROM_DEVICE;
      BV_CACHE(bvlib.bv_cache, &copparams);
      if(err != BVERR_NONE)
      {
        goto Error;
      }
      // The profiling number means the same as an OK BLT.
      if(!profile)
        VODS("bv_blt() returned BVERR_NONE.\n");

      if(profile)
      {
        ODS("Elapsed time = %lld nS (avg: %lld)\n", delta, delta/loop_times);
        if(pps)
          ODS("Perf: %lld p/s.\n", pps);
        else
          ODS("Perf: Too quick to measure (below the precision of the timer).\n");
      }
    }

    if(szProfout)
    {
      FILE* hRawDst = fopen(szProfout, "a");
      if(pps)
        fprintf(hRawDst, "%lld", pps);
      fclose(hRawDst);
    }
  }

}

#ifndef ENABLE_OMAPDRM
  if (premap) {
    if (bvlib.bv_unmap) {
    	if (parms.dstdesc) {
    		VODS( "UnMapping dst buffer from bvlib\n");
    		err = bvlib.bv_unmap(parms.dstdesc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping dst 0x%x\n", err);
    			goto Error;
    		}
    	}

    	if (parms.src1.desc) {
    		VODS( "UnMapping src1 buffer\n");
    		err = bvlib.bv_unmap(parms.src1.desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping src1 0x%x\n", err);
    			goto Error;
    		}
    	}

    	if (parms.src2.desc) {
      	        VODS( "UnMapping src2 buffer\n");
    		err = bvlib.bv_unmap(parms.src2.desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping src2 0x%x\n", err);
    			goto Error;
    		}
    	}

    	if (parms.mask.desc) {
      	        VODS( "UnMapping mask buffer\n");
    		err = bvlib.bv_unmap(parms.mask.desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping mask 0x%x\n", err);
    			goto Error;
    		}
    	}
    }
  }
#endif


Error:
  if (maptest)
    TestUnmapping();
  else if (crossmap)
    CrossUnMapSurfaces();

  // Test the callback
  if(parms.flags & BVFLAG_ASYNC)
  {
      // We initialized our_callback_data to 0x12345678.  A successful
      // bv_blt should have called our callback function.
      // our callback function will have changed our_callback_data to
      // 0xAAAAAAAA.
      // A non-successful bv_blt() call should leave the callback data
      // untouched.
      if (err == BVERR_NONE)
      {
        // Make sure the callback was called
        if (our_callback_data != 0xAAAAAAAA)
        {
          ODS("ERROR: bv_blt() did not properly call our callback function!\n");
          ODS("Callback function test FAILED.\n");
        }
        else
        {
          ODS("Callback function test PASSED.\n");
        }
      }
      else
      {
        // Make sure the callback was NOT called
        if (our_callback_data != 0x12345678)
        {
          ODS("ERROR: bv_blt() modified our callback data even though the blt returned an error!\n");
          ODS("Callback function test FAILED.\n");
        }
        else
        {
          ODS("Callback function test PASSED.\n");
        }
      }
  }

  // If we are testing batching, see if the batch launched ok.
  if (err == BVERR_NONE && batchtest)
    TestBatch();

  return(err == BVERR_NONE);
}


bool SaveFiles()
{
  bool success = false;
#ifdef ENABLE_OMAPDRM
  enum bverror err = BVERR_UNK;
#endif

  /*
   * If specified, prepare to write a destination file.
   */
  if(szDst)
  {
    struct bvbltparams tmpparms;
    int containersize;
    int true_width, true_height;

    if (parms.dstgeom->orientation == 0 || parms.dstgeom->orientation == 180)
    {
      true_width = parms.dstgeom->width;
      true_height = parms.dstgeom->height;
    }
    else if (parms.dstgeom->orientation == 90 || parms.dstgeom->orientation == 270)
    {
      true_width = parms.dstgeom->height;
      true_height = parms.dstgeom->width;
    }
    else
    {
        VODS("Cannot save destination file. Orientation %d is invalid.\n", parms.dstgeom->orientation);
        goto Error;
    }

    VODS("Destination file specified...saving.\n");
    if(parms.dstgeom->format == (ocdformat)-1)
    {
      ODS("Destination format not specified.  Unable to save results.\n");
      goto Error;
    }

    if(IsFileRaw(szDst))
    {
      char szRawDst[256];
      FormatToContainerSize(containersize, parms.dstgeom->format);
      int len = strlen(szDst);
      strncpy(szRawDst, szDst, len - 4);
      sprintf(&(szRawDst[len-4]), "-%s-%dx%d(%ld).raw", FormatToText(parms.dstgeom->format), true_width, true_height, parms.dstgeom->virtstride);
      FILE* hRawDst = fopen(szRawDst, "wb");
      if(!hRawDst)
      {
        ODS("Error opening destination file.\n");
        goto Error;
      }
      unsigned long filesize;
      switch(parms.dstgeom->format)
      {
        // Planar needs to output all planes

        case OCDFMT_YV12:
        case OCDFMT_NV12:
        case OCDFMT_NV21:
        case OCDFMT_IYUV: /* And OCDFMT_I420 */
        case OCDFMT_IMC2:
        case OCDFMT_IMC4:
          filesize = true_height * parms.dstgeom->virtstride;
          filesize += filesize / 2;
          break;

        case OCDFMT_NV16:
        case OCDFMT_NV61:
        case OCDFMT_IMC1:
        case OCDFMT_IMC3:
          filesize = true_height * parms.dstgeom->virtstride * 2;
          break;

        // LUTs need to output palette
        case OCDFMT_LUT1:
        case OCDFMT_LUT2:
        case OCDFMT_LUT4:
        case OCDFMT_LUT8:
          if(fwrite(parms.dstgeom->palette, sizeof(unsigned long), 1 << containersize, hRawDst) != (1 << containersize))
          {
            ODS("ERROR writing palette to destination file.\n");
            goto Error;
          }
          // fallthrough

        // For everyone else, this will work
        default:
          filesize = true_height * parms.dstgeom->virtstride;
      }
      if(fwrite(parms.dstdesc->virtaddr, filesize, 1, hRawDst) != 1)
      {
        ODS("Error writing destination file.\n");
        fclose(hRawDst);
        goto Error;
      }
      fclose(hRawDst);
      VODS("Destination file %s is %d x %d x %d.\n", szRawDst, true_width, true_height, containersize);
    }
    else
    {
      int comp;

      dstfile = *(parms.dstgeom);

      switch(parms.dstgeom->format & OCDFMTDEF_CS_MASK)
      {
        case OCDFMTDEF_CS_LUT:
          VODS("dst format is LUT. comp = 4. dstfile.format = nRGBA24\n");
          comp = 4;
          dstfile.format = OCDFMT_RGBA24_NON_PREMULT;
          break;

        case OCDFMTDEF_CS_RGB:
          VODS("dst format is RGB. ");
          if(parms.dstgeom->format & OCDFMTDEF_ALPHA)
          {
            VODS("dst format has alpha. comp = 4. ");
            comp = 4;
            if(parms.dstgeom->format & OCDFMTDEF_NON_PREMULT)
              VODS("dst format is non-premultiplied. dstfile.format = nRGBA24.\n");
            else
              VODS("dst format is premultiplied. dstfile.format = RGBA24.\n");
            dstfile.format = (parms.dstgeom->format & OCDFMTDEF_NON_PREMULT) ?
                             OCDFMT_RGBA24_NON_PREMULT :
                             OCDFMT_RGBA24;
          }
          else
          {
            VODS("dst format does not have alpha. comp = 3. dstfile.format = RGB24.\n");
            comp = 3;
            dstfile.format = OCDFMT_RGB24;
          }
          break;

        case OCDFMTDEF_CS_MONO:
        case OCDFMTDEF_CS_YCbCr:
          VODS("dst format is YUV. comp = 3; dstfile.foramt = RGB24.\n");
          comp = 3;
          dstfile.format = OCDFMT_RGB24;
          break;
      }

      FormatToContainerSize(containersize, dstfile.format);
      dstfile.virtstride = ((true_width * containersize) + 7) / 8;
      unsigned long size = SurfaceSize(dstfile.virtstride, dstfile.format, true_width, true_height);
      dstfiledesc.length = size;
      dstfiledesc.virtaddr = memalign(64,size);
      if(!dstfiledesc.virtaddr)
      {
        ODS("Error allocating memory for destination file surface.\n");
        dstfiledesc.length = 0;
        goto Error;
      }

      VODS("Copying destination surface to destination file surface.\n");

      memset(&tmpparms, 0, sizeof(tmpparms));
      tmpparms.structsize = sizeof(tmpparms);
      tmpparms.flags = BVFLAG_ROP;
      tmpparms.op.rop = 0xCCCC;
      tmpparms.dstgeom = &dstfile;
      tmpparms.src1geom = parms.dstgeom;
      tmpparms.src1.desc = parms.dstdesc;

      tmpparms.dstgeom = &dstfile;
      tmpparms.dstdesc = &dstfiledesc;

      tmpparms.dstrect.left = 0;
      tmpparms.dstrect.top = 0;
      tmpparms.dstrect.width = dstfile.width;
      tmpparms.dstrect.height = dstfile.height;
      tmpparms.src1rect.left = 0;
      tmpparms.src1rect.top = 0;
      tmpparms.src1rect.width = parms.dstgeom->width;
      tmpparms.src1rect.height = parms.dstgeom->height;

      {
//TODO: Try all libraries
        bverror err = pblt(&tmpparms);
        if(err != BVERR_NONE)
        {
          const char* szErr;
          ErrorToText(szErr, err);
          ODS("Error copying destination surface image to destination file surface: (0x%08X) %s\n", err, szErr);
          goto Error;
        }
      }

      VODS("Destination file %s is %d x %d x %d\n", szDst, true_width, true_height, comp);
      int success;
      if(IsFileTGA(szDst))
        success = stbi_write_tga(szDst, true_width, true_height, comp, dstfiledesc.virtaddr);
      else
      {
        if (comp == 4)
        {
          ODS("bvtest: Four component files must be saved to TGA.\n");
        }
        else
            success = stbi_write_bmp(szDst, true_width, true_height, comp, dstfiledesc.virtaddr);
      }

      if(!success)
      {
        ODS("Error writing destination file.");
        goto Error;
      }
    }
  }

  success = true;
Error:
  return(success);
}

#define SAFEFREE(ptr) if(ptr) { free(ptr); ptr = 0; }
#define SAFESTBIFREE(ptr) if(ptr) { stbi_image_free(ptr); ptr = 0; }


// ==========================================================================
// DestroyPalettes()
//
// We could carefully examine the palette dependencies, and only test
// for equalities that can actually exist.  But instead we will take
// the "spray and pray" approach because:
//  1) this is a test program and cleaning up is not speed-critical
//  2) it's faster to code
//  3) it's safe
//  4) it allows future changes to bvtest surface copying/sharing without
//     having to come back here and re-think the palette dependencies
//     (which is how we got here in the first place).
//
void DestroyPalettes()
{
  // For each possible palette, test it against all other
  // palettes and if they are the same, zero the others out
  // before freeing this one.
  if(dst.palette)
  {
    if (src1.palette     == dst.palette) src1.palette = 0;
    if (src1file.palette == dst.palette) src1file.palette = 0;
    if (src2.palette     == dst.palette) src2.palette = 0;
    if (src2file.palette == dst.palette) src2file.palette = 0;
    if (mask.palette     == dst.palette) mask.palette = 0;
    if (maskfile.palette == dst.palette) maskfile.palette = 0;
    if (dstfile.palette  == dst.palette) dstfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing dst.palette.\n");
    free(dst.palette);
    dst.palette = 0;
  }

  if(dstfile.palette)
  {
    if(src1.palette     == dstfile.palette) src1.palette = 0;
    if(src1file.palette == dstfile.palette) src1file.palette = 0;
    if(src2.palette     == dstfile.palette) src2.palette = 0;
    if(src2file.palette == dstfile.palette) src2file.palette = 0;
    if(mask.palette     == dstfile.palette) mask.palette = 0;
    if(maskfile.palette == dstfile.palette) maskfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing dstfile.palette.\n");
    free(dstfile.palette);
    dstfile.palette = 0;
  }

  if(src1.palette)
  {
    if(src1file.palette == src1.palette) src1file.palette = 0;
    if(src2.palette     == src1.palette) src2.palette = 0;
    if(src2file.palette == src1.palette) src2file.palette = 0;
    if(mask.palette     == src1.palette) mask.palette = 0;
    if(maskfile.palette == src1.palette) maskfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing src1.palette.\n");
    free(src1.palette);
    src1.palette = 0;
  }

  if(src1file.palette)
  {
    if(src2.palette     == src1file.palette) src2.palette = 0;
    if(src2file.palette == src1file.palette) src2file.palette = 0;
    if(mask.palette     == src1file.palette) mask.palette = 0;
    if(maskfile.palette == src1file.palette) maskfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing src1file.palette.\n");
    free(src1file.palette);
    src1file.palette = 0;
  }

  if(src2.palette)
  {
    if(src2file.palette == src2.palette) src2file.palette = 0;
    if(mask.palette     == src2.palette) mask.palette = 0;
    if(maskfile.palette == src2.palette) maskfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing src2.palette.\n");
    free(src2.palette);
    src2.palette = 0;
  }

  if(src2.palette)
  {
    if(src2file.palette == src2.palette) src2file.palette = 0;
    if(mask.palette     == src2.palette) mask.palette = 0;
    if(maskfile.palette == src2.palette) maskfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing src2.palette.\n");
    free(src2.palette);
    src2.palette = 0;
  }

  if(src2file.palette)
  {
    if(mask.palette     == src2file.palette) mask.palette = 0;
    if(maskfile.palette == src2file.palette) maskfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing src2file.palette.\n");
    free(src2file.palette);
    src2file.palette = 0;
  }

  if(mask.palette)
  {
    if(maskfile.palette == mask.palette) maskfile.palette = 0;

    //ODS("DestroySurfaces(): Freeing mask.palette.\n");
    free(mask.palette);
    mask.palette = 0;
  }

  if(maskfile.palette)
  {
    //ODS("DestroySurfaces(): Freeing maskfile.palette.\n");
    free(maskfile.palette);
    maskfile.palette = 0;
  }
}


void DestroySurfaces()
{
  DestroyPalettes();

#ifdef ENABLE_OMAPDRM
  if (premap) {
    if (bvlib.bv_unmap) {
        enum bverror err = BVERR_UNK;
    	if (dmabufDst.handle) {
    		VODS( "UnMapping dst buffer from bvlib\n");
    		err = bvlib.bv_unmap(&dstdesc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping dst 0x%x\n", err);
    		}
                gc_dealloc_dmabuf(SO_DST);
    	}

    	if (dmabufSrc1.handle) {
    		VODS( "UnMapping src1 buffer\n");
    		err = bvlib.bv_unmap(&src1desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping src1 0x%x\n", err);
    		}
                gc_dealloc_dmabuf(SO_SRC1);
    	}

    	if (dmabufSrc2.handle) {
      	        VODS( "UnMapping src2 buffer\n");
    		err = bvlib.bv_unmap(&src2desc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping src2 0x%x\n", err);
    		}
                gc_dealloc_dmabuf(SO_SRC2);
    	}

    	if (dmabufMask.handle) {
      	        VODS( "UnMapping mask buffer\n");
    		err = bvlib.bv_unmap(&maskdesc);
    		if (err != BVERR_NONE) {
    			ODS( "error mapping mask 0x%x\n", err);
    		}
                gc_dealloc_dmabuf(SO_MASK);
    	}
    }
  }
#endif

  VODS("DestroySurfaces(): Freeing dst.virtaddr.\n");
  SAFEFREE(dstdesc.virtaddr)

  VODS("DestroySurfaces(): Freeing dstfile.virtaddr.\n");
  SAFESTBIFREE(dstfiledesc.virtaddr)

  if(src1desc.virtaddr == src1filedesc.virtaddr)
  {
    src1desc.virtaddr = 0;
    src1desc.length = 0;
  }
  VODS("DestroySurfaces(): Freeing src1.virtaddr.\n");
  SAFEFREE(src1desc.virtaddr)

  VODS("DestroySurfaces(): Freeing src1file.virtaddr.\n");
  SAFESTBIFREE(src1filedesc.virtaddr)

  if(src2desc.virtaddr == src1filedesc.virtaddr)
  {
    src2desc.virtaddr = 0;
    src2desc.length = 0;
  }
  VODS("DestroySurfaces(): Freeing src2.virtaddr.\n");
  SAFEFREE(src2desc.virtaddr)

  VODS("DestroySurfaces(): Freeing src2file.virtaddr.\n");
  SAFESTBIFREE(src1filedesc.virtaddr)

  if(maskdesc.virtaddr == maskfiledesc.virtaddr)
  {
    maskdesc.virtaddr = 0;
    maskdesc.length = 0;
  }
  VODS("DestroySurfaces(): Freeing mask.virtaddr.\n");
  SAFEFREE(maskdesc.virtaddr)

  VODS("DestroySurfaces(): Freeing maskfile.virtaddr.\n");
  SAFESTBIFREE(maskfiledesc.virtaddr)
}

int main(int         argc,
         const char* argv[])
{
  int retval;

  if(argc < 2)
  {
    Usage(argv[0]);
    retval = EXIT_SUCCESS;
  }
  else
  {
    retval = EXIT_FAILURE;
    if(Init())
    {
      if(ParseCmdLine(argc, argv))
      {
        if(verbose)
          PrintVersion();

        if(CreateSurfaces())
        {
          if(LoadFiles())
          {
            if(PerformBLT())
            {
              retval = SaveFiles() ? EXIT_SUCCESS : EXIT_FAILURE;
            }
          }
        }
        DestroySurfaces();
      }
      Cleanup();
    }
  }

//ODS("bvtest returns %d\n", retval);
Error:
  return(retval);
}

