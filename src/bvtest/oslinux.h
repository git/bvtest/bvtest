#ifndef BVOSLINUX_H
#define BVOSLINUX_H

#include <dlfcn.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define stricmp strcasecmp
#define strnicmp strncasecmp

#define BVEXPORT __attribute__((visibility("default")))

typedef void* BVLIBHANDLE;

#endif // BVOSLINUX_H
