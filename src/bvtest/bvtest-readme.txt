bvtest

------

bvtest provides a command line interface to the Bltsville library.

bvtest performs a single BLTsville operation each time it is invoked.

bvtest supports reading the most common formats of some standard file types
like JPG, PNG, BMP, TGA, etc. It can also read in a RAW file, consisting of
just the image data (not to be confused with digital camera RAW files). For
RAW files, the image format and dimensions must be encoded into the filename
using a specific convention:

anynameyoulike-FORMAT-WIDTHxHEIGHT(STRIDE).raw

 FORMAT is one of the enumerated formats in OCD.h without the
 OCDFMT_ prefix. (For example, OCDFMT_LUT8 would be
 LUT8.) Non-premultiplied formats are designated with a
 preceding 'n' instead of the NONPREMULT suffix. (For example,
 OCDFMT_BGRA24_NONPREMULT would be nBGRA24.)
 WIDTH, HEIGHT, and STRIDE are positive integers, in decimal format,
 with no sign.

For palettized formats, the RAW file contains the palette in nBGRA24 format
followed by the image data (i.e. a 100x100 LUT8 image would be 11024 bytes).

bvtest supports writing uncompressed BMP and TGA.

bvtest's file format support is based on some public domain source (see
stb_image.c), which has its own limitations.  In particular, it provides and
requires data to be presented in the RGB24 format.  bvtest converts to
and from this format for loading and saving purposes, using bv_blt.  So if
the current implementation of bv_blt does not support the needed
conversions, saving and/or loading of the standard types will not be possible.
For example, if one of the inputs to the desired BLT is IYUV, but the current
bv_blt implementation does not support a RGB24 to IYUV conversion, loading
the image from a standard format will fail. For those cases, the RAW format
can be used. GIMP has successfully been used to generate images for these
cases.

bvtest exposes most, but not all BLTsville functions. See the source code for
up-to-date features.

Running bvtest with no arguments displays the usage.

Examples:

Convert a UYVY image to BGR124, using a JPG file as the source and saving the
results as a BMP file:

bvtest -verbose -srcfile kings_1_bg_051802.jpg -src1 UYVY 800x600 -srcrect 0,0-800x600 -dst BGR124 800x600 -dstrect 0,0-800x600 -dstfile BGR124.bmp -rop CCCC

Convert a UYVY image to RGB16, using a JPG file as the source and saving the
results as a BMP file:

bvtest -verbose -srcfile kings_1_bg_051802.jpg -src1 UYVY 800x600 -srcrect 0,0-800x600 -dst RGB16 800x600 -dstrect 0,0-800x600 -dstfile RGB16.bmp -rop CCCC


