#ifndef BVOS_H
#define BVOS_H

#undef OSDEFINED

#ifdef UNDER_CE
#include "oswindows.h"
#define OSDEFINED "Windows"
#endif /* UNDER_CE */

#ifdef ANDROID
#include "oslinux.h"
#define OSDEFINED "Android"
#endif /* ANDROID */

#ifndef OSDEFINED
#ifdef __linux__
#include "oslinux.h"
#define OSDEFINED "Linux"
#endif /* __linux__ */
#endif /* !OSDEFINED */

#ifndef OSDEFINED
#error Operating System is not defined in os.h
#endif /* !OSDEFINED */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */
extern int BVLIB_Init(void);
extern void BVLIB_DeInit(void);

void ODS(const char* szMsg, ...);

void OSDebugPrint(const char*);
BVLIBHANDLE OSLoadLibrary(const char* szLibPrefix);
void* OSGetProcAddress(BVLIBHANDLE, const char* szFn);
void OSUnloadLibrary(BVLIBHANDLE);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#ifndef BVEXPORT
#define BVEXPORT
#endif /* !BVEXPORT */

#endif /* BVOS_H */

