/*
 * ======================================================================
 *             Texas Instruments OMAP(TM) Platform Software
 *
 * This Software is provided "as-is," without warranty of any kind,
 * express or implied, including but not limited to the warranties of
 * merchantability, fitness for a particular purpose and noninfringement.
 * In no event shall Texas Instruments, Inc. be liable for any claim,
 * damages or other liability, whether in an action of contract, tort or
 * otherwise, arising from, out of or in connection with the software or
 * the use or other dealings in the Software.
 *
 * Except as contained in this notice, the name of Texas Instruments,
 * Inc. shall not be used in advertising or otherwise to promote the
 * sale, use or other dealings in this Software without prior written
 * authorization from Texas Instruments, Inc.
 *
 * Use at your own risk.
 *=======================================================================
 */

#ifndef BVTEST_H
#define BVTEST_H

#include "os.h"
#include <bltsville.h>

#define STBI_HEADER_FILE_ONLY
#define STBI_FAILURE_USERMSG
#include "stb_image.c"

#include <stdlib.h>
#include <string.h>
#define stricmp strcasecmp

#include <time.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#endif // BVTEST_H
